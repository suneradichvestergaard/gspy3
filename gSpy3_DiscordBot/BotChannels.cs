﻿using Discord.WebSocket;
using gSpy3_Common.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_DiscordBot
{
    public static class BotChannels
    {
        /// <summary>
        /// Returns 0 on error
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public static ulong RequireChannel(DatabaseManager dbManager, SocketMessage arg)
        {
            var chnl = arg.Channel as SocketGuildChannel;
            if (chnl == null) return 0;
            if (chnl.Guild == null) return 0;
            ulong idbot = arg.Author.Id;
            ulong idguild = chnl.Guild.Id;

            var res = dbManager.BackendDB.Query("replace into bots (bot_id,server_id,server_name) values(" + idbot + "," + idguild + ",@name)", new Dictionary<string, string>(){
                {"@name", chnl.Guild.Name }
            });

            res = dbManager.BackendDB.Query("replace into botchannels (channel_id,server_id,channel_name) values (" + chnl.Id + "," + idguild + ",@name)", new Dictionary<string, string>(){
                {"@name", chnl.Name }
            });

            return chnl.Id;

        }

        public static T GetBotSetting<T>(DatabaseManager dbManager, ulong bot, ulong channel, string key, T defaultValue)
        {
            var res = dbManager.BackendDB.Query("select setting_value from botsettings where bot_id=" + bot + " and channel_id=" + channel + " and setting_key=@key",
                new Dictionary<string, string>()
                {
                    {"@key",key }
                });
            if (!res.Success || res.Rows.Count == 0) return defaultValue;
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(res.Rows[0][0]);
        }
        public static void SetBotSetting<T>(DatabaseManager dbManager, ulong bot, ulong channel, string key, T value)
        {
            var res = dbManager.BackendDB.Query("replace into botsettings (setting_key,setting_value,bot_id,channel_id) values (@key,@value," + bot + "," + channel + ")", new Dictionary<string, string>()
            {
                {"@value",Newtonsoft.Json.JsonConvert.SerializeObject(value) },
                {"@key",key }
            });

        }
    }
}
