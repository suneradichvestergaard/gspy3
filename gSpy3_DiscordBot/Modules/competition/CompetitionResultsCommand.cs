﻿using Discord;
using Discord.WebSocket;
using gSpy3_Common.Database;
using gSpy3_Common.Database.Format;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_DiscordBot.Modules.competition
{
    public class CompetitionResultsCommand
    {
        public static void Parse(DatabaseManager dbManager, SocketMessage arg, string msg, ref EmbedBuilder reply, string compName, string compStringList)
        {
            msg = Command.NextWord(msg);
            int count = 10;
            if (msg != "") { count = int.Parse(msg); }

            // First get some competition info ,for format etc
            Dictionary<string, string> replacer = new Dictionary<string, string>();
            replacer["in:idcompetition"] = compStringList;

            var req = new FilteredQueryRequest();
            req.idmap = new Dictionary<string, string>()
            {
                {"idcompetition","0" }
            };
            req.limit = 1;
            req.id = "comp";
            var sqlC = FilteredQueryManager.Get().CreateSQL(req, replacer);
            var qresC = dbManager.StatsDB.Query(sqlC.Item1, sqlC.Item2, null, 10);
            var sformat = qresC.Rows[0][qresC.Cols["format"]];
            var ssorting = qresC.Rows[0][qresC.Cols["sorting"]];
            var idorigin = qresC.Rows[0][qresC.Cols["idorigin"]];
            bool isRanked = string.IsNullOrEmpty(ssorting) || ssorting.Contains("rank");


            // And then the main data
            req.id = "compResults";
            req.order = null;
            req.ordercol = null;
            req.limit = count;
            var sql = FilteredQueryManager.Get().CreateSQL(req, replacer);
            var qres = dbManager.StatsDB.Query(sql.Item1, sql.Item2, null, 10);
            if (qres.Rows.Count > 0)
            {
                reply.Description += OutputCreator.GetResultsLink(compName, compName, (OriginID)int.Parse(idorigin), false);//, race != null ? req : null);
                

                foreach (var row in qres.Rows)
                {
                    var round = row[qres.Cols["round"]];
                    reply.Description += Environment.NewLine +
                         (row[qres.Cols["finished"]]) + " " + (round != "" ? ", round " + round + ", " : "") +
                        OutputCreator.GetMatchLink(compName, int.Parse(row[qres.Cols["idmatch"]]), row[qres.Cols["team_name_home"]], row[qres.Cols["team_name_away"]]) + " "+
                   
                    "|| "+row[qres.Cols["score_home"]] + " - " + row[qres.Cols["score_away"]] + " ||" 
                    
                    ;

                }
                
            }
            else reply.Description += "No results";
        }

    }
}
