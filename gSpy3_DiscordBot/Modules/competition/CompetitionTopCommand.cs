﻿using Discord;
using Discord.WebSocket;
using gSpy3_Common;
using gSpy3_Common.Database;
using gSpy3_Common.Database.Format;
using gSpy3_Common.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace gSpy3_DiscordBot.Modules.competition
{
    public class CompetitionTopCommand
    {
        public static void Parse(DatabaseManager dbManager, SocketMessage arg, string msg, ref EmbedBuilder reply, string compName,string compStringList)
        {
            try
            {
                msg = Command.NextWord(msg);
                string race = null;
                int num = 10;
                if(msg.Length > 0 && char.IsNumber(msg[0]))
                {
                    int nextSpace = msg.IndexOf(" ");
                    if (nextSpace < 0) nextSpace = msg.Length;
                    num = int.Parse(msg.Substring(0, nextSpace));
                    msg = msg.Substring(nextSpace).Trim();
                                        
                }

                if (msg != "")
                {
                    race = msg;
                }
                



                var req = new FilteredQueryRequest()
                {
                    id = "compStandings"
                };
                if (race != null)
                {
                    int? idrace = RaceParser.Parse(race);
                    if (idrace == null)
                    {
                        reply.Description = "Unknown race '" + race + "'." + Environment.NewLine +
                            "Try starting with first letters in race" + Environment.NewLine +
                            "like 'dark', 'liz','ama' etc, or common" + Environment.NewLine +
                            "shorthands like 'dorf','chorf'" + Environment.NewLine;
                        return;
                    }
                    req.filters = new Dictionary<string, string>()
                {
                    
                };

                }
                else
                {
                    req.filters = new Dictionary<string, string>()
                    {
                    };

                }

                req.idmap = new Dictionary<string, string>();
                req.idmap["idcompetition"] = "0";
                var replacer = new Dictionary<string, string>();
                replacer["in:idcompetition"] = compStringList;

                // First get some competition info ,for format etc
                req.limit = 1;
                req.id = "comp";
                var sqlC = FilteredQueryManager.Get().CreateSQL(req, replacer);
                var qresC = dbManager.StatsDB.Query(sqlC.Item1, sqlC.Item2, null, 10);
                var sformat = qresC.Rows[0][qresC.Cols["format"]];
                var ssorting = qresC.Rows[0][qresC.Cols["sorting"]];
                bool isRanked = string.IsNullOrEmpty(ssorting) || ssorting.Contains("rank");


                // And then the standings
                req.id = "compStandings";
                req.order = null;
                req.ordercol = null;
                req.limit = num;
                if (race != null)
                {
                    int? idrace = RaceParser.Parse(race);
                    if (idrace != null)
                        req.filters["idrace"] =""+(int)idrace;
                }
                var sql = FilteredQueryManager.Get().CreateSQL(req, replacer);
                var qres = dbManager.StatsDB.Query(sql.Item1, sql.Item2, null, 10);
                if (qres.Rows.Count > 0)
                {
                    reply.Description += OutputCreator.GetStandingsLink(compName, compName + (race == null ? "" : " : " + race), null, race != null ? req : null, false);//, race != null ? req : null);

                    reply.Description += Environment.NewLine+" ```" +
                        "#".PadLeft(2) + " " + 
                        OutputCreator.PadRightFit("Coach", 8) + " " + 
                        OutputCreator.PadRightFit("Team", 16) + " " +
                    OutputCreator.PadLeftFit("W", 2) + " " +
                       OutputCreator.PadLeftFit("D", 2) + " " +
                       OutputCreator.PadLeftFit("L", 2) + " " +
                        (!isRanked?OutputCreator.PadRightFit("Pts", 2):OutputCreator.PadRightFit("Rnk", 6)) + 
                       OutputCreator.PadLeftFit("TDD", 3) + " " +
                       OutputCreator.PadLeftFit("CaD", 3) + " " + Environment.NewLine +

                        "--".PadLeft(2) + " " + 
                        OutputCreator.PadRightFit("--------", 8) + " " + 
                        OutputCreator.PadRightFit("-----------------", 16) + " " +
                   OutputCreator.PadRightFit("--", 2) + " " +
                        OutputCreator.PadRightFit("--", 2) + " " +
                        OutputCreator.PadRightFit("--", 2) + " " +
                        (!isRanked ? OutputCreator.PadRightFit("--", 2):OutputCreator.PadRightFit("------", 6)) + 
                         OutputCreator.PadLeftFit("------------", 3) + " " +
                        OutputCreator.PadLeftFit("------------", 3) + " " + Environment.NewLine 
                          ;
                    foreach (var row in qres.Rows)
                    {
                        reply.Description += row[qres.Cols["position"]].PadLeft(2) + " " + 
                            OutputCreator.PadRightFit(row[qres.Cols["coach_name"]],8) + " " + 
                            OutputCreator.PadRightFit(row[qres.Cols["team_name"]], 16) + " " +
                            OutputCreator.PadRightFit(row[qres.Cols["wins"]], 2) + " " +
                            OutputCreator.PadLeftFit(row[qres.Cols["draws"]], 2) + " " +
                            OutputCreator.PadLeftFit(row[qres.Cols["losses"]], 2) + " " +
                            (!isRanked ? OutputCreator.PadLeftFit(row[qres.Cols["points"]], 2):OutputCreator.PadLeftFit(double.Parse(row[qres.Cols["ranking"]],CultureInfo.InvariantCulture).ToString("0.000").Replace(",","."), 6)) + 
                            OutputCreator.PadLeftFit(row[qres.Cols["td_diff"]], 3) + " " +
                            OutputCreator.PadLeftFit(row[qres.Cols["cas_diff"]], 3) + " " + Environment.NewLine 
                            ;

                    }

                    reply.Description += "```";
                }
                else if (qres.Error != null)
                {
                    reply.Description += qres.Error;
                }
                else
                {
                    reply.Description += "No data found";
                }

            }catch(Exception ex)
            {
                reply.Description += "Something went terribly wrong...";
            }
        }
    }
}