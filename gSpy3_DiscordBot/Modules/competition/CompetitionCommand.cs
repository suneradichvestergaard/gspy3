﻿using Discord;
using Discord.WebSocket;
using gSpy3_Common;
using gSpy3_Common.Database;
using gSpy3_Common.Database.Format;
using gSpy3_DiscordBot.Modules.status;
using Org.BouncyCastle.Asn1;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_DiscordBot.Modules.competition
{
    public class CompetitionCommand
    {
        public static void Parse(DatabaseManager dbManager, SocketMessage arg, string msg, ref EmbedBuilder reply)
        {
            msg = Command.NextWord(msg);
            if (msg.StartsWith("?")==false && (msg =="top" || msg.Contains("top ") || msg.Contains(" top") || msg.Contains("coach ") || msg.Contains(" coach") || msg.Contains("team ") || msg.Contains(" team") || msg.Contains("sched ") || msg.Contains(" sched") || msg.Contains("result ") || msg.Contains(" result") || msg.Contains("overtake ")))
            {
                string compName = null;
                if (msg.StartsWith("top")==false && msg.StartsWith("coach") == false && msg.StartsWith("overtake ") == false && msg.StartsWith("team ") == false && msg.StartsWith("sched") == false && msg.StartsWith("result") == false)
                {
                    int indexofTop = msg.IndexOf(" top");
                    int indexofCoach = msg.IndexOf(" coach");
                    int indexofSched = msg.IndexOf(" sched");
                    int indexOfResult = msg.IndexOf(" result");
                    int index = Math.Max(indexofCoach, Math.Max(indexofTop, Math.Max(indexofSched, indexOfResult)));
                    compName = msg.Substring(0, index).Trim();
                    msg = msg.Substring(index).Trim();
                }

                var ambigous = new Dictionary<string, List<CompetitionWrapper>>();
                var results = new List<CompetitionWrapper>();
                string comp = compName;
                if (compName == null)
                {
                    ulong channel_id = BotChannels.RequireChannel(dbManager, arg);
                    var curList = new List<int>();
                    var curStringList = BotChannels.GetBotSetting<List<string>>(dbManager, arg.Author.Id, channel_id, "comp list", new List<string>());
                    compName = string.Join(',', curStringList);
                    curList = InputParser.GetCompetitionIDListFromIDNameOrAliasList(dbManager, curStringList, out results, out ambigous);
                    comp = string.Join('|', curList);
                    if (string.IsNullOrEmpty(comp))
                    {
                        reply.Description += ("You need to enter a competition id or name (or you must be in a channel with a registered competition, see '!gs channel comp?'");
                        return;
                    }
                }
                else
                {
                    comp = InputParser.GetCompetitionIDListFromIDNameOrAlias(dbManager, compName, out results, out ambigous);
                    if (ambigous.Count > 0)
                    {
                        reply.Description += OutputCreator.ShowAmbigousCompetitions(ambigous);
                        return;
                    }
                    if (results.Count == 0)
                    {
                        reply.Description += ("Unable to find any competition named '" + comp + "'");
                        return;

                    }
                }



                if (msg.StartsWith("top"))
                {
                    CompetitionTopCommand.Parse(dbManager, arg, msg, ref reply, compName, comp);
                }
                else if(msg.StartsWith("coach"))
                {
                    CompetitionCoachCommand.Parse(dbManager, arg, msg, ref reply, compName, comp);
                }
                else if (msg.StartsWith("team"))
                {
                    CompetitionTeamCommand.Parse(dbManager, arg, msg, ref reply, compName, comp);
                }
                else if (msg.StartsWith("overtake"))
                {
                    CompetitionOvertakeCommand.Parse(dbManager, arg, msg, ref reply, compName, comp);
                }
                else if (msg.StartsWith("sched"))
                {
                    CompetitionScheduleCommand.Parse(dbManager, arg, msg, ref reply, compName, comp);
                }
                else if (msg.StartsWith("result"))
                {
                    CompetitionResultsCommand.Parse(dbManager, arg, msg, ref reply, compName, comp);
                }

            }
            else
            {
                int colsize = 27;
                reply.Description = "" +
                    "```" +
                    "!gs comp (<id|name>) top (X)".PadRight(colsize) + " - Show top teams in competition" + Environment.NewLine +
                    "!gs comp (<id|name>) top (X) (<race>)".PadRight(colsize) + " - Show top teams for race in competition" + Environment.NewLine +
                    "!gs comp (<id|name>) coach <id|name>".PadRight(colsize) + " - Show coach info for this competition" + Environment.NewLine +
                    "!gs comp (<id|name>) team <id|name>".PadRight(colsize) + " - Show team info for this competition" + Environment.NewLine +
                    "!gs comp (<id|name>) result (<X>)".PadRight(colsize) + " - Show last X results" + Environment.NewLine +
                    "!gs comp (<id|name>) sched?".PadRight(colsize) + " - Show schedule command help" + Environment.NewLine +
                   "!gs comp (<id|name>) overtake <pos> from <pos>".PadRight(colsize) + " - Wins needed to overtake pos" + Environment.NewLine +
                    " Example: !gs comp CCL top orc" + Environment.NewLine +
                    " Example: !gs comp overtake 3 from 5" + Environment.NewLine +
                    "```";
            }
            reply.Description += "";
        }
    }
}
