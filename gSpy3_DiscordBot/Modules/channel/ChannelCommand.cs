﻿using Discord;
using Discord.WebSocket;
using gSpy3_Common.Database;
using gSpy3_DiscordBot.Modules.comp;
using gSpy3_DiscordBot.Modules.status;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_DiscordBot.Modules.channel
{
    public class ChannelCommand
    {
        public static void Parse(DatabaseManager dbManager, SocketMessage arg, string msg, ref EmbedBuilder reply)
        {
            msg = Command.NextWord(msg);
            if (msg.StartsWith("c")) ChannelCompCommand.Parse(dbManager, arg, msg, ref reply);
            else if (msg.StartsWith("s")) ChannelStatusCommand.Parse(dbManager, arg, msg, ref reply);

            else
            {
                int colsize = 27;
                reply.Description = "" +
                    "```" +
                    "!gs channel comp?".PadRight(colsize) + " - Adding competitions to channel" + Environment.NewLine +
                    "!gs channel status?".PadRight(colsize) + " - Adding status events to channel" + Environment.NewLine +
                    "```";
            }
            reply.Description += "";
        }
    }
}
