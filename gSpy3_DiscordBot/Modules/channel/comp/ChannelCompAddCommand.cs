﻿using Discord;
using Discord.WebSocket;
using gSpy3_Common;
using gSpy3_Common.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_DiscordBot.Modules.comp
{
    public class ChannelCompAddCommand
    {
        public static void Parse(DatabaseManager dbManager, SocketMessage arg, string msg, ref EmbedBuilder reply)
        {
            ulong channel_id = BotChannels.RequireChannel(dbManager, arg);
            if (channel_id <= 0) { reply.Description += "This command requires you to be in a channel"; }
            else
            {

                msg = Command.NextWord(msg);
                var ambigous = new Dictionary<string, List<CompetitionWrapper>>();
                var results = new List<CompetitionWrapper>();
                var idlist = InputParser.GetCompetitionIDListFromIDNameOrAlias(dbManager, msg, out results, out ambigous);

                var curList = new List<string>();
                curList = BotChannels.GetBotSetting<List<string>>(dbManager, arg.Author.Id, channel_id, "comp list", new List<string>());

               
                {
                    if (curList.Contains("" + msg))
                    {
                        reply.Description += "Already in list: " + msg + Environment.NewLine;
                    }
                    else
                    {
                        curList.Add("" + msg);
                        reply.Description += "Added: " + msg + Environment.NewLine;
                    }
                }
                if (results.Count == 0)
                {
                    reply.Description += "Could not find any competition matching '"+msg+"'";
                }
                reply.Description += OutputCreator.ShowAmbigousCompetitions(ambigous);

                BotChannels.SetBotSetting(dbManager, arg.Author.Id, channel_id, "comp list", curList);
            }

        }
    }
}
