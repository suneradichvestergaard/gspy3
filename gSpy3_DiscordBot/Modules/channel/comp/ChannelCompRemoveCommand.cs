﻿using Discord;
using Discord.WebSocket;
using gSpy3_Common;
using gSpy3_Common.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_DiscordBot.Modules.comp
{
    public class ChannelCompRemoveCommand
    {
        public static void Parse(DatabaseManager dbManager, SocketMessage arg, string msg, ref EmbedBuilder reply)
        {
            ulong channel_id = BotChannels.RequireChannel(dbManager, arg);
            if (channel_id < 0) reply.Description += "This command requires you to be in a channel";

            msg = Command.NextWord(msg);
            var ambigous = new Dictionary<string, List<CompetitionWrapper>>();
            var results = new List<CompetitionWrapper>();
            var idlist = InputParser.GetCompetitionIDListFromIDNameOrAlias(dbManager, msg, out results, out ambigous);

            var curList = new List<string>();
            curList = BotChannels.GetBotSetting<List<string>>(dbManager, arg.Author.Id, channel_id, "comp list", new List<string>());

            
            {
                if (curList.Contains("" + msg) == false)
                {
                    reply.Description += "Not in list: " + msg;
                }
                else
                {
                    curList.Remove("" + msg);
                    reply.Description += "Removed: " + msg + Environment.NewLine;
                }
            }
            reply.Description += OutputCreator.ShowAmbigousCompetitions(ambigous);

            BotChannels.SetBotSetting(dbManager, arg.Author.Id, channel_id, "comp list", curList);
        }
    }
}
