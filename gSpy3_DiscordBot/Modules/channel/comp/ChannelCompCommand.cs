﻿using Discord;
using Discord.WebSocket;
using gSpy3_Common.Database;
using gSpy3_Common.Database.Format;
using gSpy3_DiscordBot.Modules.channel.comp;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_DiscordBot.Modules.comp
{
    public class ChannelCompCommand 
    {
        public static void Parse(DatabaseManager dbManager, SocketMessage arg, string msg, ref EmbedBuilder reply) 
        {
            msg = Command.NextWord(msg);
            if (msg.StartsWith("l")) ChannelCompListCommand.Parse(dbManager, arg, msg, ref reply);
            else if (msg.StartsWith("a")) ChannelCompAddCommand.Parse(dbManager, arg, msg, ref reply);
            else if (msg.StartsWith("r")) ChannelCompRemoveCommand.Parse(dbManager, arg, msg, ref reply);
            else if (msg.StartsWith("e")) ChannelCompEventsCommand.Parse(dbManager, arg, msg, ref reply);

            else
            {
                int colsize = 27;
                reply.Description = "" +
                    "```" +
                    "!gs channel comp list".PadRight(colsize) + " - List this channels competitions" + Environment.NewLine +
                    "!gs channel comp add <id|name>".PadRight(colsize) + " - Add competition to this channel" + Environment.NewLine +
                    "!gs channel comp remove <id|name>".PadRight(colsize) + " - Remove comp. from this channel" + Environment.NewLine +
                    "!gs channel comp events <true/false>".PadRight(colsize) + " - Get match reports from comps" + Environment.NewLine +
                    "```";
            }
            reply.Description += "";
        }
    }
}
