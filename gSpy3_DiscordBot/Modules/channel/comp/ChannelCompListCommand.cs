﻿using Discord;
using Discord.WebSocket;
using gSpy3_Common;
using gSpy3_Common.Database;
using gSpy3_Common.Database.Format;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_DiscordBot.Modules.comp
{
    public class ChannelCompListCommand
    {
        public static void Parse(DatabaseManager dbManager, SocketMessage arg, string msg, ref EmbedBuilder reply)
        {
            ulong channel_id = BotChannels.RequireChannel(dbManager, arg);
            if (channel_id < 0) reply.Description += "This command requires you to be in a channel";

          
            var curStringList = BotChannels.GetBotSetting<List<string>>(dbManager, arg.Author.Id, channel_id, "comp list", new List<string>());

            foreach (var row in curStringList)
            {
                var ambigous = new Dictionary<string, List<CompetitionWrapper>>();
                var results = new List<CompetitionWrapper>();
                InputParser.GetCompetitionIDListFromIDNameOrAlias(dbManager, row, out results, out ambigous);
                if(results.Count > 1)
                {
                    reply.Description += OutputCreator.GetCompetitionLink(row, row, results[0].idorigin, false) + Environment.NewLine;
                } else if(results.Count == 1)
                {
                    reply.Description += OutputCreator.GetCompetitionLink("" + results[0].idcompetition, results[0].name, results[0].idorigin) + Environment.NewLine;
                } else if (results.Count == 0)
                {
                    reply.Description += row + " (no competitions found)" + Environment.NewLine; ;
                }

                
            }
            if (curStringList.Count == 0)
            {
                reply.Description += "List is empty. Add new comptitions with !gs channel comp add <id|name>";
            }
        }

    }
}
