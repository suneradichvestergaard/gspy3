﻿using Discord;
using Discord.WebSocket;
using gSpy3_Common.Database;
using gSpy3_Common.Database.Format;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_DiscordBot.Modules.status
{
    public class ChannelStatusCommand
    {
        public static void Parse(DatabaseManager dbManager, SocketMessage arg, string msg, ref EmbedBuilder reply) 
        {
            msg = Command.NextWord(msg);
            if (msg.StartsWith("a")) ChannelStatusAddCommand.Parse(dbManager, arg, msg, ref reply);
            else if (msg.StartsWith("r")) ChannelStatusRemoveCommand.Parse(dbManager, arg, msg, ref reply);

            else
            {
                int colsize = 27;
                reply.Description = "" +
                    "```" +
                    "!gs channel status add".PadRight(colsize) + " - Add channel as status listener" + Environment.NewLine +
                    "!gs channel status remove".PadRight(colsize) + " - Remove channel as status listener" + Environment.NewLine +
                    "```";
            }
            reply.Description += "";
        }
    }
}
