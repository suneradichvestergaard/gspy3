﻿using Discord;
using Discord.WebSocket;
using gSpy3_Common.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_DiscordBot.Modules.status
{
    public class ChannelStatusRemoveCommand
    {
        public static void Parse(DatabaseManager dbManager, SocketMessage arg, string msg, ref EmbedBuilder reply)
        {
            ulong channel_id = BotChannels.RequireChannel(dbManager, arg);
            if (channel_id < 0) reply.Description += "This command requires you to be in a channel";

            string status = "";
            status = BotChannels.GetBotSetting<string>(dbManager, arg.Author.Id, channel_id, "status alert", "");
            if (status != "status")
            {
                reply.Description += "Not a status alert listener";
                return;
            }
            BotChannels.SetBotSetting(dbManager, arg.Author.Id, channel_id, "status alert", "");
            reply.Description += "Removed channel from status alerts" + Environment.NewLine;
        }
    }
}
