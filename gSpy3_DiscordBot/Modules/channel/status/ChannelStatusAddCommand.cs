﻿using Discord;
using Discord.WebSocket;
using gSpy3_Common.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_DiscordBot.Modules.status
{
    public class ChannelStatusAddCommand
    {
        public static void Parse(DatabaseManager dbManager, SocketMessage arg, string msg, ref EmbedBuilder reply)
        {
            ulong channel_id = BotChannels.RequireChannel(dbManager, arg);
            if (channel_id <= 0) { reply.Description += "This command requires you to be in a channel"; }
            else
            {

                var curList = "";
                curList = BotChannels.GetBotSetting<string>(dbManager, arg.Author.Id, channel_id, "status alert", "");
 
                if(curList == "status")
                {
                    reply.Description += "Already a status listener" + Environment.NewLine;
                    return;
                }

                BotChannels.SetBotSetting(dbManager, arg.Author.Id, channel_id, "status alert", "status");
                reply.Description += "Added channel as status listener" + Environment.NewLine;
            }

        }
    }
}
