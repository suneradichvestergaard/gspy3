﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_DiscordBot.Modules
{
    public class Command
    {
        public static string NextWord(string msg)
        {
            int nextSpace = msg.IndexOf(" ");
            if(nextSpace >= 0)
            {
                return msg.Substring(nextSpace + 1);
            }
            return "";
        }
    }
}
