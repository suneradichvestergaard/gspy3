﻿using Discord;
using Discord.WebSocket;
using gSpy3_Common.Database;
using gSpy3_Common.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_DiscordBot.Modules
{
    public class RankCommand
    {
        public static void Parse(DatabaseManager dbManager, SocketMessage arg, string msg, ref EmbedBuilder reply)
        {
            msg = Command.NextWord(msg);
            string[] splits = msg.Split(" ");
            if(splits.Length != 3)
            {
                reply.Description += "You must enter values for wins, draws and losses: '!gs rank 2 1 2'";
                return;
            }
            int wins = int.Parse(splits[0]);
            int draws = int.Parse(splits[1]);
            int losses = int.Parse(splits[2]);
            int concedes = 0;
            if ((wins + draws + losses) <= 0)
            {
                reply.Description += "Cannot caluclate rank for zero games played";
                return;

            }
            double rank = Ranking.CalculateRank(wins, draws, losses, concedes);
            reply.Description +="Rank for "+wins+" wins, "+draws+" draws and "+losses+" losses :"+Environment.NewLine+ rank.ToString("0.000").Replace(",", ".");


        }
    }
}
