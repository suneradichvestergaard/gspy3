﻿using gSpy3_Common;
using gSpy3_Common.Database;
using gSpy3_Common.Database.Format;
using gSpy3_Common.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_DiscordBot
{
    public static class OutputCreator
    {
        public static string GetCompetitionLink(string idcomp, string name, OriginID idorigin, bool showNum=true)
        {
            OriginID org = (OriginID)idorigin;
            string sOrg = showNum?( org != OriginID.BB2_pc ? ("§" + OriginConversion.ToLegacyGoblinSpy(org)) : ""):"";
            return "[" + name + "](https://www.mordrek.com/gspy/comp/" + idcomp.Replace(" ","%20") + ")" + sOrg + (showNum?("#" + idcomp + ""):"");
        }
        public static string GetScheduleLink(string idcomp, string name, OriginID idorigin, bool showNum = true)
        {
            OriginID org = (OriginID)idorigin;
            string sOrg = showNum ? (org != OriginID.BB2_pc ? ("§" + OriginConversion.ToLegacyGoblinSpy(org)) : "") : "";
            return "[" + name + " / schedule](https://www.mordrek.com/gspy/comp/" + idcomp.Replace(" ", "%20") + "/schedule)" + sOrg + (showNum ? ("#" + idcomp + "") : "");
        }
        public static string GetResultsLink(string idcomp, string name, OriginID idorigin, bool showNum = true)
        {
            OriginID org = (OriginID)idorigin;
            string sOrg = showNum ? (org != OriginID.BB2_pc ? ("§" + OriginConversion.ToLegacyGoblinSpy(org)) : "") : "";
            return "[" + name + " / results](https://www.mordrek.com/gspy/comp/" + idcomp.Replace(" ", "%20") + "/results)" + sOrg + (showNum ? ("#" + idcomp + "") : "");
        }
        public static string GetCoachLink(string idcomp, string idcoach, string name, OriginID? idorigin, bool showNum = true)
        {
            var org = idorigin;
            string sOrg = showNum && org != null ? (org != OriginID.BB2_pc ? ("§" + OriginConversion.ToLegacyGoblinSpy((OriginID)org)) : "") : "";
            if (idcomp != null && idcomp != "0")
            {
                return "[" + name + "](https://www.mordrek.com/gspy/comp/" + idcomp.Replace(" ", "%20") + "/coach/" + idcoach.Replace(" ", "%20") + ")" + sOrg + (showNum ? ("#" + idcoach + "") : "");
            }
            return "[" + name + "](https://www.mordrek.com/gspy/coach/" + idcoach.Replace(" ", "%20") + ")" + sOrg + (showNum ? ("#" + idcoach + "") : "");
        }
        public static string GetTeamLink(string idcomp, string idteam, string name, OriginID? idorigin, bool showNum = true)
        {
            var org = idorigin;
            string sOrg = showNum && org != null ? (org != OriginID.BB2_pc ? ("§" + OriginConversion.ToLegacyGoblinSpy((OriginID)org)) : "") : "";
            if (idcomp != null && idcomp != "0")
            {
                return "[" + name + "](https://www.mordrek.com/gspy/comp/" + idcomp.Replace(" ", "%20") + "/team/" + idteam.Replace(" ", "%20") + ")" + sOrg + (showNum ? ("#" + idteam + "") : "");
            }
            return "[" + name + "](https://www.mordrek.com/gspy/team/" + idteam.Replace(" ", "%20") + ")" + sOrg + (showNum ? ("#" + idteam + "") : "");
        }
        public static string GetTeamStandingLink(string idcomp, string idteam, string name, OriginID? idorigin, bool showNum = true)
        {
            var org = idorigin;
            string sOrg = showNum && org != null ? (org != OriginID.BB2_pc ? ("§" + OriginConversion.ToLegacyGoblinSpy((OriginID)org)) : "") : "";
            if (idcomp != null && idcomp != "0")
            {
                return "[" + name + " / standing](https://www.mordrek.com/gspy/comp/" + idcomp.Replace(" ", "%20") + "/team/" + idteam.Replace(" ", "%20") + "/standing)" + sOrg + (showNum ? ("#" + idteam + "") : "");
            }
            return "[" + name + " / standing](https://www.mordrek.com/gspy/team/" + idteam.Replace(" ", "%20") + "/standing)" + sOrg + (showNum ? ("#" + idteam + "") : "");
        }
        public static string GetMatchLink(string idcomp, int idmatch, string home, string away)
        {
            return "[" + home + " vs " + away + "](https://www.mordrek.com/gspy/comp/" + idcomp.Replace(" ", "%20") + "/match/" + idmatch + ")";
        }

        public static string GetStandingsLink(string idcomp, string name, OriginID? idorigin, FilteredQueryRequest filter=null, bool showNum=true)
        {
            Dictionary<string, FilteredQueryRequest> map = new Dictionary<string, FilteredQueryRequest>();
            if (filter != null)
            {
                map[filter.id] = filter;
                filter.idmap = null;
                filter.limit = null;
            }
            OriginID? org = (OriginID?)idorigin;
            string sOrg = org==null?"":( org != OriginID.BB2_pc ? ("§" + OriginConversion.ToLegacyGoblinSpy((OriginID)org)) : "");
            return "[" + name + "]("+
                ("https://www.mordrek.com/gspy/comp/" + idcomp.Replace(" ","%20") + "/standings")+
                (filter==null?"":("?filter="+ (Newtonsoft.Json.JsonConvert.SerializeObject(map).Replace(" ", "%20"))))+
                
                ")" + sOrg + (showNum?"#" + idcomp + "":"");
        }

        public static string PadLeftFit(string name, int size)
        {
            if (name.Length > size) return name.Substring(0, size);
            return name.PadLeft(size);
        }
        public static string PadRightFit(string name, int size)
        {
            if (name.Length > size) return name.Substring(0, size);
            return name.PadRight(size);
        }

        public static string ShowAmbigousCoaches(Dictionary<string, List<CoachWrapper>> unknowns)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Ambigous coaches:");
            sb.Append(Environment.NewLine);
            if (unknowns.Values.Count == 0) return "";
            foreach (var kvp in unknowns)
            {
                sb.Append("  ");
                sb.Append(kvp.Key);
                sb.Append(":");
                sb.Append(Environment.NewLine);
                foreach (var item in kvp.Value)
                {
                    sb.Append("    " + GetCoachLink("" + item.idcompetition, "" + item.idcoach, item.coach_name, item.idorigin));
                    sb.Append(Environment.NewLine);

                }
            }
            sb.Append("");
            return sb.ToString();
        }
        public static string ShowAmbigousTeams(Dictionary<string, List<TeamWrapper>> unknowns)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Ambigous teams:");
            sb.Append(Environment.NewLine);
            if (unknowns.Values.Count == 0) return "";
            foreach (var kvp in unknowns)
            {
                sb.Append("  ");
                sb.Append(kvp.Key);
                sb.Append(":");
                sb.Append(Environment.NewLine);
                foreach (var item in kvp.Value)
                {
                    sb.Append("    " + GetTeamLink("" + item.idcompetition, "" + item.idteam, item.team_name, item.idorigin));
                    sb.Append(Environment.NewLine);

                }
            }
            sb.Append("");
            return sb.ToString();
        }
        public static string ShowAmbigousCompetitions(Dictionary<string, List<CompetitionWrapper>> unknowns)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Ambigous competitions:");
            sb.Append(Environment.NewLine);
            if (unknowns.Values.Count == 0) return "";
            foreach (var kvp in unknowns)
            {
                sb.Append("  ");
                sb.Append(kvp.Key);
                sb.Append(":");
                sb.Append(Environment.NewLine);
                foreach (var item in kvp.Value)
                {
                    sb.Append("    " + GetCompetitionLink("" + item.idcompetition, item.name, item.idorigin));
                    sb.Append(Environment.NewLine);

                }
            }
            sb.Append("");
            return sb.ToString();
        }
    }
}
