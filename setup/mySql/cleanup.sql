use gSpy3;
TRUNCATE  coaches ;
TRUNCATE  competitions;
TRUNCATE  leagues ;
TRUNCATE  matches ;
TRUNCATE  playermatchstats;
TRUNCATE  players ;
TRUNCATE  schedules; 
TRUNCATE  standings;
TRUNCATE  teammatchstats ;
TRUNCATE  teams ;

truncate gspy3_backend.work;
truncate gspy3_backend.leagueupdates;
truncate gspy3_backend.resourceusage;
truncate gspy3_backend.users;
truncate gspy3_backend.modules;
truncate gspy3_backend.modulestates;

ALTER TABLE coaches AUTO_INCREMENT = 1;
ALTER TABLE competitions AUTO_INCREMENT = 1;
ALTER TABLE leagues AUTO_INCREMENT = 1;
ALTER TABLE playermatchstats AUTO_INCREMENT = 1;
ALTER TABLE players AUTO_INCREMENT = 1;
ALTER TABLE matches AUTO_INCREMENT = 1;
ALTER TABLE schedules AUTO_INCREMENT = 1;
ALTER TABLE standings AUTO_INCREMENT = 1;
ALTER TABLE teammatchstats AUTO_INCREMENT = 1;
ALTER TABLE  gspy3_backend.work AUTO_INCREMENT = 1;
ALTER TABLE  gspy3_backend.leagueupdates AUTO_INCREMENT = 1;
ALTER TABLE  gspy3_backend.resourceusage AUTO_INCREMENT = 1;
ALTER TABLE  gspy3_backend.users AUTO_INCREMENT = 1;
ALTER TABLE  gspy3_backend.modules AUTO_INCREMENT = 1;
ALTER TABLE  gspy3_backend.modulestates AUTO_INCREMENT = 1;

TRUNCATE  casualties ;
TRUNCATE  casualtycombos ;
TRUNCATE  skillcombos ;
TRUNCATE  skills ;
TRUNCATE  logos ;
TRUNCATE  playernames ;
TRUNCATE  highlights ;
TRUNCATE  userschedules ;
ALTER TABLE logos AUTO_INCREMENT = 1;
ALTER TABLE skills AUTO_INCREMENT = 1;
ALTER TABLE skillcombos AUTO_INCREMENT = 1;
ALTER TABLE casualties AUTO_INCREMENT = 1;
ALTER TABLE casualtycombos AUTO_INCREMENT = 1;
ALTER TABLE playernames AUTO_INCREMENT = 1;
ALTER TABLE highlights AUTO_INCREMENT = 1;
ALTER TABLE userschedules AUTO_INCREMENT = 1;


