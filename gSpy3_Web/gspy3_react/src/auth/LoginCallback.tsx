import React, { FC, useEffect } from "react";
import GSpy3_api from  "../api/gSpy3_api"

class LoginCodeChecker{
   public static _lastLoginCallbackCode : string =""
}
const LoginCallback: //FC<RouteComponentProps> = ({ location }) => { 
  FC = () => {
  useEffect(() => {
    //const code = (location.search.match(/code=([^&]+)/) || [])[1];
    //const state = (location.search.match(/state=([^&]+)/) || [])[1];
    const code = (window.location.search.match(/code=([^&]+)/) || [])[1];
    const state = (window.location.search.match(/state=([^&]+)/) || [])[1];
    let decoded = decodeURIComponent(state)
    let stateObj = JSON.parse(decoded);
    let api = new GSpy3_api();
    if(api.isLoggedIn()==false && code != LoginCodeChecker._lastLoginCallbackCode){
      LoginCodeChecker._lastLoginCallbackCode = code;
    api.getAuthTokenWithCode(stateObj["provider"],decodeURIComponent(code)).then((data)=>{
      
        api.setUserLoginData(data);
        window.location.assign("/gspy")
    }).catch((e)=>{
        console.log(e);
    });
  }
    });
  
    return <p>

  </p>;
}

export default LoginCallback;