import React, { FC, useCallback } from "react";
import GSpy3_api from "../api/gSpy3_api"

const Login: FC = () => {

  const handleGoogleLogin = useCallback(async () => {

    
    try {
        let api = GSpy3_api.getAPI();
        let url = await api.getAuthUrl("google");
        window.location.assign(url);
    } catch (e) {
      console.error(e);
    }
    
  }, []);

  return (
    <div className="Login">
      <button onClick={handleGoogleLogin}>
        Login 
      </button>
    </div>
  );
};
/* Was <div className="Login">
      <button onClick={handleGoogleLogin}>
        Login with Google
      </button>
    </div>*/
export default Login;