export default class timeManager {

    static  utcToLocalDatetime(utc:string) : string{
        if(!utc) return "";
        if(utc.endsWith("Z")==false) utc+="Z";
        var date = new Date(utc);

        var yyyy = date.getFullYear();
        var mm = date.getMonth() < 9 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1); // getMonth() is zero-based
        var dd = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
        
        var yyyymmdd = "".concat(""+yyyy).concat("-"+mm).concat("-"+dd);
        var hh = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
        var min = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        var sec = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
        return "".concat(yyyymmdd).concat(" "+hh).concat(":"+min).concat(":"+sec);
    }

}