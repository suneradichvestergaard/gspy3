import React  from 'react';
import { ColumnID } from '../api/views/columns';
import {Link} from "react-router-dom"
import LookVs from "../gfx/look-vs.png"

export default class highlightManager {

    static getReactKilledMinimmalRow(cols : {[id: string] : number}, row : string[]) : React.ReactElement
    {
        let type= +row[cols[ColumnID[ColumnID.idhighlighttype]]] 
        let pid = +row[cols[ColumnID[ColumnID.idplayer]]] 
        let cause=""

        if(type==2){
            switch(+row[cols[ColumnID[ColumnID.idcause]]]){
                case 1:cause = "Failed rush"; break;
                case 2:cause = "Failed dodge";break;
                case 5:cause = "Failed block";break;
                case 18:cause = "Pitch invasion";break;
                case 24:cause = "Failed landing";break;
                case 28:cause = "Being eaten";break;
                case 34:cause = "Stabbed to death";break;
                case 39:cause = "Failed chainsaw";break;
                case 43: case 59: case 60:cause = "Piled on";break;
                case 54:cause = "Fireball";break;
                case 72:case 74:cause = "Bomb";break;
                case 73: cause = "Chainsaw"; break;
                    default: cause= "Uknown reason "+row[cols[ColumnID[ColumnID.idcause]]];
            }
        }


        let url2 = "/comp/"+row[cols[ColumnID[ColumnID.idtargetcompetition]]]+"/match/"+row[cols[ColumnID[ColumnID.idmatch]]]+"/replay?subtick="+row[cols[ColumnID[ColumnID.subturn]]];
        return <div>
            {type == 1 && row[cols[ColumnID[ColumnID.target_name]]] && <span>
                    {row[cols[ColumnID[ColumnID.target_name]]]} <Link to={url2} title="Click to view match"><img className="iconSmall" src={LookVs} alt="vs"></img></Link>
                </span>}
            {type == 2&& <span>
                    {cause} 
                    <Link to={url2} title="Click to view match"><img className="iconSmall" src={LookVs} alt="vs"></img></Link>
                </span>}

        </div>
    }


}