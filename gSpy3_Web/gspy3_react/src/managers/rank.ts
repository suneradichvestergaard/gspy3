export default class Rank{
    static calculateRank(wins:number, draws:number, losses:number, concessions:number){
        var played = wins + draws + losses;
      var CrossPoint = 0.2;
      var Limit = 42;
      //var NonConcedeBonus = (played - concedes) * 0.02;
    var NonConcedeBonus = (played) * 0.02;
      var a = 0.05;
      var Target = 28;
      var x = Math.log10(a / (1 - CrossPoint)) / Math.log10(1 - (Target / Limit));
      var winPct = 100.0 * (wins + draws / 2.0) / played;
      var RankPoints = winPct * (CrossPoint + (1 - CrossPoint) * (1 - Math.pow((1 - Math.min(Limit, played) / Limit), x))) + NonConcedeBonus;
      return RankPoints;
    }
}