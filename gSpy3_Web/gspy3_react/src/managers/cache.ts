class Cache{

    maxSize : number
    maxAgeMinutes : number
    map : {[id:string]:any} = {}

 
    constructor(amaxSize : number, amaxAgeMinutes : number){
        this.maxSize = amaxSize;
        this.maxAgeMinutes = amaxAgeMinutes;
    }

    addItem(key : string, obj : any){
        
        this.map[key] = new CacheItem(obj)
        while(Object.keys(this.map).length > this.maxSize){
            var maxKey = "";
            var minAge = (new Date()).valueOf();
            for(var key2 in this.map){
                var ci = this.map[key2];
                if(ci.added.valueOf() < minAge){
                    minAge = ci.added.valueOf();
                    maxKey = key2;
                }
            }
            delete this.map[maxKey];
        }

    }

    getItem(key : string) : any|undefined{
        var cached = this.map[key];
        if(cached){
            let now = new Date();
            let diff = Math.abs(now.valueOf() - cached.added.valueOf());
            let diffDate = new Date(diff);
            if( (diffDate.getTime() / (1000*60)) > this.maxAgeMinutes){
               
                delete this.map[key];
                return undefined;
                
            }
            
            return cached.content;
        }
        return undefined;
    }

}

class CacheItem{
    content : any
    added : Date

    constructor(obj : any){
        this.content = obj
        this.added = new Date();
    }
}


export default Cache