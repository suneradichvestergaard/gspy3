
class messageManager {
    popup : PopupData = new PopupData()
    popupChangedCallback : {():void;}|undefined

    static instance : messageManager

    static getMessageManager() : messageManager{
        if(!this.instance){
            this.instance = new messageManager();
        }
        return this.instance;
    }

    constructor(){
        this.popup.autoCloseCallback = (()=>{
            this.popup.isVisible = false;
            if(this.popupChangedCallback)
            this.popupChangedCallback();
        });
    }

    addMessage(display : React.ReactElement, autoCloseTime : number|undefined, popupType: PopupTypeEnum){

        console.log(display);
        this.popup.display = display;
        this.popup.autoCloseTime = autoCloseTime;
        this.popup.isVisible = true;
        this.popup.popupType = popupType;
        if(this.popupChangedCallback)
            this.popupChangedCallback();
    }

}

export enum PopupTypeEnum{
    Success, Failure, Neutral
}

export class PopupData{
    isVisible : boolean = false
    display : React.ReactElement|undefined
    autoCloseTime: number|undefined
    popupType : PopupTypeEnum=PopupTypeEnum.Neutral
    autoCloseCallback :  {():void;}|undefined
}

export default messageManager