import GSpyAPI from "../api/gSpy3_api"
import ServerStatus from "../api/views/statusCollection";

class StatusManager{

    onUpdate : (stats: ServerStatus) => void = (o)=>{}; 

    constructor(){
        this.onTick = this.onTick.bind(this);
        this.onTick();

    }



    onTick(){
        
        GSpyAPI.getAPI().getStatus().then((data)=>{
            if(this.onUpdate){
                this.onUpdate(data);
            }

        }).catch((err)=>{
            console.log("Error getting status");
            console.log(err);
            if(this.onUpdate){
                let status = new ServerStatus()
                this.onUpdate(status);
            }
        })

        let that = this;
        // Get new data every now and then
        setTimeout(that.onTick, 1000 * 60 * 5);
    }

}





export default StatusManager