import messageManager, {PopupTypeEnum} from "./messageManager"

import GSpy3_api from "../api/gSpy3_api";
import React from "react"
import {QueryPageRequest, QueryPageResponse} from "../api/queryPageRequests"
import DataView, {DataViewGroup,DataViewID} from "../api/views/dataView"
import QueryPageRequests, {QueryPageResponseView} from "../api/queryPageRequests"
import DataCache from "../managers/cache"
import QueryResult from "../api/views/queryResult";

export class PageSelectionManager{
    requests : QueryPageRequests = new QueryPageRequests(); // Requests received. Set to failed if dirty
    responses : QueryPageResponseView = new QueryPageResponseView(); // Responses received
    filters : QueryPageRequests = new QueryPageRequests(); //Filters that are merged onto the requests
    selections : {[id: string] : string;} = {}; // Selected team, coach, player etx
    messages : messageManager = messageManager.getMessageManager()
    callback : {(newState:QueryPageResponseView):void} = (o)=>void{}
    failed : boolean = false
    cache : DataCache = new DataCache(30,5);
    loadingCallbacks : {[id: string] : {(isLoading:boolean):void}} = {}
    loads : number = 0

    init(messages : messageManager, updateCallback : {(newState:QueryPageResponseView):void}){
        this.messages = messages;
        this.callback = updateCallback;
        this.locationChanged();
    }

    registerLoadingCallback(name : string,callback: {(isLoading:boolean):void}){
        this.loadingCallbacks[name] = callback;
    }

    createIdmap(view: DataView) : {[id:string]:string}{
        let retMap :{[id: string]:string} = {};
        let viewKey = DataView[view];
        DataViewGroup[viewKey]?.forEach((dv)=>{
            let groupKey = DataView[dv];
            let baseID = DataViewID[groupKey];
            retMap[baseID] = this.selections[groupKey];
        });
       
        return retMap;
    }

    // Ensure that the given views have been loaded or load if needed
    ensureViews(views : DataView[]) : Promise<QueryPageResponseView> {
        
        let promise = new Promise<QueryPageResponseView>((acc,rej)=>{
        let anyNeedUpdate :boolean = false;
console.log("Ensure views");
        // Go through all given views and check that they are downloaded and up to date
        // If not, download them
        views.forEach((v)=>{
            let viewKey = DataView[v];
            let currentViewQuery = this.requests.request[viewKey];
            if(!currentViewQuery){ 
               // No existing query, so create one with our current values for the IDs
          //      console.log("NO QUERY FOR CURRENT VIEW");
               var newRequest = new QueryPageRequest();
               this.requests.request[viewKey] = newRequest;
               newRequest.id = viewKey
               newRequest.idmap = this.createIdmap(v);
               anyNeedUpdate = true;
               currentViewQuery = newRequest;
            } else {
                this.requests.request[viewKey].idmap = this.createIdmap(v);
            }


            if(!this.responses.response[viewKey]){
                anyNeedUpdate = true;
                this.requests.request[viewKey].idmap = this.createIdmap(v);
                this.responses.response[viewKey] = new QueryPageResponse();
                this.responses.response[viewKey].loading = true;
                this.responses.response[viewKey].result = new QueryResult();
                this.responses.response[viewKey].request = this.requests.request[viewKey];
                this.responses.response[viewKey].result.success=true;
            }else{
                // If we already have one, we need to compare the response to the request to see if it has changed
                let f = this.filters.request[viewKey];
                let q = this.requests.request[viewKey];
                if(f){
                    q.filters = f.filters;
                    q.from = f.from;
                    q.limit = f.limit;
                    q.order = f.order;
                    q.ordercol = f.ordercol;
                    q.group = f.group;
                    q.aggr = f.aggr;
                    } 
                

                let newReq =  JSON.stringify(q);
                let oldReqFromResponse = JSON.stringify(this.responses.response[viewKey].request);
                console.log(this.responses.response[viewKey]);
                console.log(newReq);
                console.log(oldReqFromResponse);
                if(newReq !== oldReqFromResponse || this.responses.response[viewKey].result.success==false){
                    // It has changed , so make sure response have been removed for this key and that idmap is updated
                    // (unless we have a cache of it of course)
                    let cachedItem = this.cache.getItem(newReq);
                    if(cachedItem){
                        this.responses.response[viewKey] = JSON.parse(cachedItem);
                        
                    } else{
                       
                        anyNeedUpdate = true;
                        this.requests.request[viewKey].idmap = this.createIdmap(v);
                        this.responses.response[viewKey].loading=true;
                        this.responses.response[viewKey].result.success=true;
                        this.responses.response[viewKey].request = q;
                        
                  //      this.responses.response[viewKey] = new QueryPageResponse();
                  //      this.responses.response[viewKey].loading = true;
                   //     this.responses.response[viewKey].result = new QueryResult();
    
                    }
                } else if(!this.responses.response[viewKey]){
                    anyNeedUpdate = true; // We do still need update if the corresponding response has not been aquired yet
                   
                }
                
            }
        });



        if(anyNeedUpdate){
            this.responses.loading = true;
            console.log("At least one needs update");
            // We need to update, which means make a new query to the server
            // Go through all requests. If they lack response, it means we should ask for new response
            let reqArr : QueryPageRequest[] = [];
            
            for (let key in this.requests.request){
                // Is the key in the views we are trying to ensure?
                let partOfView : boolean= false;
                views.forEach((v)=>{
                    if(DataView[v]==key){
                        partOfView = true;
                    }

                });



                if(partOfView && (!this.responses.response[key] || this.responses.response[key].loading || this.responses.response[key].result?.success==false)) 
                {
                    let q=this.requests.request[key];
                    
                    if(this.filters.request){
                        let f = this.filters.request[key];
                        if(f){
                            q.filters = f.filters;
                            q.from = f.from;
                            q.limit = f.limit;
                            q.order = f.order;
                            q.ordercol = f.ordercol;
                            q.group = f.group;
                            q.aggr = f.aggr;
                            } 
                        }
                    
                    reqArr.push(q);
                }
            }
            for (let key in reqArr){
                let q = reqArr[key];
                if(q.id === DataView[DataView.searchCoach]){
                    var txt = q.idmap?q.idmap["search"]:"";
                    delete q.idmap;
                    q.filters={"coach_name":txt};
                }
                if(q.id === DataView[DataView.searchTeam]){
                    var txt3 = q.idmap?q.idmap["search"]:"";
                    delete q.idmap;
                    q.filters={"team_name":txt3}
                }
                if(q.id === DataView[DataView.searchCompetition]){
                    var txt2 = q.idmap?q.idmap["search"]:"";
                    delete q.idmap;
                    delete q.filters;
                    q.filters={"competition_name":txt2}
                }
            }

            

            this.setLoading(true);
            
            GSpy3_api.getAPI().getQueries(reqArr).then((res)=>{
                if(!res.success){
                    this.messages.addMessage(<span>Query FAILED</span>, 3000, PopupTypeEnum.Failure);
                    this.failed = true;
                    this.responses.success = false;
                    this.responses.loading = false;
                    for (let key in res.response) {
                        let qr = res.response[key];
                        this.responses.response[key] = qr;
                        this.responses.response[key].loading = false;
                        this.responses.response[key].request = new QueryPageRequest()
                    }
                    console.log("FAIL");
                    console.log(res);
                    this.setLoading(false);
                    this.callback(this.responses);
                    
                    rej();
                }else{
                    console.log("data downloaded");
                    for (let key in res.response) {
                        this.responses.response[key] = JSON.parse(JSON.stringify(res.response[key]));
                        res.response[key].loading=false;
                        this.responses.response[key].loading = false;
                        this.cache.addItem(JSON.stringify(this.requests.request[key]),JSON.stringify(res.response[key]));
                    }
                    this.responses.success = true;
                    this.responses.loading = false;
                    this.setLoading(false)
                    this.callback(this.responses);
                    acc(this.responses);
                }

            },
            (e)=>{
                console.log("Rejected");
                var eMsg = ""+e
                var msg = <span>Query FAILED<br/>{eMsg}</span>;
                this.messages.addMessage(msg, 3000, PopupTypeEnum.Failure);
                this.failed = true;
                this.responses.success = false;
                this.responses.loading = false;
                this.setLoading(false)
                for (let key in this.requests.request) {
                    this.responses.response[key].request = new QueryPageRequest()
                    this.responses.response[key].loading = false;
                    this.responses.response[key].result.success = false;
                }
       
                this.callback(this.responses);
                

                console.log(e);
                rej();
            }
            ).catch((e)=>{
                console.log("Catched the rejection");
                var eMsg = ""+e
                var msg = <span>Query FAILED<br/>{eMsg}</span>;
                this.messages.addMessage(msg, 3000, PopupTypeEnum.Failure);
                this.failed = true;
                this.responses.success = false;
                this.responses.loading = false;
                for (let key in this.requests.request) {
                    this.responses.response[key].request = new QueryPageRequest()
                    this.responses.response[key].loading = false;
                    this.responses.response[key].result.success = false;
                }
       
                this.callback(this.responses);
                this.setLoading(false)

                console.log(e);
                rej();
            });

        } else{
           /* this.responses.loading = false;
            for (let key in this.responses.response) {
                this.responses.response[key].loading = false;
            }*/
            console.log("no update needed");
            this.callback(this.responses);
            acc(this.responses);
        }
    })
        
  
    return promise;
    }

    setLoading(loading : boolean){
        if(loading){this.loads++;}
        else  {this.loads--;}
        for (let key in this.loadingCallbacks)
        {
            let cb= this.loadingCallbacks[key]
            cb(this.loads > 0)
        }
    }

    locationChanged(){
        this.failed = false;
        let loc = window.location.pathname;
        this.selections = {};
       
        // Looks for DataView keys in the path so store the selection
        for (const key of Object.keys(DataView)) {
            let index = loc.indexOf("/"+key+"/");
            let id :string|undefined = undefined;
            
            if(index >= 0){
                // Key was found
                let sub = loc.substr(index+("/"+key+"/").length);
                let stopper = sub.indexOf("/");
                
                if(stopper >= 0)
                    id = (sub.substr(0,stopper));
                else id = sub;
                // Found a proper ID, so let's store it. And if it differs from existing, we must clear any response using that view
                id = decodeURI(id);

                this.selections[key] = id;
            }
        }

        // Look for filters
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        let filters = urlParams.get('filter');
        if(filters)
        {
            try{
            this.filters.request = JSON.parse(filters);
            }catch{
                console.log("ERROR");
                console.log(filters);
            }
        } else{
           // console.log("CLEAR FILTERS PLEASE");
            this.filters = new QueryPageRequests();
     
            
        }
       // this.responses = new QueryPageResponseView();

        
    }


    
}




export default PageSelectionManager