export class UserConfig
{
    presentation : UserConfigPresentation = new UserConfigPresentation()
    follows : UserConfigFollows = new UserConfigFollows()

}

class UserConfigPresentation{
    displayStyle : string = ""
    showHelp : boolean = true
}
class UserConfigFollows{
    competitions :string[] = []
    coaches : number[]=[]
}


export default class UserManager 
{

    static getConfig(id : string|undefined) : UserConfig
    {
        if(!id)
        {
            let res = localStorage.getItem("gspy3.user.config");
            if(!res){
                return new UserConfig();
            }
            var o= JSON.parse(res) as UserConfig;
            if(!o.presentation){o.presentation = new UserConfigPresentation();}
            if(!o.follows){o.follows = new UserConfigFollows();}
            if(!o.follows.coaches){o.follows.coaches = []}
            if(!o.follows.competitions){o.follows.competitions = []}
            return o;
        }
        return new UserConfig();

    }
        
    static saveConfig(id : string|undefined, config : UserConfig)
    {
        if(!id)
        {
            localStorage.setItem("gspy3.user.config",JSON.stringify(config));
        }

    }

    
}