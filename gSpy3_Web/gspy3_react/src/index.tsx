import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter as Router  } from "react-router-dom";
import process from "process"

let dev =  !process.env.NODE_ENV || process.env.NODE_ENV === 'development';
if(!dev && document.location.href.toLowerCase().startsWith("https:") == false){
    document.location.assign(document.location.href.replace("http:","https:"));
}


ReactDOM.render(<Router basename="gspy"><App /></Router>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
