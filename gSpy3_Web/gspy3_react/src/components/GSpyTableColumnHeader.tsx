import React, { Component } from 'react';
import {Column, ViewColumns, ColumnID} from "../api/views/columns"
import Up from "../gfx/up.png"
import Down from "../gfx/down.png"
import Look from "../gfx/look.png"
import QueryResult from '../api/views/queryResult';
import CloseImg from "../gfx/close.png"
import HelpImg from "../gfx/help.png"
import {QueryPageRequest} from "../api/queryPageRequests"


type GSpyTableColumnHeaderState = {
  
   // isHovering : boolean,
    isToolboxOpen : boolean,
    isEditing : boolean,
    isShowingEditHelp : boolean,
    curFilter :string,
    editFilter: string
  };
type GSpyTableColumnHeaderProps = {
    wall : string
    column : Column
    table : QueryResult
    command : {(col : Column,cmd:string):void},
    filter? : QueryPageRequest
};

class GSpyTableColumnHeader extends Component<GSpyTableColumnHeaderProps,GSpyTableColumnHeaderState> {
  toolboxCloser : NodeJS.Timeout|null = null
  
  constructor(props : GSpyTableColumnHeaderProps) {
    super(props);
    this.handleMouseHover = this.handleMouseHover.bind(this);
    this.handleMouseHoverLeave = this.handleMouseHoverLeave.bind(this)
    this.onFilter = this.onFilter.bind(this);
    this.onRemoveFilter = this.onRemoveFilter.bind(this);
    this.onSortAsc = this.onSortAsc.bind(this);
    this.onSortDesc = this.onSortDesc.bind(this);
    this.onClickHelp = this.onClickHelp.bind(this);
    this.onPerformFilter = this.onPerformFilter.bind(this);
    this.onCloseFilter = this.onCloseFilter.bind(this);
    this.onFilterKeyPress = this.onFilterKeyPress.bind(this);
    this.onFilterChange = this.onFilterChange.bind(this);
    this.onHeaderClick = this.onHeaderClick.bind(this);
    this.onCustomFilterChange = this.onCustomFilterChange.bind(this);
    this.onCustomFilterPerform = this.onCustomFilterPerform.bind(this);
    let curFilter = "";
    if(this.props.filter?.filters){
      curFilter = this.props.filter.filters[ColumnID[this.props.column.id]];
    }

    this.state = {
     // isHovering: false,
      isEditing :false,
      isShowingEditHelp: false,
      isToolboxOpen: false,
      curFilter :curFilter,
      editFilter : curFilter

    };
  }

  componentDidUpdate(){ 
    
    let newFilter = "";
    if(this.props.filter && this.props.filter.filters)
      newFilter = this.props.filter.filters[ColumnID[this.props.column.id]];

    if(newFilter !== this.state.curFilter){
      this.setState({curFilter:newFilter})
    }
 }

 componentWillUnmount(){
  if(this.toolboxCloser!=null){
    clearTimeout(this.toolboxCloser);
  }
 }

  handleMouseHover() {
    //if(!this.state.isHovering){
   // this.setState({isHovering:true});
   //}
   if(this.toolboxCloser!=null){
     clearTimeout(this.toolboxCloser);
   }
    this.toolboxCloser = setTimeout(()=>{
       // this.setState({isHovering:false});
       this.setState({isToolboxOpen:false});
        },3000)
}
  handleMouseHoverLeave(){
    //this.setState({isHovering:false});
    //this.setState({isToolboxOpen:false});
    if(this.toolboxCloser!=null){
      clearTimeout(this.toolboxCloser);
    }
     this.toolboxCloser = setTimeout(()=>{
        // this.setState({isHovering:false});
        this.setState({isToolboxOpen:false});
         },1500)
  }

  onHeaderClick(event : React.MouseEvent){
    this.setState({isToolboxOpen:true});
  }


  onFilter(){
    if(!this.props.filter){return;}
    let colFilters = this.props.filter.filters;
    let filterToUse = ""
    if(colFilters){
      filterToUse=colFilters[ColumnID[this.props.column.id]];
    }
    this.setState({isEditing:true, editFilter : filterToUse})
  }
  onRemoveFilter(){
    this.setState({isEditing:false, editFilter : "", curFilter:""})
    this.props.command(this.props.column, "filter:") ;
  }
  onSortAsc(){
    this.props.command(this.props.column, "sort:asc");
  }
  onSortDesc(){
    this.props.command(this.props.column, "sort:desc");
  }

  onClickHelp(){
    this.setState({isShowingEditHelp:!this.state.isShowingEditHelp});
  }
  onCloseFilter(){
    this.setState({isEditing:false});
  }

  onFilterKeyPress(event : React.KeyboardEvent){
      if (event.key === "Enter") {
       this.setState({isEditing:false, curFilter:(event.target as HTMLInputElement).value})
        this.props.command(this.props.column, "filter:"+(event.target as HTMLInputElement).value)  ;
        event.preventDefault();
      }
    
  }

  onPerformFilter(event : React.MouseEvent){
    let value = this.state.editFilter;
    this.setState({isEditing:false, curFilter:""+value})
        this.props.command(this.props.column, "filter:"+value)  ;
  }

  onFilterChange(event : React.ChangeEvent<HTMLInputElement>){
    let filterToUse = (event.target as HTMLInputElement).value;
    this.setState({editFilter : filterToUse});
  }

  onCustomFilterChange(value : string)
  {
    this.setState({editFilter : value});
  }
  onCustomFilterPerform(value : string)
  {
    this.setState({isEditing:false, curFilter:""+value})
    this.props.command(this.props.column, "filter:"+value)  ;
  }
 


  render() {
    
      let that = this;
      if(!this.props.column.visible)
       {
        let colClass= this.props.wall+" hidden";
        return <th key={this.props.column.id} className={colClass}></th>
       }
    let colClass= this.props.wall + " "+ColumnID[this.props.column.id];
    let title = ViewColumns.getColumnTooltip(this.props.column);
    let filtertitle = "Filter "+title;
    let editId = "filter"+this.props.column.id;
    let optionsId = "options"+this.props.column.id;
    let autoCompleteList = undefined;
    if(this.state.isEditing){
      let rowmap : {[id:string] : string; } = {}
      let rowlist : string[]=[]
      let colindex = this.props.table.cols[ColumnID[this.props.column.id]]
      if(this.props.table.rows){
      this.props.table.rows.forEach((row)=>{
        rowmap[row[colindex]] = row[colindex]
        });
      }
      for (let key in rowmap) {rowlist.push(key);}
      autoCompleteList = <datalist id={optionsId}>
        {
          rowlist.map((k)=>{ return <option key={k}>{k}</option>;  })
        }

      </datalist>
    }
    
      let tooltip = ViewColumns.getColumnTooltip(this.props.column)+( this.props.column.altered == true || !this.props.filter ? "":", click to filter or sort");
      let customFilter = ViewColumns.getCustomFilter(this.props.column,this.state.editFilter, this.onCustomFilterChange,this.onCustomFilterPerform );
      let filterDisplay = ViewColumns.getCustomFilterMarker(this.props.column,this.state.curFilter)
      if(filterDisplay=="") filterDisplay = this.state.curFilter
    return (
        <th key={this.props.column.id} className={colClass}  title={tooltip}
            >
            
            <div  onMouseEnter={this.handleMouseHover}  onMouseLeave={this.handleMouseHoverLeave} 
                   >
                     {this.props.filter && <button onClick={this.onHeaderClick}>{ViewColumns.getColumnDisplay(this.props.column)}</button>}
                     {!this.props.filter && ViewColumns.getColumnDisplay(this.props.column)}

            {
          this.state.isToolboxOpen && this.props.column.altered != true && 
            <div className="headerTools" onMouseLeave={this.handleMouseHoverLeave}  onMouseMove={this.handleMouseHover}>
              <div>{title}</div>
                <button title={filtertitle} onClick={() => that.onFilter()}><img alt="Filter"  src={Look}></img></button>
                <button title="Order ascending"  onClick={that.onSortAsc}><img alt="Order asc"  src={Up}></img></button>
                <button title="Order descending"  onClick={that.onSortDesc}><img alt="Order desc"  src={Down}></img></button>
            </div>
            }</div>
            {this.state.curFilter&& <div className="currentFilter"><button title={filtertitle} onClick={() => that.onFilter()}>{filterDisplay}</button><button className="removeFilter" title="Remove filter" onClick={()=>that.onRemoveFilter()}>x</button></div>}
            {
              this.state.isEditing &&
              <div className="headerFilterEdit information" title={filtertitle}>
                <div className="toolButton" >
                    <button title="Help" onClick={this.onClickHelp}><img src={HelpImg} alt="?"></img></button>
                    <button title="Close" onClick={this.onCloseFilter}><img  src={CloseImg}  alt="Close"></img></button>
                </div>
                <h3>{filtertitle}</h3>
                {customFilter}
                {!customFilter&&<React.Fragment>
                  <input autoFocus={true}  value={this.state.editFilter}  id={editId} list={optionsId} placeholder="Enter filter text here" onChange={this.onFilterChange} onKeyPress={this.onFilterKeyPress}></input>
                  <button className="executeFilter" onClick={this.onPerformFilter}>Set filter</button>
                  {this.state.isShowingEditHelp&&<div className="editHelpText">
                    <span>Enter filter and press enter to submit</span>
                    <div className="panel information">
                    <table>
                      <tbody>
                      <tr><td>To filter on value</td><td>partOfName</td></tr>
                      <tr><td>To filter on exact value</td><td>=partOfName</td></tr>
                      <tr><td>To filter with wildcard</td><td>part*Name</td></tr>
                      <tr><td>To filter with exclusion</td><td>!20</td></tr>
                      <tr><td>To filter with operator</td><td>%gt;20</td></tr>
                      <tr><td>To filter with 'or'</td><td>20|11</td></tr>
                      <tr><td>To filter with 'and'</td><td>20&amp;11</td></tr>
                      <tr><td></td><td></td></tr>
                      </tbody>
                      </table>
                      </div>
                  </div>
                  }
                  {autoCompleteList}
                  </React.Fragment>}
              </div>
            }
        </th>
    );

  }
}

export default GSpyTableColumnHeader