
import React from 'react';

import { withRouter,RouteComponentProps } from "react-router-dom";
import helpImg from "../gfx/help.png"
import HelpPanel from "./HelpPanel"
import "./SearchBar.css"
import { PageSelectionManager } from "../managers/pageSelectionManager";
import DataView from "../api/views/dataView"
import {Link} from "react-router-dom"

type SearchBarState = {
    isHelpOpen : boolean
    editSearch : string
};
interface SearchBarProps extends 
RouteComponentProps{
  selections : PageSelectionManager 
}


class  SearchBar extends React.Component<SearchBarProps,SearchBarState> {

    searchInput : HTMLInputElement|null = null

  constructor(props : SearchBarProps){
    super(props);
    this.onHelp = this.onHelp.bind(this);
    this.onSearchKeyPress = this.onSearchKeyPress.bind(this);
    this.onSearchChange = this.onSearchChange.bind(this);
    this.state={
        isHelpOpen : false,
        editSearch : ""
    }
  }
  componentDidUpdate(){
    if(this.searchInput!=null){
      this.searchInput.focus(); 
    }
 }
  onHelp(event : React.MouseEvent)
  {
    this.setState({isHelpOpen: !this.state.isHelpOpen});
  }

  onSearchKeyPress(event : React.KeyboardEvent){
      
    if (event.key === "Enter") {
        let text = (event.target as HTMLInputElement).value
        let comp = this.props.selections.selections[DataView[DataView.comp]];
        if(comp)
          {this.props.history.push("/comp/"+comp+"/search/"+encodeURI(text));}
          else
          {this.props.history.push("/search/"+encodeURI(text));}
        this.setState({editSearch:""})
        }  
    }

    onSearchChange(event : React.ChangeEvent<HTMLInputElement>){
    let searchToUse = (event.target as HTMLInputElement).value;
    this.setState({editSearch : searchToUse});
    }

  render () : React.ReactElement {
            return <div className="SearchBar information">
                {this.state.isHelpOpen&&<HelpPanel extraClass="searchBarHelp"  left={true} title="Search bar help">
                        Type anything and press enter to search for the text amongst coaches, teams and competitions<br/>
                        <table>
                          <tbody>
                        <tr><td>To search on value</td><td><i>partOfName</i></td></tr>
                        <tr><td>To search on exact value</td><td><i>=exactName</i></td></tr>
                        <tr><td>To search with wildcard</td><td><i>part*Name</i></td></tr>
                        <tr><td>To search on not matching</td><td><i>!partOfName</i></td></tr>
                        <tr><td>To search with 'or' logic</td><td><i>name1 | name2</i></td></tr>
                        <tr><td>To search with 'and' logic</td><td><i>name1 &amp; name2</i></td></tr>
                        <tr><td colSpan={2}>You can also use the Discord bot commands, like:<br/>
                              <span className="command">!gs comp top dorf</span> or  <span className="command">!gs comp coach mordrek</span><br/>
                              See available commands at <Link className="imageButton" to="/help/bot">Bot help</Link>
                          </td>
                          </tr></tbody></table>
                 
                    </HelpPanel>}
                <input  
                placeholder="Search..."
                    ref={(input) => { this.searchInput = input; }} 
                   onChange={this.onSearchChange} 
                   onKeyPress={this.onSearchKeyPress}
                   value={this.state.editSearch}
                ></input><button onClick={this.onHelp} className="flowButton"><img alt="Help" src={helpImg}></img></button></div>
   
    }

   
}

export default withRouter(SearchBar)