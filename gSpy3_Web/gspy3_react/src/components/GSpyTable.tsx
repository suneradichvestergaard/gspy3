
import React from 'react';
import {ViewColumns, ColumnLayout, ColumnID, Column} from "../api/views/columns"
import './GSpyTable.css';
import GSpyTableColumnHeader from "./GSpyTableColumnHeader"
import CogImg from "../gfx/cog.png"
import LeftImg from "../gfx/left.png" 
import RightImg from "../gfx/right.png"
import SpinnerImg from "../gfx/spinner.gif"
import QueryPageRequests, { QueryPageRequest, QueryPageResponse } from "../api/queryPageRequests"
import HelpPanel from "../components/HelpPanel"
import { withRouter,RouteComponentProps } from "react-router-dom";
import GSpy3_api from '../api/gSpy3_api';

type GSpyTableState = {
  
    layout : ViewColumns,
    isTableSettingsOpen : boolean
  };

interface GSpyTableProps extends 
RouteComponentProps  {
    source : QueryPageResponse 
    layout : ColumnLayout
    filters? : QueryPageRequests
    groupInfo? : {[key:string] : React.ReactElement }
    marked? : {idcolumn:ColumnID,value:string} | undefined
    
};

export interface GSpyTableCommandReceiver{
  onCommand(col: Column, cmd : string) : void
}

class  GSpyTable extends React.Component<GSpyTableProps,GSpyTableState>  implements GSpyTableCommandReceiver {

  constructor(props : GSpyTableProps){
    super(props);
    this.onCommand = this.onCommand.bind(this);
    this.onPrevious = this.onPrevious.bind(this);
    this.onNext = this.onNext.bind(this);
    this.onSettings = this.onSettings.bind(this);

    
    
    this.state = {
       layout : ViewColumns.getLayout(props.layout),
       isTableSettingsOpen : false
    }

  }
  onPrevious(){
    if(!this.props.filters) return;
    var thisId = this.props.source.request.id ? this.props.source.request.id:"";
    let thisFilter = this.props.filters.getFilter(thisId);
    if(!thisFilter.limit){ thisFilter.limit = this.props.source.result?.limit;}
    if(!thisFilter.from){
      thisFilter.from=0;
    }
    else{
      thisFilter.from -= thisFilter.limit?thisFilter.limit:50;
    }
      if(thisFilter.from< 0) thisFilter.from = 0;
      this.props.history.push(this.props.filters.getFilterURL());
  }
  onNext(){
    if(!this.props.filters) return;
    var thisId = this.props.source.request.id ? this.props.source.request.id:"";
    let thisFilter = this.props.filters.getFilter(thisId);
    if(!thisFilter.limit){ thisFilter.limit = this.props.source.result?.limit;}
    if(!thisFilter.from){
      thisFilter.from=thisFilter.limit; 
    }
    else{
      thisFilter.from += thisFilter.limit?thisFilter.limit:50;
    }
    this.props.history.push(this.props.filters.getFilterURL());
  }

  onSettings(){
    this.setState({isTableSettingsOpen:!this.state.isTableSettingsOpen})

  }

  onCommand(col: Column, cmd : string){
    if(!this.props.filters) return;
    var thisId = this.props.source.request.id ? this.props.source.request.id:"";
    
    if(cmd.startsWith("sort:")){ 
      let thisFilter = this.props.filters.getFilter(thisId);
      thisFilter.ordercol = ColumnID[col.id];
      thisFilter.order= cmd.substr("sort:".length);
      this.props.history.push(this.props.filters.getFilterURL());
    }
    if(cmd.startsWith("filter:")){ 
      let thisFilter = this.props.filters.getFilter(thisId);
      let thisValue = cmd.substr("filter:".length);
      if(thisValue){
        if(!thisFilter.filters){
          thisFilter.filters = {}
        }
        thisFilter.filters[ColumnID[col.id]] = thisValue;
      } else if(thisFilter.filters) {
        delete thisFilter.filters[ColumnID[col.id]];
      }
      this.props.history.push(this.props.filters.getFilterURL());
    }
    
  }

  renderTopHeader() : React.ReactElement{
    
    if(! this.props.source){
      return <thead><tr><td><img className="iconSmall" alt="Wait" src={SpinnerImg}></img></td></tr></thead>
    }
      let lastIndx = this.state.layout.groups.length-1;
      let that= this;
      let canGoLeft = this.props.source.result?.page > 1;
      let canGoRight = this.props.source.result?.rows && this.props.source.result?.rows.length === this.props.source.result?.limit;
      let canGoAnywhere = canGoRight || canGoLeft;
    return  <thead><tr>{this.state.layout.groups.map(function(group,gindex){
            let colSpan = 0;
            for(let i =0; i < group.cols.length;i++)
            {
                if(group.cols[i].visible){
                    colSpan++;
                }
            }
             if(colSpan > 0)   
             {
               let api = new GSpy3_api();
               let arr = new Array<QueryPageRequest>();
               let req = that.props.source.request;
               arr.push(req);
               let apiurl = api.getBase()+api.getQueriesURL(arr)
               let apiurlcsv = api.getBase()+api.getQueriesFormatURL("csv",arr)
               let apiurlscsv = api.getBase()+api.getQueriesFormatURL("scsv",arr)
               let downloadnamecsv = ""+req.id+".csv"
               let downloadnamejson = ""+req.id+".json"
               return <td className="tableToolContainer" key={group.name} colSpan={colSpan}> {(gindex === lastIndx)&&
              <div>
                {that.state.isTableSettingsOpen&& 
                  <HelpPanel left={true} title="Table menu" onClose={()=>{that.setState({isTableSettingsOpen:false})}}>
                    <div className="menu padded">
                    <a target="_blank" href={apiurl} download={downloadnamejson} onClick={that.onSettings}>Table data in JSON format</a><br/>
                    <a target="_blank" href={apiurlcsv}  download={downloadnamecsv} onClick={that.onSettings}>Table data in CSV (,) format</a><br/>
                    <a target="_blank" href={apiurlscsv}  download={downloadnamecsv} onClick={that.onSettings}>Table data in CSV (;) format</a><br/>
                        </div>
                        
                  </HelpPanel>}
                <div className="tableTools">
                    {that.props.source && that.props.source.loading && <img className="iconSmall" alt="Wait" src={SpinnerImg}></img>}
                    {canGoLeft&&
                    <button title="Previous results" onClick={that.onPrevious} className="tableSettingsButton">
                        <img alt="Previous" src={LeftImg}></img>
                    </button>}
                    {canGoAnywhere&&<span>{that.props.source.result?.page}</span>}
                    {canGoRight&&
                    <button title="Next results" onClick={that.onNext} className="tableSettingsButton">
                        <img alt="Next" src={RightImg}></img>
                    </button>
                    }
                    <button title="Table settings and information"  onClick={that.onSettings} className="tableSettingsButton">
                        <img alt="Settings" src={CogImg}></img>
                    </button>
                </div>
                </div>
             }
             {group.name}{that.props.groupInfo?that.props.groupInfo[group.name]:<span></span>}</td>;
             }
            return undefined;
          })
    }</tr></thead>
  }

  renderBodyHeader(): React.ReactElement{
      let that = this;
    return  <thead><tr key="root">{
        this.state.layout.groups.map(function(group){
            let firstCol = -1; let lastCol = -1;
            for(let i =0; i < group.cols.length;i++)
            {
                let col = group.cols[i];
                if(col.visible){
                    if(firstCol===-1) firstCol = i;
                    lastCol = i;
                }
            }

            if(!that.props.source){
              return <td key={group.name}></td>
            }
            var thisId = that.props.source.request.id ? that.props.source.request.id:"";
            let thisFilter = that.props.filters?.getFilter(thisId);

            return group.cols.map(function(col,icol){
                let colClass = (icol===firstCol) ? "leftCell" : (icol===lastCol?"rightCell":"");
                colClass+=" "+(ColumnID[col.id]);
                return <GSpyTableColumnHeader filter={thisFilter} key={icol} command={that.onCommand} wall={colClass} column={col} table={that.props.source.result}></GSpyTableColumnHeader>;
                
            })
          })
    }</tr></thead>
  }
  renderBody(): React.ReactElement{
    let that = this
    return  <tbody>
        { 
        this.props.source?.result?.rows &&
        this.props.source?.result?.rows.map(function(row,irow){
            let rowkey = "row_"+irow 
            let rowclass = ViewColumns.getRowClass(that.props.layout,that.state.layout, that.props.source.result?.cols, row);
            if(that.props.marked){
              if(row[that.props.source?.result?.cols[ColumnID[that.props.marked.idcolumn]]] == that.props.marked.value){
                rowclass += " selected";
              }
            }
            return <tr className={rowclass} key={rowkey}>{that.state.layout.groups.map(function(group){
                let firstCol = -1; let lastCol = -1;
                for(let i =0; i < group.cols.length;i++)
                {
                    let col = group.cols[i];
                    if(col.visible){
                        if(firstCol===-1) firstCol = i;
                        lastCol = i;
                    }
                }
                return group.cols.map(function(col, icol){
                    let colkey = "col_"+irow+"_"+icol
                    let colClass = (icol===firstCol) ? "leftCell" : (icol===lastCol?"rightCell":"");
                    colClass+=" "+(ColumnID[col.id])
                    if(col.visible){
                            let colindex = that.props.source.result?.cols[ColumnID[col.id]];
                            return <td key={colkey}  className={colClass}>{ViewColumns.getCellDisplay(that,that.state.layout, that.props.source.result?.cols, row, col, colindex!==undefined?row[colindex]:"")}</td>;
                        
                    }else{
                        colClass+= " hidden";
                        return <td key={colkey}  className={colClass}></td>
                    }
                
                })
            })}</tr>
        })
    }</tbody>
  }
  render () : React.ReactElement {

    let classNames = "GSpyTable "+ColumnLayout[this.props.layout]+((!this.props.source || !this.props.source || !this.props.source.result?.rows?.length)?(" "+ColumnLayout[this.props.layout]+"-empty"):"");
    if(this.props.source?.loading){
      classNames+=" loadingblur";
    }
  return <div>
          <table className={classNames}>{this.renderTopHeader()}{this.renderBodyHeader()}{this.renderBody()}</table></div>
  }
} 

export default withRouter(GSpyTable)