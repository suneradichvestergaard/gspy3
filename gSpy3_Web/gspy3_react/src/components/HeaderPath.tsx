
import React  from 'react';
import SelectionState from  "../managers/pageSelectionManager"
import {ColumnID} from "../api/views/columns"
import QueryResult from "../api/views/queryResult"
import './HeaderPath.css';
import {Link} from "react-router-dom"
import DataView from "../api/views/dataView"
import pc from "../gfx/pc.png"
import xb1 from "../gfx/xb1.png"
import ps4 from "../gfx/ps4.png"
import spinner from "../gfx/spinner.gif"
import { stringify } from 'querystring';
type HeaderPathState = {

};
type HeaderPathProps = {
    selections : SelectionState
    location : string
};

class  HeaderPath extends React.Component<HeaderPathProps,HeaderPathState> {
/*
  constructor(props : HeaderPathProps){
    super(props);
  }*/


componentDidMount(){
 
}

  render () : React.ReactElement {
    
    let path : Record<string,string>[] = [];
    let loc = undefined;
    if(!loc){
      loc = window.location.pathname;
    }
    let league = this.props.selections.responses.response[DataView[DataView.league]]
    let comp = this.props.selections.responses.response[DataView[DataView.comp]]
    let team = this.props.selections.responses.response[DataView[DataView.team]]
    let match = this.props.selections.responses.response[DataView[DataView.match]]
    let coach = this.props.selections.responses.response[DataView[DataView.coach]]
    let coll = this.props.selections.responses.response[DataView[DataView.collection]]
    //let player = this.props.selections.responses.response[DataView[DataView.player]]
    if(loc)
    {
      if(loc === ("/gspy/gspy") || loc==("/gspy/") || loc.startsWith("/gspy/user/"))
      {
        path.push({key:"GoblinSpy", value:"/"});
        if(loc.startsWith("/gspy/user/settings")){
          path.push({key:"User settings", value:"/user/setting"});
        }
      }else{


        if(loc.startsWith("/gspy/coach/") && coach && coach.result && coach.result.success)
        { 
          
          path.push({key:"Global Coach "+QueryResult.get(coach.result,0,ColumnID[ColumnID.coach_name]), 
            value: "/coach/"+QueryResult.get(coach.result,0,ColumnID[ColumnID.idcoach])
            });
        }
        if(loc.startsWith("/gspy/league/") && league && league.result && league.result.success)
        {
          path.push({key:QueryResult.get(league.result,0,ColumnID[ColumnID.league_name]), value: "/league/"+QueryResult.get(league.result,0,ColumnID[ColumnID.idleague])});
        }
        if(coll && coll.result && coll.result.rows.length > 0 && loc.startsWith("/gspy/collection/"))
          {
             
            let idcoll = coll.result.rows[0][coll.result.cols[ColumnID[ColumnID.idcollection]]];
            let collname = coll.result.rows[0][coll.result.cols[ColumnID[ColumnID.collection_name]]];
            path.push({key:collname, value: "/collection/"+idcoll});
            if(loc.indexOf("/teams") > 0)
            {
              path.push({key:"Teams", value: "/collection/"+idcoll+"/teams"});
            }
          }
        if(loc.startsWith("/gspy/comp/") && comp && comp.result && comp.result.success)
        {
          if(comp.result?.rows?.length == 0) return <span>Unknown competition</span>;
          let idcomp="";
          let idmap = comp?.request?.idmap;
          let namefilter = "";
          if(idmap) idcomp = idmap["idcompetition"]
          if(comp?.request?.filters) namefilter = comp?.request?.filters["competition_name"]
          let compName = "";
          console.log(comp.result);
          if(comp.result.rows.length <= 3 || !namefilter){
            for(let i=0; i < comp.result.rows.length;i++){
              
              if (i!=0) compName+=", ";
              compName += QueryResult.get(comp.result,i,ColumnID[ColumnID.competition_name]);
              if(compName.startsWith("@")){ compName = compName.substr(1);
                break;
              }
            }
          } else if(namefilter){
            compName = namefilter;
          }
          path.push({key:QueryResult.get(comp.result,0,ColumnID[ColumnID.league_name]), value: "/league/"+QueryResult.get(comp.result,0,ColumnID[ColumnID.idleague])});
          path.push({key:compName, value: "/comp/"+idcomp});
          if(loc.indexOf("/coach/") > 0 && coach && coach.result && coach.result.success)
          { 
            
            path.push({key:"Coach "+QueryResult.get(coach.result,0,ColumnID[ColumnID.coach_name]), 
              value: "/comp/"+QueryResult.get(comp.result,0,ColumnID[ColumnID.idcompetition])
                    +"/coach/"+QueryResult.get(coach.result,0,ColumnID[ColumnID.idcoach])
              });
          }
          if(loc.indexOf("/team/") > 0 && team &&  team.result && team.result.success && comp && comp.result.success)
          { 
            
            path.push({key:QueryResult.get(team.result,0,ColumnID[ColumnID.team_name]), 
              value: "/comp/"+QueryResult.get(comp.result,0,ColumnID[ColumnID.idcompetition])
                    +"/team/"+QueryResult.get(team.result,0,ColumnID[ColumnID.idteam])
              });
              
          }
          else
            if(loc.indexOf("/match/") > 0 && match &&  match.result && match.result.success && comp && comp.result.success)
            { 
              
            path.push({key:"Match "+QueryResult.get(match.result,0,ColumnID[ColumnID.idmatch]), 
              value: "/comp/"+QueryResult.get(comp.result,0,ColumnID[ColumnID.idcompetition])
                    +"/match/"+QueryResult.get(match.result,0,ColumnID[ColumnID.idmatch])
              });
             
            
           
          }
          else if(loc.indexOf("/stats") > 0 || loc.indexOf("/players") >= 0 || loc.indexOf("/coaches")>0)
            {
              path.push({key:"Stats ", 
              value: "/comp/"+QueryResult.get(comp.result,0,ColumnID[ColumnID.idcompetition])+
                      "/stats"});

            }
            
          
        }
        if(loc.indexOf("/user/settings") > 0)
        { 
          path.push({key:"User settings", 
            value: "/user/settings"
            });
        }

        if(loc.startsWith("/gspy/match/") && match &&  match && match.result.success)
        { 
          
          path.push({key:"Match "+QueryResult.get(match.result,0,ColumnID[ColumnID.idmatch]), 
            value: "/match/"+QueryResult.get(match.result,0,ColumnID[ColumnID.idmatch])
            });
        }
        if(loc.startsWith("/gspy/search/"))
        { 
          
          path.push({key:"Search '"+decodeURI(loc.substr(loc.indexOf("/search")+"/search/".length))+"'", 
            value: "/search/"+decodeURI(loc.substr(loc.indexOf("/search")+"/search/".length))
            });
        }
        if(loc.startsWith("/gspy/leagues")){
        path.push({key:"Leagues", value:"/leagues"});
        }

      switch(loc){
          
        case "/gspy/status":  
          path.push({key:"Server status", value:"/status"});
          break;
        case "/gspy/help/bot":  
        case "/gspy/help":  
          path.push({key:"Help", value:"/help"});
          break;
  }
        }
    }
    else console.log("WHAT!?");
    const items = path.map((item : Record<string,string>, index : number) =>{
        let isLast = index === (path.length-1);
        


        if(isLast)
          {
            return  <div key={index} ><Link to={item.value}>{item.key}</Link></div>;
          }

        return  <div key={index} ><Link to={item.value}>{item.key}</Link><div className="separator"></div></div>;
        });

        let idorigin = undefined;
        if(comp?.result?.rows && comp?.result?.rows[0] && comp?.result?.cols && comp?.result?.cols[ColumnID[ColumnID.idorigin]]) idorigin=comp?.result?.rows[0][comp?.result?.cols[ColumnID[ColumnID.idorigin]]]
        if(!idorigin){
          let row = league?.result?.rows[0]
          if(row && league?.result?.cols){
          idorigin = row[league?.result?.cols[ColumnID[ColumnID.idorigin]]]
          }
        }
        if (loc.indexOf("/league/") < 0 && loc.indexOf("/comp/") < 0){
          idorigin = "";
        }
        let idoriginpc = idorigin&&idorigin==="1"
        let idoriginxb1 = idorigin&&idorigin==="2"
        let idoriginps4 = idorigin&&idorigin==="3"
        document.title = "GoblinSpy - "+loc; 
        return <div  className="HeaderPath">
            <div className="pathItems">
                {idorigin&&idoriginpc&&<img alt="PC" className="iconSmall" title="PC" src={pc}></img>}
                {idorigin&&idoriginxb1&&<img alt="XB1" className="iconSmall" title="XB1" src={xb1}></img>}
                {idorigin&&idoriginps4&&<img alt="PS4" className="iconSmall" title="PS4" src={ps4}></img>}
              {(comp && comp.loading || league && league.loading || match && match.loading || team&&team.loading||coach&&coach.loading) &&
              <img src={spinner} className="iconSmall"></img>}
              {items}
            </div>
            </div>
  }
}

/* previously
 return <div  className="HeaderPath">
            <div className="menuHeader break">
                {idorigin&&idoriginpc&&<img alt="PC" className="iconSmall" title="PC" src={pc}></img>}
                {idorigin&&idoriginxb1&&<img alt="XB1" className="iconSmall" title="XB1" src={xb1}></img>}
                {idorigin&&idoriginps4&&<img alt="PS4" className="iconSmall" title="PS4" src={ps4}></img>}
                Location
            </div>
            <div className="pathItems">
              {items}
            </div>
            </div>
*/

export default HeaderPath