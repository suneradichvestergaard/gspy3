
import React  from 'react';
import GSpyChart from "./GSpyChart"
import {ColumnLayout} from "../api/views/columns"
import { QueryPageResponse} from "../api/queryPageRequests"
import DataView from "../api/views/dataView"
import BaseComponent,{BaseComponentProps, BaseComponentState} from "./BaseComponent"


interface ActivityState extends BaseComponentState  {
    compactivitydates : QueryPageResponse,
    compactivityraces : QueryPageResponse
    compactivitydays : QueryPageResponse
    compactivitytime : QueryPageResponse

  };
interface ActivityProps extends BaseComponentProps  {

};


class  Activity extends BaseComponent<ActivityProps,ActivityState> {

 
    
  constructor(props : ActivityProps){ 
    super(props);
    this.views = [DataView.comp, DataView.compActivityDates, DataView.compActivityRaces, DataView.compActivityDays, DataView.compActivityTime];
    this.onUpdate = this.onUpdate.bind(this);
  }

  onUpdate(){
    super.onUpdate();

    this.setState(
      {
        compactivitydates:this.props.selections.responses.response[DataView[DataView.compActivityDates]],
        compactivityraces:this.props.selections.responses.response[DataView[DataView.compActivityRaces]],
        compactivitydays:this.props.selections.responses.response[DataView[DataView.compActivityDays]],
        compactivitytime:this.props.selections.responses.response[DataView[DataView.compActivityTime]],
      });
  }

  
  

  render () : React.ReactElement {
    if(!this.state) return <div></div>;
    return <div className="flexcontainer">     
        <div className="panel" style={{width:"500pt"}}>
          <GSpyChart source={this.state.compactivitydates}  filters={this.props.selections.filters} layout={ColumnLayout.ActivityDateView}></GSpyChart>
        </div> 
        <div className="panel" style={{width:"500pt"}}>
          <GSpyChart source={this.state.compactivitydays}  filters={this.props.selections.filters} layout={ColumnLayout.ActivityDayView}></GSpyChart>
        </div> 
        <div className="panel" style={{width:"500pt"}}>
          <GSpyChart source={this.state.compactivitytime}  filters={this.props.selections.filters} layout={ColumnLayout.ActivityTimeView}></GSpyChart>
        </div> 
        <div className="panel" style={{width:"500pt"}}>
          <GSpyChart source={this.state.compactivityraces}  filters={this.props.selections.filters} layout={ColumnLayout.ActivityRacesView}></GSpyChart>
        </div>
      </div>
  }
}

export default Activity