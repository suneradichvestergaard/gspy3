
import React from 'react';
import {ViewColumns, ColumnLayout, ColumnID, Column} from "../api/views/columns"
import './GSpyChart.css';
 

import QueryPageRequests, { QueryPageResponse } from "../api/queryPageRequests"
import { withRouter,RouteComponentProps } from "react-router-dom";
import "./GSpyTable.css"
import {Bar} from "react-chartjs-2"
import Races from '../api/dataformat/apiRaces';
import { renderToString } from 'react-dom/server'
import {GSpyTableCommandReceiver} from "./GSpyTable"

type GSpyChartState = { 
  
    layout : ViewColumns,
    isTableSettingsOpen : boolean
  };
interface GSpyChartProps extends 
RouteComponentProps  {
    source : QueryPageResponse 
    layout : ColumnLayout
    filters : QueryPageRequests
    labels? : string[]
    title? : string
    
};

class  GSpyChart extends React.Component<GSpyChartProps,GSpyChartState> implements GSpyTableCommandReceiver {

  constructor(props : GSpyChartProps){
    super(props);

    
    this.state = {
       layout : ViewColumns.getLayout(props.layout),
       isTableSettingsOpen : false
    }

  }
  onCommand(col: Column, cmd : string){
  }

  render () : React.ReactElement {

    if(!this.props?.source?.result?.rows){
        return <div></div>
    }
    if(this.props.source.result?.rows.length <= 0){
        return <div></div>
    }

    


  let colors=[
    "rgba(100,100,160,1)",
    "rgba(100,160,100,1)",
    "rgba(100,160,100,1)",
    "rgba(100,160,160,1)",
    "rgba(160,100,100,1)",
    "rgba(160,100,160,1)",
    "rgba(160,160,100,1)",
    "rgba(130,130,130,1)",
  ];
 // Collect column keys
  let layout = ViewColumns.getLayout(this.props.layout);
  let datasetlabels :string[] = []
  let xKey = "";
  let xKeyCol :Column=new Column();
  let mainTitle = "";
  let yKeys:string[][] = [];
  for(let i=0; i<  layout.groups.length;i++){
      let group = layout.groups[i];
      mainTitle = group.name;
      for(let j=0; j<  group.cols.length;j++){
          let col = group.cols[j];
          if(col.selected === true){
              xKey = ""+this.props.source.result?.cols[ColumnID[col.id]];
              xKeyCol = col;
          } else if(col.visible){
              yKeys.push([ViewColumns.getColumnDisplayString(col),""+this.props.source.result?.cols[ColumnID[col.id]]]);
              datasetlabels.push(ViewColumns.getColumnDisplayString(col));
          }
      }
  }
  if(this.props.title){mainTitle = this.props.title;}
console.log(this.props.source);
  // Collect data
  let datasets = [];
  let labels=[];
  for(let j=0; j < yKeys.length;j++){
      let yKey = yKeys[j];
      let datapoints : number[] = []
      for(let i=0; i < this.props.source.result?.rows.length;i++){
              let row = this.props.source.result?.rows[i];
              datapoints.push(+row[+yKey[1]])    
              if(j===0){
                  switch(xKeyCol.id)
                  {
                      case ColumnID.idrace:
                          labels.push(Races.GetName(+row[+xKey]));
                          break;
                      default:
                        let content = ViewColumns.getCellDisplay(this,ViewColumns.getLayout(this.props.layout),this.props.source.result?.cols,row,xKeyCol,row[+xKey]);
                        let htmlcontent = renderToString(content);
                        let dv = document.createElement("div");
                        dv.innerHTML = htmlcontent;
                        labels.push(dv.innerText);
                        break;
                  }
                }
                  
              }
          
     
      datasets.push(
            { 
            label: this.props.labels? this.props.labels[j] : datasetlabels[datasets.length],
            data : datapoints,
            backgroundColor: colors[datasets.length%7]
            });
  }

  let data={
      labels : labels,
      datasets: datasets
  }

  let options={
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      yAxes: [{
          ticks: {
              beginAtZero: true
          }
      }]
  }
      }
   

      return<div  className="GSpyChart">
      <table className="GSpyTable">
          <thead>
              <tr><td>{mainTitle}</td></tr>
          </thead>
          <tbody>
              <tr>
                  <td className="bothCell" >
                      <div className="chartContainer">
                        <Bar options={options} data={data}>
                        </Bar>
                    </div>
                  </td>
              </tr>
          </tbody>

     </table></div>
       
  }
}

export default withRouter(GSpyChart)