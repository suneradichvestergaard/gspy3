
import React from 'react';
import "./Header.css"
import SubMenus from "./SubMenus"
import SearchBar from "./SearchBar"
import UserNotSignedIn from "../pages/user/UserNotSignedIn"
import UserHeader from "./UserHeader"
import goblin_icon from "../gfx/goblin_icon.png"
import ok_icon from "../gfx/status-ok.png"
import warning_icon from "../gfx/status-work.png"
import error_icon from "../gfx/status-error.png"
import  {PageSelectionManager} from "../managers/pageSelectionManager"
import { Userinfoplus } from '../auth/UserLoginData';
import HeaderPath from "./HeaderPath"
import Login from "../auth/Login"
import spinner from "../gfx/spinner.gif"
import cog from "../gfx/cog.png"

import {
    Switch,
    Route, 
    Link
  } from "react-router-dom";
import ServerStatus from '../api/views/statusCollection';
type HeaderState = {
  isLoading : Boolean 
};
type HeaderProps = {
    location : string,
    pageSelectionManager : PageSelectionManager ,
    userInfo : Userinfoplus |undefined
    status : ServerStatus
};

class  Header extends React.Component<HeaderProps,HeaderState> {


  constructor(props : HeaderProps){
    super(props);

    props.pageSelectionManager.registerLoadingCallback("header",(b)=>{this.onLoadingStatus(this,b)});
    this.state={isLoading:false}
  }

  onLoadingStatus (that : Header,load : boolean )
  {
   setTimeout(() => {
     console.log("LOADING: "+load);
      that.setState({isLoading : load})
  }, 10);
  }


  render () : React.ReactElement {

        return <div className="header">  
        {this.state.isLoading && <div className="mainLogoSpinner" ><img src={spinner}></img></div>}
        <div className="mainLogo"><Link title="GoblinSpy3" to="/"><img alt="GSpy" src={goblin_icon}></img></Link></div>
        <div className="mainLogoStatus"><Link title="Status" to="/status">
        { this.props.status.modules?.length>0 &&  <img alt="OK" src={ok_icon}></img>}
        { this.props.status.modules?.length==0 &&  <img alt="Error" src={error_icon}></img>}
            </Link></div>
        <div className="competitionArea">
          <HeaderPath location={this.props.location} selections={this.props.pageSelectionManager}></HeaderPath>
          <SubMenus  path={this.props.location}></SubMenus>
         </div>
        
        <div className="emptyMenuSection"></div>
        <div className="menuCategory">
            <div className="menu">
                <Link to="/" className={this.props.location===""||this.props.location.startsWith("/?")?"selected":""}>Home</Link>
                <Link to="/leagues" className={this.props.location.startsWith("/leagues")?"selected":""}>Leagues</Link>
                <Link to="/links" className={this.props.location.startsWith("/links")?"selected":""}>Links</Link>
                <Link to="/help" className={this.props.location.startsWith("/help")?"selected":""}>Help</Link>
                { /* <Link to="/user/settings" className={"imageButton "+(this.props.location.startsWith("/user/settings")?"selected":"")} ><img src={cog} className="iconSmall"></img></Link>*/}
                <SearchBar selections={this.props.pageSelectionManager}></SearchBar> 
            </div>
        </div>
        { /*
        <div  className="userSection">
            <Switch>
              <Route path="/login">
                <Login></Login>
              </Route>
              <Route path="/">
                <UserNotSignedIn userInfo={this.props.userInfo}></UserNotSignedIn>
                <UserHeader userInfo={this.props.userInfo}></UserHeader>
              </Route>
            </Switch>
          </div>
        */}
          
        </div>
   
  }
}

export default Header