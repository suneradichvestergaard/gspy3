
import React from 'react';
import './HelpPanel.css';
import CloseImg from "../gfx/close.png";

type HelpPanelState = {
    isOpen : boolean
};


type HelpPanelProps = {
  title : string
  left : boolean
  onClose? : ()=>void
  extraClass?: string 

};

class  HelpPanel extends React.Component<HelpPanelProps,HelpPanelState> {


  constructor(props : HelpPanelProps){
    super(props);
    this.onClose = this.onClose.bind(this);
    this.state={isOpen:true};
  
  }

  onClose(){
    this.setState({isOpen:false});
    if(this.props.onClose){this.props.onClose();}
  }

  render () : React.ReactElement {

    if(!this.state.isOpen) return <div></div>;
    let classNameText = "floater bordered panel"
    if (this.props.left ) classNameText += " lefter";
    else classNameText += " righter";
    if(this.props.extraClass) classNameText += " "+this.props.extraClass;

   return <div className="HelpPanel"><div className={classNameText}>
                <button  className="toolButton" title="Close" onClick={this.onClose}><img  src={CloseImg}  alt="Close"></img></button>
            <div className="content">
              <h3>{this.props.title}</h3>
              {this.props.children}
            </div>
       </div></div>
   
  }
}

export default HelpPanel