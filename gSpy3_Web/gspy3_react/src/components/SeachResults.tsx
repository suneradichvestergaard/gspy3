
import React  from 'react';
import GSpyTable from "./GSpyTable"
import {ColumnLayout} from "../api/views/columns"
import { QueryPageResponse} from "../api/queryPageRequests"
import DataView from "../api/views/dataView"
import BaseComponent,{BaseComponentProps, BaseComponentState} from "./BaseComponent"
import {Link} from "react-router-dom"
import Races from "../api/dataformat/apiRaces"
import { stringify } from 'querystring';
import Rank from '../managers/rank';
import { getSupportedCodeFixes } from 'typescript';

interface SearchResultsState extends BaseComponentState  {
    searchCoach : QueryPageResponse
    searchTeam : QueryPageResponse
    searchCompetition : QueryPageResponse
    botErrorMessage : string
    outputMessage : string

  };
interface SearchResultsProps extends BaseComponentProps  {

};

class  SearchResults extends BaseComponent<SearchResultsProps,SearchResultsState> {

    
  constructor(props : SearchResultsProps){ 
    super(props);
    this.views = [DataView.searchCoach,DataView.searchTeam, DataView.searchCompetition];
    this.onUpdate = this.onUpdate.bind(this);
  }

  getData(){
    let search = this.props.selections.selections[DataView[DataView.search]];
    if(!search || !search.startsWith("!gs"))
    {
      super.getData();
    }
    else{
      this.onUpdate();
    }
  }

  onUpdate(){
    super.onUpdate();

    let search = this.props.selections.selections[DataView[DataView.search]];
    if(!search) return;
    if(search.startsWith("!"))
    {
      
      let nextSpace = search.indexOf(" ");
      let msg = search.substr(nextSpace).trim();
      if(msg.startsWith("rank")){
        msg = msg.substr(msg.indexOf(" ")+1);
        let splits = msg.split(" ");
        let rnk = Rank.calculateRank(+splits[0],+splits[1],+splits[2],0);
        this.setState({outputMessage:"Rank for "+splits[0]+" "+splits[1]+" "+splits[2]+" is " +rnk.toFixed(3)})
        return;
      }
      else if(msg.startsWith("ch")) // Channel commands
      {
        this.setState({outputMessage:"Channel commands are only available when using the discord bot in a channel"})
        return;
      }
      else if(msg.startsWith("coa")) // coach command
      {
        msg = msg.substr(msg.indexOf(" ")+1);
        this.props.history.push("/coach/"+msg)
      }
      else if(msg.startsWith("com")) // comp command
      {
        msg = msg.substr(msg.indexOf(" ")+1);
   
        
          
          let compName = this.props.selections.selections[DataView[DataView.comp]]
          if(!compName){
            this.setState({botErrorMessage:"Competition commands only work when a competition is selected"})
            return;
          }
          if (msg.startsWith("top")==false && msg.startsWith("coach") == false&& msg.startsWith("team") == false&& msg.startsWith("sched") == false && msg.startsWith("result") == false && msg.startsWith("overtake ") ==false)
          {
              let indexofTop = msg.indexOf(" top");
              let indexofCoach = msg.indexOf(" coach");
              let indexofSched = msg.indexOf(" sched");
              let indexofRes = msg.indexOf(" result");
              let index = indexofCoach > indexofTop ? indexofCoach : indexofTop;
              if(indexofSched > index) index= indexofSched;
              if(indexofRes > index) index = indexofRes;
              compName = msg.substring(0, index).trim();
              msg = msg.substring(index).trim();
          }

          if (msg.startsWith("top"))
          {
            
            if(msg.indexOf(" ") > 0) // race
            {
              let race = msg.substr(msg.indexOf(" ")+1)
              let idrace = Races.Parse(race)
              if(!idrace) { this.setState({botErrorMessage:"Unable to decipher the race '"+race+"'"})}
              else{
                this.props.history.push("/comp/"+compName+"?filter={\"compStandings\":{\"filters\":{\"idrace\":\"="+idrace+"\"}}}")
              }

            }else { this.props.history.push("/comp/"+compName);}
          }
          else if(msg.startsWith("coach"))
          {
            if(msg.indexOf(" ") > 0) // coach
            {
              let coach = msg.substr(msg.indexOf(" ")+1)
              this.props.history.push("/comp/"+compName+"/coach/"+coach)
            }
            else{ this.setState({botErrorMessage:"Expected coach name after coach"})}
          }
          else if(msg.startsWith("team"))
          {
            if(msg.indexOf(" ") > 0) // team
            {
              let team = msg.substr(msg.indexOf(" ")+1)
              this.props.history.push("/comp/"+compName+"/team/"+team+"/standing")
            }
            else{ this.setState({botErrorMessage:"Expected coach name after coach"})}
          }
          else if (msg.startsWith("overtake"))
          {
            msg = msg.substr("overtake".length+1);
            let msgSplit = msg.split(" ");
            this.props.history.push("/comp/"+compName+"/overtake/"+msgSplit[0]+"/"+msgSplit[2])
          }
          else if(msg.startsWith("sched")){
            msg = msg.substr("sched".length+1).trim();
            if(msg.startsWith("next")){
              this.props.history.push("/comp/"+compName+"/schedule")
            }

          }
          else if (msg.startsWith("result"))
          {
            this.props.history.push("/comp/"+compName+"/results")
          }

        } else
        { this.setState({botErrorMessage:"Unknown command. Expected !gs comp top, !gs comp coach or similar"})}
          
      

    }
    else{
    this.setState(
      {
        searchCoach:this.props.selections.responses.response[DataView[DataView.searchCoach]],
        searchTeam:this.props.selections.responses.response[DataView[DataView.searchTeam]],
        searchCompetition:this.props.selections.responses.response[DataView[DataView.searchCompetition]],

      });
    }
  }

  render () : React.ReactElement {
    if(!this.state) return <div></div>;
    let numCoaches = this.state.searchCoach?.result?.rows.length;
    let numTeams = this.state.searchTeam?.result?.rows.length;
    let numComps = this.state.searchCompetition?.result?.rows.length;
    return  <div className="flexcontainer">
      {this.state.outputMessage && <div>{this.state.outputMessage}</div>}
      {this.state.botErrorMessage && <div>{this.state.botErrorMessage} <br></br>Check out the documentation over at: <Link to="/help/bot" >Discord bot help</Link></div>}
      {numComps==0&&numCoaches==0&&numTeams==0&&<div>...</div>}
        {numCoaches > 0 &&  <div className="panel">
            <GSpyTable source={this.state.searchCoach} layout={ColumnLayout.CompCoachView} filters={this.props.selections.filters}></GSpyTable>
            </div>}
            {numTeams > 0 &&  <div className="panel">
            <GSpyTable source={this.state.searchTeam} layout={ColumnLayout.SearchTeamsView} filters={this.props.selections.filters}></GSpyTable>
            </div>}
            {numComps > 0 &&  <div className="panel">
            <GSpyTable source={this.state.searchCompetition} layout={ColumnLayout.CompetitionsView} filters={this.props.selections.filters}></GSpyTable>
            </div>}
          </div>
      
  }
}

export default SearchResults