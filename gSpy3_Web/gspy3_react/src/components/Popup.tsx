
import React, { CSSProperties } from 'react';
import { PopupData ,PopupTypeEnum} from '../managers/messageManager';
import "./Popup.css"
import yesImg from "../gfx/yes.png"
import errorImg from "../gfx/error.png"

const hiddenStyle : CSSProperties= {
    display: "none"
  };
  

type PopupState = {

};
type PopupProps = {
  data : PopupData
};

class  Popup extends React.Component<PopupProps,PopupState> {

  timer : NodeJS.Timeout|undefined;
/*
  constructor(props : PopupProps){
    super(props);

  
  }*/

  

  render () : React.ReactElement {


    if(!this.props.data.isVisible)
    {
      return <div style={hiddenStyle}></div>
    }
    
    if(this.timer){
      clearTimeout(this.timer);
      this.timer= undefined;
    }
    if(this.props.data.autoCloseTime)
    {
      
      this.timer = setTimeout((o)=>{
        if(this.props.data.isVisible){
          if(this.props.data.autoCloseCallback){
            this.props.data.autoCloseCallback();
          }
          
        }

      }, this.props.data.autoCloseTime);
    }

   return <div className="Popup">
          <div className="panel ">
          {(this.props.data.popupType == PopupTypeEnum.Failure) && <img className="iconSmall" src={errorImg}></img>}
          {(this.props.data.popupType == PopupTypeEnum.Success) && <img className="iconSmall" src={yesImg}></img>}
            {this.props.data.display}
            {this.props.children}
          </div>
       </div>
   
  }
}

export default Popup