import React from 'react';


export function NeedLoggedInMessage() : React.ReactElement {

      return <div>
          Making these changes requires you to be logged in.<br/>
          GoblinSpy currently supports login using Google accounts. <br/>
          The whole login is handled by the third party and GoblinSpy never sees the password.<br/>
          The token received from third party is only used to check that the login was valid<br/>
          and is then discarded.
      </div>
  }

