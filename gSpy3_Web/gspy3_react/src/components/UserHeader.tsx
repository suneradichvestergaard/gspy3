
import React, { CSSProperties } from 'react';
import {Userinfoplus} from "../auth/UserLoginData"
import logout from "../gfx/logout.png"
import GSpy3_api from '../api/gSpy3_api';
import "./UserHeader.css"

const hiddenStyle : CSSProperties= {
    display: "none"
  };

const userButtonStyle : CSSProperties={
  display:"inline-block"
}

type UserHeaderState = {


};
type UserHeaderProps = {
    userInfo : Userinfoplus|undefined
};

class  UserHeader extends React.Component<UserHeaderProps,UserHeaderState> {
/*
  constructor(props : UserHeaderProps){
    super(props);
  }*/

  logoutClicked(event: React.MouseEvent<HTMLButtonElement>){
    GSpy3_api.getAPI().logout();
  }

  render () : React.ReactElement {
    let img = this.props.userInfo?.picture;
    let tooltip = "Logged in as "+this.props.userInfo?.name;
    if(!img) img = "gfx/unknown_user.png"
      if(!this.props.userInfo) return <div style={hiddenStyle}></div>;
      return <div className="UserHeader">
        <div className="userButton" style={userButtonStyle}>
          <span title={tooltip}><img alt="User" src={img} className="userImage"></img></span>
         {//<div>{this.props.userInfo.given_name}</div> 
         }
        </div>
        <div className="userIcons">
          <button onClick={this.logoutClicked} title="Click to logout"><img alt="Logout" src={logout}  className="iconSmall"></img></button>
        </div>
      </div>;
  }
}

export default UserHeader