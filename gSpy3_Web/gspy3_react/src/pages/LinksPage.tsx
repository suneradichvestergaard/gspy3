import React from 'react';
import GSpy3_api from "../api/gSpy3_api"
import {NeedLoggedInMessage} from "../components/NeedLoggedInMessage"
import messageManager, { PopupTypeEnum } from '../managers/messageManager';
import { withRouter,RouteComponentProps } from "react-router-dom";
import BaseComponent,{BaseComponentProps, BaseComponentState} from "../components/BaseComponent"
interface LinkPageState  extends BaseComponentState {
};
interface LinkPageProps extends BaseComponentProps  {
  
}; 

class  LinkPage extends BaseComponent<LinkPageProps,LinkPageState> {

  constructor(props : LinkPageProps){
    super(props);
  }


  render () : React.ReactElement {
      return <div className="bordered panel ">
        <div><h2>Links</h2>
        <div>
          <div className="bordered">
                <h3>GoblinSpy links</h3>
                <a className="button" target="_blank" href="https://discord.gg/ZH6m42H">GoblinSpy Discord</a>
          </div>
          <div className="bordered">
          <h3>Official links</h3>
                <a className="button" target="_blank" href="https://discord.gg/QG935NF">BloodBowl 2 Champion Ladder Discord</a>
                <a className="button" target="_blank" href="https://discord.gg/S7R4yqR">BloodBowl 3 Discord</a>
          </div>
          <div className="bordered">
              <h3>Forums and knowledge links</h3>
                <div><a className="button" target="_blank" href="https://bbtactics.com/">BBTactics</a>, lots of knowledge</div>
          </div>
          <div className="bordered">
          <h3>Theorycrafting</h3>
                 <div><a className="button" target="_blank" href="http://www.elyoukey.com/sac//">Samba</a>, action probability calculator</div>
                 <div><a className="button" target="_blank" href="http://onesandskulls.com/">OnesAndSkulls</a>, getting thorough dice stats from replay files</div>
          </div>
          <div className="bordered">
          <h3>Digital tools</h3>
                <div><a className="button" target="_blank" href="https://discord.gg/2vqVXv">Spike! Discord</a>, discord bot and web page similar to GoblinSpy</div>
         
          </div>
        </div>
      </div>
      </div>
  }
}

export default withRouter(LinkPage)