
import React  from 'react';
import GSpyTable from "../../../components/GSpyTable"
import {ColumnLayout} from "../../../api/views/columns"
import { QueryPageResponse} from "../../../api/queryPageRequests"
import DataView from "../../../api/views/dataView"
import BaseComponent,{BaseComponentProps, BaseComponentState} from "../../../components/BaseComponent"
import CoachPanel from "../../../components/CoachPanel"
import {Userinfoplus} from "../../../auth/UserLoginData"

interface CompetitionCoachState extends BaseComponentState  {
  coach : QueryPageResponse
  compcoachteams : QueryPageResponse

  };
interface CompetitionCoachProps extends BaseComponentProps  {
  userInfo : Userinfoplus|undefined
};

class  CompetitionCoachPage extends BaseComponent<CompetitionCoachProps,CompetitionCoachState> {

     
  constructor(props : CompetitionCoachProps){ 
    super(props);
    this.views = [DataView.coach,DataView.compCoachTeams, DataView.comp];
    this.onUpdate = this.onUpdate.bind(this);
  }

  onUpdate(){
    super.onUpdate();
    this.setState(
      {
        coach:this.props.selections.responses.response[DataView[DataView.coach]],
        compcoachteams:this.props.selections.responses.response[DataView[DataView.compCoachTeams]]
      });
  }

  render () : React.ReactElement {
    if(!this.state) return <div></div>;
      
    return <div className="flexcontainer">
        <CoachPanel selections={this.props.selections} userInfo={this.props.userInfo} coach={this.state.coach} global={false}></CoachPanel>

        <div className="panel">
            <GSpyTable source={this.state.compcoachteams} layout={ColumnLayout.CoachTeamsView} filters={this.props.selections.filters}></GSpyTable>
        </div>
      </div>
      
  }
}

export default CompetitionCoachPage