
import React  from 'react';
import GSpyTable from "../../components/GSpyTable"
import {ColumnLayout, ColumnID} from "../../api/views/columns"
import { QueryPageResponse} from "../../api/queryPageRequests"
import DataView from "../../api/views/dataView"
import BaseComponent,{BaseComponentProps, BaseComponentState} from "../../components/BaseComponent"
import Races from '../../api/dataformat/apiRaces';
import APICompFormat from "../../api/dataformat/apiCompformat"
import lookVs from "../../gfx/look-vs.png"
import "./CompetitionPage.css" 
import cogimg from "../../gfx/cog.png"
import closeimg from "../../gfx/close.png"
import pc from "../../gfx/pc.png"
import xb1 from "../../gfx/xb1.png"
import ps4 from "../../gfx/ps4.png"
import messageManager, { PopupTypeEnum } from '../../managers/messageManager';
import ReactDateTime from "react-datetime"
import UserManager, { UserConfig } from "../../managers/userManager"
import GSpy3_api from "../../api/gSpy3_api"
import {Userinfoplus} from "../../auth/UserLoginData"
import HelpPanel from "../../components/HelpPanel"
import Rank from "../../managers/rank"
import {ViewColumns} from "../../api/views/columns"
import UserDate from "../../components/UserDate"

interface CompetitionState extends BaseComponentState  {
  compschedules : QueryPageResponse
  compresults : QueryPageResponse
  comp : QueryPageResponse
  compstandings : QueryPageResponse
  compMenuOpen : boolean
  alterSortingOpen : boolean
  requestDataOpen : ConstrainBoolean,
  requestFromDate : Date
  requestToDate : Date
  userConfig : UserConfig
  isFollowing : boolean,
  showRankCalc : boolean
  };
interface CompetitionProps extends BaseComponentProps  {
  userInfo : Userinfoplus|undefined
}; 

class CupTeam {
  score : number = 0  
  idteam : number = 0
  idrace : number = 0 
  team_name : string =""
  logo : string=""
  coach_name : string =""
}

class  Competition extends BaseComponent<CompetitionProps,CompetitionState> {

    
  constructor(props : CompetitionProps){ 
    super(props);
    
    this.views = [DataView.compResults, DataView.comp, DataView.compStandings,DataView.compSchedules];
    this.onUpdate = this.onUpdate.bind(this);
    this.onCompMenu = this.onCompMenu.bind(this);
    this.onAlterSorting = this.onAlterSorting.bind(this);
    this.onExecuteAlterSorting = this.onExecuteAlterSorting.bind(this);
    this.onRequestMissingData = this.onRequestMissingData.bind(this);
    this.onRequestMaintenance = this.onRequestMaintenance.bind(this);
    this.onExecuteRequestMissingData = this.onExecuteRequestMissingData.bind(this);
    this.onRequestFromDateChanged = this.onRequestFromDateChanged.bind(this);
    this.onRequestToDateChanged = this.onRequestToDateChanged.bind(this);
    this.onRequestFollow = this.onRequestFollow.bind(this);
    this.onRequestUnfollow = this.onRequestUnfollow.bind(this);
  }
 
 
  onUpdate(){
    super.onUpdate();
    //console.log(this.props.selections.responses.response)
    let ucfg = UserManager.getConfig(this.props.userInfo?.id)
    this.setState(
      {
        comp:this.props.selections.responses.response[DataView[DataView.comp]],
        compresults:this.props.selections.responses.response[DataView[DataView.compResults]],
        compschedules:this.props.selections.responses.response[DataView[DataView.compSchedules]],
        compstandings : this.props.selections.responses.response[DataView[DataView.compStandings]],
        userConfig : ucfg,
        isFollowing : ucfg.follows.competitions.includes(this.props.selections.selections[DataView[DataView.comp]])
      }); 

      
  }

  onRequestFollow(){
    this.setState({compMenuOpen:false});
    let idcomp = this.props.selections.selections[DataView[DataView.comp]]
    if(this.state.userConfig.follows.competitions.includes(idcomp)==false){
      this.state.userConfig.follows.competitions.push(idcomp);
      UserManager.saveConfig(this.props.userInfo?.id,this.state.userConfig);
      this.setState({userConfig:this.state.userConfig, isFollowing:true})
      this.props.selections.messages.addMessage(<span>Competition followed...</span>, 3000, PopupTypeEnum.Success);
    
    }else{
      this.props.selections.messages.addMessage(<span>Competition already followed...</span>, 3000, PopupTypeEnum.Neutral);

    }

  }
  onRequestUnfollow(){
    this.setState({compMenuOpen:false});
    let idcomp = this.props.selections.selections[DataView[DataView.comp]]
    if(this.state.userConfig.follows.competitions.includes(idcomp)){
      this.state.userConfig.follows.competitions.splice(this.state.userConfig.follows.competitions.indexOf(idcomp),1);
      UserManager.saveConfig(this.props.userInfo?.id,this.state.userConfig);
      this.setState({userConfig:this.state.userConfig, isFollowing:false})
      this.props.selections.messages.addMessage(<span>Competition unfollowed...</span>, 3000, PopupTypeEnum.Success);
    
    }else{
      this.props.selections.messages.addMessage(<span>Competition not followed...</span>, 3000, PopupTypeEnum.Neutral);

    }
  }
  onCompMenu(){
    
    this.setState({compMenuOpen:true});
  }

  onAlterSorting(){
    this.setState({compMenuOpen:false, alterSortingOpen:!this.state.alterSortingOpen});
  }

  onExecuteAlterSorting(){
    this.setState({alterSortingOpen:false});
    let value = (document.getElementById("sortingInput") as HTMLInputElement).value;
    this.props.selections.messages.addMessage(<span>Altering competition sorting...</span>, 10000, PopupTypeEnum.Neutral);
    
    let idcomp = +this.state.comp.result.rows[0][this.state.comp.result.cols[ColumnID[ColumnID.idcompetition]]];
    GSpy3_api.getAPI().postCompSorting(idcomp, value).then((res)=>{ 
        this.props.selections.messages.addMessage(<span>Sorting of competition has been altered. </span>, 5000, PopupTypeEnum.Success);
        setTimeout(function () {
              document.location.reload();
              }, 1000);
        
          }).catch((e : Error)=>{
      let sError = ""+(e)
      this.props.selections.messages.addMessage(<span>Altering competition sorting  FAILED ({sError})</span>, 3000, PopupTypeEnum.Failure);
    });
  }
  onRequestMaintenance(){
    this.props.selections.messages.addMessage(<span>Requesting competition maintenance...</span>, 10000, PopupTypeEnum.Neutral);
    
    let idcomp = +this.state.comp.result.rows[0][this.state.comp.result.cols[ColumnID[ColumnID.idcompetition]]];
    GSpy3_api.getAPI().postCompMaintenance(idcomp).then((res)=>{ 
        this.props.selections.messages.addMessage(<span>Maintenance of competition has been requested. </span>, 5000, PopupTypeEnum.Success);
        setTimeout(function () {
              document.location.reload();
              }, 1000);
        
          }).catch((e : Error)=>{
      let sError = ""+(e)
      this.props.selections.messages.addMessage(<span>Request of maintenance of competition FAILED ({sError})</span>, 3000, PopupTypeEnum.Failure);
    });
  }
 

  onRequestMissingData(){
    this.setState({compMenuOpen:false, requestDataOpen:!this.state.requestDataOpen});
  }

  onExecuteRequestMissingData(){
    this.setState({compMenuOpen:false, requestDataOpen:false});
    let newFromUTCDate = "";
    if (this.state.requestFromDate){
      newFromUTCDate = this.state.requestFromDate?.toISOString();
    }

    let newToUTCDate = "";
    if (this.state.requestToDate){
      newToUTCDate = this.state.requestToDate?.toISOString();
    }


    this.props.selections.messages.addMessage(<span>Requesting data collection...</span>, 10000, PopupTypeEnum.Neutral);
    
    let idleague = +this.state.comp.result.rows[0][this.state.comp.result.cols[ColumnID[ColumnID.idleague]]];
    GSpy3_api.getAPI().postLeagueRequestCollection(idleague, newFromUTCDate, newToUTCDate).then((res)=>{ 
          if(res){
          this.props.selections.messages.addMessage(<span>Data collection request SUCCESSFUL. </span>, 5000, PopupTypeEnum.Success);
          }
          else{
            this.props.selections.messages.addMessage(<span>Data collection request FAILED</span>, 3000, PopupTypeEnum.Failure);      
          }
          }).catch((e : Error)=>{
      let sError = ""+(e)
      this.props.selections.messages.addMessage(<span>Data collection request FAILED ({sError})</span>, 3000, PopupTypeEnum.Failure);
    });
  }

  onRequestFromDateChanged(date : string|any)
  {
    this.setState({requestFromDate : date});
    
  }
  onRequestToDateChanged(date : string|any)
  {
    this.setState({requestToDate : date});
    
  }

  calcRank = function()
  {
      var wins = Math.round(+(document.getElementById("rankCalcW") as HTMLInputElement).value);
      var draws = Math.round(+(document.getElementById("rankCalcD")as HTMLInputElement).value);
      var losses = Math.round(+(document.getElementById("rankCalcL")as HTMLInputElement).value);
      //var concedes = Math.round(+(document.getElementById("rankCalcC")as HTMLInputElement).value);
      var RankPoints = Rank.calculateRank(wins,draws,losses,0);

      let tgt = document.getElementById("rankCalcOut");
      if(tgt != null)tgt.innerHTML = RankPoints.toFixed(3).replace(",", ".");

  }

  render () : React.ReactElement {

    if(!this.state || !this.state.compschedules) return <div></div>;
    let schedresp : QueryPageResponse = JSON.parse(JSON.stringify(this.state.compschedules));
    let sched = schedresp?.result;
    let res = this.state.compresults?.result;
    let format = 0;
    let sorting = "";
    if(this.state.comp.result.rows?.length> 0){
      format = +this.state.comp.result.rows[0][this.state.comp.result.cols["format"]]
      sorting =this.state.comp.result.rows[0][this.state.comp.result.cols["sorting"]]
    }
    
    if(false)//format==1 && this.state.compschedules.result.rows.length > 2) // 1== single elimintation
    {
      if(!this.state.compschedules) return <span></span>
      
      let schedRoundCol= this.state.compschedules.result.cols[ColumnID[ColumnID.round]]
      let score1col = this.state.compschedules.result.cols[ColumnID[ColumnID.score_home]]
      let score2col = this.state.compschedules.result.cols[ColumnID[ColumnID.score_away]]
      let logo1col = this.state.compschedules.result.cols[ColumnID[ColumnID.logo_home]]
      let logo2col = this.state.compschedules.result.cols[ColumnID[ColumnID.logo_away]]
      let race1col = this.state.compschedules.result.cols[ColumnID[ColumnID.idrace_home]]
      let race2col = this.state.compschedules.result.cols[ColumnID[ColumnID.idrace_away]]
      let team1col = this.state.compschedules.result.cols[ColumnID[ColumnID.idteam_home]]
      let team2col = this.state.compschedules.result.cols[ColumnID[ColumnID.idteam_away]]
      let teamname1col = this.state.compschedules.result.cols[ColumnID[ColumnID.team_name_home]]
      let teamname2col = this.state.compschedules.result.cols[ColumnID[ColumnID.team_name_away]]
      let matchcol = this.state.compschedules.result.cols[ColumnID[ColumnID.idmatch]]
      let coach1col = this.state.compschedules.result.cols[ColumnID[ColumnID.coach_name_home]]
      let coach2col = this.state.compschedules.result.cols[ColumnID[ColumnID.coach_name_away]]
      
     
      var lrows = this.state.compstandings.result.rows.length + 1;
      var r1r = 0; for (var i = 0; i < this.state.compschedules.result.rows.length; i++)
      {
          if (this.state.compschedules.result.rows[i][schedRoundCol] == "1")
              r1r++;
      }
      if (lrows < r1r * 2) lrows = r1r * 2;
      
      var rows = 0;
      var cols = 0;
      
      for (let divide = lrows/2; divide >= 1; divide /= 2)
      {
          cols++;
          rows += divide;  
      }

      return <table className="cup bordered">
          <thead>
            {(()=>
              {
                let items : React.ReactElement[] = []
                let splits = rows/2;
                for(let col=0; col < cols;col++)
                {
                  items.push(<th key={col}>Round {(col+1)}</th>)
                  splits/=2;
                }
                return <tr>{items}</tr>

              })()
            }
          </thead>
          
            {
              (()=>
              {
                var height = 25;
                var splits = lrows;
                var extra = 5;
                var spaceAdd = height * 2+extra;
                var space = 0;
                let mid=0
                let colElements : React.ReactElement[] = []
                let lastRoundIds : string[] = []
                let curRoundsIds : string[] = []
                for (var round = 1; round <= cols ; round++) // Step through all given rounds
                  {
                    lastRoundIds = curRoundsIds;
                    curRoundsIds = [];
                  splits/=2;
                  space = spaceAdd-height*2;
                  spaceAdd *= 2;
                  spaceAdd += 4;// Extra per round
                  colElements.push(<td key={"r"+round}>{(()=>
                    {
                    let subItems : React.ReactElement[]=[]
                    for (var row = 0; row < splits; row++) // Step through all items in this round
                      {
                        var mid = 0;
                        var t : CupTeam[] = [new CupTeam(),new CupTeam()];
                        var roundIndex = 0;
                        var gotIt = false;
                        let compSchedRow : string[] = []
                        let cols = this.state.compschedules.result.cols;
                        // Go through all of schedule to try and find the correct item for this location
                        for (var si = 0; si < this.state.compschedules.result.rows.length; si++) 
                        {
                          var sc = this.state.compschedules.result.rows[si];
                          if (schedRoundCol < sc.length && +sc[schedRoundCol] == round)  // Same round, so it's a candidate
                          {
                            if ((roundIndex == row && lastRoundIds.length == 0)  // If this is first round, then we grab them by the index they return
                                  ||
                                  // Otherwise we grab it if it matches the content of lastRoundIds
                                  ( 
                                     (lastRoundIds[row/2]?.indexOf((""+sc[team1col]))>=0 ||lastRoundIds[row/2]?.indexOf((""+sc[team1col]))>=0)
                                  || (lastRoundIds[row*2]?.indexOf((""+sc[team2col]))>=0 ||lastRoundIds[row*2+1]?.indexOf((""+sc[team2col]))>=0)
                                  )
                            )
                                 {
                                  curRoundsIds[row] =  (""+sc[team1col])+":"+(""+sc[team2col]);
                                t[0].score = +sc[score1col];
                                t[1].score = +sc[score2col];
                                t[0].idteam = +sc[team1col];
                                t[1].idteam = +sc[team2col];
                                t[0].idrace = +sc[race1col];
                                t[1].idrace = +sc[race2col];
                                t[0].team_name = sc[teamname1col];
                                t[1].team_name = sc[teamname2col];
                                mid = +sc[matchcol];
                                t[0].logo = sc[logo1col];
                                t[1].logo = sc[logo2col];
                                t[0].coach_name = sc[coach1col];
                                t[1].coach_name = sc[coach2col];
                                gotIt = true;
                                compSchedRow = sc;
                                break;
                            }
                            else roundIndex++;
                          }
                        }
                        if(gotIt) 
                        {
                          var isPlayed = mid!=0;
                          let cupTableItems :React.ReactElement[]=[]
                          let matchurl = this.props.location.pathname+"/match/"+mid;
                          var oddOrEven = (row % 2 == 0)?"even":"odd";
                          for (var team = 0; team < 2; team++) {
                              let teamurl = this.props.location.pathname+"/team/"+t[team].idteam;
                          
                              var winLossClass = "";
                              if(isPlayed)
                              {
                                  if (team == 0 && t[0].score && t[0].score > t[1].score) winLossClass = "winner";
                                  else if (team == 1 && t[1].score && t[1].score > t[0].score) winLossClass = "winner";
                                  else if(t[0].score || t[1].score) winLossClass = "loser";
                              }
                              let cupTableItemHeightStyle = {height:""+height+"pt"}
                              let tdClassName = ""+oddOrEven+" "+winLossClass;
                              let logoimg = "../gfx/logos/"+t[team].logo+".png";
                              let raceimg = "../gfx/races/"+Races.GetName(t[team].idrace)+".png";

                              let idcontest = +compSchedRow[cols[ColumnID[ColumnID.schedule_origin_id]]]
                              let idcomp = +compSchedRow[cols[ColumnID[ColumnID.idcompetition]]]
                              let datevalue = compSchedRow[cols[ColumnID[ColumnID.userdate]]]
                              if(datevalue){datevalue+="Z"}
                                          console.log("Date: "+datevalue);

                              cupTableItems.push(<tr key={"r"+row+"t"+team} style={cupTableItemHeightStyle}>
                                { gotIt && <td className={tdClassName}>
                                      <img className='iconSmall' src={logoimg} />
                                      <img className='iconSmall' title={Races.GetName(t[team].idrace)} src={raceimg} />
                                      <span  className='cupTeam'><a href={teamurl} title={"Coached by "+t[team].coach_name}>{t[team].team_name}</a></span>
                                      </td>
                                }
                                { team == 0 && <React.Fragment>
                                      <td rowSpan={2} className="cupVs"><div >
                                        {!isPlayed && <div>
                                            <span title={datevalue}><UserDate idcontest={idcontest} idcomp={idcomp} userDate={datevalue} /></span>
                                          </div>}
                                        {isPlayed &&
                                              <div>
                                              <a href={matchurl}><img className='iconSmall' src={lookVs} /></a>
                                              </div>
                                        }</div>
                                        </td>
                                    </React.Fragment>
                                }
                                <td className={oddOrEven}>
                                {isPlayed && <span>{t[team].score}</span>}
                                </td>
                              </tr>)
 
                              }
                              let cupTableStyle = {marginBottom:""+(gotIt?space:0)+"pt", marginTop:""+(row==0?space-spaceAdd/4+height:space)+"pt"}
                              let cupTable = <table className="cupRoundTable" style={cupTableStyle}><tbody>{cupTableItems}
                                </tbody></table>
                                subItems.push(<React.Fragment key={"r"+row+"tct"+team}>{cupTable}</React.Fragment>)
                        }

                      }
                      return <React.Fragment>{subItems}</React.Fragment>
                    })()}</td>)
                    
                  }
                return <tbody><tr>{colElements}</tr></tbody>
              })()
            }
          
      </table>
    }

    let groupinfo : {[key:string]:React.ReactElement} = {};
    if(sorting){
        groupinfo["Standings"] = <span><small> - Sorted by {sorting}</small></span>
      }
      if(!this.state || !this.state.comp|| ! this.state.comp.result || this.state.comp.result.rows.length<=0)
        return <div></div>


    let idorigin = this.state.comp.result.rows[0][this.state.comp.result.cols[ColumnID[ColumnID.idorigin]]];
    let idoriginpc = idorigin&&idorigin==="1"
    let idoriginxb1 = idorigin&&idorigin==="2"
    let idoriginps4 = idorigin&&idorigin==="3"

    return  <div className="flexcontainer">
          <div className="panel bordered ">
            <div className="menu" style={{position:"absolute", right:"1pt",top:"0pt"}}>
              <a title="Competition settings"  onClick={this.onCompMenu} className="imageButton"><img src={cogimg} className="iconSmall"></img></a>
            </div>
            {this.state.compMenuOpen&&
            <HelpPanel title="Competition settings" left={true} onClose={()=>{this.setState({compMenuOpen:false});}} >
                <div className="menu padded">
                  {this.state.isFollowing&& 
                        <span><button onClick={this.onRequestUnfollow}>No longer follow this competition</button><br></br></span>}
                  {this.state.isFollowing==false&& 
                        <span><button onClick={this.onRequestFollow}>Follow this competition</button><br></br></span>}
                  <button onClick={this.onRequestMissingData}>Request missing data</button><br></br>
                  <button onClick={this.onRequestMaintenance}>Request maintenance</button><br></br>
                  {Number(this.props.selections.selections[DataView[DataView.comp]])>0 &&  <button onClick={this.onAlterSorting}>Alter sorting</button>}
                  <br></br>
                  </div>
              </HelpPanel>
            }
            {this.state.alterSortingOpen &&
              <div className="menuPopup padded">
                <table >
                  <thead>
                    <tr><td><h3>Alter points calculation and sorting</h3></td><td style={{textAlign:"right"}}><a className="imageButton"><img onClick={this.onAlterSorting} src={closeimg} className="iconSmall"></img></a></td></tr>
                  </thead>
                  <tbody>
                    <tr><td  colSpan={2}>
                    By default, the competitions in goblinSpy sorts the league standings based on rank.<br/>
                    If you have a private league you might want to change this so that it instead sorts by points. <br/>
                    You can in addition select how the points are calculated and how tie-breakers are determined.<br/>
                    The sorting format is "W-D-L, TIE1, TIE2, TIE3", for example "3-1-0,TD,CAS"<br/>
                    W,D and L are the points given per Win, Draw or Loss.<br/>
                    TIE1-3 are optional tie-breakers:<br/>
                    TD = Touchdowns<br />
                    TDD = Touchdown difference<br /> 
                    CAS = Casualties<br />
                    CASD = Casualties difference<br/>
                    CW =  Coleman Waldorf ( CAS/2 + TD )<br />
                    KILL = Kills<br />
                    H2H = Head 2 Head, winner in internal meetings gets ahead<br /><br/>
                  
                    If you want the standard behaviour again, just enter "rank" without quotes as sorting<br/>
                    After setting the sorting, you may need to shift-click reload in you browser on the competition page, to get the updated sorting to show<br/>
                    <input id="sortingInput" defaultValue="3-1-0, TDD" />
                    <button onClick={this.onExecuteAlterSorting}>Alter sorting</button>
                    </td></tr></tbody></table>
                    
              </div>}

              {this.state.requestDataOpen &&
              <div className="menuPopup padded">
                <table  >
                  <thead>
                    <tr><td colSpan={2}><h3>Request missing data</h3></td><td></td><td style={{textAlign:"right"}}><a className="imageButton"><img onClick={this.onRequestMissingData} src={closeimg} className="iconSmall"></img></a></td></tr>
                  </thead>
                  <tbody>
                    <tr><td  colSpan={3}>
                    Sometimes GoblinSpy misses to collect some data.<br/>
                    Fill in the date range of the missing matches and GoblinSpy will try again.<br/>
                    Datetimes are in your local time zone<br></br>
                    </td><td></td><td></td></tr>
                    <tr><td style={{width:"200pt"}}>From</td><td style={{width:"200pt"}}>To</td><td></td></tr>
                    <tr><td >
                      <ReactDateTime
                        value={this.state.requestFromDate}
                        dateFormat="YYYY-MM-DD"
                        timeFormat="HH:mm"
                        className="ReactDateTime"
                        onChange={this.onRequestFromDateChanged}
                    /></td><td >
                    <ReactDateTime
                        value={this.state.requestToDate}
                        dateFormat="YYYY-MM-DD"
                        timeFormat="HH:mm"
                        className="ReactDateTime"
                        onChange={this.onRequestToDateChanged}
                    />
                    </td><td><button onClick={this.onExecuteRequestMissingData}>Request</button></td></tr>
                    </tbody></table>
                    
                    
                    
                    
              </div>}

           <span>
            <h2>
                {idorigin&&idoriginpc&&<img alt="PC" className="iconSmall" title="PC" src={pc}></img>}
                {idorigin&&idoriginxb1&&<img  alt="XB1" className="iconSmall" title="XB1" src={xb1}></img>}
                {idorigin&&idoriginps4&&<img  alt="PS4" className="iconSmall" title="PS4" src={ps4}></img>}                
                
                {this.state.comp?.result?.rows[0][this.state.comp.result.cols[ColumnID[ColumnID.competition_name]]]}
            </h2>
            </span>
            <span>
              {(new APICompFormat()).getFormatName(format)} 
              {(!sorting || sorting=="rank")&& format!= 1&&<span>, <button className="small" onClick={()=>{this.setState({showRankCalc:!this.state.showRankCalc})}}>Rank calculator</button>
                  </span>
              }
              {this.state.showRankCalc && <div className='bordered'><small>
                  Wins: <input onChange={()=>{this.calcRank();}} id='rankCalcW' defaultValue={0} type="number" style={{marginRight:"5pt",width:"30pt",fontSize:"8pt"}}></input>
                  Draws: <input onChange={()=>{this.calcRank();}}  id='rankCalcD' defaultValue={0} type="number" style={{marginRight:"5pt",width:"30pt",fontSize:"8pt"}}></input>
                  Losses: <input onChange={()=>{this.calcRank();}}  id='rankCalcL' defaultValue={0} type="number" style={{marginRight:"5pt",width:"30pt",fontSize:"8pt"}}></input>
                  =&gt; <span id='rankCalcOut'>0</span>
                  </small>
                  </div>}
              {sorting&&<span> -  Sorted by {sorting}</span>}
              {this.state.comp?.result?.rows[0][this.state.comp.result.cols[ColumnID[ColumnID.last_checked]]] &&<span><small>, last checked  {ViewColumns.writeDateTime(this.state.comp?.result?.rows[0][this.state.comp.result.cols[ColumnID[ColumnID.last_checked]]], false)}</small></span>}
            </span>
          </div>
          <div className="panel">
            <GSpyTable source={this.state.compstandings} layout={ColumnLayout.CompStandingsView} filters={this.props.selections.filters}></GSpyTable>
          </div>
        
         
          </div>
      
  }
}

export default Competition