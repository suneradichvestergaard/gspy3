
import React  from 'react';
import GSpyTable from "../../components/GSpyTable"
import {ColumnLayout} from "../../api/views/columns"
import { QueryPageRequest,QueryPageResponse} from "../../api/queryPageRequests"
import DataView from "../../api/views/dataView"
import BaseComponent,{BaseComponentProps, BaseComponentState} from "../../components/BaseComponent"
import Rank from "../../managers/rank"

interface ResultsState extends BaseComponentState  {
    overtake : QueryPageResponse

  };
interface ResultsProps extends BaseComponentProps  {

};

class  OvertakePagePage extends BaseComponent<ResultsProps,ResultsState> {

    
  constructor(props : ResultsProps){  
    super(props);

    this.views = [DataView.comp,DataView.overtake];
    this.onUpdate = this.onUpdate.bind(this);
  }
  getData(){
    let res = this.props.selections.requests.request["overtake"]
    if(!res){
               let splits = this.props.location.pathname.split("/");
               this.props.selections.selections["frompos"] = splits[splits.length-1];
               this.props.selections.selections["topos"]  = splits[splits.length-2];
    }

      super.getData();
  }

  onUpdate(){

    super.onUpdate();

    

    this.setState(
      {
        overtake:this.props.selections.responses.response[DataView[DataView.overtake]],
      });
  }
  

  render () : React.ReactElement {

    if(!this.state || !this.state?.overtake || !this.state.overtake?.result) return <div></div>;
    let splits = this.props.location.pathname.split("/");
    let frompos = splits[splits.length-1];
    let topos = splits[splits.length-2];
    let qres = this.state.overtake.result;
    if(qres.rows.length < 2) return <div></div>

    let targetRank = +qres.rows[0][qres.cols["ranking"]]
    let curRank = +(qres.rows[1][qres.cols["ranking"]])
    let curWins = +(qres.rows[1][qres.cols["wins"]])
    let curDraws = +(qres.rows[1][qres.cols["draws"]]);
    let curLosses = +(qres.rows[1][qres.cols["losses"]]);
    let curConcedes =+(qres.rows[1][qres.cols["concedes"]]);
    let  wins = curWins;
    let newRank = 0;
    for (let i = 1; i < 100; i++) 
    {
        wins = curWins + i;
        newRank = Rank.calculateRank(wins, curDraws, curLosses, curConcedes);
        if(newRank > targetRank)
        {
            break;
        }

    }
   
    return  <div className="flexcontainer">
          <div className="panel bordered">
              <h2>Overtaking position {topos} from  {frompos}</h2>
              <table>
                  <tbody>
                  <tr>
                          <td>Wins needed</td><td>{(wins-curWins)}</td>
                      </tr>
                      <tr>
                          <td>Old rank</td><td>{curRank.toFixed(3)}</td>
                      </tr>
                      <tr>
                          <td>Target rank</td><td>{targetRank.toFixed(3)}</td>
                      </tr>
                      <tr>
                          <td>New rank</td><td>{newRank.toFixed(3)}</td>
                      </tr>
                  </tbody>
              </table>

          </div>
          <div className="panel">
            <GSpyTable source={this.state.overtake} layout={ColumnLayout.CompStandingsView} filters={this.props.selections.filters}></GSpyTable>
          </div>
          
          </div>
      
  }
}

export default OvertakePagePage