
import React  from 'react';
import GSpyTable from "../../components/GSpyTable"
import {Column, ColumnID, ColumnLayout} from "../../api/views/columns"
import { QueryPageResponse} from "../../api/queryPageRequests"
import DataView from "../../api/views/dataView"
import BaseComponent,{BaseComponentProps, BaseComponentState} from "../../components/BaseComponent"

interface StatsState extends BaseComponentState  {
  compStatsData : QueryPageResponse
 
  };
interface StatsProps extends BaseComponentProps  {
  sourceQuery : DataView
};

class  CompetitionStatsPage extends BaseComponent<StatsProps,StatsState> {

    
  constructor(props : StatsProps){  
    super(props);

    this.views = [DataView.comp,this.props.sourceQuery];
    this.onUpdate = this.onUpdate.bind(this);
    this.setAggr = this.setAggr.bind(this);
  }

  setAggr(aggr : string){
    let filt = this.props.selections.filters.getFilter(DataView[this.props.sourceQuery]);
    filt.aggr = aggr;
    this.props.history.push(this.props.selections.filters.getFilterURL());
  }
 
 
  onUpdate(){
    console.log("SOURCE QUERY");
    console.log(this.props.sourceQuery)
    this.views =  [DataView.comp,this.props.sourceQuery];
    super.onUpdate();
 

    let res = this.props.selections.responses.response[DataView[this.props.sourceQuery]];
    if(res){ 

      if(!res?.request?.group){
        res.request.group = "idteam";
      }
      if(!res?.request?.aggr){
        res.request.aggr = "sum";
      }


      let compcol = res.result.cols["idcompetition"];
       for(let i=0; i<  res.result.rows.length;i++){
        if(res?.request && res.request.idmap){
      //    console.log(this.props.selections)
          res.result.rows[i][compcol] = (this.props.selections.selections["comp"]);
        }
      }    


    }
  

    this.setState(
      {
        compStatsData:this.props.selections.responses.response[DataView[this.props.sourceQuery]],
      }); 
  }
  

  render () : React.ReactElement {

    if(!this.state || !this.state.compStatsData || !this.state.compStatsData.result || this.state.compStatsData.result.rows.length==0) return <div></div>;
   
    return  <div className="flexcontainer">
        <div className="bordered panel">
        {this.props.sourceQuery == DataView.compStatsTeam && <h2>Team match stats</h2>}
        {this.props.sourceQuery == DataView.compStatsCoaches && <h2>Coach match stats</h2>}
        {this.props.sourceQuery == DataView.compStatsPlayer && <h2>Player match stats</h2>}
        {this.props.sourceQuery == DataView.compStatsRaces && <h2>Race match stats</h2>}

        {this.state.compStatsData.result.rows[0][this.state.compStatsData.result.cols["wins"]]==""&&<div>This competition is too big to allow searching these stats</div>}
        {this.state.compStatsData.result.rows[0][this.state.compStatsData.result.cols["wins"]]!=""&&<div>
            <table>
              <tbody>
                <tr>
                  <td>Aggregation</td><td>
                    <select  onChange={(e)=>{this.setAggr(e.target.value)}}  value={""+this.state.compStatsData.request.aggr}>
                      <option value="sum">Total</option>
                      <option value="avg">Amount / game</option>
                    </select>
                  </td>
                </tr> 
              </tbody>
            </table>
            </div>}
          </div>
          {this.state.compStatsData.result.rows[0][this.state.compStatsData.result.cols["wins"]]!=""&&<div className="panel">
          {this.props.sourceQuery == DataView.compStatsTeam && <GSpyTable source={this.state.compStatsData} layout={ColumnLayout.CompTeamStatsView} filters={this.props.selections.filters}></GSpyTable>}
          {this.props.sourceQuery == DataView.compStatsCoaches && <GSpyTable source={this.state.compStatsData} layout={ColumnLayout.CompCoachesView} filters={this.props.selections.filters}></GSpyTable>}
          {this.props.sourceQuery == DataView.compStatsPlayer && <GSpyTable source={this.state.compStatsData} layout={ColumnLayout.CompPlayerView} filters={this.props.selections.filters}></GSpyTable>}
          {this.props.sourceQuery == DataView.compStatsRaces && <GSpyTable source={this.state.compStatsData} layout={ColumnLayout.CompRacesView} filters={this.props.selections.filters}></GSpyTable>}
          </div>}
          
          </div>
       
  }
}

export default CompetitionStatsPage