
import React  from 'react';
import GSpyTable from "../../../components/GSpyTable"
import {ColumnLayout} from "../../../api/views/columns"
import { QueryPageResponse} from "../../../api/queryPageRequests"
import DataView from "../../../api/views/dataView"
import BaseComponent,{BaseComponentProps, BaseComponentState} from "../../../components/BaseComponent"

import GSpyChart from "../../../components/GSpyChart"

interface DiceState extends BaseComponentState  {
    matchReplayStats : QueryPageResponse

  };
interface DiceProps extends BaseComponentProps  {

};

class  CompetitionMatchDicePage extends BaseComponent<DiceProps,DiceState> {

    
  constructor(props : DiceProps){  
    super(props);

    this.views = [DataView.comp,DataView.matchReplayStats];
    this.onUpdate = this.onUpdate.bind(this);
  }


  onUpdate(){
    super.onUpdate();
    this.setState(
      {
        matchReplayStats:this.props.selections.responses.response[DataView[DataView.matchReplayStats]]
      }); 
  }
  

  render () : React.ReactElement {

    if(!this.state || !this.state.matchReplayStats) return <div></div>;
   
    if(this.state.matchReplayStats.result.fileData=="")return <div>No match data available</div>
    let gameStats = JSON.parse(this.state.matchReplayStats.result.fileData)

    // Create playermap to match correct team for the rolls
    var playerToTeamMap : {[pid:number]:number} ={};
    for (var i = 0; i < gameStats.TeamStats.length; i++) {
        for (var j = 0; j < gameStats.TeamStats[i].PlayerStats.length; j++) {
            playerToTeamMap[gameStats.TeamStats[i].PlayerStats[j].PlayerId] = i; 
        }
    }


    // Preparations
    type  specialDiceType = {
        name :string, aid: number, response: QueryPageResponse
    };
    let tname1 = gameStats.TeamStats[0].TeamName;
    let tname2 = gameStats.TeamStats[1].TeamName;
    
    // Define columns of the skill roll row data (check with DataView layout)
    let skillRollColKeys : { [id: string] : number; } = {}; 
    skillRollColKeys["dice"]=0;
    skillRollColKeys["number_home"]=1;
    skillRollColKeys["number_away"]=2;

    // Defined columns for the block rolls
    let blockRollColKeys : { [id: string] : number; } = {}; 
    blockRollColKeys["blockdice"]=0;
    blockRollColKeys["number_home"]=1;
    blockRollColKeys["number_away"]=2;


    // Create the data containers for skill rolls
    let skillRollDices : specialDiceType[] = [
        { name: "GFI", aid: 1,response:new QueryPageResponse() },
        { name: "Dodge", aid: 2,response:new QueryPageResponse() },
        { name: "Armour", aid: 3,response:new QueryPageResponse() },
        { name: "Injury", aid: 4,response:new QueryPageResponse() },
        { name: "Stand up", aid: 6,response:new QueryPageResponse() },
        { name: "Pickup", aid: 7 ,response:new QueryPageResponse()},
       { name: "Catch", aid: 9,response:new QueryPageResponse() },
        { name: "Pass", aid: 12,response:new QueryPageResponse() },
        { name: "Interception", aid: 16,response:new QueryPageResponse() },
         { name: "Leap", aid: 36 ,response:new QueryPageResponse()},
        { name: "Bonehead", aid: 20,response:new QueryPageResponse() },
        { name: "Really stupid", aid: 21,response:new QueryPageResponse() },
        { name: "Wild Animal", aid: 22,response:new QueryPageResponse() },
        { name: "Loner", aid: 23,response:new QueryPageResponse() },
        { name: "Land", aid: 24,response:new QueryPageResponse() },
        { name: "Regeneration", aid: 25,response:new QueryPageResponse() },
        { name: "Hungry", aid: 27,response:new QueryPageResponse() },
        { name: "EatTeamMate", aid: 28,response:new QueryPageResponse() },
        { name: "Dauntless", aid: 29,response:new QueryPageResponse() },
        { name: "Jump up", aid: 31 ,response:new QueryPageResponse()},
        { name: "Shadowing", aid: 32 ,response:new QueryPageResponse()},
        { name: "Stab", aid: 34 ,response:new QueryPageResponse()},
        { name: "FoulAppearance", aid: 37,response:new QueryPageResponse() },
        { name: "Tentacles", aid: 38,response:new QueryPageResponse() },
        { name: "Chainsaw", aid: 39,response:new QueryPageResponse() },
        { name: "Take root", aid: 40,response:new QueryPageResponse() },
        { name: "Pro", aid: 45,response:new QueryPageResponse() },
        { name: "Hypnotic gaze", aid: 46,response:new QueryPageResponse()},
        { name: "Animosity", aid: 49,response:new QueryPageResponse() },
        { name: "Bloodlust", aid: 50,response:new QueryPageResponse() },
        { name:"Feed", aid:51, response:new QueryPageResponse() },
        { name:"Bribe", aid:52, response:new QueryPageResponse() },
        //{ name:"HalflingChef", aid:53, response:new QueryPageResponse() },
        { name:"FireBallHit", aid:54, response:new QueryPageResponse() },
        { name:"LightningBolt", aid:55, response:new QueryPageResponse() },
        { name:"Throw Team Mate", aid:56, response:new QueryPageResponse() },
        { name:"Necromancer", aid:66, response:new QueryPageResponse() },
        { name:"Nurgles rot", aid:67, response:new QueryPageResponse() },
        { name:"Swealtering heat", aid:71, response:new QueryPageResponse() },
        { name:"Hail mary bomb", aid:74, response:new QueryPageResponse() },
        { name:"Bomb Hit", aid:72, response:new QueryPageResponse() },
    ];
    

    // Create data containers for block dices
    let blockRollDices : specialDiceType[]=[
        { name: "Blocks (1D)", aid: 5,response:new QueryPageResponse() },
        { name: "Blocks (2D, best dice)", aid: 5,response:new QueryPageResponse() },
        { name: "Blocks (3D, best dice)", aid: 5,response:new QueryPageResponse() },
    ];


    // Populate with '0's that we can add to
    for (var i = 0; i < skillRollDices.length; i++)
        {
            skillRollDices[i].response.result.cols = skillRollColKeys;
            let numCols = 6;
            if(skillRollDices[i].aid == 38 || skillRollDices[i].aid == 3 || skillRollDices[i].aid == 4) // tents , armour, injury
            { numCols= 12;}
            for(var j=0; j < numCols;j++){
            skillRollDices[i].response.result.rows.push([""+(j+1),"0","0"]); // First column is label of x axis (see dataview layout and colkeys)
            } 

        }
    for (var i=0; i < blockRollDices.length;i++){
        blockRollDices[i].response.result.cols = blockRollColKeys;
        blockRollDices[i].response.result.rows.push(["Skull","0","0"]);
        blockRollDices[i].response.result.rows.push(["BothDown","0","0"]);
        blockRollDices[i].response.result.rows.push(["Push","0","0"]);
        blockRollDices[i].response.result.rows.push(["Stumble","0","0"]);
        blockRollDices[i].response.result.rows.push(["Pow","0","0"]);
    }

    // Add success containers
    class successRollType  { aid:number=0; name:string=""; rolls : number=0 ; success : number=0; fail : number=0; expected : number=0; }
    let successMap : {[aid : number] : successRollType} = {};
    let successMapAway : {[aid : number] : successRollType} = {};


    // Calculate rolls
    for (var a = 0; a < gameStats.Replay.Items.length; a++) {
        var aid = gameStats.Replay.Items[a].AID;
        var team = playerToTeamMap[gameStats.Replay.Items[a].PID];
        if (aid==5) // block
        {
            var team = playerToTeamMap[gameStats.Replay.Items[a].PID];
            var block = gameStats.Replay.Blocks[gameStats.Replay.Items[a].I];
            var dices = block.Roll;
            var red = block.Red;
            var dice = block.Dice;
            var bestDice = -1;
            var bestDice2D = -1;
            var bestDice3D = -1;
            for (var j = 0; j < dices.length; j++) {
                if (dices.length == 1){
                    let curVal = +blockRollDices[0].response.result.rows[dices[j]][team+1];
                    blockRollDices[0].response.result.rows[dices[j]][team+1] = ""+(curVal+1);
                }
                if (dices[j] > bestDice2D && dices.length == 2)
                    bestDice2D = dices[j];
                if (dices[j] > bestDice3D && dices.length == 3)
                    bestDice3D = dices[j];
            }
            if (bestDice2D >= 0) {
                let curVal = +blockRollDices[1].response.result.rows[bestDice2D][team+1];
                blockRollDices[1].response.result.rows[bestDice2D][team+1] = ""+(curVal+1);
       
            }
            if (bestDice3D >= 0)
            {
                let curVal = +blockRollDices[2].response.result.rows[bestDice3D][team+1];
                blockRollDices[2].response.result.rows[bestDice3D][team+1] = ""+(curVal+1);

            }
        }
        for (var i = 0; i < skillRollDices.length; i++)
        {
            try{
            if(skillRollDices[i].aid == aid)
            {
                var rollitem = gameStats.Replay.Rolls[gameStats.Replay.Items[a].I];

                if(aid==4) rollitem = gameStats.Replay.Injuries[gameStats.Replay.Items[a].I];

                if(rollitem)
                {
                    if(aid==3 ||aid == 4 || aid == 54 || aid==55 || aid==16) // armour and injury, fireball, lightning, interception
                        { team = (team == 0 ? 1 : 0);} // swap team due to how armor and inj are stored

                    var dices = rollitem.Roll;
                    if(aid==4) // injury
                    {
                        rollitem.Req = 8; // KO is success

                    }
                    var dice = dices[0];
                    if(dices.length > 1 && (skillRollDices[i].aid==38 || skillRollDices[i].aid==3 || skillRollDices[i].aid==4)){ dice += dices[1];} // tents, armour, inj
       
                    var cur = +skillRollDices[i].response.result.rows[dice-1][1+team];
                    skillRollDices[i].response.result.rows[dice-1][1+team] = ""+(1+cur);

                    let success = (dice==6 && !(skillRollDices[i].aid==38 || skillRollDices[i].aid==3 || skillRollDices[i].aid==4))
                                    || (dice != 1 && dice >= rollitem.Req);
                    let map = team == 0 ? successMap : successMapAway;

                    if(!map[aid]){map[aid] = new successRollType(); map[aid].aid = aid;
                        map[aid].name = skillRollDices[i].name;
                    }
                    map[aid].rolls++;
                    if(success){
                        map[aid].success++;
                    } else{
                        map[aid].fail++;
                    }
                    if(dices.length == 1){
                        if(rollitem.Req <= 2){map[aid].expected += 5.0/6.0;}
                        else if(rollitem.Req > 5){map[aid].expected += 1.0/6.0;}
                        else{map[aid].expected += (7-rollitem.Req) / 6.0;}
                    } else {
                        switch(rollitem.Req){
                            case 1: case 2: map[aid].expected += 1; break;
                            case 3: map[aid].expected += 0.9722; break;
                            case 4: map[aid].expected += 0.9167; break;
                            case 5: map[aid].expected += 0.8333; break;
                            case 6: map[aid].expected += 0.7222; break;
                            case 7: map[aid].expected += 0.5833; break;
                            case 8: map[aid].expected += 0.4167; break;
                            case 9: map[aid].expected += 0.2778; break;
                            case 10: map[aid].expected += 0.1667; break;
                            case 11: map[aid].expected += 0.0833; break;
                            case 12:case 13: case 14: map[aid].expected += 0.0278; break;
                        }
                    }
                    
                }else console.log("1. Missing roll for "+aid);
            }
        }catch(e){
            console.log(e)
        }
        }
    }
    // Remove empty sets
    for (var i = 0; i < skillRollDices.length; i++)
    {
        let gotData=false;
        for(var j=0; j < skillRollDices[i].response.result.rows.length;j++){
            if(+skillRollDices[i].response.result.rows[j][1] > 0 || +skillRollDices[i].response.result.rows[j][2] > 0){
                gotData=true;
                break;
            }
        }
        if(!gotData){ skillRollDices.splice(i,1); i--;}
    }
    for (var i = 0; i < blockRollDices.length; i++)
    {
        let gotData=false;
        for(var j=0; j < blockRollDices[i].response.result.rows.length;j++){
            if(+blockRollDices[i].response.result.rows[j][1] > 0 || +blockRollDices[i].response.result.rows[j][2] > 0){
                gotData=true;
                break;
            }
        }
        if(!gotData){ blockRollDices.splice(i,1); i--;}
    }

    // Show it
    return  <div className="flexcontainer">
       <div  style={{width:"100%"}}>
           <table className="GSpyTable">
               <thead>
                   <tr>
                   <td colSpan={10}>Dice rolls</td>
                   </tr>
                   <tr>
                   <th className="leftCell" style={{textAlign:"left"}}>Team</th>
                   <th style={{textAlign:"left"}}>Action</th>
                   <th title="Number of rolls made for action">Rolls</th>
                   <th title="Expected number of successes based on the target needed for the rolls">Expected</th>
                   <th title="The number of successful rolls">Successes</th>
                   <th title="The number of failed rolls">Failures</th>
                   <th title="Percent of successful rolls">%</th>
                   <th title="Successful rolls divided by expected rolls">Luck</th>
                   <th title="Number of expected failures divided by number of rolls" className="rightCell">Risk</th>
                   </tr>
               </thead>
               <tbody>
            {
                Object.entries(successMap).map(([k,v])=>{


                        return <tr>
                            <td  className="leftCell" style={{textAlign:"left"}}>{tname1}</td>
                            <td style={{textAlign:"left"}}>{v.name}</td>
                            <td>{v.rolls}</td>
                            <td>{v.expected.toFixed(1)}</td>
                            <td className="pos">{v.success}</td>
                            <td className="neg">{v.fail}</td>
                            <td>{(100*v.success / v.rolls).toFixed(0)+" %"}</td>
                            <td>{(100*v.success / v.expected).toFixed(0)+" %"}</td>
                            <td>{(100*((v.rolls-v.expected)/v.rolls)).toFixed(0)+" %"}</td>
                        </tr>
                })
            }
            {
                Object.entries(successMapAway).map(([k,v])=>{


                        return <tr>
                            <td  className="leftCell" style={{textAlign:"left"}}>{tname2}</td>
                            <td style={{textAlign:"left"}}>{v.name}</td>
                            <td>{v.rolls}</td>
                            <td>{v.expected.toFixed(1)}</td>
                            <td className="pos">{v.success}</td>
                            <td className="neg">{v.fail}</td>
                            <td>{(100*v.success / v.rolls).toFixed(0)+" %"}</td>
                            <td>{(100*v.success / v.expected).toFixed(0)+" %"}</td>
                            <td>{(100*((v.rolls-v.expected)/v.rolls)).toFixed(0)+" %"}</td>
                        </tr>
                })
            }
            </tbody>
            </table>
        </div>
        {
        blockRollDices.map((o,i)=>{
           return <div className="panel" style={{width:"250pt"}}>
                <GSpyChart source={o.response}  filters={this.props.selections.filters} layout={ColumnLayout.TeamBlockDiceGraphView} title={o.name} labels={[tname1,tname2]}></GSpyChart>
            </div> 
            })
        } 
        {
        skillRollDices.map((o,i)=>{
           return <div className="panel" style={{width:"250pt"}}>
                <GSpyChart source={o.response}  filters={this.props.selections.filters} layout={ColumnLayout.TeamDiceGraphView} title={o.name} labels={[tname1,tname2]}></GSpyChart>
            </div> 
            })
        } 
          </div>
      
  }
}

export default CompetitionMatchDicePage