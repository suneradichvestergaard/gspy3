
import React  from 'react';
import GSpyTable from "../../../components/GSpyTable"
import {ColumnLayout, ColumnID} from "../../../api/views/columns"
import { QueryPageResponse} from "../../../api/queryPageRequests"
import DataView from "../../../api/views/dataView"
import BaseComponent,{BaseComponentProps, BaseComponentState} from "../../../components/BaseComponent"


interface MatchPlayersState extends BaseComponentState  {
    matchplayershome : QueryPageResponse
    matchplayersaway : QueryPageResponse

  };
interface MatchPlayersProps extends BaseComponentProps  {

};

class  CompetitionMatchPlayersPage extends BaseComponent<MatchPlayersProps,MatchPlayersState> {

    
  constructor(props : MatchPlayersProps){ 
    super(props);
    this.views = [DataView.comp, DataView.match,DataView.matchTeams, DataView.matchPlayers];
    this.onUpdate = this.onUpdate.bind(this);
  }

  onUpdate(){
    super.onUpdate();

    if(!this.props.selections.responses.response[DataView[DataView.matchPlayers]]){
      return;
    }
    let teams : QueryPageResponse = this.props.selections.responses.response[DataView[DataView.matchTeams]];
    let mph: QueryPageResponse =  JSON.parse(JSON.stringify(this.props.selections.responses.response[DataView[DataView.matchPlayers]]))
    let mpa: QueryPageResponse =  JSON.parse(JSON.stringify(this.props.selections.responses.response[DataView[DataView.matchPlayers]]))
    let teamidcol = teams.result.cols[ColumnID[ColumnID.idteam]]; 

    if(mph?.result.rows){
      for(let i=0; i < mph.result.rows.length;i++){
        let idcol = mph.result.cols[ColumnID[ColumnID.idteam]];
        let row = mph.result.rows[i];
        
        if(row && row[idcol] && row[idcol]===teams.result.rows[1][teamidcol]){
          mph.result.rows.splice(i,1);
          i--;
        }
      }
    }
    if(mpa?.result.rows){
      for(let i=0; i < mpa.result.rows.length;i++){
        let idcol = mpa.result.cols[ColumnID[ColumnID.idteam]];
        let row = mpa.result.rows[i];
        if(row && row[idcol]&&row[idcol]===teams.result.rows[0][teamidcol]){
          mpa.result.rows.splice(i,1);
          i--;
        }
      } 
    }

    this.setState( 
      {
        matchplayershome:mph,
        matchplayersaway:mpa,
      });
  }



  render () : React.ReactElement {
    if(!this.state) return <div></div>;
    return  <div className="flexcontainer">
        <div className="panel"> 
            </div>
            <div className="panel">
            <GSpyTable source={this.state.matchplayershome} layout={ColumnLayout.MatchPlayersView} filters={this.props.selections.filters}></GSpyTable>
            </div>
            <div className="panel">
            <GSpyTable source={this.state.matchplayersaway} layout={ColumnLayout.MatchPlayersView} filters={this.props.selections.filters}></GSpyTable>
            </div>
          </div>
      
  }
}

export default CompetitionMatchPlayersPage