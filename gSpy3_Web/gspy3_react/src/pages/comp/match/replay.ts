import { openStdin } from "process";
import ts from "typescript";
import { QueryPageResponse } from "../../../api/queryPageRequests";
import APIInjuries from "../../../api/dataformat/apiInjuries"

let injuryTypes : APIInjuries = new APIInjuries();


export default {
	replayManager :
{
	w:400,h:100,  
    id: '',
    data: {Teams:Array(),TeamStats:Array(),Replay:{Rolls:new Array(), Moves:new Array(), Balls:new Array(), Blocks:new Array()}},
	ticks:new Array(),
	curTick:{curPlayer:{blitz:false,down:false,mbused:false, PID:""}, score:new Array()},
	curTickIndex:0,
	//autoplaytimer : new NodeJS.Timeout(),
	avinfoTimer : 0,
    turns:new Array(),
	pitchData: {avatars:{}, floor:{}, head:{}},
	pitch:undefined as HTMLElement|undefined, 
	avatars: undefined as HTMLElement|undefined,
	floor: undefined as HTMLElement|undefined,
	head: undefined as HTMLElement|undefined,
	slider: undefined as HTMLElement|undefined,
    avatarDisplay:{},
	log:undefined as HTMLElement|undefined,
	curAvatars: new Array(),
	onResize : function()
	{
	    var that = this;
	    that.w = that.pitch?.offsetWidth as number;
	    that.h = that.pitch?.offsetHeight as number;
	    if (that.w < 300)
	        setTimeout(that.onResize, 100);
	    else if (that.pitch && that.pitchData?.avatars && that.avatars?.style)
	        that.avatars.style.height = Math.floor(that.w * 4 / 11 * 1.20) + "px";
	},
	resizeTick()
	{
	    var that = this;
	    var w = document.getElementById("replay-pitch-floor-bg")?.clientWidth;
		var h = document.getElementById("replay-pitch-floor-bg")?.clientHeight;
		if(that.floor){
	    that.floor.style.width = w + "px";
		that.floor.style.height = h + "px";
		}
		if(that.avatars){
	    that.avatars.style.width = w + "px";
		that.avatars.style.height = h + "px";
		}
		if(that.head){
	    that.head.style.width = w + "px";
		that.head.style.height = h + "px";
		}

	    setTimeout(function () { that.resizeTick(); },500);

	}
    ,
	initialize : function(id:string,sliderId:string,logId:string,avatarId:string,data:any)
	{
		(document as any).replayManager = this;
	    var that = this;

        

		that.id = id;
		that.data = data;
		
		var tgt = document.getElementById(id);
		if(tgt)tgt.innerHTML="";
		that.pitch = document.createElement("div");
		that.pitch.id="pitch";
		that.pitch.className="replay-pitch";
		
		var pitchbg = document.createElement("img");
		pitchbg.src = "/gspy/gfx/replay/pitch.png";
		pitchbg.className = "replay-pitch-bg";
		pitchbg.id = "replay-pitch-floor-bg";
		that.pitch.appendChild(pitchbg);
		that.floor = document.createElement("div");
		that.floor.id="floor";
		that.floor.className="replay-pitch-floor";
		that.pitch.appendChild(that.floor);
		
		that.avatars = document.createElement("div");
		that.avatars.id="avatars";
		that.avatars.className="replay-avatars";
		that.pitch.appendChild(that.avatars);
		tgt?.appendChild(that.pitch);
		
		that.head = document.createElement("div");
		that.head.id="head";
		that.head.className="replay-pitch-head";
		that.pitch.appendChild(that.head);

		that.onResize();

		// create slider view
		let sl= document.getElementById(sliderId);
		if(sl)sl.innerHTML="";
		that.slider = sl?sl:new HTMLElement();

		var sliderTurns = "";

		
	    // create avatar view
		var avTgt = document.getElementById(avatarId);
		if(avTgt)
	    	avTgt.innerHTML = "";
	    var av = document.createElement("div");
	    av.id = "avatar-display";
	    av.className = "avatar-view";
	    
	    var avc = document.createElement("div");
	    av.appendChild(avc);
	    avTgt?.appendChild(av);
	    that.avatarDisplay = avc;

		// Create log
		var logTgt = document.getElementById(logId);
		var log = document.createElement("div");
		if(logTgt)
			logTgt.innerHTML=
			"<table style='width:100%;'><tbody><tr><td width='50%'><button class='replay-control' title='Previous step' onclick='document.replayManager.manPlay(-1);'>Previous</button>" +
			"</td><td width='50%'><button class='replay-control' title='Next step' onclick='document.replayManager.manPlay(1);'>Next</button></td></tr></tbody></table>";
	
		log.id="log";
		log.className="replay-log";
		logTgt?.appendChild(log);
		that.log = log;

		that.resizeTick();

		// Build ticks, turns etc from data
		let curTurnIndex=-1;
		let curTick=null;
		let lastTick = null;
		let curTurn=null;
		that.turns=[];
		that.ticks=[];
		for(var i=0; i < data.Replay.Items.length;i++)
		{
			var it = data.Replay.Items[i];
			if(it.AID == 1000) // EndTurn
			{
				if(curTick != null && curTick.items.length > 0)
					that.ticks.push(curTick);
				
				lastTick = curTick;
				curTick = null;
				curTurnIndex = 0+that.turns.length;
				curTurn = data.Replay.EndTurns[it.I];
				curTurn.firstTick = that.ticks.length;
				that.turns.push(curTurn);		
			
			}
			
			{
				// Is this a new player and not armor/inj/cas (and so a new Tick?)
				if(curTurn != null && (curTick==null || (curTick.PID != it.PID && it.AID != 3 && it.AID!=4 && it.AID != 8 && it.AID!=59 &&it.AID != 73)))
				{
					if(curTick!=null)
					{
						lastTick = curTick;
						if(curTick != null && curTick.items.length > 0)
						{
							that.ticks.push(curTick);
						}

					}
					curTick={ PID: it.PID, items :new Array(), turn: curTurnIndex, 
						moved : new Array(),  	// Players that moved this tick
						players:new Array(),  	// Players at tick start
						playPosMap:new Map<string,any>() ,	// "current" position of players (at start + moved)
						ballPosition: [curTurn.BallPosition.X, curTurn.BallPosition.Y],
						score : [0,0],
						curPlayer :{blitz:false,down:false,mbused:false,PID:it.PID},
						firstSubTick:+i
						};
					// If we got a new tick, then we need to create a new tick (and base it on the last tick)

					if(curTurn.Players)
					{
						// Setting  player status
						for(var m = 0; m < curTurn.Players.length;m++)
						{
						    var pl = curTurn.Players[m];
						    
							curTick.players.push({PID: pl.PID, pos : pl.Pos, blitz:pl.blitz, stun:pl.stun, ko:pl.ko, dead:pl.dead,cas:pl.cas, down:pl.down  }  );
							curTick.playPosMap.set(pl.PID,pl.Pos);
						}
					}

					if(lastTick!=null)
					{
						curTick.ballPosition = lastTick.ballPosition;
						curTick.score = [lastTick.score[0], lastTick.score[1]];
						if(curTurn.Players)
						{
							// Adjusting players state from last tick
							for(var m = 0; m < lastTick.players.length;m++)
							{
								var pl = lastTick.players[m];
								for(var p=0; p < curTick.players.length;p++)
								{
									var pl2 = curTick.players[p];
									if(pl.PID == pl2.PID)
									{
										if(it.AID != 1000)
										{
										    curTick.players[p] = { PID: pl.PID, pos: pl.pos, blitz: pl.blitz, stun: pl.stun, ko: pl.ko, dead: pl.dead, cas: pl.cas, down: pl.down, wasDown: pl.down, wasStunned: pl.wasStunned };
											curTick.playPosMap.set(pl.PID,pl.pos);
										}
										else {
											curTick.players[p].stun = pl.stun;
										}
										if(pl.ko || pl.dead || pl.cas)
										{
											curTick.players[p].pos=[-1,-1];
											curTick.playPosMap.set(pl.PID,[-1,-1]);
										}
										/*if (it.AID == 1000 && pl.stun)
										{
										    curTick.players[p].stun = false;
										    curTick.players[p].down = true;
										    curTick.players[p].wasStunned = true;

										}*/
										if(pl.down && it.AID==1000)
										{
										    curTick.players[p].down = true;
										    curTick.players[p].wasDown = true;
										}
									}
								}
							}
							// Adjusting players state from changes in the last tick
							for(var m = 0; m < lastTick.moved.length;m++)
							{
								var move = lastTick.moved[m];
								for(var p=0; p < curTick.players.length;p++)
								{
								    var pl = curTick.players[p];
								    if (pl.ko || pl.dead || pl.cas) {
								        curTick.players[p].pos = [-1, -1];
								        curTick.playPosMap.set(pl.PID,[-1, -1]);
								    }
									if(it.AID != 1000)
									{
										if(pl.PID == move.PID)
										{
										    curTick.players[p] = { PID: pl.PID, pos: move.pos, blitz: pl.blitz, stun: pl.stun, ko: pl.ko, dead: pl.dead, cas: pl.cas, down: pl.down, wasDown: pl.wasDown, wasStunned: pl.wasStunned };
											curTick.playPosMap.set(pl.PID,move.pos);
										}
									}
								}
							}
						}
					}
				}
				// Storing player state changes
				if(curTick!=null)
				{
					var curPlayer = {blitz:false,down:false,mbused:false, stun:false,ko:false,cas:false,dead:false, PID:"", wasStunned:false};
					var ballPos = null;
					for(var p =0; p < curTick.players.length;p++)
						{
							if(curTick.players[p].PID == it.PID)
							{
							    curPlayer = curTick.players[p];
                                if(!curTick.curPlayer)
							        curTick.curPlayer = curPlayer;
								break;
							}
						}
					
					switch(it.AID)
					{
					    case 1001: // touchdown
					        curTick.score[that.findPlayerData(it.PID).TeamIndex]++;
							curTick.items.push(it);
							for(var p =0; p < curTick.players.length;p++)
							{
								let pl = curTick.players[p];
								curTick.players[p] = { PID: pl.PID, pos: pl.pos, blitz: false, stun: false, ko: pl.ko, dead: pl.dead, cas: pl.cas, down: false, wasDown: false, wasStunned: false };
											
							}

							break;
						case 6:// Wake up
							curPlayer.stun = false;
							curPlayer.wasStunned = true;
							break;
						case 1003: // blitzes
						    curPlayer.blitz = true;
						    curPlayer.down = false;
							curTick.items.push(it);
							break;
						case 3: // armor
							curPlayer.down=true;
							curTick.items.push(it);

							var roll = data.Replay.Rolls[it.I];
							var sum = roll.Roll[0] + roll.Roll[1];
							if (sum == (roll.Req - 1) && curPlayer.mbused == false) {
							    curPlayer.mbused = true;
							    roll.Roll.push(1);
							    sum++;
							}
						    
							break;
						case 4: // Injury
							curPlayer.stun=true;
							var roll = data.Replay.Injuries[it.I];
							var sum = roll.Roll[0] + roll.Roll[1];
							if (curPlayer.mbused == false) {
							    curPlayer.mbused = true;
							    sum++;
							    roll.Roll.push(1);
							}

							var injres = sum;
							var plData = that.findPlayerData(curPlayer.PID);
							if (injres >= 8 || injres == 7 && plData.Skills && plData.Skills.indexOf("Stunty") >= 0)
							{
								curPlayer.stun=false;
								curPlayer.ko = true;
							}
							curTick.items.push(it);
							break;
						case 8: // cas
							curPlayer.stun=false;
							curPlayer.ko=false;
							var roll = data.Replay.Casualties[it.I];
							curPlayer.ko = false;
							if(roll.Roll[0] > 58)
								curPlayer.dead = true;
							else 
								curPlayer.cas = true;
							curTick.items.push(it);
							break;
						case 10: // Kickoff turn ended
							break;
							
					    case 5: // Block. Fix where block target is broken
					        curPlayer.down = false;
					        var plData = that.findPlayerData(curPlayer.PID);
					        if (plData.Skills && plData.Skills.indexOf("MightyBlow") >= 0)
					            curPlayer.mbused = false;


							var nexti=i;
							for(var ni = i+1; ni < data.Replay.Items.length;ni++)
							{
								var nitem = data.Replay.Items[ni];
								if((nitem.AID == 3 && nitem.PID != it.PID) ) // armor for enemy
								{
									data.Replay.Blocks[it.I].TID = nitem.PID;
									break;
								}
								
								 else if(nitem.AID==13) //push
								{
									var plsteps  = data.Replay.Moves[nitem.I].Steps;
									var first = plsteps[0];
									let pkRes=""
									for(let pk in curTick.playPosMap)
									{
										let pkKey = ""+(pk as string);
										var pl = curTick.playPosMap.get(pkKey); 
										if(pl[0] == first[0] && pl[1] == first[1] && pl!=null)
										{
											pkRes = pk;
											break;
										}
										
									}
									if(pkRes)
									{
										data.Replay.Blocks[it.I].TID = pkRes;
										break;
									}
									break;
								}
								if(nitem.PID != it.PID)
									break;
							}
							curTick.items.push(it);
							break;

					    case 1:// gfi
						case 2:// dodge
					    case 36: // leap
					        curPlayer.down = false;
					        // Create steps since none exists
					        var roll = that.data.Replay.Rolls[it.I];
					        if (it.AID == 1 && (i + 1) < data.Replay.Items.length && data.Replay.Items[i + 1].AID == 5 && data.Replay.Items[i + 1].PID == it.PID)
					        {
                                // Don't move if GFI to block
					        }
							else  {
							    curTick.moved.push({ PID: it.PID, pos: roll.Pos });
							    curTick.items.push(it);

							    // Only change play pos if we succeeed
							    if (roll.Roll >= roll.Req) {
							        curTick.playPosMap.set(it.PID,roll.Pos);
							    }
							    //curPlayer.pos = roll.Pos; // Move player draw position

							}
							break;
							
					    case 0: // Move
						case 14: // Follow
							if(curPlayer.stun){
								curPlayer.stun=false;
								curPlayer.wasStunned = true;
								curPlayer.down = true;
							}
							else{
							curPlayer.down = false;
							}
						    var steps = data.Replay.Moves[it.I].Steps;

						    var dist = (steps.length > 1) ? Math.abs(steps[1][0] - steps[0][0]) + Math.abs(steps[1][1] - steps[0][1]) : 0;
						    var totdist = (steps.length > 1) ? Math.abs(steps[steps.length - 1][0] - steps[0][0]) + Math.abs(steps[steps.length - 1][1] - steps[0][1]) : 0;
						 /*   if (curPlayer.stun) {
						        curPlayer.stun = false;
						        curPlayer.down = true;
						        curPlayer.wasStunned = true;
						    }*/

							if(it.AID == 14) 
							{
								// Follow logs a bit broken. First step must be changed to current pos sometimes...
//								console.log("Was "+steps[0]+" => "+steps[1]);
								var curPos = curTick.playPosMap.get(it.PID);
								if(steps[0][0] == steps[1][0] && steps[0][1] == steps[1][1])
									steps[0] = curPos;
								else
								{
									curPos = steps[0]; steps[0] = steps[1]; steps[1] = curPos;
								}
								steps[0] = curTick.playPosMap.get(it.PID);
	//							console.log("Is "+steps[0]+" => "+steps[1]);
							}
							
							
							{
							    var dist = steps.length > 1 ? (Math.abs(steps[1][0]-steps[0][0])+Math.abs(steps[1][1]-steps[0][1])) : 1;
								if(dist < 10)
								{
									curTick.moved.push({PID:it.PID, pos: steps[steps.length-1]});
									curTick.playPosMap.set(it.PID,steps[steps.length-1]);
									curTick.items.push(it);
								}
								
								
							}
							
							//    curPlayer.pos = curTick.playPosMap[it.PID]; // Move player draw position

							
							break;
						case 13: // Push
							// Pushing is not stored in the best way from BB2Stats
							// Need to first collect pushes and then handle them
							// Who's at target?
							var tgtPID="";
							var steps = data.Replay.Moves[it.I].Steps;
							var pushes = [data.Replay.Moves[it.I]];
							var push = pushes[0];
							//var tmp = push.Steps[0]; push.Steps[0] = push.Steps[1]; push.Steps[1] = tmp;
							//console.log("Pushing "+push.Steps);
							var nexti=i;
							for(var ni = i+1; ni < data.Replay.Items.length;ni++)
							{
								var nextItem = data.Replay.Items[ni];
								if(nextItem.AID != 13)
									break;
								
								push = data.Replay.Moves[nextItem.I];
								if(pushes.length > 0) // Fix for broken storing of pushes
										push.Steps[0] = [pushes[pushes.length-1].Steps[1][0],pushes[pushes.length-1].Steps[1][1]];
								//var tmp = push.Steps[0]; push.Steps[0] = push.Steps[1]; push.Steps[1] = tmp;
								
								//console.log("Pushing "+push.Steps);
								pushes.push(push);
								nexti = ni;
							}
							i=nexti;
							pushes = pushes.reverse(); // Reverse pushes to do the last one first like the game
							
							// Now handle the pushes
							for(var pi = 0; pi < pushes.length;pi++)
							{
								var step = pushes[pi];
								data.Replay.Moves[data.Replay.Items[i-pushes.length+pi+1].I] = step; // Store changed and reversed steps
								var substeps = step.Steps;
								//console.log("Pushing from "+substeps[0]+" to "+substeps[1]);
								tgtPID = "";
								var tgtpl = null;
								let players = curTick.players;
								curTick.playPosMap.forEach((val,pk, map)=>
								{
									var pl = map.get(pk); 
									
									if(pl[0] == substeps[0][0] && pl[1] == substeps[0][1])
									{
									    if (that.findPlayerData("" + pk))
									        //console.log("Found "+that.findPlayerData(""+pk).Name);
									    {
									        tgtPID = pk;
									        for(var tgti=0; tgti < players.length;tgti++)
									        {
									            
									            if(players[tgti].PID == tgtPID)
									            {
									                tgtpl = players[tgti]; 
									                break;
									            }
									        }
									    }
										    
									}
								});
								if(tgtPID!="" && substeps.length > 0)
								{
									var pu={PID:tgtPID, pos: substeps[substeps.length-1]};
									//console.log("Target "+that.findPlayerData(tgt).Name+" moved to "+substeps[substeps.length-1]);

									curTick.moved.push(pu);
									curTick.playPosMap.set(pu.PID , pu.pos);
								//	tgtpl.pos = pu.pos; // Move player draw position

								}
								else console.log("No target");
								
								curTick.items.push(data.Replay.Items[i-pushes.length+pi+1]);
							}
							break;
						case 1001: // TD
							curTick.items.push(it);
							break;
						case 11: // ball thrown in?
								steps = that.data.Replay.Moves[it.I].Steps;
								ballPos = steps[steps.length-1];
								curTick.ballPosition = ballPos;
						case 1005:
								if(!ballPos) 
								{
									ballPos = that.data.Replay.Balls[it.I].Pos; 
									curTick.ballPosition = [ballPos.X,ballPos.Y];
								}
								curTick.items.push(it);
							break;
						default:
							curTick.items.push(it);
							break;
					}
				}
			}
		}
		
		if(curTick != null && curTick.items.length > 0)
			that.ticks.push(curTick);
		curTurnIndex = 0;
		// Build log from ticks
		for(var i=0; i< that.ticks.length;i++)
		{
			var tick = that.ticks[i];
			var logrow = document.createElement("button");
			logrow.id = "log"+i;
			let clicker = i;
			logrow.onclick = ()=>{ that.gotoTick(clicker); } 
			logrow.className="replay-log-row";
			var str = "";
			var pl = that.findPlayerData(tick.PID);

			if(pl && pl.Name !=null)
				str = that.getPlayerString(pl)+" ";
			for(var j=0; j < tick.items.length;j++)
			{
				var item = tick.items[j];
				switch(item.AID)
				{
					case 0:	var steps = data.Replay.Moves[item.I].Steps; 
							
					   {

					       var dist = (steps.length > 1) ? Math.abs(steps[1][0] - steps[0][0]) + Math.abs(steps[1][1] - steps[0][1]) : 0;
					       var totdist = (steps.length > 1) ? Math.abs(steps[steps.length - 1][0] - steps[0][0]) + Math.abs(steps[steps.length - 1][1] - steps[0][1]) : 0;
					       var pltickdata = that.findPlayerTickData(tick,item.PID);
					       if (totdist == 0 && pltickdata && pltickdata.wasDown) {
					           if (pltickdata.wasStunned  || pltickdata.stun)
					           {
					               str += "Rolls over";
                               }
					           else
					               str += "Stands up";
					       }
					       else
					           if (dist <= 2 && totdist > 0)
					               str += "Move to " + that.getPosString(steps[steps.length - 1]);
					    }
							break;
				    case 1: str += "GFI " + that.getDiceRollString(data.Replay.Rolls[item.I]); break;//+data.Replay.Rolls[item.I].RR ? "":(" to "+that.getPosString(data.Replay.Rolls[item.I].Pos)); break;
				    case 2: str += "Dodge " + that.getDiceRollString(data.Replay.Rolls[item.I]); break;//+data.Replay.Rolls[item.I].RR ? "":(" to "+that.getPosString(data.Replay.Rolls[item.I].Pos)); break;
					case 3: str += "Armor "+that.getArmorRollString(data.Replay.Rolls[item.I],false);break;
				    case 4: var roll = data.Replay.Injuries[item.I]; str += "Injury " + that.getArmorRollString(data.Replay.Injuries[item.I],true);
				        break;
					case 5: var block=data.Replay.Blocks[item.I];  
					var tgtID = that.findPlayerData(block.TID);  
					str += "Block "+that.getPlayerString(tgtID)+" "+that.getBlockString(block); break; 
					case 6:  if (pltickdata.wasStunned || pltickdata.stun){ str += "Rolls over";} else {str += "Stands up";}break;
					case 7: str += "Pickup "+that.getDiceRollString(data.Replay.Rolls[item.I]);break;
					case 8: str += "Casualty " + that.getCasRollString(data.Replay.Casualties[item.I]);  break;
					case 9: str += "Catch "+that.getDiceRollString(data.Replay.Rolls[item.I]);break;
					case 10: str += "Kickoff scatter";break;
					case 11: var steps = data.Replay.Moves[item.I].Steps; str += "Ball thrown in "+that.getPosString(steps[steps.length-1]);break;
				    case 12: str += "Pass "+that.getDiceRollString(data.Replay.Rolls[item.I]);break;
					case 13:var steps = data.Replay.Moves[item.I].Steps; str += "Push "+" to "+that.getPosString(steps[steps.length-1]); ;break;  
				    case 14: var steps = data.Replay.Moves[item.I].Steps; str += "Follow to " + that.getPosString(steps[steps.length - 1]); break;
					case 15: 
							let rolls = data.Replay.Rolls[item.I];
							if(rolls?.Roll.length >= 2)
							{
								str += "Sent off!";
							}
							else{
								str += "Deceived referee";
							}

					 break;
					case 16: str += "Interception "+that.getDiceRollString(data.Replay.Rolls[item.I]); break;
					case 17: str += "KO roll "+that.getDiceRollString(data.Replay.Rolls[item.I]);break;
					case 18: str += "PITCH INVASION!";break;
					case 19: str += "TOUCH BACK!";break;
					case 20: str += "Bonehead " + that.getDiceRollString(data.Replay.Rolls[item.I]); break;
				    case 21: str += "Really stupid " + that.getDiceRollString(data.Replay.Rolls[item.I]); break;
				    case 22: str += "Wild animal " + that.getDiceRollString(data.Replay.Rolls[item.I]);  break;
				    case 23: str += "Loner " + that.getDiceRollString(data.Replay.Rolls[item.I]); break;
				    case 24: str += "Lands " + that.getDiceRollString(data.Replay.Rolls[item.I]); break;
				    case 25: str += "Regeneration " + that.getDiceRollString(data.Replay.Rolls[item.I]); break;
					case 26: // Scatter player
						break;
					case 27: str += "Hungry " + that.getDiceRollString(data.Replay.Rolls[item.I]); break;
					case 28: str += "Eat Team Mate " + that.getDiceRollString(data.Replay.Rolls[item.I]); break;
					case 29: str += "Dauntless " + that.getDiceRollString(data.Replay.Rolls[item.I]); break;
					case 30: str += "Safe throw " + that.getDiceRollString(data.Replay.Rolls[item.I]); break;
				    case 31: str += "Jump up " + that.getDiceRollString(data.Replay.Rolls[item.I]); break;
				    case 32: str += "Shadowing " + that.getDiceRollString(data.Replay.Rolls[item.I]); break;
				    case 33: str += "Dump off "; break;
				    case 34: str += "Stab " + that.getDiceRollString(data.Replay.Rolls[item.I]); break;
				    case 35: str += "Frenzy Stab " + that.getDiceRollString(data.Replay.Rolls[item.I]); break;
				    case 36: str += "Leap " + that.getDiceRollString(data.Replay.Rolls[item.I]); break;//+data.Replay.Rolls[item.I].RR ? "":(" to "+that.getPosString(data.Replay.Rolls[item.I].Pos)); break;
				    case 37: str += "Foul appearance " + that.getDiceRollString(data.Replay.Rolls[item.I]); break;
				    case 38: str += "Tentacles " + that.getDiceRollString(data.Replay.Rolls[item.I]); break;
				    case 39: str += "Chainsaw " + that.getDiceRollString(data.Replay.Rolls[item.I]); break;
					case 40: str += "Take root "+that.getDiceRollString(data.Replay.Rolls[item.I]);break;
					case 41: str += "Ball n chain "+that.getDiceRollString(data.Replay.Rolls[item.I]);break;
					case 42: str += "Hail mary pass "+that.getDiceRollString(data.Replay.Rolls[item.I]);break;
					case 43: str += "Piling on "+that.getDiceRollString(data.Replay.Rolls[item.I]);break;
					case 44: str += "Diving tackle "+that.getDiceRollString(data.Replay.Rolls[item.I]);break;
					case 45: str += "Pro "+that.getDiceRollString(data.Replay.Rolls[item.I]);break;
					case 46: str += "Hypnotic gaze "+that.getDiceRollString(data.Replay.Rolls[item.I]);break;
					case 47: str += "Kick off return "+that.getDiceRollString(data.Replay.Rolls[item.I]);break;
					case 48: str += "Pass block "+that.getDiceRollString(data.Replay.Rolls[item.I]);break;
					case 49: str += "Animosity "+that.getDiceRollString(data.Replay.Rolls[item.I]);break;
					case 50: str += "Bloodlust "+that.getDiceRollString(data.Replay.Rolls[item.I]);break;
				    case 51: str += "Bites! " + that.getDiceRollString(data.Replay.Rolls[item.I]); break;
				    case 52: str += "Bribe " + that.getDiceRollString(data.Replay.Rolls[item.I]); break;
					case 53: 
						if(data.Replay.Rolls[item.I].Roll.length == 3) // chef rolls three dices
						{
						str += "Halfling chef " + that.getDiceRollStringNoSuccess(data.Replay.Rolls[item.I]); 
						let rs = data.Replay.Rolls[item.I].Roll; let numRR =0;
						for(let ir = 0; ir < rs.length;ir++){
							if(rs[ir] >= 4){ numRR++;}
						}
						str += "<br/>"+numRR+" Rerolls gained";
					}
					break;
				    case 54: str += "Fireball " + that.getDiceRollString(data.Replay.Rolls[item.I]); break;
				    case 55: str += "Lightning " + that.getDiceRollString(data.Replay.Rolls[item.I]); break;
				    case 56: str += "Throw team mate " + that.getDiceRollString(data.Replay.Rolls[item.I]); break;
				    case 57: str += "Additional block " + that.getDiceRollString(data.Replay.Rolls[item.I]); break;
				    case 58: str += "Weather scatter"; break;
				    case 59: str += "Piling on armor " + that.getDiceRollString(data.Replay.Rolls[item.I]); break;
				    case 60: str += "Piling on injury " + that.getDiceRollString(data.Replay.Rolls[item.I]); break;
					case 61: str += "Wrestle "; break;
					case 62: // blodge
					str+="Blodge"
					break;
					case 63: // block stand firmed
					break;
					case 64: // Juggered
					str+="Juggernaut"
					break;
					case 65: // push stand firm
					str+="Stand firm"
					break;
					case 66: // Necromancer
					str += "Necromancer " + that.getDiceRollString(data.Replay.Rolls[item.I]); break;
					break;
					case 67: 
					str += "Nurgles rot " + that.getDiceRollString(data.Replay.Rolls[item.I]); break;
					break;
					case 68: // push sidestep
					str+="Sidestep"
					break;
					case 69: str += "Fans roll"; break;
				    case 70: str += "Weather roll"; break;
				    case 71: str += "Swealtering heat! " + that.getDiceRollString(data.Replay.Rolls[item.I]);break;
				    case 72: str += "Bomb hit! " + that.getDiceRollString(data.Replay.Rolls[item.I]);break;
					case 73: str += "Chainsaw armour " + that.getDiceRollString(data.Replay.Rolls[item.I]);break;
					case 74: str += "Hail mary bomb " + that.getDiceRollString(data.Replay.Rolls[item.I]);break;
				   					
				    case 1000:
				        logrow.className += " replay-log-turnrow";
				            var turnNum = data.Replay.EndTurns[item.I].Turn;
				            if (turnNum > 0) {
				                var teamindex = that.turns[tick.turn].NextTeam;
				                var team = data.TeamStats[teamindex];
				                str += "<div style='fontsize:22px;font-weight:bold;' class='replay-team"+(teamindex+1)+" newturn'>Turn " + data.Replay.EndTurns[item.I].Turn + " for " + team.TeamName + "</div>"; 
				                sliderTurns += "<td id='replay-turn-"+curTurnIndex+"' class='replay-sliderteam" + (1 + teamindex) + "'><a onclick='document.replayManager.gotoTurn(" + curTurnIndex + ");document.replayManager.manPlay(0);'>" + turnNum + "</a></td>";
				                
				            }
				            else {
				                sliderTurns += "<td  id='replay-turn-" + curTurnIndex + "' class='replay-sliderteam0'><a onclick='document.replayManager.gotoTurn(" + curTurnIndex + ");document.replayManager.manPlay(0);'>-</a></td>";
								let turndata = that.turns[tick.turn];
								var teamindex = that.turns[tick.turn].NextTeam;
				                var team = data.TeamStats[teamindex];
								str += "<div style='fontsize:22px;font-weight:bold;' class='replay-team"+(teamindex+1)+" newturn'>Preparations for " + team.TeamName + "</div>"; 
				            }
				            curTurnIndex++;
								break;
					case 1001: str += "<span style='fontsize:22px;font-weight:bold;'>Touchdown!</span>";break;
					case 1002: str += "Kickoff! ";
						var kickoff = data.Replay.Kickoffs[item.I];
						switch(kickoff.KickOffResult)
						{	
							case 0: str += "Get the ref!";break;
							case 1: str += "Riot!";break;
							case 2: str += "Perfect defense!";break;
							case 3: str += "High kick!";break;
							case 4: str += "Cheering fans!";break;
							case 5: str += "Changing weather!";break;
							case 6: str += "Brilliant coaching!";break;
							case 7: str += "Quick Snap!";break;
							case 8: str += "BLITZ!";break;
							case 9: str += "Throw a rock!";break;
							case 10: str += "Pitch invasion!";break;
							default:
								str += " Unknown kickoff: "+kickoff.KickOffResult+"";
								break;
						}					
						break;
					case 1003: str += "Blitz! <img className='row-blitz' src='/gspy/gfx/replay/blitz.png' />";break;
					case 1005: var pos = data.Replay.Balls[item.I].Pos; 
							   if(pos.X >= 0 && pos.Y >= 0)
								{ str += "Ball at " + that.getPosString([pos.X, pos.Y]); }
								break; 
					case 2000: str += "Fouls!"; break;
					case 2001: str += "Hands off the ball"; break;
					default: 
					str += "Unknown action ("+item.AID+")";break;
						
				}
				
				if(j < (tick.items.length-1))
					str += "<br/>";//<img src='/gspy/gfx/replay/separator.png' /> ";
			}
			logrow.innerHTML = str;
			that.log.appendChild(logrow);
		}


		let team1=data.TeamStats[0].TeamName;
		let team2 =data.TeamStats[1].TeamName;
	
		that.slider.innerHTML =
    "<table width=100% height=100% class='replay-slider'>" +
    "<tr><td width='50%' class='replay-header replay-team1 '>" + team1 + "</td><td nowrap><center class='replay-result' id='replay-result'>0 - 0</center></td><td  class='replay-header replay-team2 ' style='text-align:right;' >" + team2 + "</td></tr>" +
    "<tr><td style='width:100%;' colspan=3><table width=100% ><tr>" + sliderTurns + "</tr></table></td></tr>"+
        //"<tr><td colspan=3><center>"+
        //"<button class='replay-control' title='Rewind' onclick='replayManager.autoPlay(0);replayManager.gotoTurn(0);'>&lt&lt|</button>" +
        //"<button class='replay-control' title='Pause' onclick='replayManager.autoPlay(0);'>||</button>" +
        //"<button class='replay-control' title='Play' onclick='replayManager.autoPlay(1);'>&gt;</button>" +
        //"<button class='replay-control' title='Play fast' onclick='replayManager.autoPlay(2);'>&gt;&gt;</button>" +
        //"<button class='replay-control' title='Play faster' onclick='replayManager.autoPlay(3);'>&gt;&gt;>&gt;</button>" +
        //"<button class='replay-control' title='Previous step' onclick='document.replayManager.manPlay(-1);'>Previous</button>" +
        //"<button class='replay-control' title='Next step' onclick='document.replayManager.manPlay(1);'>Next</button>" +
        //"</center></td></tr>" +
		"</table>" +
        "";

		
		// Go to first turn
		if(window.location.href.indexOf("subtick=") >= 0){
			let tickstr = window.location.href.substr(window.location.href.indexOf("subtick=")+"subtick=".length);
			if(tickstr.indexOf("&") >= 0){ tickstr = tickstr.substr(0,tickstr.indexOf("&"));}
			let foundSubTick=false;
			for(let i=0; i < this.ticks.length;i++){
				let tick = this.ticks[i];
				if((tick.firstSubTick) >= +tickstr && i > 0){
					that.gotoTick((+i - 1));
					that.highlightRow((+i-1),true);
					foundSubTick =true;
					break;
				}
			}
			if(!foundSubTick){
				console.log("Unable to find the sub tick");
			}
			
		} else{
		that.gotoTurn(0,0); 
		}

		that.resizeTick();

	}
	
	,getDiceRollString : function(roll:{Req:number,RR:boolean,Roll:Array<number>})
	{
	    var that = this;
	    if (!roll)
	        return "";
		var str= roll.Req > 0 ? "("+roll.Req+"+): ":"(2+)";
		var res = 0;
		for (var i = 0; i < roll.Roll.length; i++) {
		    str += "<img class='replay-dice' title='" + roll.Roll[i] + "' src='/gspy/gfx/replay/dice_" + (roll.Roll[i] == 1 ? "red_" : "") + (roll.Roll[i] == 6 ? "green_" : "") + roll.Roll[i] + ".png' />" + " ";
		    res += roll.Roll[i];
		}
		if (that.isSuccess(roll.Req, res) == false)
		{
		    str += " <img title='Failure' src='/gspy/gfx/replay/fail.png' />";
		}
		else
		{
		    str += " <img title='success' src='/gspy/gfx/replay/ok.png' />";
		}
	
		if(roll.RR)
		{
		   // str += "<img src='/gspy/gfx/replay/separator.png' /><img src='/gspy/gfx/replay/reroll.png' />Reroll<img src='/gspy/gfx/replay/reroll.png' />";
		   str += "<img src='/gspy/gfx/replay/reroll.png' />Reroll<img src='/gspy/gfx/replay/reroll.png' />";
		}
		
		return str;
	}
	,getDiceRollStringNoSuccess : function(roll:{Req:number,RR:boolean,Roll:Array<number>}){
		var that = this;
	    if (!roll)
	        return "";
		var str= roll.Req > 0 ? "("+roll.Req+"+): ":"(2+)";
		var res = 0;
		for (var i = 0; i < roll.Roll.length; i++) {
		    str += "<img class='replay-dice' title='" + roll.Roll[i] + "' src='/gspy/gfx/replay/dice_" + (roll.Roll[i] == 1 ? "red_" : "") + (roll.Roll[i] == 6 ? "green_" : "") + roll.Roll[i] + ".png' />" + " ";
		    res += roll.Roll[i];
		}
		
		return str;
	}
    , getArmorRollString : function(roll:{Req:number,RR:boolean,Roll:Array<number>}, invertResult : boolean)
    {
		var str = roll.Req > 0 ? "(" + roll.Req + "+): " : "";
		let roller = 0;
        for (var i = 0; i < roll.Roll.length; i++) {
			roller += roll.Roll[i];
            str += "<img class='replay-dice' title='" + roll.Roll[i] + "' src='/gspy/gfx/replay/dice_" + (roll.Roll[i] == 1 ? "red_" : "") + (roll.Roll[i] == 6 ? "green_" : "") + roll.Roll[i] + ".png' />" + " ";
		}
		if((roller < roll.Req && !invertResult) || (invertResult && roller >= roll.Req))
		{
		    str += " <img title='Failure' src='/gspy/gfx/replay/fail.png' />";
		}
		else
		{
		    str += " <img title='success' src='/gspy/gfx/replay/ok.png' />";
		}

        return str;

    }
    , getCasRollString : function(roll:{Req:number,RR:boolean,Roll:Array<number>})
    {
        var str = roll.Req > 0 ? "(" + roll.Req + "+): " : "";
        var val = 0;
        for (var i = 0; i < 1; i++) {
            str += roll.Roll[i] + " ";
            val += roll.Roll[i];
		}
		
		

        if (val <= 40) str += "Badly Hurt "+"<img src='/gspy/"+injuryTypes.idToImg["BadlyHurt"]+"' />";
        else if (val >= 60) str += "Dead "+"<img src='/gspy/"+injuryTypes.idToImg["Dead"]+"' />";
        else {
            switch(val)
            {
                case 41: str += "Broken jaw (MNG)"+" <img src='/gspy/"+injuryTypes.idToImg["BrokenJaw"]+"' />"; break;
                case 42: str += "Broken ribs (MNG)"+" <img src='/gspy/"+injuryTypes.idToImg["BrokenJaw"]+"' />"; break;
                case 43: str += "Fractured arm (MNG)"+" <img src='/gspy/"+injuryTypes.idToImg["BrokenJaw"]+"' />"; break;
                case 44: str += "Fractured leg (MNG)"+" <img src='/gspy/"+injuryTypes.idToImg["BrokenJaw"]+"' />"; break;
                case 45: str += "Smashed hand (MNG)"+" <img src='/gspy/"+injuryTypes.idToImg["BrokenJaw"]+"' />"; break;
                case 46: str += "Gouged eye (MNG)"+" <img src='/gspy/"+injuryTypes.idToImg["BrokenJaw"]+"' />"; break;
                case 47: str += "Gron strain (MNG)"+" <img src='/gspy/"+injuryTypes.idToImg["BrokenJaw"]+"' />"; break;
                case 48: str += "Pinched nerve (MNG)"+" <img src='/gspy/"+injuryTypes.idToImg["BrokenJaw"]+"' />"; break;
                case 51: str += "Damaged back (NI)"+" <img src='/gspy/"+injuryTypes.idToImg["DamagedBack"]+"' />"; break;
                case 52: str += "Smashed knee (NI)"+" <img src='/gspy/"+injuryTypes.idToImg["DamagedBack"]+"' />"; break;
                case 53: str += "Smashed ankle (-1MV)"+" <img src='/gspy/"+injuryTypes.idToImg["SmashedHip"]+"' />"; break;
                case 54: str += "Smashed hip (-1MV)"+" <img src='/gspy/"+injuryTypes.idToImg["SmashedHip"]+"' />"; break;
                case 55: str += "Fractured skull (-1AV)"+" <img src='/gspy/"+injuryTypes.idToImg["FracturedSkull"]+"' />"; break;
                case 56: str += "Serious Concussion (-1AV)"+" <img src='/gspy/"+injuryTypes.idToImg["FracturedSkull"]+"' />"; break;
                case 57: str += "Broken neck (-1AG)"+" <img src='/gspy/"+injuryTypes.idToImg["BrokenNeck"]+"' />"; break;
                case 58: str += "Smashed collar bone (-1ST)"+" <img src='/gspy/"+injuryTypes.idToImg["SmashedCollarBone"]+"' />"; break;
            }
        }

        return str;
    }
	,getPlayerString : function(pl:{Name:string})
	{
		let name = pl.Name.replace("PLAYER_NAMES_CHAMPION_","");
		
		return "<span class='playername'>"+name+"</span>";
	}
	,getBlockString : function(bl:{Roll:Array<number>,RR:boolean,Dice:number,Red:boolean,})
	{
		var str="";
		for(var i=0; i < bl.Roll.length;i++)
		{
			str+="<img src='/gspy/gfx/replay/block"+(bl.Red?"_red_":"_")+bl.Roll[i]+".png' />";
		}
		if(bl.RR)
		{
		    str += "<img src='/gspy/gfx/replay/separator.png' /><img src='/gspy/gfx/replay/reroll.png' />Reroll<img src='/gspy/gfx/replay/reroll.png' />";
		}
		else
			str += " Select <img src='/gspy/gfx/replay/block_"+bl.Dice+".png' />"
		return str;
	}
	,getPosString: function(pos:Array<number>)
	{
		return "["+pos[0]+","+pos[1]+"]";
	}
    , manPlay: function (speed:number) {

        var that = this;
        that.curTickIndex += speed;
		that.gotoTick(that.curTickIndex);
		that.highlightRow(that.curTickIndex,true);
    },
     autoPlay : function(speed : number)
    {
            var that = this;
      //  clearTimeout(that.autoplaytimer);
        if(speed > 0)
        {
            that.performAutoTick(speed);
        }
    },
    performAutoTick : function(speed : number)
    {
        var that = this;
        that.curTickIndex++;
        that.gotoTick(that.curTickIndex);
   /*     that.autoplaytimer = setTimeout(function () {
            that.performAutoTick(speed);
        }, 500 * (5 - speed)) as NodeJS.Timeout;*/
    }

	,
	gotoTurn : function(nr:number, tick:number)
	{
		console.log("GOTO TURN "+nr+","+tick);
		
		
		var that = this;
		var turn = that.turns[nr];
		for (var i = 0; i < 1000; i++) { 
		    var rturn = document.getElementById('replay-turn-' + i);
		    if (!rturn)
		        break;
		    if (i == nr)
		        { rturn.className = rturn.className.replace(" selected","")+" selected"; console.log("New classname:"+rturn.className);}
		    else
			{ rturn.className = rturn.className.replace(" selected","");}
		}
 
		
		// Clean up all avatars and then create the ones for this turn
		for(var i=0;i < that.curAvatars.length;i++)
		{
			that.avatars?.childNodes.forEach((n)=>{
				that.avatars?.removeChild(n);
			});
		}
		that.curAvatars = [];
		for(var i=0;i < turn.Players.length;i++)
		{
			var pl = document.createElement("div"); 
			let pldata = turn.Players[i];
			pl.className="replay-avatar";
			pl.id="player"+pldata.PID;
			
			
			if(!that.findPlayerData(pldata.PID)) continue;
			var plImgWrap = document.createElement("div"); 
			plImgWrap.id =pldata.PID+".wrap";

			pl.appendChild(plImgWrap);
			that.avatars?.appendChild(pl);
			
			that.curAvatars.push(pl);
		}			
		if(tick == undefined)
			tick = turn.firstTick;
		that.curTickIndex = tick;
		this.showTick(tick);
	},
	gotoTick : function(nr:number)
	{
	    var that = this;
		//console.log(nr);
		if(nr < that.ticks.length){
		var tick = that.ticks[nr];
		if(tick)
			that.gotoTurn(0+tick.turn, nr);
		}
		else{
			console.log("ERROR, OUTSIDE OF TICKS")
		}
	},


	highlightRow(nr : number,jumpToRow : boolean){
		var that = this;
		// Highlight row
		for(var i=0; i < that.ticks.length;i++)
		{
		    var row = document.getElementById("log"+i);
			if(!row){continue;}
			if(i!=nr)row.className = row.className.replace("replay-selected","");
			else {
			    row.className = row.className + " replay-selected";
				var rect = row.getBoundingClientRect();
				var ownerRect = that.log?.getBoundingClientRect();
				if(that.log && ownerRect && jumpToRow)
				{
					that.log.scrollTop = rect.top+that.log?.scrollTop-ownerRect.top-40;
				}
			}
		}

	},
	showTick : function(nr:number)
	{
	    var that = this;


		// Clear all arrows, markers etc, so that we can create new ones
		if(that.head)
			that.head.innerHTML="";
		if(that.floor)
			that.floor.innerHTML="";
		
		var tick = that.ticks[nr];
		that.curTick = tick;
		var lastPlayerPos=new Map<string,Array<number>>();
		// Update players
	for(var i=0;i < tick.players.length;i++)
		{
			var pl = tick.players[i];
			
			that.updateAvatar(pl, tick.curPlayer && pl.PID == tick.curPlayer.PID);
			lastPlayerPos.set(pl.PID,pl.pos);
			
			//console.log("POS from "+that.findPlayerData(pl.PID).Name+" "+" pos: "+pl.pos);
		}		
		// Show markers
		var lastStep = [];
		var ballCaught = false;
		for(var j=0; j < tick.items.length;j++)
		{
		   


			var moveMarker=null;
			var item = tick.items[j];
			var imgName = undefined;
			var steps = [];



		    // Roll markers
			switch (item.AID) {
			    case 2:// dodge
			    case 36: // leap
			    case 1:// gfi
			        var roll = that.data.Replay.Rolls[item.I];
			        var p = lastPlayerPos.get(item.PID);
					var p2 = roll.Pos;
					if(p){
					let req = roll.Req; if(req < 0) req = 2; if(req > 6) req=6;
			        var img = that.createGfx("/gspy/gfx/replay/req" + req + ".png", (p[0] + p2[0]) / 2 + 0.5, (p[1] + p2[1]) / 2, 400, roll.Roll[0] >= roll.Req ? "replay-good" : "replay-bad", 0.3);
					that.head?.appendChild(img);
					}
			        break;

			    case 12: // pass
			        for (var q = 0; q < 1; q++)
			        {
			            var source = lastPlayerPos.get(item.PID);
			            var roll = that.data.Replay.Rolls[item.I];
						var target = [roll.Pos[0],roll.Pos[1]+q];
						if(source){
			            var passImg = that.createGfx("/gspy/gfx/replay/pass_arrow.png", source[0], source[1], 200, "pass",undefined);
			            that.floor?.appendChild(passImg);
			            var srcpx = that.getCoordinate(source[0], source[1],undefined,undefined);
			            var tgtpx = that.getCoordinate(target[0], target[1],undefined,undefined);
			            var dy = (tgtpx[1] - srcpx[1]) * 0.5; // *0.5 for some really strange reason
			            var dx = tgtpx[0] - srcpx[0];
			            var deg = 270 + Math.atan2(dy, dx) * 180 / Math.PI;
			            var l = Math.sqrt(dx * dx + dy * dy);
			            var lx = 0.4;//1 / l;
						l =  (l) / (3.9);
				

			            var midX = 26 / 2; midX /= lx;  // midX *= 1.25;
			            var midY = 26 / 2; midY /= l; // midY *= 0.35;
						
			            passImg.style.transform = "rotate(" + deg + "deg) scale(" + lx + "," + l + ") ";
			            passImg.style.transformOrigin = midX + "px " + midY + "px";
						passImg.style.transformOrigin = midX + "px " + midY + "px";
						}
			        }

			    case 7: // pickup
			    case 9: // catch
			    case 20: // bonehead
			    case 21: // stupid
			    case 22: // wild
			    case 23: // loner
			    case 25: // regeneration
			    case 29: // dauntless
			    case 30: // safe throw
			    case 31: // jump up
			    case 38: // tentacles
			    case 40: // root
			    case 46: // hypno
			    case 37: // disturbing
				case 45: // Pro
				case 50: // Bloodlust

			        var p = lastPlayerPos.get(item.PID);
			        var roll = that.data.Replay.Rolls[item.I];
					var req = roll.Req; if (req > 6) req = 6;
					if(p){
			        	var img = that.createGfx("/gspy/gfx/replay/req" + req + ".png", p[0]-0.25, p[1]+0.25, 400, that.isSuccess(req, roll.Roll[0]) ? "replay-good" : "replay-bad", 0.3);
						that.head?.appendChild(img);
					}
			        break;
			}


			switch(item.AID)
			{
			case 7: // pickup
			case 9: // catch
			    var roll = that.data.Replay.Rolls[item.I];
			    if (roll.Roll[0] >= roll.Req) {
			        ballCaught = true;
			    }
			        break;
			case 5: // Block
				var block = that.data.Replay.Blocks[item.I];
				var tgtId = block.TID;
				if(tgtId==-1||tgtId==item.PID)
					break;
				var tgtPos = lastPlayerPos.get(tgtId);
				var pos = lastPlayerPos.get(item.PID);
				//console.log("Block from "+that.findPlayerData(item.PID).Name+" "+" pos: "+pos+" ; "+that.findPlayerData(tgtId).Name+" "+tgtPos+" => "+"["+(pos[0]+tgtPos[0])/2+","+(pos[1]+tgtPos[1])/2+"]");
				if(pos&&tgtPos)
				{
					if(block.Dice > 0){
					var img = that.createGfx("/gspy/gfx/replay/block_"+block.Dice+".png", (pos[0]+tgtPos[0])/2,(pos[1]+tgtPos[1])/2, 300,"replay-block", 0.7);
					that.head?.appendChild(img);
					}
				}
				break;

			case 1:// gfi
			case 2:// dodge
			case 36: // leap
			    var roll = that.data.Replay.Rolls[item.I];
			    // Create steps since none exists
				steps[0] = lastPlayerPos.get(item.PID);
				steps[1] = that.data.Replay.Rolls[item.I].Pos;
				
				var roll = that.data.Replay.Rolls[item.I];
			case 14: // Follow	
				if(!moveMarker)
					moveMarker = "move";
			case 13: // Push				
				if(!moveMarker)
					moveMarker ="push";
			case 0: // Move
				if(!moveMarker)
					moveMarker="move";
				if(steps.length == 0)
				    steps = that.data.Replay.Moves[item.I].Steps;

//				var startimg = that.createGfx("gfx/replay/selected.png", steps[0][0], steps[0][1], 8, "startpoint",0.5);
//				that.floor.appendChild(startimg);

				if(!imgName) imgName = moveMarker+"_";
				if(steps.length==1){}
				else if(steps.length > 1)
				{
					lastStep = steps[0];
					for(var i=1; i < steps.length;i++)
					{
						var s = steps[i];
						var dx = lastStep[0]-s[0];
						var dy = lastStep[1] - s[1];
						if((dx==1||dx==-1||dx==0)&&(dy==-1||dy==1||dy==0))
						{
							var depth= 10;
							var newImg :undefined| HTMLImageElement = undefined;
							var flip="";
							if(dx==0 && dy==-1)
							newImg=that.createGfx("/gspy/gfx/replay/"+imgName+"u.png",s[0],lastStep[1],depth,moveMarker, undefined);
							else if(dx==0 && dy==1)
								{
									newImg=that.createGfx("/gspy/gfx/replay/"+imgName+"u.png",lastStep[0],s[1],depth,moveMarker, undefined);
								flip = "flipY";
								}
							else if(dx==-1 && dy==0)
							newImg=that.createGfx("/gspy/gfx/replay/"+imgName+"r.png",lastStep[0],s[1],depth,moveMarker, undefined);
							else if(dx==1 && dy==0)
								{
									newImg=that.createGfx("/gspy/gfx/replay/"+imgName+"r.png",s[0],s[1],depth,moveMarker, undefined);
								flip = "flipX";
								}
							else if(dx==-1 && dy==-1)
								{
									newImg=that.createGfx("/gspy/gfx/replay/"+imgName+"ur.png",lastStep[0],lastStep[1],depth,moveMarker, undefined);
								}
							else if(dx==1 && dy==-1)
								{
									newImg=that.createGfx("/gspy/gfx/replay/"+imgName+"ur.png",s[0],lastStep[1],depth,moveMarker, undefined);
								flip = "flipX";
								}
							else if(dx==-1 && dy==1)
								{
									newImg=that.createGfx("/gspy/gfx/replay/"+imgName+"ur.png",lastStep[0],s[1],depth,moveMarker, undefined);
								flip = "flipY";
								}
							else if(dx==1 && dy==1)
								{
									newImg=that.createGfx("/gspy/gfx/replay/"+imgName+"ur.png",s[0],s[1],depth,moveMarker, undefined);
								flip = "flipXY";
								}

							if(newImg)
							{
								newImg.className+=" "+flip;
								that.floor?.appendChild(newImg);
							}
						}
						lastStep = s;
						lastPlayerPos.set(item.PID,lastStep);
					}
				}
				break;
			}


		    
		}
		// Ball
		if(tick.ballPosition)
		{
		    var ballInHand = false;
			var ballpos = tick.ballPosition;
			for(var i=0; i < tick.players.length;i++)
			{
				var pl = tick.players[i];
				var p2 = tick.playPosMap.get(pl.PID);
				if(p2){
				if(p2[0] == ballpos[0] && p2[1] == ballpos[1])
				{
					ballInHand=true;
					break;
				}
			}
				
			}
			if(ballpos){
			var img = that.createGfx("/gspy/gfx/replay/ball"+(!ballInHand?"_loose":"")+".png", ballpos[0],ballpos[1], 300,"ball", undefined);
			that.head?.appendChild(img);
			}
		}
		
		that.highlightRow(nr,false);

		let avatarDisplay = document.getElementById("avatar-display");
		if (this.curTick.curPlayer && avatarDisplay && that.findGamePlayerData(this.curTick.curPlayer.PID))
					avatarDisplay.innerHTML = that.getAvatarCard(this.curTick.curPlayer.PID) as string;
		else if(avatarDisplay) avatarDisplay.innerHTML = "";

		let replayRes = document.getElementById("replay-result");
		if(replayRes)
		replayRes.innerHTML = that.curTick.score[0] + " - " + that.curTick.score[1];

	},
	isSuccess : function(req:number,roll:number) 
	{
	    if (roll != 1 && roll >= req || roll == 6)
	        return true;
	    return false;

	},
	getAvatarCard : function(playerid:string)
	{
	    var that = this;
	    var str = "";
	    var plData = that.findPlayerData(playerid);
	    var plGameData = that.findGamePlayerData(playerid);
	    if (!plGameData)
			return "";
			
	    str += "<div class='avatar-card'>";
	    str += "<table width=100%>";
	    str += "<tr><th style='text-align:center;' colspan='2' >" + plGameData.Name.replace("PLAYER_NAMES_CHAMPION","") + "</th></tr>";
	    str += "<tr><td></td><td>" + plGameData.PlayerTypeName?.split("_")[1] + "</td></tr>";
	    str += "<tr><td></td><td>Ma: " + plGameData.Ma + " St: " + plGameData.St + " Ag: " + plGameData.Ag + " Av: " + plGameData.Av + "</td></tr>";
		str += "<tr><td></td><td>";
		if(plGameData.Skills){
	    for (var j = 0; j < plGameData.Skills.length; j++)
	    {
	        str += "<img title='" + plGameData.Skills[j] + "' class='replay-skill-image' src='/gspy/gfx/skills/" + plGameData.Skills[j] + ".png' />";
		}
	}
	    str += "</td></tr>";
	    str += "</table>";
	    str += "</div>";
	    return str;

	},
	// Update position img etc of given avatar element
	updateAvatar : function(pldata:{PID:string,pos:Array<number>, down:boolean,blitz:boolean, stun:boolean, cas:boolean,ko:boolean, dead:boolean}, isSelected:boolean)
	{

	    var avInfo = document.getElementById("avatar-popup");
	    if (avInfo) avInfo.style.display = "none";

		var id = pldata.PID;
		var pos = pldata.pos;
		var that = this;
		var item = document.getElementById("player"+id);
		var pl = that.findPlayerData(id);
		var gamePlayer = that.findGamePlayerData(id);
		if(item){
	    item.onmouseenter = function()
	    {
			if(!that.findGamePlayerData(id))return;

	        var avInfo = document.getElementById("avatar-popup");
	        if(!avInfo)
	        {
	            avInfo = document.createElement("span");
	            avInfo.id = "avatar-popup";
	            avInfo.style.position = "relative";
	            avInfo.style.width = "200px";
	            avInfo.style.height = "150px";
	            avInfo.style.borderRadius = "10px";
	            avInfo.style.margin = "3";
	            avInfo.style.zIndex = ""+2000;
	            avInfo.className = "avatar-view";
	            that.avatars?.appendChild(avInfo);
	        }
	        var ppos = that.getCoordinate(pos[0],pos[1],undefined, undefined);
	        avInfo.style.left = ppos[0] + "%";
	        avInfo.style.top = ppos[1] + "%";
	        avInfo.innerHTML = that.getAvatarCard(id) as string;
	        avInfo.style.display = "block";
	        clearTimeout(that.avinfoTimer);
	        that.avinfoTimer =  <any>setTimeout(function () {
	            var avInfo = document.getElementById("avatar-popup");
	            if (avInfo) avInfo.style.display = "none";
	        }, 5000);
	    }
	    

		var img = document.getElementById(pl.PlayerId+".mainimg") as HTMLImageElement;
		if(img){
		img.src="/gspy/gfx/replay/players/"+pl.PlayerType+pl.TeamIndex+(pldata.down?"down":"")+".png";
		}
		var plImgWrap = document.getElementById(pl.PlayerId+".wrap");
		if(plImgWrap){
			//(plImgWrap as HTMLObjectElement).data = "/gspy/gfx/replay/players/player" + (pl.TeamIndex==0?1:0) + (pldata.down ? "down" : "") + (gamePlayer && gamePlayer.St < 3 ? "_small" : "") + (gamePlayer && gamePlayer.St == 4 ? "_big" : "") + (gamePlayer && gamePlayer.St > 4 ? "_huge" : "") + ".png";
			
			(plImgWrap as HTMLObjectElement).data = (pldata.down)?"/gspy/gfx/replay/player_down.png":"";
			plImgWrap.className="teamplayer"+pl.TeamIndex+" "+(pldata.down?"playerdown":"")+" playertype"+pl.PlayerType+" playersize"+(gamePlayer && gamePlayer.St < 3 ? "_small" : "") + (gamePlayer && gamePlayer.St == 4 ? "_big" : "") + (gamePlayer && gamePlayer.St > 4 ? "_huge" : "");
		} 
		
		var coords =  that.getCoordinate(pos[0], pos[1],undefined,undefined);
		var left = 1.6;
		var top = 2.9;
		var w = 2.4; var h=4;
		switch(pl.PlayerType+pl.TeamIndex)
		{
			default: 
				break;
		}
		var baseScale = 1.4;
		//left *= baseScale;
		//top *= baseScale;
		w *= baseScale;
		h *= baseScale;
		
		
		var size = that.getSize(14-pos[1]) / that.getSize(7.5);
		item.style.width = size*w+"%";
		item.style.height = size * h + "%";
		//item.style.zoom = size;
		item.style.left = (coords[0]-left)+"%";
		item.style.top = (coords[1]-top)+"%";
		item.style.display=(pos[0] < 0)?"none":"block";
		item.style.zIndex = ""+(100 + pos[1]);

		if (isSelected && plImgWrap)
		{
		    plImgWrap.className = plImgWrap.className + "  replay-avatar-selected";
		}

		if(pldata.blitz)
		{
			var blitzImg = document.getElementById(pl.PlayerId+".blitz") as HTMLImageElement;
			if(!blitzImg)
			{
				blitzImg = document.createElement("img");
				blitzImg.id = pl.PlayerId+".blitz";
				blitzImg.src="/gspy/gfx/replay/blitz.png";
				blitzImg.style.zIndex=""+400;
				blitzImg.style.position = "absolute";
				blitzImg.style.width = "80%";
				blitzImg.style.height = "80%";
				blitzImg.style.left = w / 2 + "%";
				blitzImg.style.left = w / 2 + "%";
				blitzImg.style.top = 4* Math.floor(2 * h) + "%";
				item.appendChild(blitzImg);
			}
		}
		if(pldata.stun || pldata.ko || pldata.cas || pldata.dead)
		{
			var injImg = document.getElementById(pl.PlayerId+".inj") as HTMLImageElement;
			if(!injImg)
			{
				injImg = document.createElement("img");
				injImg.id = pl.PlayerId+".inj";
				injImg.style.zIndex=""+400;
				injImg.style.position = "absolute";
				//injImg.style.zoom = size;
				injImg.style.left = w + "%";
				injImg.style.width = "80%";
				injImg.style.height = "80%";
				injImg.style.top = Math.floor(2*h)+"%";
				injImg.className="injuryimage"; 
				item.appendChild(injImg);
			}
			if(pldata.stun) injImg.src="/gspy/gfx/replay/player_stunned.png";
			if(pldata.ko) injImg.src="/gspy/gfx/replay/player_ko.png";
			if(pldata.cas) injImg.src="/gspy/gfx/replay/player_injured.png";
			if(pldata.dead) injImg.src="/gspy/gfx/replay/player_dead.png";
		}
	}		
 
	},
	findGamePlayerData : function(id:string) : undefined|{St:number,Av:number,Ma:number,Ag:number,Name:string,PlayerTypeName:string,Skills:Array<string>}
	{
	    var that = this;
	    for (var i = 0; i < that.data.TeamStats.length; i++)
	    {
			var t = that.data.TeamStats[i];
			if(t && t.PlayerStats){
	        for(var j=0; j < t.PlayerStats.length;j++)
	        {
	            var pl = t.PlayerStats[j];
	            if (pl.PlayerId == id)
	                return pl;
			}
		}else console.log(t);
	    } 
	    return undefined;
	},
	findPlayerData : function(id:string)
	{
		var that = this;
		for(var t=0; t < that.data.TeamStats.length;t++)
		{
			for(var i=0; i<  that.data.TeamStats[t].PlayerStats.length;i++)
			{
				var pl = that.data.TeamStats[t].PlayerStats[i];
				pl.TeamIndex = t;
				if(pl.PlayerId == id)
					return pl;
			}
		}
	},
	findPlayerTickData: function (tick:{players:Array<any>},id:string) {
	    var that = this;
	    for (var i = 0; i < tick.players.length; i++) {
	        var pl = tick.players[i];
	            if (pl.PID == id)
	                return pl;
	        }
	},
	getSize : function(z:number)
	{
	    return 1;
	},
	getCoordinate : function(x:number,y:number, szx:number|undefined,szy:number|undefined)
	{
		//y = 14-y;
		var that=this;
		var xm = (-12.5+x);
		var ym = (7-y);
		var x2 = xm * (!szx ? that.getSize(y):szx);
		var y2 = ym * (!szy ? that.getSize(y):szy);
		
		return [x2*3.85+50, y2*6.6 + 50];
	},
	createGfx : function (src:string, x:number, y:number, lvl:number, className:string, zoom:number|undefined)
	{

		var that = this;
		var item= document.createElement("img");
		item.className = className;
		item.src=src;
		item.style.position="absolute";
		var pos=[x,y];
		
		var coords =  [];
		var left = 0.1;
		var top = 2.6;
		var w = 3; 
		var h=5;
		var baseScale = 2;
		coords = that.getCoordinate(pos[0], pos[1], undefined,undefined);

		switch(className)
		{
			case "move":
		    case "push":
		        baseScale = 2.5;
		        left = 0.7; top = 3.1;
		        break;
		    case "pass":
		        baseScale = 2;
		        left= 1; top= -0.2;
				break;
		    case "replay-block":
		        baseScale = 1.2;
		        top= 1.5; left=1.5;
		        break;
		    case "ball":
		        baseScale = 1;
		        left = 1; top = 1.5;
				break;
		    case "startpoint":
		        break;
			default: 
				break;
		}
		if (zoom) {
		    baseScale *= zoom;
		}
		left *= baseScale;
		top *= baseScale;
		w *= baseScale;
		h *= baseScale;

		var size = 1;// that.getSize(14-pos[1]) / that.getSize(7.5);
		item.style.width = w+"%";
		item.style.height = h+"%";
		//item.style.zoom = size;
		item.style.left = (coords[0]-left)+"%";
		item.style.top = (coords[1]-top)+"%";
		item.style.display=(pos[0] < 0)?"none":"block";
		
		item.style.zIndex = ""+parseInt((""+lvl) + (""+pos[1]));
		return item;
	}
}
	};

 
	