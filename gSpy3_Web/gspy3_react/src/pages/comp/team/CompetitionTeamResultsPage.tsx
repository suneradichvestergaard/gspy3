
import React  from 'react';
import GSpyTable from "../../../components/GSpyTable";
import {ColumnLayout} from "../../../api/views/columns";
import { QueryPageResponse} from "../../../api/queryPageRequests"
import DataView from "../../../api/views/dataView"
import BaseComponent,{BaseComponentProps, BaseComponentState} from "../../../components/BaseComponent"

interface CompetitionTeamResultsState extends BaseComponentState {
  compteammatches : QueryPageResponse
  };
interface CompetitionTeamResultsProps extends BaseComponentProps  {
};

class  CompetitionTeamResultsPage extends BaseComponent<CompetitionTeamResultsProps,CompetitionTeamResultsState> {

  constructor(props : CompetitionTeamResultsProps){ 
    super(props);

    this.views = [DataView.comp, DataView.compTeamMatches];
    this.onUpdate = this.onUpdate.bind(this);
     
  }

  onUpdate(){
    super.onUpdate();
    this.setState(
      {
        compteammatches:this.props.selections.responses.response[DataView[DataView.compTeamMatches]],
      })
  }

  render () : React.ReactElement {
    
   if(!this.state){return <div></div>}
   return <div className="panel">
      <div className="flexcontainer"> 
          <div className="panel">
            <GSpyTable source={this.state.compteammatches} layout={ColumnLayout.CompTeamMatches} filters={this.props.selections.filters}></GSpyTable>
          </div>
        </div>
      
      </div>
  }
}

export default CompetitionTeamResultsPage