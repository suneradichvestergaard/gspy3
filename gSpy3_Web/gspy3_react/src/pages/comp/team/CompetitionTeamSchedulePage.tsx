
import React  from 'react';
import GSpyTable from "../../../components/GSpyTable";
import {ColumnLayout} from "../../../api/views/columns";
import { QueryPageResponse} from "../../../api/queryPageRequests"
import DataView from "../../../api/views/dataView"
import BaseComponent,{BaseComponentProps, BaseComponentState} from "../../../components/BaseComponent"

interface CompetitionTeamScheduleState extends BaseComponentState {
  compteamschedule : QueryPageResponse
  };
interface CompetitionTeamScheduleProps extends BaseComponentProps  {
};

class  CompetitionTeamSchedulePage extends BaseComponent<CompetitionTeamScheduleProps,CompetitionTeamScheduleState> {

  constructor(props : CompetitionTeamScheduleProps){ 
    super(props);

    this.views = [DataView.comp, DataView.compTeamSchedule];
    this.onUpdate = this.onUpdate.bind(this);
     
  }

  onUpdate(){
    super.onUpdate();
    this.setState(
      {
        compteamschedule:this.props.selections.responses.response[DataView[DataView.compTeamSchedule]],
      })
  }

  render () : React.ReactElement {
    
   if(!this.state){return <div></div>}
   if(this.state.compteamschedule?.result?.rows?.length==0){
    return <div>Nothing scheduled</div>
  }

   return <div className="panel">
      <div className="flexcontainer"> 
          {this.state.compteamschedule?.result?.rows?.length>0 && <div className="panel">
            <GSpyTable source={this.state.compteamschedule} layout={ColumnLayout.CompTeamSchedule} filters={this.props.selections.filters}></GSpyTable>
          </div> 
          }
        </div>
      
      </div>
  }
}

export default CompetitionTeamSchedulePage