
import React  from 'react';
import GSpyTable from "../../../components/GSpyTable";
import {ColumnLayout} from "../../../api/views/columns";
import { QueryPageResponse} from "../../../api/queryPageRequests"
import DataView from "../../../api/views/dataView"
import BaseComponent,{BaseComponentProps, BaseComponentState} from "../../../components/BaseComponent"

interface CompetitionTeamState extends BaseComponentState {
  compteamwstats : QueryPageResponse
  compteamplayers :QueryPageResponse
  compteamschedule : QueryPageResponse 
  compteammatches : QueryPageResponse
  };
interface CompetitionTeamProps extends BaseComponentProps  {
};

class  CompetitionTeamPage extends BaseComponent<CompetitionTeamProps,CompetitionTeamState> {

  constructor(props : CompetitionTeamProps){ 
    super(props);

    this.views = [DataView.comp,DataView.team,DataView.compTeamwStats,  DataView.compTeamPlayers, ];
    this.onUpdate = this.onUpdate.bind(this);
     
  }

  onUpdate(){
    super.onUpdate();
    this.setState(
      {
        compteamwstats:this.props.selections.responses.response[DataView[DataView.compTeamwStats]],
        compteamplayers:this.props.selections.responses.response[DataView[DataView.compTeamPlayers]],
      })
  }

  render () : React.ReactElement {
    
   if(!this.state){return <div></div>}
   return <div className="panel">
      <div className="flexcontainer"> 
          <div className="panel">
            <GSpyTable source={this.state.compteamwstats} layout={ColumnLayout.CompTeamView} filters={this.props.selections.filters}></GSpyTable>
          </div>
          <div className="panel">
            <GSpyTable source={this.state.compteamplayers} layout={ColumnLayout.CompTeamPlayerView} filters={this.props.selections.filters}></GSpyTable>
          </div>
        </div>
      
      </div>
  }
}

export default CompetitionTeamPage