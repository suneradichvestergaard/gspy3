
import React  from 'react';
import GSpyTable from "../../../components/GSpyTable";
import {ColumnID, ColumnLayout} from "../../../api/views/columns";
import { QueryPageResponse} from "../../../api/queryPageRequests"
import DataView from "../../../api/views/dataView"
import BaseComponent,{BaseComponentProps, BaseComponentState} from "../../../components/BaseComponent"
import Rank from "../../../managers/rank"

interface CompetitionTeamStandingState extends BaseComponentState {
  compTeamStanding : QueryPageResponse
  };
interface CompetitionTeamStandingProps extends BaseComponentProps  {
};

class  CompetitionTeamStandingPage extends BaseComponent<CompetitionTeamStandingProps,CompetitionTeamStandingState> {

  constructor(props : CompetitionTeamStandingProps){ 
    super(props);

    this.views = [DataView.comp,DataView.team, DataView.compTeamStanding];
    this.onUpdate = this.onUpdate.bind(this);
     
  }

  onUpdate(){
    super.onUpdate();
    let format = 0;
    let sorting = "";
    let compres = this.props.selections.responses.response[DataView[DataView.comp]]
    if(compres && compres.result.rows?.length> 0){
      format = +compres.result.rows[0][compres.result.cols["format"]]
      sorting =compres.result.rows[0][compres.result.cols["sorting"]]
    }
    if(this.props.selections.responses.response[DataView[DataView.compTeamStanding]]){
    let localCopy : QueryPageResponse = JSON.parse(JSON.stringify(this.props.selections.responses.response[DataView[DataView.compTeamStanding]]))

    if(!sorting || sorting=="" || sorting=="rank"){
      if(localCopy.result.rows.length > 0){
        let colkeys = Object.keys(localCopy.result.cols);
        localCopy.result.cols["overtake"] = localCopy.result.rows[0].length;
        let cols = localCopy.result.cols;
        let srcrow = new Array()
        let markedValue = "";
        if(localCopy?.request?.idmap){ markedValue = localCopy?.request?.idmap[ColumnID[ColumnID.idteam]]}

        for(let i=0; i < localCopy.result.rows.length;i++){
          if(localCopy.result.rows[i][cols["idteam"]] == markedValue){
            srcrow=localCopy.result.rows[i];
            break;
          }
        }

        for(let i=0; i < localCopy.result.rows.length;i++){
          let overtake = "";
          let reversed = (+localCopy.result.rows[i][cols["ranking"]] < +srcrow[cols["ranking"]])
          let qrow = reversed ? srcrow : localCopy.result.rows[i];
          let qrow2 = reversed? localCopy.result.rows[i] : srcrow;
          let targetRank = +qrow[cols["ranking"]]
          let curRank = +(qrow2[cols["ranking"]])
          let curWins = +(qrow2[cols["wins"]])
          let curDraws = +(qrow2[cols["draws"]]);
          let curLosses = +(qrow2[cols["losses"]]);
          let curConcedes =+(qrow2[cols["concedes"]]);
          let  wins = curWins;
          
          let newRank = 0;
          for (let i = 1; i < 100; i++) 
          {
              wins = curWins + i;
              newRank = Rank.calculateRank(wins, curDraws, curLosses, curConcedes);
              if(newRank > targetRank)
              {
                  overtake= targetRank == curRank ? "" : ((reversed?"-":"+")+(wins-curWins));
                  break;
              }
      
          }

          localCopy.result.rows[i].push(overtake);
        }
      }
      }
    


    this.setState(
      {
        compTeamStanding:localCopy,
      })
    }
  }

  render () : React.ReactElement {
    
   if(!this.state){return <div></div>}
   if(this.state.compTeamStanding?.result?.rows?.length==0){
    return <div>No standing available</div>
  }
  let markedValue=""
  if(this.state?.compTeamStanding?.request && this.state.compTeamStanding?.request?.idmap){
        markedValue = this.state.compTeamStanding?.request?.idmap[ColumnID[ColumnID.idteam]]
       
  }

   return <div className="panel">
      <div className="flexcontainer"> 
          {this.state.compTeamStanding?.result?.rows?.length>0 && <div className="panel">
            <GSpyTable marked={{idcolumn:ColumnID.idteam,value:markedValue}} source={this.state.compTeamStanding} layout={ColumnLayout.CompTeamOvertakeStanding} filters={this.props.selections.filters}></GSpyTable>
          </div> 
          }
        </div>
      
      </div>
  }
}

export default CompetitionTeamStandingPage