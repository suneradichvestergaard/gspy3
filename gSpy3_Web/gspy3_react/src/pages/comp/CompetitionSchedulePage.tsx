
import React  from 'react';
import GSpyTable from "../../components/GSpyTable"
import {ColumnID, ColumnLayout} from "../../api/views/columns"
import { QueryPageResponse} from "../../api/queryPageRequests"
import DataView from "../../api/views/dataView"
import BaseComponent,{BaseComponentProps, BaseComponentState} from "../../components/BaseComponent"
import { throws } from 'assert';
 
interface ScheduleState extends BaseComponentState  {
  compSchedule : QueryPageResponse

  };
interface ScheduleProps extends BaseComponentProps  {

};

class  CompetitionSchedulePage extends BaseComponent<ScheduleProps,ScheduleState> {

    
  constructor(props : ScheduleProps){  
    super(props);

    this.views = [DataView.comp,DataView.compSchedules];
    this.onUpdate = this.onUpdate.bind(this);
  }


  onUpdate(){
    super.onUpdate();

    let sched = this.props.selections.responses.response[DataView[DataView.compSchedules]]?.result;
    let idmatchcol = sched?.cols[ColumnID[ColumnID.idmatch]]
    for(let i=0; i < sched?.rows.length;i++)
    {
      if((+sched.rows[i][idmatchcol])>0){
          sched.rows.splice(i,1);
          i--;
      }

    }
    this.setState(
      {
        compSchedule:this.props.selections.responses.response[DataView[DataView.compSchedules]],
      });
  }
  

  render () : React.ReactElement {

    if(!this.state) return <div></div>;
    if(this.state.compSchedule?.result?.rows?.length==0){
      return <div>Nothing scheduled</div>
    }
    return  <div className="flexcontainer">
          <div className="panel">
            <GSpyTable source={this.state.compSchedule} layout={ColumnLayout.CompTeamSchedule} filters={this.props.selections.filters}></GSpyTable>
          </div>
        
          </div>
      
  }
}

export default CompetitionSchedulePage