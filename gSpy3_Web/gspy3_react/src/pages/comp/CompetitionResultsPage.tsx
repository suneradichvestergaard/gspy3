
import React  from 'react';
import GSpyTable from "../../components/GSpyTable"
import {ColumnLayout} from "../../api/views/columns"
import { QueryPageResponse} from "../../api/queryPageRequests"
import DataView from "../../api/views/dataView"
import BaseComponent,{BaseComponentProps, BaseComponentState} from "../../components/BaseComponent"

interface ResultsState extends BaseComponentState  {
  compresults : QueryPageResponse

  };
interface ResultsProps extends BaseComponentProps  {

};

class  CompetitionResultsPage extends BaseComponent<ResultsProps,ResultsState> {

    
  constructor(props : ResultsProps){  
    super(props);

    this.views = [DataView.comp,DataView.compResults];
    this.onUpdate = this.onUpdate.bind(this);
  }


  onUpdate(){
    super.onUpdate();
    this.setState(
      {
        compresults:this.props.selections.responses.response[DataView[DataView.compResults]],
      });
  }
  

  render () : React.ReactElement {

    if(!this.state) return <div></div>;
   
    return  <div className="flexcontainer">
          <div className="panel">
            <GSpyTable source={this.state.compresults} layout={ColumnLayout.CompResultView} filters={this.props.selections.filters}></GSpyTable>
          </div>
          
          </div>
      
  }
}

export default CompetitionResultsPage