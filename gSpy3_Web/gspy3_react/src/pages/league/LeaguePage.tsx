
import React  from 'react';
import GSpyTable from "../../components/GSpyTable"
import {ColumnLayout, ColumnID} from "../../api/views/columns"
import { QueryPageResponse} from "../../api/queryPageRequests"
import DataView from "../../api/views/dataView"
import BaseComponent,{BaseComponentProps, BaseComponentState} from "../../components/BaseComponent"
import cogimg from "../../gfx/cog.png"
import HelpPanel from '../../components/HelpPanel';
import messageManager, { PopupTypeEnum } from '../../managers/messageManager';
import GSpy3_api from "../../api/gSpy3_api"

interface LeagueState extends BaseComponentState  {
  league : QueryPageResponse
  leaguecomps : QueryPageResponse
  localBackup : QueryPageResponse;
  showLeagueMenu : boolean
  };
interface LeagueProps extends BaseComponentProps  {

};


class  League extends BaseComponent<LeagueProps,LeagueState> {

  filterInput : HTMLInputElement|null = null
    
  constructor(props : LeagueProps){ 
    super(props);
    this.views = [DataView.league, DataView.leagueComps];
    this.onUpdate = this.onUpdate.bind(this);
    this.onSearch = this.onSearch.bind(this);
    this.onReactivate = this.onReactivate.bind(this);
  }

  onUpdate(){
    super.onUpdate();

    let comps = this.props.selections.responses.response[DataView[DataView.leagueComps]];
    let backup : QueryPageResponse = new QueryPageResponse();
    if(comps){
      backup = JSON.parse(JSON.stringify(comps));
    }

    this.setState(
      {
        localBackup : backup,
        league:this.props.selections.responses.response[DataView[DataView.league]],
        leaguecomps:this.props.selections.responses.response[DataView[DataView.leagueComps]]
      });

  }

  onReactivate()
  {
    let origin = this.state.league.result.rows[0][this.state.league.result.cols[ColumnID[ColumnID.idorigin]]];
    let league = this.state.league.result.rows[0][this.state.league.result.cols[ColumnID[ColumnID.league_name]]];
    this.setState({showLeagueMenu:false})
    this.props.selections.messages.addMessage(<span>Activating {league}...</span>, 10000, PopupTypeEnum.Neutral);
    GSpy3_api.getAPI().postLeagueActivate(origin,  [league]).then((res)=>{ 
      this.props.selections.messages.addMessage(<span>Activating {league} succeeded. Collection will begin shortly.</span>, 5000, PopupTypeEnum.Success);
        this.props.history.push("/league/"+res.leagues[0].idleague);
          }).catch((e : Error)=>{
      let sError = ""+(e) 
      this.props.selections.messages.addMessage(<span>Activating {league} FAILED ({sError})</span>, 3000, PopupTypeEnum.Failure);
    });

  }

  onSearch(event : React.ChangeEvent){
    let filterText = (event.target as HTMLInputElement).value.toLocaleLowerCase();

    let newComps : QueryPageResponse = new QueryPageResponse();
    newComps.result.cols = this.state.localBackup.result.cols;
    newComps.result.rows = [];



    let nameCol = this.state.localBackup.result.cols[ColumnID[ColumnID.competition_name]];
    for(let i=0; i < this.state.localBackup.result.rows.length;i++){
      let row = this.state.localBackup.result.rows[i];
      let name = row[nameCol].toLocaleLowerCase();
      if(filterText === ""||name.indexOf(filterText) >= 0){
        newComps.result.rows.push(row);
      }
    }
    this.setState({leaguecomps: newComps});
  }

  componentDidUpdate(){
    if(this.filterInput!=null){
      this.filterInput.focus(); 
    }
 }

  render () : React.ReactElement {
    if(!this || !(this.state?.league?.result?.rows?.length >0)) return <div></div>;
    return <div>     
        <div className="bordered searchpanel">
          <div className="menu" style={{position:"absolute", right:"1pt",top:"0pt"}}>
              <a title="Competition settings"  onClick={()=>{this.setState({showLeagueMenu:true})}} className="imageButton"><img src={cogimg} className="iconSmall"></img></a>
              {this.state.showLeagueMenu&&<HelpPanel title="League menu" left={true} onClose={()=>{this.setState({showLeagueMenu:false})}}>
                  <div className="menu padded">
                      <button onClick={this.onReactivate}>Reactivate league collection</button><br></br>
                      </div>
                </HelpPanel>}
            </div>

            <h2>{this.state.league?.result?.rows[0][this.state.league.result.cols[ColumnID[ColumnID.league_name]]]}</h2>
            <h3>Search for your competition</h3>
            <input ref={(input) => { this.filterInput = input; }} onChange={this.onSearch} placeholder="Enter text to search"></input>
        </div>
        <div className="panel">
          <GSpyTable source={this.state.leaguecomps} layout={ColumnLayout.CompetitionsView} ></GSpyTable>
        </div>
      </div>
  }
}

export default League