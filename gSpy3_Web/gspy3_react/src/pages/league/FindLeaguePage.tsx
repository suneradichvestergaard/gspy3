
import React  from 'react';

import GSpyTable from "../../components/GSpyTable"
import {ColumnLayout, ColumnID} from "../../api/views/columns"
import { QueryPageResponse} from "../../api/queryPageRequests"
import DataView from "../../api/views/dataView"
import BaseComponent,{BaseComponentProps, BaseComponentState} from "../../components/BaseComponent"
import { withRouter,RouteComponentProps } from "react-router-dom";
import ActivateLeaguePage from "./ActivateLeaguePage"
import {Link} from "react-router-dom"
import pc from "../../gfx/pc.png"
import xb1 from "../../gfx/xb1.png"
import ps4 from "../../gfx/ps4.png"
import yesimg from "../../gfx/yes.png"
import noimg from "../../gfx/no.png"
import CheckBoxImage from "../../components/CheckBoxImage"
import APIOrigins from '../../api/dataformat/apiOrigins';

interface FindLeagueState extends BaseComponentState  {
  leagues : QueryPageResponse,
  filterPC : boolean ,
  filterXB1 : boolean,
  filterPS4 : boolean,
  localBackup : QueryPageResponse 
  };
interface FindLeagueProps extends BaseComponentProps  {

};

class  FindLeague extends BaseComponent<FindLeagueProps,FindLeagueState> {

  
  filterInput : HTMLInputElement|null = null

  constructor(props : FindLeagueProps){ 
    super(props);
    this.views = [DataView.leagues];
    this.onUpdate = this.onUpdate.bind(this);
    this.onSearch = this.onSearch.bind(this);
  }

  onUpdate(){

    super.onUpdate();
    
    // Make sure Cabalvision leagues are on top
    let league = this.props.selections.responses.response[DataView[DataView.leagues]];
    let backup : QueryPageResponse = new QueryPageResponse()
    if(league){
     
      let list : Array<string[]> = [];
      for(let i=0; i < league.result.rows.length;i++){
        if(league.result.rows[i]){
        let name = league.result.rows[i][league.result.cols[ColumnID[ColumnID.league_name]]];
        let idorigin = +league.result.rows[i][league.result.cols[ColumnID[ColumnID.idorigin]]];
         
        if(name && name.indexOf("Cabalvision Official")>=0){
          list.push(league.result.rows.splice(i,1)[0]); 
          i--;
          }
        }
      }
      
        for(let i=list.length-1; i >= 0;i--){
          league.result.rows.unshift(list[i]);
      }

        
        
      
      backup = JSON.parse(JSON.stringify(league));
    }
    this.setState(
      {
        leagues:this.props.selections.responses.response[DataView[DataView.leagues]],
        filterPC:true, filterXB1:true, filterPS4:true,
        localBackup:backup
      });
  }
  
 
  onSearchFull(event : React.ChangeEvent|undefined, fpc : boolean|undefined,fxb1 : boolean|undefined, fps4 : boolean|undefined){
    let filterText = ""+this.filterInput?.value.toLocaleLowerCase();//(event.target as HTMLInputElement).value.toLocaleLowerCase();

    if(fpc==undefined) fpc = this.state.filterPC;
    if(fxb1==undefined) fxb1 = this.state.filterXB1;
    if(fps4==undefined) fps4 = this.state.filterPS4;

    let newComps : QueryPageResponse = new QueryPageResponse();
    newComps.result.cols = this.state.localBackup.result.cols;
    newComps.result.rows = [];
    
    let nameCol = this.state.localBackup.result.cols[ColumnID[ColumnID.league_name]];
    let originCol = this.state.localBackup.result.cols[ColumnID[ColumnID.idorigin]];
    for(let i=0; i < this.state.localBackup.result.rows.length;i++){
      let row = this.state.localBackup.result.rows[i];
      let name = row[nameCol].toLocaleLowerCase();
      let origin = +row[originCol];
      if(fpc==false && origin == 1){continue;}
      if(fxb1==false && origin == 2){continue;}
      if(fps4==false && origin == 3){continue;}
      if(name.indexOf(filterText) >= 0){
        newComps.result.rows.push(row);
      }
    }
    this.setState({leagues: newComps});
  }
  onSearch(event : React.ChangeEvent|undefined){
    this.onSearchFull(event, undefined,undefined,undefined);
  }
  componentDidUpdate(){
    if(this.filterInput!=null){
      this.filterInput.focus(); 
    }
 }

  render () : React.ReactElement {
    if(!this.state) return <div></div>;
       return <div  className="FindLeague ">
        <div className="searchPanel bordered" >
          <div className=" searchpanel">
            <table><tbody><tr><td>
              <h3>Search for your league</h3>
              <input ref={(input) => { this.filterInput = input; }} onChange={this.onSearch} placeholder="Enter text to search"></input>
              <small>If you cannot find the league, you may need to <Link to="../leagues/activate"><small>Activate it</small></Link></small>
            </td>
            <td></td>
            <td>
              <h3>Filter on origin</h3>
                <CheckBoxImage checked={this.state.filterPC} onCheck={(b)=>{this.setState({filterPC:b});this.onSearchFull(undefined, b,undefined,undefined)}}>
                  <img alt="PC"  className="iconSmall"  title="PC" src={pc}></img>
                </CheckBoxImage>
                <CheckBoxImage checked={this.state.filterXB1} onCheck={(b)=>{this.setState({filterXB1:b});this.onSearchFull(undefined,undefined,b,undefined)}}>
                  <img alt="XB1" className="iconSmall"  title="XB1" src={xb1}></img>
                </CheckBoxImage>
                <CheckBoxImage checked={this.state.filterPS4} onCheck={(b)=>{this.setState({filterPS4:b});this.onSearchFull(undefined,undefined,undefined,b)}}>
                  <img alt="PS4" className="iconSmall"  title="PS4" src={ps4}></img>
                </CheckBoxImage>
            </td>
           
            </tr></tbody></table>
          </div>

         
        </div>
        <div className="panel">
            <GSpyTable source={this.state.leagues} layout={ColumnLayout.LeaguesView} ></GSpyTable>
        </div>


      </div>
      
  }
}

export default withRouter(FindLeague)