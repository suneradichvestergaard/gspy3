import React from 'react';
import GSpy3_api from "../../api/gSpy3_api"
import {NeedLoggedInMessage} from "../../components/NeedLoggedInMessage"
import messageManager, { PopupTypeEnum } from '../../managers/messageManager';
import { withRouter,RouteComponentProps, Link } from "react-router-dom";
import BaseComponent,{BaseComponentProps, BaseComponentState} from "../../components/BaseComponent"
interface AboutPageState  extends BaseComponentState {
};
interface AboutPageProps extends BaseComponentProps  {
  
}; 

class  AboutPage extends BaseComponent<AboutPageProps,AboutPageState> {

  constructor(props : AboutPageProps){
    super(props);
  }


  render () : React.ReactElement {
      return <div className="bordered panel information">
      <div><h2>Help</h2>
      <div>
        <div className="bordered">
              <h3>About GoblinSpy</h3>
              GoblinSpy collects data from the BloodBowl 2. <br></br>
              In order to collect data from a new league, it must first be <Link to="/leagues/activate">Activated</Link>.
        </div>
        <div className="bordered">
        <h3>User settings</h3>
              User login is currently not supported, but you can still make <Link to="/user/settings">User Settings</Link> and these will be stored in the browser.<br></br>
              If needed, user login may be introduced at a later stage in order to lock certain features from the public.
        </div>
        <div className="bordered">
            <h3>Following competitions and coaches</h3>
              You can follow competitions and coaches, making them appear on the start page of GoblinSpy.<br></br>
              Go to the coach or competition you want to follow, open the cog menu and select to follow.<br></br>
              Only active competitions will be shown and inactive coaches and teams will be automatically removed from the follow list.<br></br>
        </div>
        <div className="bordered">
        <h3>Filtering the data shown</h3>
              Most tables allow you to click on the column header to change sorting or filter.<br/>
              The filtering allow you to select to show only values within certain ranges or matching different patterns. <br></br>
              Help about how to filter is available in the filter menu.<br/>
        </div>
        <div className="bordered">
        <h3>Using the discord bot</h3>
            Goblinspy has a discord bot that you can use to access data, see: <Link to="/help/bot">More information</Link>
        </div>
        <div className="bordered">
        <h3>Getting data from GoblinSpy</h3>
            You can use the <a target="_blank" href="https://www.mordrek.com:666/swagger/index.html">API</a> or use the cog menu on most tables to get a link to the filtered data as shown in the table, but returned as json.<br></br>
            You are free to use the data, but please keep the requests within sane intervals or I will lock the access.
        </div>
        <div className="bordered">
        <h3>Collections</h3>
            If you want to view team stats from several different competitions as one, then you can create a collection.<br/>
            There is for example some one predefined collection to view all data for all PC Champion Ladder competitions<br/>
             and another to always view the latest PC Champion Ladder competition <br/>
             Collections are accesssed from the league page.<br/>
            Contact mordrek on discord to set this up.<br/> 
        </div>
      </div>
    </div>
    </div>
      
  }
}

export default withRouter(AboutPage)