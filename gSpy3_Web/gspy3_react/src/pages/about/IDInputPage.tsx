import React from 'react';
import GSpy3_api from "../../api/gSpy3_api"
import {NeedLoggedInMessage} from "../../components/NeedLoggedInMessage"
import messageManager, { PopupTypeEnum } from '../../managers/messageManager';
import { withRouter,RouteComponentProps, Link } from "react-router-dom";
import BaseComponent,{BaseComponentProps, BaseComponentState} from "../../components/BaseComponent"
interface IDInputPageState  extends BaseComponentState {
};
interface IDInputPageProps extends BaseComponentProps  {
  
}; 

class  IDInputPage extends BaseComponent<IDInputPageProps,IDInputPageState> {

  constructor(props : IDInputPageProps){
    super(props);
  }


  render () : React.ReactElement {
      return <div className="bordered panel ">
      <div><h2>ID Input</h2>
      <div>
      <h3>Introduction</h3>
        When using the bot, URL links, or the API, you often have to specify competition by id.<br />
        If you don't know the ID, you can instead use a part of, or the whole, competition name<br/>
        <br/>
      <h3>Ambiguity</h3>
        If the competition name is not unique, you may have to specify an exact competition name and/or what origin it is from<br />
        <br/>
      <h3>Origins</h3>
        Origins are set at the end of the comptition name after an '§'.<br/>
        Valid origins are 'pc','xb1','ps4'<br/>
        Example: <i>My Comp§pc</i><br/>
        <br/>
      <h3>Exact or searched name</h3>
        By default , the name will be searched for (so if you enter <i>banana</i> it will match <i>bananarama</i>).<br/>
        You can enter '*' to represent wildcard characters ( <i>banana*ma</i>)<br/>
        If you start the name with a '=', no search will be made and only exact name will give a match.<br/>
        Example: <i>=myExactComp</i><br/>
        <br/>
      <h3>Multiple IDs</h3>
        It will often be allowed to enter several IDs/names , in which case you separate them by '|'<br></br>
        Example:<i>myComp1 | myComp2</i><br/>
        <br/>

      </div>
    </div>
    </div>
      
  }
}

export default withRouter(IDInputPage)