



class APIOrigins{

    getOriginName(idorigin: number) : string{
        switch(idorigin){
            case 1: return "BB2_pc";
            case 2: return "BB2_xb1";
            case 3: return "BB2_ps4";
        }
        return "";
    }
}

export default APIOrigins