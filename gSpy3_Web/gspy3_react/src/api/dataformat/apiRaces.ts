class APIRaces
{
    static GetMap() : {[id:number] : string}
    {
        let list : {[id:number] : string} = {};
        for(var i=1; i < 43;i++){
            list[i]  =(APIRaces.GetName(i));
        }
        return list;
    }

    static GetList(): {id:number,name:string}[]{ 
        let list : {id:number,name:string}[] = [];
        for(var i=1; i < 43;i++){
            list.push({id:i,name:APIRaces.GetName(i)});
        }
        list.sort((a,b)=>{return a.name.localeCompare(b.name)})
        return list;
    }

  static Parse(str : string) : number|undefined
        {
            switch (str.toLowerCase())
            {
                case "delf": return 9;
                case "chorf": return 21;
                case "dorf": return 2;
            }
            str = str.toLowerCase()
            if(str.startsWith("hu")) return 1;
            if(str.startsWith("dw")) return 2;
            if(str.startsWith("sk")) return 3;
            if(str.startsWith("or")) return 4;
            if(str.startsWith("li")) return 5;
            if(str.startsWith("go")) return 6;
            if(str.startsWith("wo")) return 7;
            if(str.startsWith("chaosd")) return 21;

            if(str.startsWith("ch")) return 8;
            if(str.startsWith("da")) return 9;
            if(str.startsWith("undead")) return 10;
            if(str.startsWith("ha")) return 11;
            if(str.startsWith("no")) return 12;
            if(str.startsWith("am")) return 13;
            if(str.startsWith("pr")) return 14;
            if(str.startsWith("hi")) return 15;
            if(str.startsWith("kh")) return 16;
            if(str.startsWith("ne")) return 17;
            if(str.startsWith("nu")) return 18;
            if(str.startsWith("og")) return 19;
            if(str.startsWith("va")) return 20;
            if(str.startsWith("under")) return 22;
            if(str.startsWith("br")) return 24;
            if(str.startsWith("ki")) return 25;
            if(str.startsWith("star")) return 26;
            if(str.startsWith("ex")) return 27;
         
            return undefined
        }

    

    static GetName(idrace:number) : string{
        switch(idrace){
            case 1: return "Human";
            case 2: return "Dwarf";
            case 3: return "Skaven";
            case 4: return "Orc";
            case 5: return "Lizardman";
            case 6: return "Goblin";
            case 7: return "WoodElf";
            case 8: return "Chaos";
            case 9: return "DarkElf";
            case 10: return "Undead";
            case 11: return "Halfling";
            case 12: return "Norse";
            case 13: return "Amazon";
            case 14: return "ProElf";
            case 15: return "HighElf";
            case 16: return "Khemri";
            case 17: return "Necromantic";
            case 18: return "Nurgle";
            case 19: return "Ogre";
            case 20: return "Vampire";
            case 21: return "ChaosDwarf";
            case 22: return "Underworld";
            case 24: return "Bretonnia";
            case 25: return "Kislev";
            case 26: return "StarPlayer";
            case 27: return "Extra";
            case 28: return "Common";
            case 30: return "AllStars";
            case 32: return "MercenaryAristo";
            case 33: return "MercenaryChaos";
            case 34: return "MercenaryChaosGods";
            case 35: return "MercenaryEasterners";
            case 36: return "MercenaryElf";
            case 37: return "MercenaryExplorers";
            case 38: return "MercenaryGoodGuys";
            case 39: return "MercenaryHuman";
            case 40: return "MercenarySavage";
            case 41: return "MercenaryStunty";
            case 42: return "MercenaryUndead";
        }
        return "Unknown";        
    }
}

export default APIRaces