class APISkillTypes{

    idToName : {[id:string] : string } = {}

    alterStat(id:string, value : number, skills: string) : number {
        if(skills){
            for(let i=0; i < skills.length;i+=2){
                let inj = skills.substr(i,2);
                if ((inj==="Im") && id === "ma"){
                    value++;
                }
                if ((inj==="Ir") && id === "av"){
                    value++;
                }
                if ((inj==="Ia") && id === "ag"){
                    value++;
                }
                if ((inj==="Is") && id === "st"){
                    value++;
                }
            }
        }
        return value;

    }

    constructor(){
        this.idToName["Sb"] = "Strip Ball";
        this.idToName["Is"] = "Increase Strength";
        this.idToName["Ia"] = "Increase Agility";
        this.idToName["Im"] = "Increase Movement";
        this.idToName["Ir"] = "Increase Armour";
        this.idToName["Ca"] = "Catch";
        this.idToName["Dd"] = "Dodge";
        this.idToName["Sp"] = "Sprint";
        this.idToName["Pb"] = "Pass Block";
        this.idToName["Fa"] = "Foul Appearance";
        this.idToName["Le"] = "Leap";
        this.idToName["Ea"] = "Extra Arms";
        this.idToName["Mb"] = "Mighty Blow";
        this.idToName["Ld"] = "Leader";
        this.idToName["Ho"] = "Horns";
        this.idToName["Th"] = "Two Heads";
        this.idToName["Sf"] = "Stand Firm";
        this.idToName["Ah"] = "Always Hungry";
        this.idToName["Sl"] = "Strip Ball";
        this.idToName["Re"] = "Regeneration";
        this.idToName["Tr"] = "Take Root";
        this.idToName["Ac"] = "Accurate";
        this.idToName["Bt"] = "Break Tackle";
        this.idToName["Sg"] = "Sneaky Git";
        this.idToName["Cs"] = "Chainsaw";
        this.idToName["Da"] = "Dauntless";
        this.idToName["Dp"] = "Dirty Player";
        this.idToName["Dc"] = "Diving Catch";
        this.idToName["Do"] = "Dump Off";
        this.idToName["Bk"] = "Block";
        this.idToName["Bh"] = "Bonehead";
        this.idToName["Vl"] = "Very Long Legs";
        this.idToName["Dr"] = "Disturbing Presence";
        this.idToName["Dt"] = "Diving Tackle";
        this.idToName["Fe"] = "Fend";
        this.idToName["Fr"] = "Frenzy";
        this.idToName["Gr"] = "Grab";
        this.idToName["Gu"] = "Guard";
        this.idToName["Hm"] = "Hail Mary Pass";
        this.idToName["Jg"] = "Juggernaut";
        this.idToName["Ju"] = "Jump Up";
        this.idToName["Lo"] = "Loner";
        this.idToName["Ns"] = "Nerved of Steel";
        this.idToName["Nh"] = "No Hands";
        this.idToName["Pa"] = "Pass";
        this.idToName["Po"] = "Piling On";
        this.idToName["Pt"] = "Prehensile Tail";
        this.idToName["Pr"] = "Pro";
        this.idToName["Rs"] = "Really Stupid";
        this.idToName["Rf"] = "Right Stuff";
        this.idToName["Sa"] = "Safe Throw";
        this.idToName["Sw"] = "Secret Weapon";
        this.idToName["Sh"] = "Shadowing";
        this.idToName["Ss"] = "Sidestep";
        this.idToName["Ta"] = "Tackle";
        this.idToName["Sm"] = "Strong Arm";
        this.idToName["Sy"] = "Stunty";
        this.idToName["St"] = "Sure Feet";
        this.idToName["Sn"] = "Sure Hands";
        this.idToName["Ts"] = "ThickSkull";
        this.idToName["Tm"] = "Throw Teammate";
        this.idToName["Wa"] = "Wild Animal";
        this.idToName["Wr"] = "Wrestle";
        this.idToName["Tt"] = "Tentacles";
        this.idToName["Mk"] = "Multiple Block";
        this.idToName["Ki"] = "Kick";
        this.idToName["Kr"] = "Kick Off Return";
        this.idToName["Bn"] = "Big Hand";
        this.idToName["Cl"] = "Claw";
        this.idToName["Bc"] = "Ball and Chain";
        this.idToName["Sb"] = "Stab";
        this.idToName["Hg"] = "Hypnotic Gaze";
        this.idToName["Sk"] = "Stakes";
        this.idToName["Bb"] = "Bombardier";
        this.idToName["Dy"] = "Decay";
        this.idToName["Nr"] = "Nurgles Rot";
        this.idToName["Ty"] = "Titchy";
        this.idToName["Bl"] = "Blood Lust";
        this.idToName["Ff"] = "Fan Favourite";
        this.idToName["An"] = "Animosity";

      
    }


}


export default APISkillTypes