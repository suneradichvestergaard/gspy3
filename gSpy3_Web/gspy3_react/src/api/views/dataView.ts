enum DataView{
    league, 
    comp, 
    search,
    match, coach, player, team, compResults, compStandings, leagues,
    leagueComps,
    compTeamwStats, compTeamMatches, compTeamPlayers, compTeamSchedule, compSchedules,
    compCoachTeams, coachCompetitions, compTeamGraveyard, 
    matchTeams, matchPlayers,
    searchCoach,searchTeam,searchCompetition,
    compActivityDates,compActivityRaces, compActivityDays,compActivityTime,
    lastComps , lastCoachComps, compPlayers,
    // compCoaches,
    matchReplayStats ,
    compTeamStanding,
    leagueCollections,

    collection,collectionCompetitions,collectionTeams,

    compStatsTeam,compStatsPlayer,compStatsCoaches,compStatsRaces,

    compMatches, compCoachRaces,
    overtake,frompos,topos,
};  

// Map between a DataView and one or more DataViewID that determines the dependent table keys
export const DataViewGroup : {[view:string]:DataView[];} = {
    "compTeamwStats" : [DataView.comp, DataView.team],
    "compTeamMatches" : [DataView.comp, DataView.team],
    "compTeamPlayers" : [DataView.comp,DataView.team],
    "compTeamSchedule" : [DataView.comp, DataView.team],
    "compTeamStanding" : [DataView.comp, DataView.team],
    "compTeamGraveyard" :[DataView.comp, DataView.team],
    "compCoachRaces":[DataView.comp, DataView.coach],
    "compMatches" :[DataView.comp],
    "compSchedules" : [DataView.comp],
    "compResults" : [DataView.comp],
    "compStandings" : [DataView.comp],
    "compPlayers": [DataView.comp],
    //"compCoaches": [DataView.comp],
    "league" : [DataView.league],
    "comp" : [DataView.comp],
    "match" : [DataView.match],
    "coach" : [DataView.coach],
    "player" : [DataView.player],
    "team" : [DataView.team],
    "leagueComps": [DataView.league],
    "leagues": [],
    "compCoachTeams": [DataView.comp, DataView.coach],
    "coachCompetitions": [DataView.coach],
    "matchTeams": [DataView.match],
    "matchPlayers": [DataView.match],
    "searchCoach": [DataView.search],
    "searchTeam": [DataView.search],
    "searchCompetition": [DataView.search],
    "compActivityDates":[DataView.comp],
    "compActivityRaces":[DataView.comp],
    "compActivityDays":[DataView.comp],
    "compActivityTime":[DataView.comp],
    "matchReplayStats":[DataView.match ],
    "collections":[DataView.league],
    "leagueCollections":[DataView.league],
    "collectionCompetitions":[DataView.collection],
    "collection":[DataView.collection],
    "collectionTeams":[DataView.collection],
    "compStatsTeam":[DataView.comp],
    "compStatsPlayer":[DataView.comp],
    "compStatsCoaches":[DataView.comp],
    "compStatsRaces":[DataView.comp],
    "overtake":[DataView.comp, DataView.frompos, DataView.topos]
}
// Map between DataView key name and the corresponding table key
// Every table key must only exist in this map once
export const DataViewID : {[view:string]:string;} = {
    "league" : "idleague",
    "comp" : "idcompetition",
    "match" : "idmatch",
    "coach" : "idcoach",
    "player" : "idplayer",
    "team" : "idteam",
    "search" : "search",
    "collection": "idcollection",
    "frompos":"frompos",
    "topos":"topos"

}


export default DataView

