import QueryResult from "./queryResult"



class ServerStatus{
    modules : ModuleStatus[] = []
 
}

export class ModuleStatus{
    module_id : number = 0
    module_name : string = ""
    heartbeat_datetime :string=""
    work : WorkStatus[] = []
}

export class WorkStatus{
    idwork: number = 0
    module_id:number = 0
    command:string =""
    data:string=""
    weight:number=0
    added:string=""
    started:string=""
    finished:string=""
    source:string=""
    result:string=""
    success:number|undefined=0


}


export default ServerStatus
