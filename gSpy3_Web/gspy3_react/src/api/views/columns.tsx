

import React from "react"
import pc from "../../gfx/pc.png"
import xb1 from "../../gfx/xb1.png"
import ps4 from "../../gfx/ps4.png"
import yesimg from "../../gfx/yes.png"
import noimg from "../../gfx/no.png"
import inactiveimg from "../../gfx/inactive.png"
import Races from "../dataformat/apiRaces"
import LookVs from "../../gfx/look-vs.png"
import Look from "../../gfx/look.png"
import {Link} from "react-router-dom"
import APIPlayerTypes from "../dataformat/apiPlayertypes"
import APIInjuries from "../dataformat/apiInjuries"
import APISkillTypes from "../dataformat/apiSkills"
import UserDate from "../../components/UserDate"
import APIOrigins from "../dataformat/apiOrigins"
import youtubeImg from "../../gfx/youtube.png"
import twitchImg from "../../gfx/twitch.png"
import QueryResult from "../views/queryResult";
import Rank from "../../managers/rank"
import { isCallLikeExpression } from "typescript"
import highlightManager from "../../managers/highlightManager"
import {GSpyTableCommandReceiver} from "../../components/GSpyTable"

export enum ColumnLayout{
    LeaguesView,
    CompetitionsView,
    CompStandingsView,
    CoachTeamsView,
    CompTeamView,
    CompTeamStatsView,
    CompPlayerView,
    CompTeamPlayerView,
    CompCoachesView,
    CompResultView,
    CompTeamStanding, CompTeamOvertakeStanding,
    CompTeamSchedule,
    CompTeamMatches,
    CompCoachView,
    CoachCompeitionsView,
    MatchTeamsView,MatchTeamsPlayView,MatchTeamsKillView,
    MatchPlayersView,
    SearchTeamsView,
    ActivityDateView,
    ActivityRacesView,
    ActivityDayView,
    ActivityTimeView,
    FollowedCompetitionView,
    FollowedCoachView,
    CompTeamGraveyardView,
    TeamDiceGraphView, TeamBlockDiceGraphView,
    CollectionsView,
    CompRacesView,
    CoachRacesView
}


export enum ColumnID{
    idleague, idorigin,  league_origin_id, league_name, collecting,gp,
    idcompetition, competition_origin_id, competition_name, active, format, last_game, num_coaches, num_teams, num_games, sorting,
    idteam, ranking, points,   wins, draws, losses, td, td_opp, td_diff, cas, cas_opp,cas_home,cas_away, cas_diff, concedes, concedes_opp, team_value, kills,position, team_name,
    idcoach, coach_name, 
    idrace, logo, started, finished,score,idmatch,duration,
    idteam_home, idteam_away, idcoach_home, idcoach_away, team_name_home,team_value_away, team_name_away, coach_name_home, coach_name_away, score_home, score_away, logo_home, logo_away,
    idplayer, player_name, idplayertype,
    ma,av,ag,st,skills,cas_state,cas_sustained,xp,xp_gain,level,number,
    blocks_for, breaks_for, stuns_for, kos_for, casualties_for, kills_for,
    blocks_against, breaks_against, stuns_against, kos_against, casualties_against, kills_against,
    passes, catches, interceptions, turnovers, pushouts, expulsions, completions,
    round,pass_meters,run_meters, occ_own,occ_their, conceded, conceded_opp,day, time, 
    rerolls, apo, asscoaches, possession, cheerleaders, cash_spent, cash_earned, cash_before_match, popularity_before_match, popularity_gained,
    mvp , home, idrace_home, idrace_away, userdate , schedule_origin_id,
    win_pct,supporters, description,

    dice, blockdice, number_home, number_away,
    

    youtube, twitch,
    idcollection,collection_name,filter,
    dodges, dodges_failed,
    passes_failed, catches_failed,
    rushes,rushes_failed, pickups, pickups_failed, sacks, boneheads, fouls,

    killed_by,idhighlighttype,idtarget, target_name,idcause,idtargetcompetition,overtake,

    last_checked,subturn
}   



export class ColumnGroup{
    name : string = ""
    cols : Column[] = []
}
export class Column{
    id : ColumnID = ColumnID.idleague
    visible: boolean = true     // Visible or not
    short : boolean = false     // Use short or long column name
    selected? : boolean = false // Selected as a value column for graphs
    altered? : boolean = false // Altered columns cannot be sorted or filtered

}


export class ViewColumns{

    groups :  ColumnGroup[] = []
    static playerTypes : APIPlayerTypes = new APIPlayerTypes()
    static injuryTypes : APIInjuries = new APIInjuries();
    static skillTypes : APISkillTypes = new APISkillTypes();

  static getLayout(layout : ColumnLayout) : ViewColumns{
      switch(layout){
        case ColumnLayout.TeamDiceGraphView:
            return{
                groups:
                [
                      {  name : "Home team" ,cols: 
                          [
                            {id : ColumnID.dice, visible:true, short:false, selected:true} ,
                            {id : ColumnID.number_home, visible:true, short:true} 
                          ]
                      },
                      {  name : "Away team" ,cols: 
                          [
                            {id : ColumnID.dice, visible:true, short:false, selected:true} ,
                            {id : ColumnID.number_away, visible:true, short:true} 
                          ]
                      }

                ]
            };
            case ColumnLayout.TeamBlockDiceGraphView:
                return{
                    groups:
                    [
                          {  name : "Home team" ,cols: 
                              [
                                {id : ColumnID.blockdice, visible:true, short:false, selected:true} ,
                                {id : ColumnID.number_home, visible:true, short:true} 
                              ]
                          },
                          {  name : "Away team" ,cols: 
                              [
                                {id : ColumnID.blockdice, visible:true, short:false, selected:true} ,
                                {id : ColumnID.number_away, visible:true, short:true} 
                              ]
                          }
    
                    ]
                };            
        case ColumnLayout.ActivityDateView:
            return{
                groups:
                [
                      {  name : "Matches per date" ,cols: 
                          [
                            {id : ColumnID.number, visible:true, short:false} ,
                            {id : ColumnID.finished, visible:true, short:true, selected:true} 
                          ]
                      }
                ]
            };
        case ColumnLayout.ActivityDayView:
            return{
                groups:
                [
                        {  name : "Matches per day" ,cols: 
                            [
                            {id : ColumnID.number, visible:true, short:false} ,
                            {id : ColumnID.day, visible:true, short:true, selected:true} 
                            ]
                        }
                ]
            };
        case ColumnLayout.ActivityTimeView:
            return{
                groups:
                [
                        {  name : "Matches per hour" ,cols: 
                            [
                            {id : ColumnID.number, visible:true, short:false} ,
                            {id : ColumnID.time, visible:true, short:true, selected:true} 
                            ]
                        }
                ]
            };
        case ColumnLayout.ActivityRacesView:
            return{
                groups:
                [
                      {  name : "Teams per race" ,cols: 
                          [
                            {id : ColumnID.number, visible:true, short:false} ,
                            {id : ColumnID.idrace, visible:true, short:true, selected:true} 
                          ]
                      }
                ]
            };
        case ColumnLayout.CoachCompeitionsView:
            return{
                groups:
                [
                      {  name : "Competitions" ,cols: 
                          [
                            {id : ColumnID.idcoach, visible:false, short:true} ,
                            {id : ColumnID.youtube, visible:false, short:true} ,
                            {id : ColumnID.twitch, visible:false, short:true} ,
                            {id : ColumnID.coach_name, visible:true, short:true} ,
                            {id : ColumnID.idleague, visible:false, short:true} ,
                            {id : ColumnID.league_name, visible:true, short:true} ,
                            {id : ColumnID.idcompetition, visible:false, short:true} ,
                            {id : ColumnID.competition_name, visible:true, short:true} ,
                            {id : ColumnID.gp, visible:true, short:true} ,
                            {id : ColumnID.wins, visible:true, short:true} ,
                            {id : ColumnID.draws, visible:true, short:true} ,
                            {id : ColumnID.losses, visible:true, short:true} ,
                            {id : ColumnID.win_pct, visible:true, short:true} ,
                            {id : ColumnID.concedes, visible:true, short:true} ,


                          ]
                      }
                ]
            };
        case ColumnLayout.CompCoachView:
            return{
                groups:
                [
                      {  name : "Global Coach" ,cols: 
                          [
                            {id : ColumnID.youtube, visible:false, short:true} ,
                            {id : ColumnID.twitch, visible:false, short:true} ,
                            {id : ColumnID.idcoach, visible:false, short:true} ,
                            {id : ColumnID.coach_name, visible:true, short:true} ,
                          ]
                      }
                ]
            };
        case ColumnLayout.CompCoachesView:
            return{
                groups:
                [
                      {  name : "Coaches" ,cols: 
                          [
                            {id : ColumnID.youtube, visible:false, short:true} ,
                            {id : ColumnID.twitch, visible:false, short:true} ,
                            {id : ColumnID.idcoach, visible:false, short:true} ,
                            {id : ColumnID.coach_name, visible:true, short:true} ,
                            {id : ColumnID.gp, visible:true, short:true} ,
                            {id : ColumnID.wins, visible:true, short:true} ,
                            {id : ColumnID.draws, visible:true, short:true} ,
                            {id : ColumnID.losses, visible:true, short:true} ,
                            {id : ColumnID.win_pct, visible:true, short:true} ,
                            {id : ColumnID.concedes, visible:true, short:true} ,
                          ]
                      },
                      {   name: "Play stats", cols:
                      [
                          {id : ColumnID.dodges, visible:true, short:true},
                          {id : ColumnID.pickups, visible:true, short:true},
                          {id : ColumnID.rushes, visible:true, short:true},
                          {id : ColumnID.pass_meters, visible:true, short:true},
                          {id : ColumnID.run_meters, visible:true, short:true},
                          {id : ColumnID.passes, visible:true, short:true},
                          {id : ColumnID.completions, visible:true, short:true},
                          {id : ColumnID.catches, visible:true, short:true},
                          {id : ColumnID.interceptions, visible:true, short:true},
                          {id : ColumnID.pushouts, visible:true, short:true},
                      ]},
                  {name: "Kill stats", cols:
                      [
                          {id : ColumnID.blocks_for, visible:true, short:true},
                          {id : ColumnID.blocks_against, visible:true, short:true},
                          {id : ColumnID.breaks_for, visible:true, short:true},
                          {id : ColumnID.breaks_against, visible:true, short:true},
                          {id : ColumnID.stuns_for, visible:true, short:true},
                          {id : ColumnID.stuns_against, visible:true, short:true},
                          {id : ColumnID.kos_for, visible:true, short:true},
                          {id : ColumnID.kos_against, visible:true, short:true},
                          {id : ColumnID.casualties_for, visible:true, short:true},
                          {id : ColumnID.casualties_against, visible:true, short:true},
                          {id : ColumnID.kills_for, visible:true, short:true},
                          {id : ColumnID.kills_against, visible:true, short:true},
                          {id : ColumnID.fouls, visible:true, short:true},
                          {id : ColumnID.expulsions, visible:true, short:true},
                                                   
                      ]
              }                      
                ]
            };
        case ColumnLayout.CoachRacesView:
            return {
                groups:
                [
                    {name:"Races", cols:
                    [
                        {id : ColumnID.idcompetition, visible:false, short:true} , 
                        {id : ColumnID.competition_name, visible:false, short:true} , 
                        {id : ColumnID.idcoach, visible:false, short:true} , 
                        {id : ColumnID.coach_name, visible:false, short:true} , 
                        {id : ColumnID.idrace, visible:true, short:false} , 
                        {id : ColumnID.num_teams, visible:true, short:true} , 
                        {id : ColumnID.gp, visible:true, short:true} , 
                        {id : ColumnID.wins, visible:true, short:true} , 
                        {id : ColumnID.draws, visible:true, short:true} , 
                        {id : ColumnID.losses, visible:true, short:true} , 
                        {id : ColumnID.win_pct, visible:true, short:true} , 
                        {id : ColumnID.concedes, visible:true, short:true} , 
                    ]
                    }
                ]
            }
        case ColumnLayout.CompTeamMatches:
            return      { 
                groups:
                [
                      {  name : "Results" ,cols: 
                          [
                            {id : ColumnID.round, visible:true, short:true} ,
                            {id : ColumnID.idmatch, visible:true, short:true} ,
                            {id : ColumnID.finished, visible:true, short:true} ,
                            {id : ColumnID.duration, visible:true, short:true} ,
                            {id : ColumnID.td, visible:true, short:true},
                            {id : ColumnID.td_opp, visible:true, short:true},
                          ]},
                        {name:"Team", cols:
                          [
                            {id : ColumnID.team_value, visible:true, short:true} ,
                            {id : ColumnID.conceded, visible:true, short:true} ,
                          ]
                        },
                      {
                        name : "Opponent" ,cols: 
                        [
                            {id : ColumnID.idcoach_away, visible:false, short:false} ,
                            {id : ColumnID.coach_name_away, visible:true, short:false} ,
                            {id : ColumnID.idrace_away, visible:true, short:true} ,
                            {id : ColumnID.idteam_away, visible:false, short:false} ,
                            {id : ColumnID.logo_away, visible:true, short:true} ,
                            {id : ColumnID.team_name_away, visible:true, short:false} ,
                            {id : ColumnID.team_value_away, visible:true, short:false} ,
                            {id : ColumnID.conceded_opp, visible:true, short:true} ,
                          ]
                      },
                      {   name: "Play stats", cols:
                                [
                                    {id : ColumnID.pass_meters, visible:true, short:true},
                                    {id : ColumnID.run_meters, visible:true, short:true},
                                    {id : ColumnID.occ_own, visible:true, short:true},
                                    {id : ColumnID.occ_their, visible:true, short:true},
                                    {id : ColumnID.passes, visible:true, short:true},
                                    {id : ColumnID.completions, visible:true, short:true},
                                    {id : ColumnID.catches, visible:true, short:true},
                                    {id : ColumnID.interceptions, visible:true, short:true},
                                    {id : ColumnID.pushouts, visible:true, short:true},
                                    {id : ColumnID.expulsions, visible:true, short:true},
                                    {id : ColumnID.turnovers, visible:true, short:true},
                                ]},
                            {name: "Kill stats", cols:
                                [
                                    {id : ColumnID.blocks_for, visible:true, short:true},
                                    {id : ColumnID.blocks_against, visible:true, short:true},
                                    {id : ColumnID.breaks_for, visible:true, short:true},
                                    {id : ColumnID.breaks_against, visible:true, short:true},
                                    {id : ColumnID.stuns_for, visible:true, short:true},
                                    {id : ColumnID.stuns_against, visible:true, short:true},
                                    {id : ColumnID.kos_for, visible:true, short:true},
                                    {id : ColumnID.kos_against, visible:true, short:true},
                                    {id : ColumnID.casualties_for, visible:true, short:true},
                                    {id : ColumnID.casualties_against, visible:true, short:true},
                                    {id : ColumnID.kills_for, visible:true, short:true},
                                    {id : ColumnID.kills_against, visible:true, short:true},
                                ]
                        }
                  ]  
              };
            
              case ColumnLayout.MatchTeamsView:
                return      { 
                    groups:
                    [
                          {  name : "Teams" ,cols: 
                              [

                                {id : ColumnID.idcoach, visible:false, short:false} ,
                                {id : ColumnID.coach_name, visible:true, short:false} ,
                                {id : ColumnID.idteam, visible:false, short:false} ,
                                {id : ColumnID.logo, visible:true, short:true} ,
                                {id : ColumnID.team_name, visible:true, short:false} ,
                       
                            ]
                            },
                          {   name: "Overview", cols:
                                    [
                                        {id : ColumnID.round, visible:true, short:true} ,
                                        {id : ColumnID.finished, visible:true, short:true} ,
                                        {id : ColumnID.duration, visible:true, short:true} ,
                                        {id : ColumnID.conceded, visible:false, short:true} ,
                                        {id : ColumnID.td, visible:true, short:true},
                                        {id : ColumnID.team_value, visible:true, short:true},
                                        {id : ColumnID.rerolls, visible:true, short:true},
                                        {id : ColumnID.apo, visible:true, short:true},
                                        {id : ColumnID.cheerleaders, visible:true, short:true},
                                        {id : ColumnID.cash_spent, visible:true, short:true},
                                        {id : ColumnID.mvp, visible:true, short:true},
                                        {id : ColumnID.turnovers, visible:true, short:true},
                                        {id : ColumnID.boneheads, visible:true, short:true},
                                        {id : ColumnID.sacks, visible:true, short:true},                                        
                                        {id : ColumnID.possession, visible:true, short:true},
                                     ]},
                                    {name: "Kill stats", cols:
                                    [
                                        {id : ColumnID.blocks_for, visible:true, short:true},
                                        {id : ColumnID.blocks_against, visible:true, short:true},
                                        {id : ColumnID.breaks_for, visible:true, short:true},
                                        {id : ColumnID.breaks_against, visible:true, short:true},
                                        {id : ColumnID.stuns_for, visible:true, short:true},
                                        {id : ColumnID.stuns_against, visible:true, short:true},
                                        {id : ColumnID.kos_for, visible:true, short:true},
                                        {id : ColumnID.kos_against, visible:true, short:true},
                                        {id : ColumnID.casualties_for, visible:true, short:true},
                                        {id : ColumnID.casualties_against, visible:true, short:true},
                                        {id : ColumnID.kills_for, visible:true, short:true},
                                        {id : ColumnID.kills_against, visible:true, short:true},
                                        {id : ColumnID.pushouts, visible:true, short:true},
                                         {id : ColumnID.expulsions, visible:true, short:true},
                                        
                                    ]},
                                    { name:  "Move stats", cols:[
                                        {id : ColumnID.run_meters, visible:true, short:true},
                                        {id : ColumnID.dodges, visible:true, short:true},
                                        {id : ColumnID.dodges_failed, visible:true, short:true},
                                        {id : ColumnID.rushes, visible:true, short:true},
                                        {id : ColumnID.rushes_failed, visible:true, short:true},
                                        {id : ColumnID.occ_own, visible:true, short:true},
                                        {id : ColumnID.occ_their, visible:true, short:true},
                   
                                    ]},
                                 {   name: "Ball stats", cols:
                                 [
                                     {id : ColumnID.pickups, visible:true, short:true},
                                     {id : ColumnID.pickups_failed, visible:true, short:true},
                                     {id : ColumnID.pass_meters, visible:true, short:true},
                                     {id : ColumnID.passes, visible:true, short:true},
                                     {id : ColumnID.passes_failed, visible:true, short:true},
                                     {id : ColumnID.catches, visible:true, short:true},
                                     {id : ColumnID.catches_failed, visible:true, short:true},
                                     {id : ColumnID.completions, visible:true, short:true},
                                     {id : ColumnID.interceptions, visible:true, short:true},

                                 ]}   
                                
                      ]  
                  };

        case ColumnLayout.CompTeamSchedule:
            return      { 
                groups:
                [
                      {  name : "Schedule" ,cols: 
                          [
                            {id : ColumnID.round, visible:true, short:true} ,
                            {id : ColumnID.userdate, visible:true, short:true} ,
                            {id : ColumnID.idcoach_home, visible:false, short:false} ,
                              {id : ColumnID.coach_name_home, visible:false, short:false} ,
                              {id : ColumnID.logo_home, visible:true, short:true} ,
                              {id : ColumnID.idteam_home, visible:false, short:false} ,
                              {id : ColumnID.team_name_home, visible:true, short:false} ,
                              {id : ColumnID.team_name_away, visible:true, short:false} ,
                              {id : ColumnID.idteam_away, visible:false, short:false} ,
                              {id : ColumnID.logo_away, visible:true, short:true} ,
                              {id : ColumnID.coach_name_away, visible:false, short:false} ,
                              {id : ColumnID.idcoach_away, visible:false, short:false} ,
                              {id : ColumnID.schedule_origin_id, visible:false, short:false} ,
                          ]
                      }
                  ]  
              };
          case ColumnLayout.LeaguesView:
              return      { 
                  groups:
                  [
                        {  name : "Leagues" ,cols: 
                            [
                                {id : ColumnID.idleague, visible:false, short:false} ,
                                {id : ColumnID.idorigin, visible:true, short:false} ,
                                {id : ColumnID.league_name, visible:true, short:false} ,
                                {id : ColumnID.num_games, visible:true, short:false} ,
                                {id : ColumnID.collecting, visible:true, short:false} 
                            ]
                        }
                    ]  
                };
            case ColumnLayout.CollectionsView:
                return {
                    groups:
                    [
                        {  name : "Competitions" ,cols: 
                                [
                                    {id : ColumnID.idleague, visible:false, short:false} ,
                                    {id : ColumnID.idcollection, visible:false, short:false} ,
                                    {id : ColumnID.collection_name, visible:true, short:false} ,
                                    {id : ColumnID.description, visible:true, short:false} 
                                ]
                        }
                    ]
                };
        case ColumnLayout.CompetitionsView:
            return {
                groups:
                [
                    {  name : "Competitions" ,cols: 
                            [
                                {id : ColumnID.idleague, visible:false, short:false} ,
                                {id : ColumnID.idcompetition, visible:false, short:false} ,
                                {id : ColumnID.idorigin, visible:false, short:false} ,
                                {id : ColumnID.competition_name, visible:true, short:false} ,
                                {id : ColumnID.last_game, visible:true, short:false} ,
                                {id : ColumnID.num_coaches, visible:true, short:false} ,
                                {id : ColumnID.num_teams, visible:true, short:false} ,
                                {id : ColumnID.num_games, visible:true, short:false} ,
                                {id : ColumnID.active, visible:true, short:false} 
                            ]
                    }
                ]
            };
            case ColumnLayout.FollowedCoachView:
                return {
                    groups:
                    [
                        {  name : "Followed coach competitions" ,cols: 
                                [
                                    {id : ColumnID.idorigin, visible:false, short:false} ,
                                    {id : ColumnID.idcoach, visible:false, short:false} ,
                                    {id : ColumnID.coach_name, visible:true, short:false} ,
                                    {id : ColumnID.idleague, visible:false, short:false} ,
                                    {id : ColumnID.league_name, visible:true, short:false} ,
                                    {id : ColumnID.idcompetition, visible:false, short:false} ,
                                    {id : ColumnID.competition_name, visible:true, short:false} ,
                                    {id : ColumnID.wins, visible:true, short:false} ,
                                    {id : ColumnID.draws, visible:true, short:false} ,
                                    {id : ColumnID.losses, visible:true, short:false} ,
                                    {id : ColumnID.win_pct, visible:true, short:false} ,
                                    {id : ColumnID.concedes, visible:true, short:false},
                                    {id : ColumnID.active, visible:true, short:false}
                                ]
                        }
                    ]
                }
           case ColumnLayout.FollowedCompetitionView:
            return {
                groups:
                [
                    {  name : "Followed competitions" ,cols: 
                            [
                                {id : ColumnID.idorigin, visible:false, short:false} ,
                                {id : ColumnID.idleague, visible:false, short:false} ,
                                {id : ColumnID.league_name, visible:true, short:false} ,
                                {id : ColumnID.idcompetition, visible:false, short:false} ,
                                {id : ColumnID.competition_name, visible:true, short:false} ,
                                {id : ColumnID.last_game, visible:true, short:false} ,
                                {id : ColumnID.num_coaches, visible:true, short:false} ,
                                {id : ColumnID.num_teams, visible:true, short:false} ,
                                {id : ColumnID.num_games, visible:true, short:false} ,
                                {id : ColumnID.active, visible:true, short:false} 
                            ]
                    }
                ]
            }
        case ColumnLayout.CompTeamGraveyardView:
            return{
                groups:
                    [
                        {  name : "Player" ,cols: 
                                [
                                    {id : ColumnID.last_game, visible:true, short:true} ,
                                    {id : ColumnID.idplayer, visible:false, short:false} ,
                                    {id : ColumnID.idteam, visible:false, short:false} ,
                                    {id : ColumnID.idcompetition, visible:false, short:false} ,
                                    {id : ColumnID.idplayertype, visible:true, short:true} ,
                                    {id : ColumnID.player_name, visible:true, short:false} ,
                                    {id : ColumnID.level, visible:true, short:true} ,
                                    {id : ColumnID.xp, visible:true, short:true} ,
                                    {id : ColumnID.killed_by, visible:true, short:false} ,
                                    {id : ColumnID.idhighlighttype, visible:false, short:false} ,
                                    {id : ColumnID.idtarget, visible:false, short:false} ,
                                    {id : ColumnID.target_name, visible:false, short:false} ,
                                    {id : ColumnID.idcause, visible:false, short:false} ,
                                    {id : ColumnID.idtargetcompetition, visible:false, short:false} ,
                                    {id : ColumnID.idmatch, visible:false, short:false} ,

                                ]
                        },{
                            name : "Statline" ,cols: 
                            [
                                {id : ColumnID.ma, visible:true, short:true} ,
                                {id : ColumnID.st, visible:true, short:true} ,
                                {id : ColumnID.ag, visible:true, short:true} ,
                                {id : ColumnID.av, visible:true, short:true} ,
                                {id : ColumnID.skills, visible:true, short:false} ,
                                {id : ColumnID.cas_state, visible:true, short:true} ,
                                {id : ColumnID.cas_sustained, visible:false, short:true} ,
                            ]
                        },
                        {   name: "Play stats", cols:
                                [
                                    {id : ColumnID.gp, visible:true, short:true},
                                    {id : ColumnID.xp_gain, visible:true, short:true} ,
                                    {id : ColumnID.td, visible:true, short:true},
                                    {id : ColumnID.passes, visible:true, short:true},
                                    {id : ColumnID.completions, visible:true, short:true},
                                    {id : ColumnID.catches, visible:true, short:true},
                                    {id : ColumnID.interceptions, visible:true, short:true},
                                    {id : ColumnID.pushouts, visible:true, short:true},
                                    {id : ColumnID.expulsions, visible:true, short:true},
                                    {id : ColumnID.turnovers, visible:true, short:true},
                                    {id : ColumnID.run_meters, visible:true, short:true},
                                    {id : ColumnID.pass_meters, visible:true, short:true},                                      
                                ]},
                            {name: "Kill stats", cols:
                                [
                                    {id : ColumnID.blocks_for, visible:true, short:true},
                                    {id : ColumnID.breaks_for, visible:true, short:true},
                                    {id : ColumnID.stuns_for, visible:true, short:true},
                                    {id : ColumnID.kos_for, visible:true, short:true},
                                    {id : ColumnID.casualties_for, visible:true, short:true},
                                    {id : ColumnID.casualties_against, visible:true, short:true},
                                    {id : ColumnID.kills_for, visible:true, short:true},
                                    {id : ColumnID.kills_against, visible:true, short:true},
                                ]
                        }
                    ]
            }
            case ColumnLayout.CompTeamOvertakeStanding:
            case ColumnLayout.CompTeamStanding:
                case ColumnLayout.CompStandingsView:
                    let res= {
                groups:
                [
                    {  name : "Standings" ,cols: 
                            [
                                {id : ColumnID.idcompetition, visible:false, short:false} ,
                                {id : ColumnID.idteam, visible:false, short:false} ,
                                {id : ColumnID.position, visible:true, short:true} ,
                                {id : ColumnID.youtube, visible:false, short:true} ,
                                {id : ColumnID.twitch, visible:false, short:true} ,
                                {id : ColumnID.idcoach, visible:false, short:false} ,
                                {id : ColumnID.coach_name, visible:true, short:false} ,
                                {id : ColumnID.idrace, visible:true, short:true} ,
                                {id : ColumnID.logo, visible:true, short:true} ,
                                {id : ColumnID.team_name, visible:true, short:false} ,
                                {id : ColumnID.points, visible:true, short:false} 
                            ]
                        },
                    {   name: "Statistics", cols:
                            [
                                {id : ColumnID.ranking, visible:true, short:false} ,
                                {id : ColumnID.gp, visible:true, short:true},
                                {id : ColumnID.wins, visible:true, short:true} ,
                                {id : ColumnID.draws, visible:true, short:true} ,
                                {id : ColumnID.losses, visible:true, short:true},
                                {id : ColumnID.td, visible:true, short:true},
                                {id : ColumnID.td_opp, visible:false, short:true},
                                {id : ColumnID.td_diff, visible:true, short:true},
                                {id : ColumnID.cas, visible:true, short:true},
                                {id : ColumnID.cas_opp, visible:false, short:true},
                                {id : ColumnID.cas_diff, visible:true, short:true},
                                {id : ColumnID.kills, visible:false, short:false},
                                {id : ColumnID.team_value, visible:true, short:false},
                                {id : ColumnID.concedes, visible:false, short:true},
                                {id : ColumnID.active, visible:false, short:false},
                            ]
                    }
                ]
            };
            if(layout==ColumnLayout.CompTeamOvertakeStanding){
                res.groups[1].cols.splice(1,0,{id:ColumnID.overtake,visible:true,short:true});
            }
            return res;
            case ColumnLayout.CoachTeamsView:
            return {
                groups:
                [
                    {  name : "Teams" ,cols: 
                            [
                                {id : ColumnID.idcompetition, visible:false, short:false} ,
                                {id : ColumnID.idteam, visible:false, short:false} ,
                                {id : ColumnID.idrace, visible:true, short:true} ,
                                {id : ColumnID.logo, visible:true, short:true} ,
                                {id : ColumnID.team_name, visible:true, short:false} ,
                                {id : ColumnID.points, visible:true, short:false} 
                            ]
                        },
                    {   name: "Statistics", cols:
                            [
                                {id : ColumnID.ranking, visible:true, short:false} ,
                                {id : ColumnID.gp, visible:true, short:false},
                                {id : ColumnID.wins, visible:true, short:false} ,
                                {id : ColumnID.draws, visible:true, short:false} ,
                                {id : ColumnID.losses, visible:true, short:false},
                                {id : ColumnID.win_pct, visible:true, short:false},
                                
                                {id : ColumnID.td, visible:true, short:false},
                                {id : ColumnID.td_opp, visible:true, short:false},
                                {id : ColumnID.td_diff, visible:true, short:false},
                                {id : ColumnID.cas, visible:true, short:false},
                                {id : ColumnID.cas_opp, visible:true, short:false},
                                {id : ColumnID.cas_diff, visible:true, short:false},
                                {id : ColumnID.kills, visible:true, short:false},
                                {id : ColumnID.team_value, visible:true, short:false},
                                {id : ColumnID.concedes, visible:true, short:false},
                                {id : ColumnID.active, visible:true, short:false},
                            ]
                    }
                ]
            }
            case ColumnLayout.SearchTeamsView:
            return {
                groups:
                [
                    {  name : "Team" ,cols: 
                            [
                                {id : ColumnID.position, visible:true, short:true} ,
                                {id : ColumnID.idcoach, visible:false, short:false} ,
                                {id : ColumnID.coach_name, visible:true, short:false} ,
                                {id : ColumnID.idcompetition, visible:false, short:false} ,
                                {id : ColumnID.competition_name, visible:true, short:false} ,
                                {id : ColumnID.idteam, visible:false, short:false} ,
                                {id : ColumnID.idrace, visible:true, short:true} ,
                                {id : ColumnID.logo, visible:true, short:true} ,
                                {id : ColumnID.team_name, visible:true, short:false} ,
                                {id : ColumnID.points, visible:true, short:false} 
                            ]
                        },
                    {   name: "Statistics", cols:
                            [
                                {id : ColumnID.ranking, visible:true, short:true} ,
                                {id : ColumnID.gp, visible:true, short:true},
                                {id : ColumnID.wins, visible:true, short:true} ,
                                {id : ColumnID.draws, visible:true, short:true} ,
                                {id : ColumnID.losses, visible:true, short:true},
                                {id : ColumnID.td, visible:true, short:true},
                                {id : ColumnID.td_opp, visible:true, short:true},
                                {id : ColumnID.td_diff, visible:true, short:true},
                                {id : ColumnID.cas, visible:true, short:true},
                                {id : ColumnID.cas_opp, visible:true, short:true},
                                {id : ColumnID.cas_diff, visible:true, short:true},
                                {id : ColumnID.kills, visible:true, short:true},
                                {id : ColumnID.team_value, visible:true, short:true},
                                {id : ColumnID.concedes, visible:true, short:true},
                                {id : ColumnID.active, visible:true, short:true},
                            ]
                    }
                ]
            }
            case ColumnLayout.CompTeamView:
            return {
                groups:
                [
                    {  name : "Team" ,cols: 
                            [
                                {id : ColumnID.position, visible:true, short:true} ,
                                {id : ColumnID.youtube, visible:false, short:true} ,
                                {id : ColumnID.twitch, visible:false, short:true} ,
                                {id : ColumnID.idcoach, visible:false, short:false} ,
                                {id : ColumnID.coach_name, visible:true, short:false} ,
                                {id : ColumnID.idcompetition, visible:false, short:false} ,
                                {id : ColumnID.idteam, visible:false, short:false} ,
                                {id : ColumnID.idrace, visible:true, short:true} ,
                                {id : ColumnID.logo, visible:true, short:true} ,
                                {id : ColumnID.team_name, visible:true, short:false} ,
                                {id : ColumnID.points, visible:true, short:false} 
                            ]
                        },
                    {   name: "Statistics", cols:
                            [
                                {id : ColumnID.ranking, visible:true, short:true} ,
                                {id : ColumnID.gp, visible:true, short:true},
                                {id : ColumnID.wins, visible:true, short:true} ,
                                {id : ColumnID.draws, visible:true, short:true} ,
                                {id : ColumnID.losses, visible:true, short:true},
                                {id : ColumnID.td, visible:true, short:true},
                                {id : ColumnID.td_opp, visible:true, short:true},
                                {id : ColumnID.casualties_for, visible:true, short:true},
                                {id : ColumnID.casualties_against, visible:true, short:true},
                                {id : ColumnID.kills_for, visible:true, short:true},
                                {id : ColumnID.team_value, visible:true, short:true},
                                {id : ColumnID.concedes, visible:true, short:true},
                                {id : ColumnID.active, visible:true, short:true},
                            ]
                    }
                ]
            }

            case ColumnLayout.CompRacesView:
                return {
                    groups:
                    [
                        {  name : "" ,cols: 
                                [
                                    {id : ColumnID.idcompetition, visible:false, short:false} ,
                                    {id : ColumnID.idrace, visible:true, short:false} ,
                                  ]
                            },
                            {   name: "Overview", cols:
                            [
                                {id : ColumnID.duration, visible:true, short:true},
                                {id : ColumnID.gp, visible:true, short:true},
                                {id : ColumnID.wins, visible:true, short:true},
                                {id : ColumnID.draws, visible:true, short:true},
                                {id : ColumnID.losses, visible:true, short:true},
                                {id : ColumnID.win_pct, visible:true, short:true},
                                {id : ColumnID.boneheads, visible:true, short:true},
                                {id : ColumnID.turnovers, visible:true, short:true},
                            ]},
                            {   name: "Play stats", cols:
                            [
                                {id : ColumnID.dodges, visible:true, short:true},
                                {id : ColumnID.pickups, visible:true, short:true},
                                {id : ColumnID.rushes, visible:true, short:true},
                                {id : ColumnID.pass_meters, visible:true, short:true},
                                {id : ColumnID.run_meters, visible:true, short:true},
                                {id : ColumnID.passes, visible:true, short:true},
                                {id : ColumnID.completions, visible:true, short:true},
                                {id : ColumnID.catches, visible:true, short:true},
                                {id : ColumnID.interceptions, visible:true, short:true},
                                {id : ColumnID.pushouts, visible:true, short:true},
                            ]},
                        {name: "Kill stats", cols:
                            [
                                {id : ColumnID.blocks_for, visible:true, short:true},
                                {id : ColumnID.blocks_against, visible:true, short:true},
                                {id : ColumnID.breaks_for, visible:true, short:true},
                                {id : ColumnID.breaks_against, visible:true, short:true},
                                {id : ColumnID.stuns_for, visible:true, short:true},
                                {id : ColumnID.stuns_against, visible:true, short:true},
                                {id : ColumnID.kos_for, visible:true, short:true},
                                {id : ColumnID.kos_against, visible:true, short:true},
                                {id : ColumnID.casualties_for, visible:true, short:true},
                                {id : ColumnID.casualties_against, visible:true, short:true},
                                {id : ColumnID.kills_for, visible:true, short:true},
                                {id : ColumnID.kills_against, visible:true, short:true},
                                {id : ColumnID.fouls, visible:true, short:true},
                                {id : ColumnID.expulsions, visible:true, short:true},
                                                         
                            ]
                    }
                    ]
                }
            case ColumnLayout.CompTeamStatsView:
            return {
                groups:
                [
                    {  name : "" ,cols: 
                            [
                                {id : ColumnID.youtube, visible:false, short:true} ,
                                {id : ColumnID.twitch, visible:false, short:true} ,
                                {id : ColumnID.idcoach, visible:false, short:false} ,
                                {id : ColumnID.coach_name, visible:true, short:false} ,
                                {id : ColumnID.idcompetition, visible:false, short:false} ,
                                {id : ColumnID.idteam, visible:false, short:false} ,
                                {id : ColumnID.idrace, visible:true, short:true} ,
                                {id : ColumnID.logo, visible:true, short:true} ,
                                {id : ColumnID.team_name, visible:true, short:false} ,

                                {id : ColumnID.concedes, visible:true, short:true},
                                {id : ColumnID.active, visible:true, short:true},
                            ]
                        },
                        {   name: "Overview", cols:
                        [
                            {id : ColumnID.duration, visible:true, short:true},
                            {id : ColumnID.gp, visible:true, short:true},
                            {id : ColumnID.wins, visible:true, short:true},
                            {id : ColumnID.draws, visible:true, short:true},
                            {id : ColumnID.losses, visible:true, short:true},
                            {id : ColumnID.win_pct, visible:true, short:true},
                            {id : ColumnID.boneheads, visible:true, short:true},
                            {id : ColumnID.turnovers, visible:true, short:true},
                        ]},
                        {   name: "Play stats", cols:
                        [
                            {id : ColumnID.dodges, visible:true, short:true},
                            {id : ColumnID.pickups, visible:true, short:true},
                            {id : ColumnID.rushes, visible:true, short:true},
                            {id : ColumnID.pass_meters, visible:true, short:true},
                            {id : ColumnID.run_meters, visible:true, short:true},
                            {id : ColumnID.passes, visible:true, short:true},
                            {id : ColumnID.completions, visible:true, short:true},
                            {id : ColumnID.catches, visible:true, short:true},
                            {id : ColumnID.interceptions, visible:true, short:true},
                            {id : ColumnID.pushouts, visible:true, short:true},
                        ]},
                    {name: "Kill stats", cols:
                        [
                            {id : ColumnID.blocks_for, visible:true, short:true},
                            {id : ColumnID.blocks_against, visible:true, short:true},
                            {id : ColumnID.breaks_for, visible:true, short:true},
                            {id : ColumnID.breaks_against, visible:true, short:true},
                            {id : ColumnID.stuns_for, visible:true, short:true},
                            {id : ColumnID.stuns_against, visible:true, short:true},
                            {id : ColumnID.kos_for, visible:true, short:true},
                            {id : ColumnID.kos_against, visible:true, short:true},
                            {id : ColumnID.casualties_for, visible:true, short:true},
                            {id : ColumnID.casualties_against, visible:true, short:true},
                            {id : ColumnID.kills_for, visible:true, short:true},
                            {id : ColumnID.kills_against, visible:true, short:true},
                            {id : ColumnID.fouls, visible:true, short:true},
                            {id : ColumnID.expulsions, visible:true, short:true},
                                                     
                        ]
                }
                ]
            }
            case ColumnLayout.CompTeamPlayerView:
                return {
                    groups:
                    [
                        {  name : "Player" ,cols: 
                                [
                                    {id : ColumnID.number, visible:true, short:false} ,
                                    {id : ColumnID.idplayer, visible:false, short:false} ,
                                    {id : ColumnID.idteam, visible:false, short:false} ,
                                    {id : ColumnID.idcompetition, visible:false, short:false} ,
                                    {id : ColumnID.idplayertype, visible:true, short:true} ,
                                    {id : ColumnID.player_name, visible:true, short:false} ,
                                    {id : ColumnID.level, visible:true, short:true} ,
                                    {id : ColumnID.xp, visible:true, short:true} ,
                                    {id : ColumnID.xp_gain, visible:false, short:true} ,
                                    ]
                        },{
                            name : "Statline" ,cols: 
                            [
                                {id : ColumnID.ma, visible:true, short:true} ,
                                {id : ColumnID.st, visible:true, short:true} ,
                                {id : ColumnID.ag, visible:true, short:true} ,
                                {id : ColumnID.av, visible:true, short:true} ,
                                {id : ColumnID.skills, visible:true, short:false} ,
                                {id : ColumnID.cas_state, visible:true, short:true} ,
                                {id : ColumnID.cas_sustained, visible:true, short:true} ,
                            ]
                        },
                        {   name: "Play stats", cols:
                                [
                                    {id : ColumnID.gp, visible:true, short:true},
                                    {id : ColumnID.td, visible:true, short:true},
                                    {id : ColumnID.xp_gain, visible:true, short:true},
                                    {id : ColumnID.passes, visible:true, short:true},
                                    {id : ColumnID.completions, visible:true, short:true},
                                    {id : ColumnID.catches, visible:true, short:true},
                                    {id : ColumnID.interceptions, visible:true, short:true},
                                    {id : ColumnID.pushouts, visible:true, short:true},
                                    {id : ColumnID.expulsions, visible:true, short:true},
                                    {id : ColumnID.turnovers, visible:true, short:true},
                                    {id : ColumnID.run_meters, visible:true, short:true},
                                    {id : ColumnID.pass_meters, visible:true, short:true},                                    
                                ]},
                            {name: "Kill stats", cols:
                                [
                                    {id : ColumnID.blocks_for, visible:true, short:true},
                                    {id : ColumnID.breaks_for, visible:true, short:true},
                                    {id : ColumnID.stuns_for, visible:true, short:true},
                                    {id : ColumnID.kos_for, visible:true, short:true},
                                    {id : ColumnID.casualties_for, visible:true, short:true},
                                    {id : ColumnID.casualties_against, visible:true, short:true},
                                    {id : ColumnID.kills_for, visible:true, short:true},
                                    {id : ColumnID.kills_against, visible:true, short:true},
                                ]
                        }
                    ]
                }
            case ColumnLayout.CompPlayerView:
                return {
                    groups:
                    [
                        {  name : "Player" ,cols: 
                                [
                                    {id : ColumnID.idteam, visible:false, short:false} ,
                                    {id : ColumnID.team_name, visible:true, short:false} ,
                                    {id : ColumnID.idplayer, visible:false, short:false} ,
                                    {id : ColumnID.idteam, visible:false, short:false} ,
                                    {id : ColumnID.idcompetition, visible:false, short:false} ,
                                    {id : ColumnID.idplayertype, visible:true, short:true} ,
                                    {id : ColumnID.player_name, visible:true, short:false} ,
                                    {id : ColumnID.level, visible:true, short:true} ,
                                    {id : ColumnID.mvp, visible:true, short:true} ,
                                    {id : ColumnID.xp, visible:true, short:true} ,
                                    {id : ColumnID.xp_gain, visible:false, short:true} ,
                                    ]
                        },{
                            name : "Statline" ,cols: 
                            [
                                {id : ColumnID.ma, visible:true, short:true} ,
                                {id : ColumnID.st, visible:true, short:true} ,
                                {id : ColumnID.ag, visible:true, short:true} ,
                                {id : ColumnID.av, visible:true, short:true} ,
                                {id : ColumnID.skills, visible:true, short:false} ,
                                {id : ColumnID.active, visible:true, short:true} ,
                            ]
                        },
                        {   name: "Play stats", cols:
                                [
                                    {id : ColumnID.gp, visible:true, short:true},
                                    {id : ColumnID.td, visible:true, short:true},
                                    {id : ColumnID.xp_gain, visible:true, short:true},
                                    {id : ColumnID.passes, visible:true, short:true},
                                    {id : ColumnID.completions, visible:true, short:true},
                                    {id : ColumnID.catches, visible:true, short:true},
                                    {id : ColumnID.interceptions, visible:true, short:true},
                                    {id : ColumnID.pushouts, visible:true, short:true},
                                    {id : ColumnID.fouls, visible:true, short:true},
                                    {id : ColumnID.expulsions, visible:true, short:true},
                                    {id : ColumnID.turnovers, visible:true, short:true},
                                    {id : ColumnID.run_meters, visible:true, short:true},
                                    {id : ColumnID.pass_meters, visible:true, short:true},                                    
                                   ]},
                            {name: "Kill stats", cols:
                                [
                                    {id : ColumnID.blocks_for, visible:true, short:true},
                                    {id : ColumnID.breaks_for, visible:true, short:true},
                                    {id : ColumnID.stuns_for, visible:true, short:true},
                                    {id : ColumnID.kos_for, visible:true, short:true},
                                    {id : ColumnID.casualties_for, visible:true, short:true},
                                    {id : ColumnID.casualties_against, visible:true, short:true},
                                    {id : ColumnID.kills_for, visible:true, short:true},
                                    {id : ColumnID.kills_against, visible:true, short:true},
                                ]
                        }
                    ]
                }
                case ColumnLayout.MatchPlayersView:
                    return {
                        groups:
                        [
                            {  name : "Player" ,cols: 
                                    [
                                        {id : ColumnID.idcompetition, visible:false, short:false} ,
                                        {id : ColumnID.idplayer, visible:false, short:false} ,
                                        {id : ColumnID.idcoach, visible:false, short:false} ,
                                        {id : ColumnID.coach_name, visible:false, short:false} ,
                                        {id : ColumnID.idteam, visible:false, short:false} , 
                                        {id : ColumnID.team_name, visible:true, short:false} ,
                                        {id : ColumnID.logo, visible:true, short:true} ,
                                        {id : ColumnID.home, visible:false, short:false} ,
                                        
                                        {id : ColumnID.number, visible:true, short:true} ,
                                        {id : ColumnID.idplayertype, visible:true, short:true} ,
                                        {id : ColumnID.player_name, visible:true, short:false} ,
                                        {id : ColumnID.level, visible:true, short:true} ,
                                        {id : ColumnID.xp, visible:true, short:true} ,
                                        {id : ColumnID.xp_gain, visible:false, short:true} ,
                                        ]
                            },{
                                name : "Statline" ,cols: 
                                [
                                    {id : ColumnID.ma, visible:true, short:true} ,
                                    {id : ColumnID.st, visible:true, short:true} ,
                                    {id : ColumnID.ag, visible:true, short:true} ,
                                    {id : ColumnID.av, visible:true, short:true} ,
                                    {id : ColumnID.skills, visible:true, short:false} ,
                                    {id : ColumnID.cas_state, visible:true, short:true} ,
                                    {id : ColumnID.cas_sustained, visible:false, short:true} ,
                                ]
                            },
                            {   name: "Play stats", cols:
                                    [
                                        {id : ColumnID.td, visible:true, short:true},
                                        {id : ColumnID.xp_gain, visible:true, short:true},
                                        {id : ColumnID.passes, visible:true, short:true},
                                        {id : ColumnID.completions, visible:true, short:true},
                                        {id : ColumnID.catches, visible:true, short:true},
                                        {id : ColumnID.interceptions, visible:true, short:true},
                                        {id : ColumnID.pushouts, visible:true, short:true},
                                        {id : ColumnID.expulsions, visible:true, short:true},
                                        {id : ColumnID.turnovers, visible:true, short:true},
                                        {id : ColumnID.run_meters, visible:true, short:true},
                                        {id : ColumnID.pass_meters, visible:true, short:true},
                                                                                
                                    ]},
                                {name: "Kill stats", cols:
                                    [
                                        {id : ColumnID.blocks_for, visible:true, short:true},
                                        {id : ColumnID.breaks_for, visible:true, short:true},
                                        {id : ColumnID.stuns_for, visible:true, short:true},
                                        {id : ColumnID.kos_for, visible:true, short:true},
                                        {id : ColumnID.casualties_for, visible:true, short:true},
                                        {id : ColumnID.casualties_against, visible:true, short:true},
                                        {id : ColumnID.kills_for, visible:true, short:true},
                                        {id : ColumnID.kills_against, visible:true, short:true},
                                    ]
                            }
                        ]
                    }

           


            case ColumnLayout.CompResultView:
            return {
                groups:
                [
                    {  name : "Results" ,cols: 
                            [
                                {id : ColumnID.idcompetition, visible:false, short:true} ,
                                {id : ColumnID.idmatch, visible:true, short:true} ,
                                {id : ColumnID.round, visible:true, short:true, altered:true} ,
                                {id : ColumnID.started, visible:false, short:true} ,
                                {id : ColumnID.finished, visible:true, short:true} ,
                            ]
                        },
                    {  name : "Home" ,cols: 
                            [
                                {id : ColumnID.idcoach_home, visible:false, short:true} ,
                                {id : ColumnID.coach_name_home, visible:true, short:false} ,
                                {id : ColumnID.idteam_home, visible:false, short:false} ,
                                {id : ColumnID.logo_home, visible:true, short:true} ,
                                {id : ColumnID.team_name_home, visible:true, short:false} ,
                                {id : ColumnID.cas_home, visible:true, short:false} ,
                                {id : ColumnID.score_home, visible:true, short:false} ,
                            ]
                        },
                    {   name: "Away", cols:
                            [
                                {id : ColumnID.score_away, visible:true, short:false} ,
                                {id : ColumnID.cas_away, visible:true, short:false} ,
                                {id : ColumnID.team_name_away, visible:true, short:false} ,
                                {id : ColumnID.idteam_away, visible:false, short:false} ,
                                {id : ColumnID.logo_away, visible:true, short:true} ,
                                {id : ColumnID.coach_name_away, visible:true, short:false} ,
                                {id : ColumnID.idcoach_away, visible:false, short:true} ,
                            ]
                    }
                ]
            }
      }
      return {groups:[]};

  }

  static writeDate(date : Date, short : boolean) : string{
    if(short) return (date.getDate())+"/"+(1+date.getMonth());
    return date.getFullYear()
    + '-' + this.leftpad(""+(date.getMonth() + 1), 2, "0")
    + '-' + this.leftpad(""+(date.getDate()), 2, "0");
}

static leftpad(str : string, num : number, padder : string) : string{
    while(str.length < num) str = padder+str;
    return str;
}

static writeDay(date : number, short : boolean) : string{
    if(short){
        let days= ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        return days[date];
    } 
    let days= ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    return days[date];
}
static writeTime(date : Date, short : boolean) : string{
    if(short) return date.toLocaleTimeString();
    return  (date.getHours() )+ ':' + this.leftpad(""+(date.getMinutes() ), 2, "0")
    + ':' + this.leftpad(""+(date.getSeconds()), 2, "0");
}

public static writeDateTime(str : string, short : boolean) : string{
    if(!str) return "";
    if(str.endsWith("Z") == false){ str+="Z";}
    var date = new Date(Date.parse(str));
    return this.writeDate(date, short)+" "+this.writeTime(date, short);
}

  static getRowClass(layoutID : ColumnLayout ,layout : ViewColumns, cols : {[id: string] : number}, row : string[]) : string{


        let injColIndex = cols[ColumnID[ColumnID.cas_sustained]];
        if(injColIndex!==undefined){
            let inj = row[injColIndex];
            if(inj==="") return "";
            if(this.injuryTypes.missesGame(inj)){
                return "missGame";
            }

        }

        let activeColIndex = cols[ColumnID[ColumnID.active]];
        if(activeColIndex!==undefined){
            let active = row[activeColIndex];
            if(active==="0")
            { return "inactive";
            }
        }

   
        return "";
  }
 
  static getCellDisplay(table : GSpyTableCommandReceiver ,layout : ViewColumns, cols : {[id: string] : number}, row : string[], col : Column, value : string ) : React.ReactElement
  {
  
   
    switch(col.id){
        case ColumnID.number:
            if(value=="0")return <span></span>
            return <span>{value}</span>
        case ColumnID.userdate:
            let idcontest =+row[cols[ColumnID[ColumnID.schedule_origin_id]]]
            let idcomp = +row[cols[ColumnID[ColumnID.idcompetition]]]
            if(value){value+="Z"}
            return <span title={value}><UserDate idcontest={idcontest} idcomp={idcomp} userDate={value} /></span>
        case ColumnID.win_pct:
            {
            let twoDigits = (+value).toFixed(2)
            let threeDigits = (+value).toFixed(3)
            return <span title={threeDigits}>{twoDigits}</span>;
            }
        case ColumnID.overtake:
            let tt = value.startsWith("-")?(""+value.substr(1)+" wins required to overtake marked teams position"):
            (""+value.substr(1)+" wins required to overtake this position for the marked team")
            return <span title={tt}>{value}</span>
        case ColumnID.ranking:
            let twoDigits = (+value).toFixed(2)
            let threeDigits = (+value).toFixed(3)

            let wincol = cols[ColumnID[ColumnID.wins]]
            let drawcol = cols[ColumnID[ColumnID.draws]]
            let losscol = cols[ColumnID[ColumnID.losses]]
            let conccol = cols[ColumnID[ColumnID.concedes]]
  
            let tooltip = "Rank "+threeDigits;
            if(wincol!=undefined&&drawcol!=undefined&&losscol!=undefined&&conccol!=undefined)
            {
                tooltip=
                "Rank "+threeDigits+"\n"+
                "Another win => "+Rank.calculateRank(+row[wincol]+1,+row[drawcol],+row[losscol],+row[conccol]).toFixed(3)+"\n"+
                "Another draw => "+Rank.calculateRank(+row[wincol],+row[drawcol]+1,+row[losscol],+row[conccol]).toFixed(3)+"\n"+
                "Another loss => "+Rank.calculateRank(+row[wincol],+row[drawcol],+row[losscol]+1,+row[conccol]).toFixed(3);
            }


            return <span title={tooltip}>{twoDigits}</span>;
        case ColumnID.cas_state:
        case ColumnID.cas_sustained:
            
            let imgArr : string[] = [];
            let nameArr : string[] = [];
            if(value){
                let valueObj = JSON.parse(value)
                for(let i=0; i < valueObj.length;i++){
                    let inj = valueObj[i]
                    
                    imgArr.push("/gspy/"+this.injuryTypes.idToImg[inj]);
                    nameArr.push(this.injuryTypes.idToName[inj]);
                    
                }
            }
            return <div>{imgArr.map((v,i)=>{return <img key={i} className="iconSmall" alt={v} src={v} title={nameArr[i]}></img>})}</div>
            
        case ColumnID.idorigin:
           
            switch(value){
            case "1": return <img alt="PC" className="iconSmall" title="PC" src={pc}></img>
            case "2": return <img alt="XB1"  className="iconSmall"  title="XB1" src={xb1}></img>
            case "3": return <img alt="PS4"  className="iconSmall"  title="PS4" src={ps4}></img>
            default:
                return <span>{value}</span>
            }
        case ColumnID.active:
        case ColumnID.collecting:
            if(value && +value>=1) return <img alt="Yes" className="iconSmall" title="Yes" src={yesimg}></img>
            return <img alt="No" className="iconSmall" title="No" src={inactiveimg}></img>
        case ColumnID.collection_name:
            let lidcol = cols[ColumnID[ColumnID.idleague]]
            let lid = row[lidcol]
            let cidcol = cols[ColumnID[ColumnID.collection_name]]
            let cid = row[cidcol] 
            return <Link to={"/comp/"+cid}><img className="iconSmall" src={Look} alt="vs"></img> {value}</Link>
        case ColumnID.league_name:
            {
            let lidcol = cols[ColumnID[ColumnID.idleague]]
            let lid = row[lidcol]
            if(!lid) return <span>{value}</span>
            let link = "/league/"+lid
            return <Link to={link}  title="Click to view">{value}</Link>
            }
        case ColumnID.competition_name:
            {
            let lidcol = cols[ColumnID[ColumnID.idcompetition]]
            let lid = row[lidcol]
            if(!lid) return <span>{value}</span>
            let link = "/comp/"+lid
            return <Link to={link} title="Click to view">{value}</Link>
            }
        case ColumnID.youtube:
            return <a target="_blank" href={value} className="imageButton"><img className="iconThin" src={youtubeImg}></img></a> 
        case ColumnID.twitch:
            return <a target="_blank" href={value} className="imageButton"><img className="iconThin" src={twitchImg}></img></a>
            
        case ColumnID.coach_name:
        case ColumnID.coach_name_home:
        case ColumnID.coach_name_away:
            {
            if(!value) value = "AI";

            let cidcol = cols[ColumnID[col.id===ColumnID.coach_name?ColumnID.idcoach:(col.id===ColumnID.coach_name_home?ColumnID.idcoach_home:ColumnID.idcoach_away)]]
            let cid = row[cidcol]
            if(!cid) return <span>{value}</span>

            let compidcol = cols[ColumnID[ColumnID.idcompetition]]
            let compid = row[compidcol]

            let link = "/coach/"+cid;
            if (compid){
                link = "/comp/"+compid+"/coach/"+cid;
            }

            let youtubecol = cols[ColumnID[ColumnID.youtube]]
            let youtube = row[youtubecol]

            let twitchcol = cols[ColumnID[ColumnID.twitch]]
            let twitch = row[twitchcol]

                

            return <span>
                {youtube&&<a title="Click to open youtube" target="_blank" href={youtube} className="imageButton"><img  className="iconThin" src={youtubeImg}></img></a>}
                {twitch&& <a title="Click to open twitch" target="_blank" href={twitch} className="imageButton"><img  className="iconThin" src={twitchImg}></img></a>}
                <Link to={link}  title="Click to view">{value}</Link></span>
            }
        case ColumnID.player_name:
            {
                let pidcol = cols[ColumnID[ColumnID.idplayer]]
                let pid = row[pidcol]
                if(!pid) return <span>{value}</span>
    
                let compidcol = cols[ColumnID[ColumnID.idcompetition]]
                let compid = row[compidcol]
    
                let link = "/player/"+pid;
                if (compid){
                    link = "/comp/"+compid+"/player/"+pid;
                }
                if(value.startsWith("PLAYER_NAMES_CHAMPION_"))
                {
                    value = value.substr("PLAYER_NAMES_CHAMPION_".length);
                }
                if(value.indexOf("_FALLBACK")>=0)
                {
                    value = value.replace("_FALLBACK","");
                }
                //return <Link to={link}  title="Click to view">{value}</Link>
            
           return <span>{value}</span>
            }
        case ColumnID.team_name:
        case ColumnID.team_name_home:
        case ColumnID.team_name_away:
            {
            let cidcol = cols[ColumnID[col.id===ColumnID.team_name?ColumnID.idteam:(col.id===ColumnID.team_name_home?ColumnID.idteam_home:ColumnID.idteam_away)]]
            let cid = row[cidcol]
            if(!cid) return <span>{value}</span>

            let compidcol = cols[ColumnID[ColumnID.idcompetition]]
            let compid = row[compidcol]

            let link = "/team/"+cid;
            if (compid){
                link = "/comp/"+compid+"/team/"+cid;
            }

            return <Link to={link}  title="Click to view">{value}</Link>
            }
        case ColumnID.idrace:
        case ColumnID.idrace_home:
        case ColumnID.idrace_away:
        {
            let name = Races.GetName(+(value));
            let src="/gspy/gfx/races/"+name+".png";
            return <button title={name+", click to add as filter"} onClick={()=>{
                table.onCommand(col, "filter:="+value);
            }}>
                <span><img alt={name} className="iconSmall" src={src}></img>
                {col.short==false&&<span >{name}</span>}</span>
            </button>
            } 
        case ColumnID.idplayertype:
            {
                let conv = ViewColumns.playerTypes.idToName[+value];
                let shortConv = conv;
                if(conv){
                let underscore = conv.indexOf("_");
                if(underscore >= 0){
                    shortConv = conv.substr(underscore+1)
                }
                }
                return <span title={conv}>{shortConv}</span>
            }
        case ColumnID.logo:
        case ColumnID.logo_home:
        case ColumnID.logo_away:
            {
            let name = ((value));
            if(!name){ name = "AI"}
            let src="/gspy/gfx/logos/"+name+".png"; 
            return <img alt={name} title={name} className="iconSmall" src={src}></img>
            }
        case ColumnID.last_game:
        case ColumnID.last_checked:
            return <span>{this.writeDateTime(value,col.short)}</span>

        case ColumnID.day:
            let daystring=this.writeDay(+value-1,col.short);
            return <span>{daystring}</span>;
        case ColumnID.time:
            let timestring=this.writeTime(new Date(Date.parse('1970-01-01T' +value+"Z")),col.short); 
            return <span>{timestring}</span>;
        case ColumnID.finished:
        case ColumnID.started:
            let datestring=this.writeDate(new Date(Date.parse(value)),col.short);
            return <span>{datestring}</span>;
        case ColumnID.idmatch:
            let compidcol = cols[ColumnID[ColumnID.idcompetition]]
            let compid = row[compidcol]
            if(compid){
                let url = "/comp/"+compid+"/match/"+value;
                return <Link to={url} title="Click to view"><img className="iconSmall" src={LookVs} alt="vs"></img></Link>;
            }
            else{
                let url2 = "/match/"+value;
                return <Link to={url2} title="Click to view"><img className="iconSmall" src={LookVs} alt="vs"></img></Link>;
            }
            
        case ColumnID.ag: 
        case ColumnID.ma: 
        case ColumnID.st: 
        case ColumnID.av: 
            let casicol = cols[ColumnID[ColumnID.cas_state]]
            let cascur = row[casicol]
            let skcol = cols[ColumnID[ColumnID.skills]]
            let sk = row[skcol]
            let className = ""
            let valToShow= +value;
            if(cascur){
                valToShow = this.injuryTypes.alterStat(ColumnID[col.id], valToShow, cascur)
            }
            if(sk){
                valToShow = this.skillTypes.alterStat(ColumnID[col.id], valToShow, sk)
            }
            if(valToShow > +value){
                className="pos bolder";
            }
            if(valToShow < +value){
                className="neg bolder";
            }
            return <span className={className}>{value}</span>
        
        case ColumnID.skills:
            let imgArrSkills : string[] = [];
            let nameArrSkills : string[] = [];
            if(value){
                let valueObj = JSON.parse(value);
                if(valueObj){
                for(let i=0; i < valueObj.length;i++){
                    let skill = valueObj[i];
                    imgArrSkills.push("/gspy/gfx/skills/"+skill+".png");
                    nameArrSkills.push(skill);
                    }
                }
            }
            return <div>{imgArrSkills.map((v,i)=>{return <img key={i} className="iconSmall" alt={v} src={v} title={nameArrSkills[i]}></img>})}</div>
        case ColumnID.killed_by:
            return highlightManager.getReactKilledMinimmalRow(cols,row)
            
        case ColumnID.wins:
        case ColumnID.td:
        case ColumnID.cas:
        case ColumnID.kills:
            case ColumnID.blocks_for:
                case ColumnID.breaks_for:
                    case ColumnID.stuns_for:
                        case ColumnID.kos_for:
                            case ColumnID.casualties_for:
                                case ColumnID.kills_for:
                                    if( value &&(value.indexOf(".") > 0 || value.indexOf(",") > 0) && parseFloat(value) != NaN) // float
                                    {
                                        value = parseFloat(value.split(",").join(".")).toFixed(2);
                                    }
                                                return <span className="pos">{value}</span>
        case ColumnID.losses: 
        case ColumnID.td_opp:
        case ColumnID.cas_opp:
        case ColumnID.concedes:
            case ColumnID.blocks_against:
                case ColumnID.breaks_against:
                    case ColumnID.stuns_against:
                        case ColumnID.kos_against:
                            case ColumnID.casualties_against:
                                case ColumnID.kills_against:   
                                if( value && (value.indexOf(".") > 0 || value.indexOf(",") > 0) && parseFloat(value) != NaN) // float
                                {
                                    value = parseFloat(value.split(",").join(".")).toFixed(2);
                                }         
        return <span className="neg">{value}</span>


            default:
                if( value &&(value.indexOf(".") > 0 || value.indexOf(",") > 0) && parseFloat(value) != NaN) // float
                {
                    value = parseFloat(value.split(",").join(".")).toFixed(2);
                }
            return <span>{value}</span>;
    }
  }
  static getColumnDisplay(col : Column) : React.ReactElement{
    return <span>{this.getColumnDisplayString(col)}</span>
}


static getCustomFilter(col : Column, value:string,onChange:(value: string) => void, onExecute:(value: string) => void) : React.ReactElement | undefined
{
    switch(col.id)
    {
        case ColumnID.idrace:
            case ColumnID.idrace_home:
                case ColumnID.idrace_away:
                    let list = Races.GetList()
        // let els : React.ReactElement[] =[]
         /*
            for (let key in  map) 
            {
                els.push(<option  value={"="+map[key].id} > {map[key].name}</option>)
            };

            return <div><select id='raceFilter' defaultValue="">{els}</select><br/>
            <button className="executeFilter" onClick={()=>{onExecute((document.getElementById("raceFilter") as HTMLSelectElement).selectedOptions[0].value);}}>Set filter</button>
            </div>*/
            let alteredValue=this.getCustomFilterMarker(col,value)
            let autoFill : React.ReactElement[]=[]
            for(let i=0; i < list.length;i++){if(list[i].name!="Unknown") autoFill.push(<option key={list[i].name}>{list[i].name}</option>)}

            return <div><datalist key="raceAutoFill"  id='raceAutofill'>
                {autoFill}
            </datalist>
                <input autoFocus={true}  id='raceFilter' defaultValue={alteredValue}  list="raceAutofill" placeholder="Enter filter text here" 
                        onChange={()=>{onChange((document.getElementById("raceFilter") as HTMLInputElement).value)}} 
                        onKeyPress={(event)=>{
                            if (event.key === "Enter") {
                                let filter=(document.getElementById("raceFilter") as HTMLInputElement).value;
                                let idrace = Races.Parse(filter)
                                onExecute("="+idrace);
                               }
                        }}
                        ></input>
                <button className="executeFilter" onClick={()=>
                        {
                            let filter=(document.getElementById("raceFilter") as HTMLInputElement).value;
                            let idrace = Races.Parse(filter)
                            onExecute("="+idrace);
                        }
                    }>Set filter</button>
            </div>

            break;
    }
    return undefined;
}
static getCustomFilterMarker(col : Column, filter : string) : string
{
    if(!filter) return filter;
    
    switch(col.id)
    {
        case ColumnID.idrace:
            case ColumnID.idrace_home:
                case ColumnID.idrace_away:
                    let map = Races.GetMap()
            for (let key in  map) 
            {
                if(map[key]!="Dwarf" && map[key]!="Chaos" && map[key]!="Human")
                    filter = filter.replace("="+key,map[key])
            }
            filter = filter.replace("=2","Dwarf")
            filter = filter.replace("=1","Human")
            filter = filter.replace("=8","Chaos")
            return filter;
            break;
    }
    return "";
}

  static getColumnTooltip(col : Column) : string{
      switch(col.id){
        case ColumnID.rerolls: return "Rerolls"
        case ColumnID.apo: return "Apothecary"
        case ColumnID.cheerleaders: return "Cheerleaders"
        case ColumnID.cash_spent: return "Cash spent"
        case ColumnID.cash_before_match: return "Cash before match"
        case ColumnID.cash_earned: return "Cash earned"
        case ColumnID.possession: return "Possession"
        case ColumnID.mvp: return "Most Valuable Player"          
        case ColumnID.round : return "Round"
        case ColumnID.active: return "Active";
        case ColumnID.collecting: return "Collecting";
        case ColumnID.competition_name: return "Competition Name";
        case ColumnID.competition_origin_id: return "Competition Origin Id";
        case ColumnID.format: return "Format";
        case ColumnID.idcompetition: return "Competition Id";
        case ColumnID.idleague: return "League Id";
        case ColumnID.idorigin: return "Origin Id";
        case ColumnID.last_game: return "Last game";
        case ColumnID.league_name: return "League";
        case ColumnID.description: return "Description";
        case ColumnID.collection_name: return "Collection";
        case ColumnID.league_origin_id: return "League Origin Id";
        case ColumnID.num_coaches: return "Number of Coaches";
        case ColumnID.num_games: return "Number of Games played";
        case ColumnID.num_teams: return "Number of Teams";
        case ColumnID.sorting: return "Sorting";
        case ColumnID.concedes: return "Concedes";
        case ColumnID.conceded: return "Conceded";
        case ColumnID.conceded_opp: return "Conceded";
        case ColumnID.concedes_opp: return "Concedes by opponent";
        case ColumnID.win_pct:return "Win percentage";
        case ColumnID.wins: return "Wins"
        case ColumnID.draws: return "Draws"
        case ColumnID.losses: return "Losses"
        case ColumnID.gp: return "Games Played"
        case ColumnID.youtube: return "Youtube"
        case ColumnID.twitch: return "Twitch"
        case ColumnID.idcoach: 
        case ColumnID.idcoach_home:
        case ColumnID.idcoach_away:
                return "Coach Id"
        case ColumnID.coach_name: 
        case ColumnID.coach_name_home: 
        case ColumnID.coach_name_away: 
                return "Coach Name"
        case ColumnID.idteam: 
        case ColumnID.idteam_home: 
        case ColumnID.idteam_away: 
            return "Team Id"
        case ColumnID.team_name: 
        case ColumnID.team_name_home: 
        case ColumnID.team_name_away: 
            return "Team Name"
        case ColumnID.logo:
        case ColumnID.logo_home: 
        case ColumnID.logo_away: 
            return "Logo"
        case ColumnID.player_name: return "Player name"
        case ColumnID.idplayer: return "Player Id"
        case ColumnID.idplayertype:  return "Player type"
        case ColumnID.xp: return "XP"
        case ColumnID.xp_gain: return "XP gain"
        case ColumnID.number: return "Number"
        case ColumnID.level: return "Level"
        case ColumnID.ma: return "Movement"
        case ColumnID.ag: return "Agility"
        case ColumnID.av: return "Armour value"
        case ColumnID.st: return "Strength"
        case ColumnID.skills: return "Skills"
        case ColumnID.cas_state: return "Injuries"
        case ColumnID.cas_sustained: return "New injuries"
        case ColumnID.passes: return "Passes"
        case ColumnID.completions: return "Completions"
        case ColumnID.pass_meters: return "Pass meters"
        case ColumnID.run_meters: return "Run meters"
        case ColumnID.occ_own: return "Occupation own half"
        case ColumnID.occ_their: return "Occupation their half"
        case ColumnID.duration: return "Duration in minutes"
        case ColumnID.catches: return "Catches"
        case ColumnID.interceptions: return "Interceptions"
        case ColumnID.pushouts: return "Pushouts"
        case ColumnID.expulsions: return "Expulsions"
        case ColumnID.turnovers: return "Turnovers"
        case ColumnID.fouls: return "Fouls"
        case ColumnID.dodges: return "Dodges"
        case ColumnID.dodges_failed: return "Failed dodges"
        case ColumnID.rushes: return "Rushes"
        case ColumnID.rushes_failed: return "Failed rushes"
        case ColumnID.killed_by: return "Killed by"
        case ColumnID.pickups: return "Pickups"
        case ColumnID.pickups_failed: return "Failed pickups"
        case ColumnID.boneheads: return "Bonehead (big guy failure)"
        case ColumnID.blocks_for: return "Blocks for"
        case ColumnID.breaks_for: return "Armor breaks for"
        case ColumnID.stuns_for: return "Stuns for"
        case ColumnID.kos_for: return "Knockouts for"
        case ColumnID.casualties_for: return "Casualties for"
        case ColumnID.kills_for: return "Kills for"
        case ColumnID.blocks_against: return "Blocks against"
        case ColumnID.breaks_against: return "Breaks against"
        case ColumnID.stuns_against: return "Stuns against"
        case ColumnID.kos_against: return "Knockouts against"
        case ColumnID.casualties_against: return "Casualties against"
        case ColumnID.kills_against: return "Kills against"        
        case ColumnID.points: return "Points"
        case ColumnID.ranking: return "Ranking points"
        case ColumnID.overtake: return "Wins needed to overtake position"
        case ColumnID.position: return "Position"
        case ColumnID.td: return "Touchdowns"
        case ColumnID.td_opp: return "Touchdowns by opponent"
        case ColumnID.td_diff: return "Touchdown difference"
        case ColumnID.cas: return "Casualties made"
        case ColumnID.cas_opp: return "Casualties made by opponent"
        case ColumnID.cas_home: return "Casualties made"
        case ColumnID.cas_away: return "Casualties made"
        case ColumnID.cas_diff: return "Casualties difference"
        case ColumnID.kills: return "Kills made"
        case ColumnID.team_value: 
        case ColumnID.team_value_away:
            return "Team value"
        case ColumnID.idrace: 
        case ColumnID.idrace_home:
        case ColumnID.idrace_away: return "Race"
        case ColumnID.score: 
        case ColumnID.score_home: 
        case ColumnID.score_away: 
            return "Score"
        case ColumnID.started: return "Started";
        case ColumnID.finished: return "Finished";
        case ColumnID.idmatch: return "Match";
        case ColumnID.day: return "Day";
        case ColumnID.time: return "Time";
        case ColumnID.userdate: return "Scheduled date and time, shown in your local time"
      
      }
      return ColumnID[col.id]
  }

  static getColumnDisplayString(col : Column) : string{
      if(col.short){
        switch(col.id){
            case ColumnID.rerolls: return "RR"
            case ColumnID.apo: return "Apo"
            case ColumnID.cheerleaders: return "Chr"
            case ColumnID.cash_spent: return "CshSp"
            case ColumnID.cash_before_match: return "CshBf"
            case ColumnID.cash_earned: return "CshEa"
            case ColumnID.possession: return "Poss"
            case ColumnID.mvp: return "MVP"

            case ColumnID.round: return "Rnd"
            case ColumnID.active: return "Act";
            case ColumnID.collecting: return "Coll";
            case ColumnID.competition_name: return "Name";
            case ColumnID.competition_origin_id: return "OrgId";
            case ColumnID.format: return "Form";
            case ColumnID.idcompetition: return "CompId";
            case ColumnID.idleague: return "LeagueId";
            case ColumnID.idorigin: return "OrgId";
            case ColumnID.last_game: return "Last";
            case ColumnID.league_name: return "League";
            case ColumnID.league_origin_id: return "OrgId";
            case ColumnID.num_coaches: return "Coach";
            case ColumnID.num_games: return "Games";
            case ColumnID.num_teams: return "Teams";
            case ColumnID.sorting: return "Sort";
            case ColumnID.concedes: return "C+";
            case ColumnID.win_pct: return "W %";
            case ColumnID.conceded: return "C";
            case ColumnID.conceded_opp: return "C";
            case ColumnID.concedes_opp: return "C-";
            case ColumnID.wins: return "W"
            case ColumnID.draws: return "D"
            case ColumnID.losses: return "L"
            case ColumnID.gp: return "GP"
            case ColumnID.youtube: return "YT"
            case ColumnID.twitch: return "TW"
    
            case ColumnID.idcoach: 
            case ColumnID.idcoach_home:
            case ColumnID.idcoach_away:
                    return "CID"
            case ColumnID.coach_name: 
            case ColumnID.coach_name_home: 
            case ColumnID.coach_name_away: 
                    return "Coach"
            case ColumnID.idteam: 
            case ColumnID.idteam_home: 
            case ColumnID.idteam_away: 
                return "TID"
            case ColumnID.team_name: 
            case ColumnID.team_name_home: 
            case ColumnID.team_name_away: 
                return "Team"
            case ColumnID.logo: 
            case ColumnID.logo_home: 
            case ColumnID.logo_away: 
                return ""
            case ColumnID.player_name:
                return "Player"
            case ColumnID.idplayertype:
                return "Type"
            case ColumnID.idplayer:
                return "PID"
            case ColumnID.xp:
                return "XP"
            case ColumnID.xp_gain:
                return "XP+"
            case ColumnID.points: return "Pts"
            case ColumnID.ranking: return "Rank"
            case ColumnID.overtake: return "OT"
            case ColumnID.position: return "#"
            case ColumnID.td: return "TD+"
            case ColumnID.td_opp: return "TD-"
            case ColumnID.td_diff: return "TD+/-"
            case ColumnID.cas: return "Cas+"
            case ColumnID.cas_opp: return "Cas-"
            case ColumnID.cas_diff: return "Cas+/-"
            case ColumnID.cas_home: return "Cas"
            case ColumnID.cas_away: return "Cas"
    
            case ColumnID.kills: return "Kills"
            case ColumnID.team_value: 
            case ColumnID.team_value_away:
                return "Value"
            case ColumnID.idrace:
            case ColumnID.idrace_home:
            case ColumnID.idrace_away:  return "Rc"
            case ColumnID.score: 
            case ColumnID.score_home: 
            case ColumnID.score_away: 
                 return "Scr"
            case ColumnID.started: return "Start";
            case ColumnID.finished: return "Date";
            case ColumnID.idmatch: return "Vs";
            case ColumnID.number: return "#"
            case ColumnID.level: return "Lvl"
            case ColumnID.ma: return "Ma"
            case ColumnID.ag: return "Ag"
            case ColumnID.av: return "Av"
            case ColumnID.st: return "St"
            case ColumnID.skills: return "Skills"
            case ColumnID.cas_state: return "Inj"
            case ColumnID.cas_sustained: return "NewInj"
            case ColumnID.passes: return "Pss"
            case ColumnID.completions: return "Cmp"
            case ColumnID.pass_meters: return "Pass[m]"
            case ColumnID.run_meters: return "Run[m]"
            case ColumnID.occ_own: return "OccO"
            case ColumnID.occ_their: return "OccT"
            case ColumnID.duration: return "Dur"
            case ColumnID.catches: return "Ctch"
            case ColumnID.interceptions: return "Int"
            case ColumnID.pushouts: return "Psh"
            case ColumnID.expulsions: return "Exp"
            case ColumnID.turnovers: return "TO"
            case ColumnID.fouls: return "F"

            case ColumnID.dodges: return "Dg"
            case ColumnID.dodges_failed: return "Dg-"
            case ColumnID.rushes: return "GFI"
            case ColumnID.rushes_failed: return "GFI-"
            case ColumnID.killed_by: return "KB"
            case ColumnID.pickups: return "Pi"
            case ColumnID.pickups_failed: return "Pi-"
    
            case ColumnID.boneheads: return "Bo"
            case ColumnID.blocks_for: return "Bl+"
            case ColumnID.breaks_for: return "Br+"
            case ColumnID.stuns_for: return "St+"
            case ColumnID.kos_for: return "KO+"
            case ColumnID.casualties_for: return "Cas+"
            case ColumnID.kills_for: return "Kll+"
            case ColumnID.blocks_against: return "Bl-"
            case ColumnID.breaks_against: return "Br-"
            case ColumnID.stuns_against: return "St-"
            case ColumnID.kos_against: return "KO-"
            case ColumnID.casualties_against: return "Cas-"
            case ColumnID.kills_against: return "Kll-"
            case ColumnID.day: return "Day";
            case ColumnID.time: return "Time";
            case ColumnID.userdate: return "Scheduled";
    
        }
        
    } else {
        
        switch(col.id){
            case ColumnID.rerolls: return "Rerolls"
            case ColumnID.apo: return "Apothecary"
            case ColumnID.cheerleaders: return "Cheerleaders"
            case ColumnID.cash_spent: return "Cash spent"
            case ColumnID.cash_before_match: return "Cash before match"
            case ColumnID.cash_earned: return "Cash earned"
            case ColumnID.possession: return "Possession"
            case ColumnID.mvp: return "Most Valuable Player"
            case ColumnID.userdate: return "Scheduled";

            case ColumnID.round: return "Round";
            case ColumnID.active: return "Active";
            case ColumnID.collecting: return "Collecting";
            case ColumnID.competition_name: return "Competition Name";
            case ColumnID.competition_origin_id: return "Competition Origin Id";
            case ColumnID.format: return "Format";
            case ColumnID.idcompetition: return "Competition Id";
            case ColumnID.idleague: return "League Id";
            case ColumnID.idorigin: return "Origin Id";
            case ColumnID.last_game: return "Last game";
            case ColumnID.league_name: return "League";
            case ColumnID.collection_name: return "Collection";
            case ColumnID.description: return "Description";
            case ColumnID.league_origin_id: return "League Origin Id";
            case ColumnID.num_coaches: return "Num Coaches";
            case ColumnID.num_games: return "Num Games";
            case ColumnID.num_teams: return "Num Teams";
            case ColumnID.sorting: return "Sorting";
            case ColumnID.concedes: return "Concedes";
            case ColumnID.conceded: return "Conceded";
            case ColumnID.conceded: return "Conceded";
            case ColumnID.concedes_opp: return "Concedes Opp";
            case ColumnID.win_pct: return "Win %";
            case ColumnID.wins: return "Wins"
            case ColumnID.draws: return "Draws"
            case ColumnID.losses: return "Losses"
            case ColumnID.gp: return "Games"
            case ColumnID.idcoach: 
            case ColumnID.idcoach_home:
            case ColumnID.idcoach_away:
                    return "Coach Id"
            case ColumnID.youtube: return "Youtube"
            case ColumnID.twitch: return "Twitch"
            
            case ColumnID.coach_name: 
            case ColumnID.coach_name_home: 
            case ColumnID.coach_name_away: 
                    return "Coach Name"
            case ColumnID.idteam: 
            case ColumnID.idteam_home: 
            case ColumnID.idteam_away: 
                return "Team Id"
            case ColumnID.team_name: 
            case ColumnID.team_name_home: 
            case ColumnID.team_name_away: 
                return "Team Name"
            case ColumnID.logo: 
            case ColumnID.logo_home: 
            case ColumnID.logo_away: 
                return "Logo"
            case ColumnID.player_name:
                return "Player"
            case ColumnID.idplayertype:
                return "Type"
            case ColumnID.idplayer:
                return "PID"
            case ColumnID.xp:
                return "XP"
            case ColumnID.xp_gain:
                return "XP+"
            case ColumnID.points: return "Points"
            case ColumnID.ranking: return "Rank"
            case ColumnID.overtake: return "Overtake"
            case ColumnID.position: return "Position"
            case ColumnID.td: return "TD+"
            case ColumnID.td_opp: return "TD-"
            case ColumnID.td_diff: return "TD+/-"
            case ColumnID.cas: return "Cas+"
            case ColumnID.cas_opp: return "Cas-"
            case ColumnID.cas_diff: return "Cas+/-"
            case ColumnID.cas_home: return "Cas"
            case ColumnID.cas_away: return "Cas"
                
            case ColumnID.kills: return "Kills"
            case ColumnID.team_value: 
            case ColumnID.team_value_away:
                return "Value"
            case ColumnID.idrace:
            case ColumnID.idrace_home:
            case ColumnID.idrace_away:  return "Race"
            case ColumnID.score: 
            case ColumnID.score_home: 
            case ColumnID.score_away: 
                return "Score"
            case ColumnID.started: return "Started";
            case ColumnID.finished: return "Finished";
            case ColumnID.idmatch: return "Vs";
            case ColumnID.number: return "Number"
            case ColumnID.level: return "Level"
            case ColumnID.ma: return "Move"
            case ColumnID.ag: return "Agility"
            case ColumnID.av: return "Armour"
            case ColumnID.st: return "Strength"
            case ColumnID.skills: return "Skills"
            case ColumnID.cas_state: return "Injuries"
            case ColumnID.cas_sustained: return "New injuries"
            case ColumnID.passes: return "Passes"
            case ColumnID.completions: return "Completions"
            case ColumnID.pass_meters: return "Pass[m]"
            case ColumnID.run_meters: return "Run[m]"
            case ColumnID.occ_own: return "Occ.Own"
            case ColumnID.occ_their: return "Occ.Their"
            case ColumnID.duration:return "Duration"
            case ColumnID.catches: return "Catches"
            case ColumnID.interceptions: return "Interceptions"
            case ColumnID.pushouts: return "Pushouts"
            case ColumnID.expulsions: return "Expulsions"
            case ColumnID.turnovers: return "Turnovers"
            case ColumnID.fouls: return "Fouls"

            case ColumnID.dodges: return "Dodges"
            case ColumnID.dodges_failed: return "Failed dodges"
            case ColumnID.rushes: return "Rushes"
            case ColumnID.rushes_failed: return "Failed rushes"
            case ColumnID.killed_by: return "Killed by"
            case ColumnID.pickups: return "Pickups"
            case ColumnID.pickups_failed: return "Failed pickups"
    
            case ColumnID.boneheads: return "Boneheads"
            case ColumnID.blocks_for: return "Blocks for"
            case ColumnID.breaks_for: return "Armor breaks for"
            case ColumnID.stuns_for: return "Stuns for"
            case ColumnID.kos_for: return "Knockouts for"
            case ColumnID.casualties_for: return "Casualties for"
            case ColumnID.kills_for: return "Kills for"
            case ColumnID.blocks_against: return "Blocks against"
            case ColumnID.breaks_against: return "Breaks against"
            case ColumnID.stuns_against: return "Stuns against"
            case ColumnID.kos_against: return "Knockouts against"
            case ColumnID.casualties_against: return "Casualties against"
            case ColumnID.kills_against: return "Kills against"           
            case ColumnID.day : return "Day";
            case ColumnID.time: return "Time";      
                      
        }
        
    }
    return ColumnID[col.id]
  }

  

}
