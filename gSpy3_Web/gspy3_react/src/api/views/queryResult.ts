


class QueryResult{
    fileData : string=""
    cols: { [id: string] : number; } = {};
    rows: string [][] =[]
    success:boolean =false
    page : number = 1
    from : number = 1
    limit : number = 50

    static get(res: QueryResult, row : number, col : string) : string{
    try{
        let rowdata = res.rows[row];
        if(rowdata)
            return rowdata[res.cols[col]]
        return "";
    }catch(e){
        console.log(e);
        console.log(res);
        return "";
    }
    }
}



export default QueryResult