﻿using gSpy3_Common;
using gSpy3_Common.API.BB2;
using gSpy3_Common.API.LegacyGoblinSpy;
using gSpy3_Common.Database;
using gSpy3_Common.Database.AccessInterface;
using gSpy3_Common.Module;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Text;
using System.Timers;

namespace gSpy3_Maintenance
{
    class MaintenanceManager : ModuleBase
    {
        DatabaseManager dbManager;
        Timer heartbeatTimer;
        protected enum Work { CreateGroupedStats , RefetchTeamstats, LegacyExport }
        Log log = new Log();
        LegacyGoblinSpyAPI legacyAPI = new LegacyGoblinSpyAPI();
        BB2API bb2API;
        string fileBaseFolder;

        public MaintenanceManager(DatabaseManager dbm, string bb2key, string aFileFolder) : base(ModuleEnum.gSpy3_Maintenance, dbm)
        {
            fileBaseFolder = aFileFolder;
            bb2API = new BB2API(bb2key);
            LeagueManager.BB2API = bb2API;
            dbManager = dbm;
            heartbeatTimer = new Timer();
            heartbeatTimer.Interval = 1;
            heartbeatTimer.Elapsed += HeartbeatTimer_Elapsed;
            heartbeatTimer.Start();
        }

        private void HeartbeatTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            heartbeatTimer.Enabled = false;
            heartbeatTimer.Interval = 1000 * 60;
            base.Tick();
            heartbeatTimer.Enabled = true;
        }

        protected override int GetMaxFuturePlanning()
        {
            return 1;
        }

        public void Run()
        {
#if DEBUG
            // int hash = GroupedStatsManager.GetHash(""+949);
            int hash = GroupedStatsManager.GetHash("CCL_All");
#endif


            while (true)
            {
                try
                {


                    var numRemoved = base.RemoveOldWork();
                    if (numRemoved > 0)
                    {
                        log.Info("Removed " + numRemoved + " old work items");
                    }

                    // Plan new work, if we don't have enough
                    try
                    {
                        int workNeeded = base.GetNumWorkNeeded();
                        if (workNeeded > 0)
                        {
                            // Check all collections for grouped stats
                            if(workNeeded > 0)
                            {
                                var list = dbManager.StatsDB.Query("select filter,collection_name from collections");
                                foreach(var item in list.Rows)
                                {
                                    try 
                                    {
                                        FilteredQueryRequest filter = Newtonsoft.Json.JsonConvert.DeserializeObject<FilteredQueryRequest>(item[0]);
                                        filter.id = "comp";
                                        var sql = FilteredQueryManager.Get().CreateSQL(filter, null);

                                        
                                        sql = new Tuple<string, Dictionary<string, string>>(sql.Item1, sql.Item2);
                                        var qres = dbManager.StatsDB.Query(sql.Item1, sql.Item2, null, 10);
                                        int num_games = 0;
                                        List<string> compIds = new List<string>();
                                        foreach(var subComp in qres.Rows)
                                        {
                                            try
                                            {
                                                num_games += int.Parse(subComp[qres.Cols["num_games"]]); ;
                                                compIds.Add(subComp[qres.Cols["idcompetition"]]);
                                            }
                                            catch { }
                                        }
                                        if(num_games > 10000)
                                        {
                                            if (GroupedStatsManager.DoesGroupedStatsExist(item[1], 2) == false)
                                            {
                                                var qres2 = dbManager.StatsDB.Query("select * from resultsview where idcompetition in (" + string.Join(',', compIds) + ") limit 1", null, null, 20 * 60 * 60);
                                                if (qres2.Success && qres2.Rows.Count > 0)
                                                {
                                                    AddWork(Work.CreateGroupedStats.ToString(), Newtonsoft.Json.JsonConvert.SerializeObject(new MaintenanceWork()
                                                    {
                                                        idcompname = item[1]
                                                    }), 10);

                                                    workNeeded--;
                                                }
                                            } else
                                            {
                                                // Well, perhaps it has been updated since last?
                                                // When was last update of the grouped stats?
                                                var lastGroupUpdate = GroupedStatsManager.GetLastGroupUpdate(item[1]);
                                                DateTime lastGame = DateTime.MinValue;
                                                foreach(var subComp in qres.Rows)
                                                {
                                                    try
                                                    {
                                                        DateTime gameTime = DateTime.Parse(subComp[qres.Cols["last_game"]]);
                                                        if (gameTime > lastGame) { lastGame = gameTime; }
                                                    }
                                                    catch { }
                                                }
                                                if(lastGame > lastGroupUpdate && (lastGame - lastGroupUpdate) > new TimeSpan(1,0,0,0))
                                                {
                                                    AddWork(Work.CreateGroupedStats.ToString(), Newtonsoft.Json.JsonConvert.SerializeObject(new MaintenanceWork()
                                                    {
                                                        idcompname = item[1]
                                                    }), 10);

                                                    workNeeded--;
                                                }
                                                
                                            }
                                        }
                                    }
                                    catch(Exception ex)
                                    {
                                        log.Error(ex.ToString());
                                    }
                                    if (workNeeded <= 0) break;
                                }
                            }

                            // Check all leagues that have more than 10000 matches for grouped stats
                            if (workNeeded > 0)
                            {
                                var list = dbManager.StatsDB.Query("select idcompetition,num_games from competitions where num_games > 10000");
                                foreach(var item in list.Rows)
                                {
                                    if (GroupedStatsManager.DoesGroupedStatsExist(item[0], 2) == false)
                                    {
                                        var qres2 = dbManager.StatsDB.Query("select * from resultsview where idcompetition ="+item[0]+ " limit 1", null, null, 20 * 60 * 60);
                                        if (qres2.Success && qres2.Rows.Count > 0)
                                        {
                                            AddWork(Work.CreateGroupedStats.ToString(), Newtonsoft.Json.JsonConvert.SerializeObject(new MaintenanceWork()
                                            {
                                                idcompname = item[0]
                                            }), 10);
                                            workNeeded--;
                                        }
                                    }
                                    if (workNeeded <= 0) break;
                                }
                            }

                          

                        }
                    }
                    catch (Exception ex)
                    {
                        log.Info("Error in work planning", ex.ToString());
                    }

                    // Execute work
                    int idwork = 0;
                    try
                    {

                        string command, data;
                        if (base.GetNextWork(out idwork, out command, out data))
                        {
                            log.Info("Work: " + command + " : " + data);
                            string result = "";
                            switch (command)
                            {
                                case "CreateGroupedStats":
                                    result = CreateGroupedStats(Newtonsoft.Json.JsonConvert.DeserializeObject<MaintenanceWork>(data));
                                    break;
                                case "RefetchTeamstats":
                                    result = RefetchTeamstats(Newtonsoft.Json.JsonConvert.DeserializeObject<MaintenanceWork>(data));
                                    break;
                                case "LegacyExport":
                                    result = LegacyExport(Newtonsoft.Json.JsonConvert.DeserializeObject<MaintenanceWork>(data));
                                    break;
                            }
                            log.Info(result);
                            base.FinishWork(idwork, result);
                        }
                        else System.Threading.Thread.Sleep(30000);

                    }
                    catch (Exception ex)
                    {
                        log.Error("Error in work execution", ex.ToString());
                        base.FailWork(idwork, ex.ToString());
                    }




                }
                catch (Exception ex)
                {
                    log.Error("Error in main loop: " + ex.ToString());
                }
                System.Threading.Thread.Sleep(1000);
            }
        }

        string LegacyExport(MaintenanceWork data)
        {
            
            string res = "";

            try
            {
                var idcompinput = data.idcompname;
                var idcompList = LeagueManager.FindCompetition(idcompinput);
                foreach (var id in idcompList) 
                {
                    string dburl = gSpy3_Maintenance.LegacyExporter.GetFilename(fileBaseFolder, id.idcompetition);
                    // Check if already exists
                    if (System.IO.File.Exists(dburl))
                    {
                        FileInfo fi = new FileInfo(dburl);
                        if ((DateTime.Now - fi.CreationTime) < new TimeSpan(1, 0, 0, 0))
                        {
                            throw new Exception("Legacy database export already exists, and it hasn't been a day since it's creation");
                        }
                        log.Info("Removing existing export");
                        System.IO.File.Delete(dburl);

                    }


                    gSpy3_Maintenance.LegacyExporter.Export(dbManager, fileBaseFolder, id.idcompetition);
                    res = "Database exported";
                }

            }
            catch (Exception ex)
            {

                log.Error("Error exporting legacy db", ex.ToString());
                throw ex;
            }

            return res;
        }

        string RefetchTeamstats(MaintenanceWork data)
        {
            string res = "";

            try
            {
                
            }
            catch (Exception ex)
            {

                log.Error("Error refreshing teamstats match " + data.idmatch, ex.ToString());
                throw ex;
            }

            return res;
        }

            string CreateGroupedStats(MaintenanceWork data)
        {
            string res = "";

            try
            {
                log.Info("Create grouped stats for " + data.idcompname);
                int hash = GroupedStatsManager.GetHash(data.idcompname);
                {
                    log.Info("Create team stats");
                    GroupedStatsManager.CreateGroupedTeamStats(hash, data.idcompname);
                }
                {
                    log.Info("Create coach stats");
                    GroupedStatsManager.CreateGroupedCoachStats(hash, data.idcompname);
                }
                {
                    log.Info("Create player stats");
                    GroupedStatsManager.CreateGroupedPlayerStats(hash, data.idcompname);
                }
                {
                    log.Info("Create race stats");
                    GroupedStatsManager.CreateGroupedRaceStats(hash, data.idcompname);
                }
            }
            catch (Exception ex)
            {

                log.Error("Error creating grouped stats for competition " + data.idcompname, ex.ToString());
                throw ex;
            }


            return res;
        }
    }
}
