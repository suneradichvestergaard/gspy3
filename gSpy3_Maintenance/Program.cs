﻿using gSpy3_Common;
using gSpy3_Common.Database;
using System;

namespace gSpy3_Maintenance
{
    class Program
    {
        static void Main(string[] args)
        {


            Log log = new Log();
            log.Info("gSpy3_Maintenance v " + typeof(Program).Assembly.GetName().Version);
            try
            {
                var settings = PrivateSettings.Load();
                DatabaseManager dbManager = new DatabaseManager(settings);

#if DEBUG
              //  LegacyExporter.Export(dbManager, settings.BaseFileFolder,4);

#endif


                MaintenanceManager maint = new MaintenanceManager(dbManager, settings.BB2APIKey, settings.BaseFileFolder);
                maint.Run();
            }
            catch (Exception ex)
            {
                log.Error("Error", ex.ToString());
            }
            log.Info("Program exit");
        }
    }
}
