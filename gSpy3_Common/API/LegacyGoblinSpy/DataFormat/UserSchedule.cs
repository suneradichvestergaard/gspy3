﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace goblinSpy.DataFormat
{
    public class UserSchedule
    {
        public int contest_id { get; set; }
        public string date { get; set; }
        public string date_changed { get; set; }
    }
}
