﻿using gSpy3_Common.API.LegacyGoblinSpy.DataFormat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace goblinSpy.DataFormat
{
    public class LeagueWrapper
    {
        public DateTime EarliestSameDayUpdate = DateTime.Now;

        public List<StatsCompetition> Competitions = new List<StatsCompetition>();
        public string League;
        public string Platform;
        public LeagueWrapper(string name,string platform)
        { League = name;  Platform = platform; }
        public void Add(StatsCompetition comp, DateTime lastSameDayUpdate)
        {
            Competitions.Add(comp);
            if (lastSameDayUpdate < EarliestSameDayUpdate)
                EarliestSameDayUpdate = lastSameDayUpdate;
        }
    }
}
