﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace goblinSpy.DataFormat
{
    public class CoachStats
    {
        public string idcoachstats { get; set; }
        public int idcoach { get; set;}
        public string coachname { get; set; }
        public int coachcyanearned { get; set;}
        public int coachxpearned { get; set; }
        public bool active { get; set; }

        public CoachStats()
        {
            active = true;
        }
    }
}
