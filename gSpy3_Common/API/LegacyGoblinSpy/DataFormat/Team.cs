﻿using goblinSpy.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace goblinSpy.DataFormat
{
    public class Team
    {
        public int id { get; set; }
        public string team { get; set; }
        public string logo { get; set; }
        public string value { get; set; }
        public string motto { get; set; }
        public string race { get; set; }
        public string coach { get; set; }

        public int active { get; set; }

        public string platform { get; set; }
        public string competition { get; set; }
        public string league { get; set; }

      /*
        public void Import(Database.DatabaseManager db)
        {
            try
            {
                active = 1;
                db.WriteClass(this);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }*/
        /*
        public GlobalTeam ToGlobal(List<Coach> coaches, double aRank, int aPoints, string aIdCompetition)
        {
            var t = this;
            if (t.platform == "")
                t.platform = "pc";
            var gt = new GlobalTeam()
            {
                idcompetition = t.platform + aIdCompetition,
                idteam = t.platform + t.id,
             
                team = t.team,
                logo = t.logo,
                rank = aRank,
                points = aPoints,
            };
            foreach (var c in coaches)
            {
                if (c.name == t.coach)
                {
                    gt.idcoach = c.platform + c.id;
                    break;
                }
            }
            try
            {
                gt.idrace = (int)Enum.Parse(typeof(RaceEnum), t.race);
            }
            catch (Exception)
            {
                Console.Write("UNKNOWN RACE " + t.race);
            }
            return gt;
        }*/
    }


    public class Teams
    {
        public List<Team> teams;
        /*
        public static int Import(Database.DatabaseManager db, string storageFolder, string specificFile, string platform, string league, string competition)
        {
            int count = 0;
            foreach (string file in Directory.GetFiles(storageFolder, "teams.*.zip"))
            {
                if (specificFile != null && Path.GetFileName(specificFile) != Path.GetFileName(file))
                    continue;

                if (File.Exists(file))
                {

                    // Remove old coaches, since coaches is always the latest
                    Dictionary<string, object> parameters = new Dictionary<string, object>();
                    parameters["p1"] = platform;
                    parameters["c1"] = competition;
                    parameters["l1"] = league;

                    var teamval = "";
                    try
                    {
                        // Reset to inactive and update from the data given
                        db.Query("update teams set active = 0 where platform=@p1 and competition=@c1 and league=@l1", parameters);
                       
                    }
                    catch (Exception e3)
                    {
                        Console.WriteLine(e3.ToString() + " when deleting old teams");
                    }
                //    db.StartTransaction();
                    try
                    {
                        string[] rawFiles = Zipper.Unzip(file);
                        if (rawFiles != null)
                        {
                            foreach (string f in rawFiles)
                            {
                                using (var sr = File.OpenText(f))
                                {
                                    string datastring = sr.ReadToEnd();
                                    if (datastring != "false")
                                    {
                                        Teams ms = SimpleJson.SimpleJson.DeserializeObject<Teams>(datastring);

                                        if (ms.teams != null)
                                        {

                                            foreach (Team m in ms.teams)
                                            {


                                                count++;
                                                try
                                                {
                                                    teamval = "";
                                                    parameters["cid"] = m.id;
                                                    try
                                                    {
                                                       var vres= db.Query("select value from teams where id=" + m.id, null);
                                                        teamval = vres.rows[0][0];
                                                            }
                                                    catch { }

                                                    m.platform = platform;
                                                    m.competition = competition;
                                                    m.league = league;
                                                    m.value = teamval;
                                                    //m.Import(db);
                                                }
                                                catch (Exception e4)
                                                {
                                                    Console.WriteLine("Exception while importing teams: " + e4.ToString());
                                                }
                                            }
                                        }
                                    }
                                }
                                File.Delete(f);
                            }
                            if (rawFiles.Length > 0)
                                Directory.Delete(Path.GetDirectoryName(rawFiles[0]), true);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                  //  db.StopTransaction();
                    try
                    {
                        db.Query("update teamstats set active = 0 where exists (select 1 FROM teams as c WHERE c.id = idteamlisting and c.active=0 and c.platform=@p1 and c.competition=@c1 and c.league=@l1)", parameters);
                        db.Query("update teamstats set active = 1 where exists (select 1 FROM teams as c WHERE c.id = idteamlisting and c.active=1 and c.platform=@p1 and c.competition=@c1 and c.league=@l1)", parameters);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Couldn't update teamstats, probably because no teams existed");
                    }
                }
                else Console.WriteLine("Unable to find the stored file " + file);
            }
            return count;
        }
        */
    }
}
