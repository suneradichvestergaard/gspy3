﻿using goblinSpy.Database;
using goblinSpy.Helpers;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace goblinSpy.DataFormat
{
    public class Matches
    {
        public List<Match> matches { get; set; }

        /*
        public static Matches Read(Database.DatabaseManager db, string platform, string league, string competition)
        {
            Matches m = new Matches();
            Dictionary<String, object> args = new Dictionary<string, object>();
            args["p0"] = platform;
            args["p1"] = league;
            args["p2"] = competition;
            m.matches = db.ReadClass<Match>("select * from matches where platform=@p0 and league=@p1 and competition=@p2 order by finished desc limit 100", args);
            return m;
           
        }
        public static void Import(Database.DatabaseManager db, MatchResult mresult)
        {
            //  db.StartTransaction();
            DateTime preCoaches = DateTime.Now;
            foreach (var cs in mresult.match.coaches)
            {
                cs.idcoachstats = mresult.uuid + "-" + cs.idcoach;
                db.WriteClass(cs);
            }
            DateTime postCoaches = DateTime.Now;
            int teamindex = -1;
            DateTime preTeams = DateTime.Now;
            foreach (var t in mresult.match.teams)
            {
                DateTime preTeamInside = DateTime.Now;
                teamindex++;
                t.current = mresult.teams[teamindex];
                t.idteamstats = mresult.uuid + "-" + t.idteamlisting;
                t.InitializeForWriting();
                if (t.teamlogo == "")
                {
                    try
                    {
                        t.teamlogo = "AI";

                        // See if it's amongst the teams
                        var tres = db.Query("select logo from teams where id = " + t.idteamlisting,null);
                        if (tres.rows != null && tres.rows.Count > 0)
                            t.teamlogo = tres.rows[0][0];
                    }
                    catch
                    {
                    }
                }
                db.WriteClass(t as TeamStats);
                db.Query("update teams set value="+t.value+" where id="+t.idteamlisting,null);
                DateTime postTeamInside = DateTime.Now;
#if DEBUG
                Console.WriteLine("team write " + (postTeamInside - preTeamInside).TotalMilliseconds + " ms");
#endif

                DateTime preDelete = DateTime.Now;
                db.Query("delete from currentplayers where idteam=" + t.idteamlisting, null);
                DateTime postDelete = DateTime.Now;
#if DEBUG
                Console.WriteLine("cur delete " + (postDelete - preDelete).TotalMilliseconds + " ms");
#endif

                DateTime preRoster = DateTime.Now;
                var trans = db.StartTransaction();
                foreach (var p in t.roster)
                {

                    (p as MatchResultPlayer).Initialize(mresult.uuid, t.idteamlisting, t.idteamstats);
                    db.WriteClass<MatchResultPlayer>(p, trans);
                    {
                        CurrentPlayer tp = p as CurrentPlayer;
                        db.WriteClass<CurrentPlayer>(tp, trans);
                    }
                }

                db.StopTransaction(trans);






                DateTime postRoster = DateTime.Now;
#if DEBUG
                Console.WriteLine("roster " + (postRoster - preRoster).TotalMilliseconds + " ms");
#endif
            }
            DateTime postTeams = DateTime.Now;
            DateTime preMatchImport = DateTime.Now;
            Match m = new Match()
            {
                checkedplayerstats = 0,
                competitionname = mresult.match.competitionname,
                externalstats = null,
                coaches = mresult.match.coaches,
                finished = mresult.match.finished,
                idcompetition = mresult.match.idcompetition == null ? 0 : (mresult.match.idcompetition),
                idcoachstatsaway = mresult.match.coaches[1].idcoachstats,
                idcoachstatshome = mresult.match.coaches[0].idcoachstats,
                idteamstatsaway = mresult.match.teams[1].idteamstats,
                idteamstatshome = mresult.match.teams[0].idteamstats,
                leaguename = mresult.match.leaguename,
                levelstadium = mresult.match.levelstadium,
                teams = new List<TeamStats>() { mresult.match.teams[0] as TeamStats, mresult.match.teams[1] as TeamStats },
                platform = mresult.match.platform,
                stadium = mresult.match.stadium,
                started = mresult.match.started,
                structstadium = mresult.match.structstadium,
                uuid = mresult.uuid


            };
           
            m.Import(db);
            DateTime postMatchImpor = DateTime.Now;


            //db.Query("update matches set idcompetition=" + m.idcompetition + " where idcompetition=0 or idcompetition is null", new Dictionary<string, object>());


            // db.StopTransaction();
#if DEBUG
           Console.WriteLine((postCoaches - preCoaches).TotalMilliseconds + " ms coaches, " + (postTeams - preTeams).TotalMilliseconds + " ms teams, " + (postMatchImpor - preMatchImport).TotalMilliseconds + " ms match");
#endif
    }*//*
        public static int Import(Database.DatabaseManager db,string storageFolder, string specificFile = null, bool deletePlayerStatsForMatches=false)
        {
            Console.Write(".");
            int count = 0;
            foreach (string file in Directory.GetFiles(storageFolder, "matches.*.zip"))
            {
                Console.Write(".");
                if (specificFile != null && Path.GetFileName(specificFile) != Path.GetFileName(file))
                    continue;

                Console.Write("!");
                if (File.Exists(file))
                {
                    Console.Write(":");
                    //   var trans=  db.StartTransaction();
                    SqliteTransaction trans = null;
                    try
                    {
                        string[] rawFiles = Zipper.Unzip(file);
                        if (rawFiles != null)
                        {
                            foreach (string f in rawFiles)
                            {
                                Console.Write(":");
                                using (var sr = File.OpenText(f))
                                {
                                    Matches ms = SimpleJson.SimpleJson.DeserializeObject<Matches>(sr.ReadToEnd());
                                    if (ms.matches != null)
                                    {
                                        foreach (Match m in ms.matches)
                                        {
                                            if (file.Contains("." + Match.Platform_PS4 + "."))
                                                m.platform = Match.Platform_PS4;
                                            else if (file.Contains("." + Match.Platform_XBOX1 + "."))
                                                m.platform = Match.Platform_XBOX1;
                                            else
                                                m.platform = Match.Platform_PC;
                                            m.competitionname = m.competitionname.Trim();
                                            count++;
                                            Console.Write(".");
                                            m.Import(db, trans);

                                            try
                                            {
                                                if (deletePlayerStatsForMatches)
                                                {
                                                    Console.Write("D");
                                                    Dictionary<string, object> plist = new Dictionary<string, object>();
                                                    plist["p0"] = m.uuid;
                                                    var res = db.Query("delete from playerstats where uuid=@p0", plist);
                                                }

                                            }
                                            catch (Exception e)
                                            {
                                                Console.WriteLine(e);
                                            }
                                        }
                                    }
                                }
                                File.Delete(f);
                            }
                            if (rawFiles.Length > 0)
                                Directory.Delete(Path.GetDirectoryName(rawFiles[0]), true);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                //    db.StopTransaction(trans);


                }
                else Console.WriteLine("Unable to find the stored file " + file);
            }
            return count;
        }*/
    }
}
