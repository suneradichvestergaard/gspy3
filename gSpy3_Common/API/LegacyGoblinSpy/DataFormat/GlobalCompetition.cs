﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace goblinSpy.DataFormat
{
    public class GlobalCompetition
    {
        public const int DBVERSION = 1;

        public string idcompetition { get; set; }
        public string competition { get; set; }
        public string league { get; set; }
        public string platform { get; set; }
        public string updated { get; set; }
        public int dbver { get; set; } 
        public string lastgame { get; set; }

        public GlobalCompetition()
        {
            dbver = DBVERSION;
        }

   
    }
}
