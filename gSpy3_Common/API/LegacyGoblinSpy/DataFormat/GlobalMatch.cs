﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace goblinSpy.DataFormat
{
    public class GlobalMatch
    {
        public string id { get; set; }
        public string uuid { get; set; }
        public int ishome { get; set; }
        public string idcompetition { get; set; }
        public string idteam1 { get; set;}
        public string idteam2 { get; set;}
        public int idrace1 { get; set; }
        public int idrace2 { get; set; }
        public string idteamstats1 { get; set; }
        public string idteamstats2 { get; set; }
        public int gp { get; set; }
        public int win { get; set; }
        public int draw { get; set; }
        public int loss { get; set; }
        public int tvdiff { get; set; }
        public int value { get; set; }
        public int gametvdiff { get; set; }
        public int gamevalue { get; set; }
        public string structstadium { get; set; }
        public string started { get; set; }
        public string finished { get; set; }
        public int duration { get; set; }

        public int score1 { get; set; }
        public int score2 { get; set; }
        public int cas1 { get; set; }
        public int cas2 { get; set; }
        public int concede1 { get; set; }
        public int concede2 { get; set; }




        public GlobalMatch()
        {

        }
       
    }
}
