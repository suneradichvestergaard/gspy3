﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace goblinSpy.DataFormat
{
    public class GlobalTeam
    {
  
        public string idcompetition { get; set; }
        public string idteam { get; set; }
        public string idcoach { get; set; }
        public int idrace { get; set; }
        public string team { get; set; }
        public string logo { get; set; }
        public double rank { get; set; }
        public int points { get; set; }

        public GlobalTeam() { }
      
    }
}
