﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace goblinSpy.DataFormat
{
    public class TeamStats
    {
        public string idmatch { get; set; }
        public string idteamstats { get; set; }
        public int idteamlisting { get; set; }
        public int idraces { get; set; }
        public string teamname { get; set; }
        public string teamlogo { get; set; }
        public int value { get; set; }
        public int score { get; set; }
        public int cashbeforematch { get; set; }
        public int popularitybeforematch { get; set; }
        public int popularitygain { get; set; }
        public int cashspentinducements { get; set; }
        public int cashearned { get; set; }
        public int cashearnedbeforeconcession { get; set; }
        public int winningsdice { get; set; }
        public int spirallingexpenses { get; set; }
        public int nbsupporters { get; set; }
        public int possessionball { get; set; }
        public int occupationown { get; set; }
        public int occupationtheir { get; set; }
        public int mvp { get; set; }
        public int inflictedpasses { get; set; }
        public int inflictedcatches { get; set; }
        public int inflictedinterceptions { get; set; }
        public int inflictedtouchdowns { get; set; }
        public int inflictedcasualties { get; set; }
        public int inflictedtackles { get; set; }
        public int inflictedko { get; set; }
        public int inflictedinjuries { get; set; }
        public int inflicteddead { get; set; }
        public int inflictedmetersrunning { get; set; }
        public int inflictedmeterspassing { get; set; }
        public int inflictedpushouts { get; set; }
        public int sustainedexpulsions { get; set; }
        public int sustainedcasualties { get; set; }
        public int sustainedko { get; set; }
        public int sustainedinjuries { get; set; }
        public int sustaineddead { get; set; }

        public bool active { get; set; }

      //  [Database.DatabaseManager.DBIgnore]
        public TeamStats_Current current { get; set; }

        public string cheerleaders { get; set; }
        public string apothecary { get; set; }
        public string rerolls { get; set; }
        public string assistantcoaches { get; set; }

        public TeamStats()
        {
            active = true;
        }

        public void InitializeForWriting()
        {
            if (current != null)
            {
                cheerleaders = current.cheerleaders == null ? null : "" + current.cheerleaders;
                apothecary = current.apothecary == null ? null : "" + current.apothecary;
                rerolls = current.rerolls == null ? null : "" + current.rerolls;
                assistantcoaches = current.assistantcoaches == null ? null : "" + current.assistantcoaches;
            }
        }

        public GlobalTeamStats ToGlobal(List<MatchResultPlayer> players, List<GlobalRace> races)
        {
            var ts = this;
            var gts = new GlobalTeamStats()
            {


                idteamstats = ts.idteamstats,
                score = ts.score,
                cashbeforematch = ts.cashbeforematch,
                popularitybeforematch = ts.popularitybeforematch,
                popularitygain = ts.popularitygain,
                cashspentinducements = ts.cashspentinducements,
                cashearned = ts.cashearned,
                cashearnedbeforeconcession = ts.cashearnedbeforeconcession,
                winningsdice = ts.winningsdice,
                spirallingexpenses = ts.spirallingexpenses,
                nbsupporters = ts.nbsupporters,
                possessionball = ts.possessionball,
                occupationown = ts.occupationown,
                occupationtheir = ts.occupationtheir,
                mvp = ts.mvp,
                inflictedpasses = ts.inflictedpasses,
                inflictedcatches = ts.inflictedcatches,
                inflictedinterceptions = ts.inflictedinterceptions,
                inflictedtouchdowns = ts.inflictedtouchdowns,
                inflictedcasualties = ts.inflictedcasualties,
                inflictedtackles = ts.inflictedtackles,
                inflictedko = ts.inflictedko,
                inflictedinjuries = ts.inflictedinjuries,
                inflicteddead = ts.inflicteddead,
                inflictedmeterspassing = ts.inflictedmeterspassing,
                inflictedmetersrunning = ts.inflictedmetersrunning,
                inflictedpushouts = ts.inflictedpushouts,
                sustainedexpulsions = ts.sustainedexpulsions,
                sustainedko = ts.sustainedko,
                sustainedinjuries = ts.sustainedinjuries,
                sustaineddead = ts.sustaineddead,
                sustainedcasualties = ts.sustainedcasualties,
                cheerleaders = ts.cheerleaders,
                apothecary = ts.apothecary,
                rerolls = ts.rerolls,
                assistantcoaches = ts.assistantcoaches
            };

            int pCount = 0;
            int lonerCount = 0;
            foreach (var p in players)
            {

                bool isLoner =
                    p.type.Contains("StarPlayer") == false &&
                    p.skills_string.Contains("Loner") &&
                    p.st <= 3;
                if (isLoner) lonerCount++;
                else pCount++;


            }
            if (lonerCount > 0 && (lonerCount + pCount) > 11)
                lonerCount = 11 - pCount;

            gts.numplayers = pCount;
            gts.journeymen = lonerCount;
            int val = value * 1000;

            if (this.cashbeforematch > 150000)
                val += 150000 - this.cashbeforematch;

            foreach (var r in races)
            {
                if (r.idrace == this.idraces)
                {
                    val += gts.journeymen * r.journeymencost;
                }
            }
            gts.gamevalue = val/ 1000;

            return gts;

        }
    }

    public class TeamStats_Current
    {
        public int? cheerleaders { get; set; }
        public int? apothecary { get; set; }
        public int? rerolls { get; set; }
        public int? assistantcoaches { get; set; }
    }
}
