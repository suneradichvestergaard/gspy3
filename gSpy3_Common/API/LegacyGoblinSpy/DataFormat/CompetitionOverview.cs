﻿using goblinSpy.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace goblinSpy.DataFormat
{
    public class CompetitionOverview
    {
        protected CompetitionOverview()
        {

        }
        /*
        protected CompetitionOverview(DatabaseManager db, string aPlatform, string aLeague, string aCompetition)
        {

        }*/
        /*
        public string GetRSS(string name)
        {
            Dictionary<string, string> map = new Dictionary<string, string>();
            map["league"] = "";
            map["competition"] = "";
            for (int i=0; i < Info.cols.Count;i++)
            {
                var c = Info.cols[i];
                foreach(var kvp in map)
                {
                    if (c == kvp.Key) { map[c] = Info.rows[0][i]; break; }
                }
            }


            string str =
                "<rss version=\"2.0\" xmlns:atom=\"http://www.w3.org/2005/Atom\" >" + Environment.NewLine ;
            str += "<atom:link href=\""+ "http://www.mordrek.com/goblinSpy/Overview/" +name+ "\" rel=\"self\" type=\"application/rss+xml\" />";
            str += "<channel>" + Environment.NewLine;
            str += "<title>" + XmlEncode(map["league"] + ", " + map["competition"] + ", Games played")+"</title>" + Environment.NewLine;
            str += "<link>" +
               XmlEncode("http://www.mordrek.com/goblinSpy/web/goblinSpy.html?platform=pc&league=" + UrlEncode(map["league"])+"&competition="+ UrlEncode(map["competition"])+"&q=*front")+
               "</link>" + Environment.NewLine;
            str += "<description>Games played</description>" + Environment.NewLine;
            str += "<language>en-us</language>" + Environment.NewLine;
            str += "<lastBuildDate>"+DateTime.Now.ToString("ddd, dd MMM yyyy HH:mm:ss K") +"</lastBuildDate>" + Environment.NewLine;
            str += "<ttl>10</ttl>" + Environment.NewLine;


            Dictionary<string, int> colmap = new Dictionary<string, int>();
            colmap["finished"] = 0;
            colmap["teamhome"] = 0;
            colmap["teamaway"] = 0;
            colmap["scorehome"] = 0;
            colmap["scoreaway"] = 0;
            colmap["vs"] = 0;
            for (int i = 0; i < Matches.cols.Count; i++)
            {
                var c = Matches.cols[i];
                foreach (var kvp in colmap)
                {
                    if (c == kvp.Key) { colmap[c] = i; break; }
                }
            }
            for (int i=0; i < Matches.rows.Count;i++)
            {
                var row = Matches.rows[i];
                string url = "http://www.mordrek.com/goblinSpy/web/goblinSpy.html?platform=pc&league=" + UrlEncode(map["league"]) + "&competition=" + UrlEncode(map["competition"]) + "&q=*vs-overview&t1="+row[colmap["vs"]];
                str += "<item>" + Environment.NewLine;
                str += "<title>"+ XmlEncode(row[colmap["teamhome"]]) +" vs "+ UrlEncode(row[colmap["teamaway"]]) + "</title>" + Environment.NewLine;
                str += "<description>"+
                        XmlEncode("<a href='"+url +
                        "'>" + XmlEncode(row[colmap["teamhome"]])+" "+ row[colmap["scorehome"]]+" - "+ row[colmap["scoreaway"]]+" "+UrlEncode(row[colmap["teamaway"]]) + "</a>")+"</description>" + Environment.NewLine;
                str += "<pubDate>" + XmlEncode(DateTime.Parse(row[colmap["finished"]]).ToString("ddd, dd MMM yyyy HH:mm:ss K")) + "</pubDate>" + Environment.NewLine;
                str += "<guid>" + XmlEncode(url) + "</guid>" + Environment.NewLine;
                str += "</item>" + Environment.NewLine;

            }
            str += "</channel>" + Environment.NewLine;
            str += "</rss>" + Environment.NewLine;
            return str;
        }*/
        protected string UrlEncode(string txt)
        {
            return HttpUtility.UrlDecode(txt).Replace("+", "%20");
        }
        protected string XmlEncode(string txt)
        {
           
            return txt.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;");
        }
        /*
        public static CompetitionOverview Create(DatabaseManager db, DatabaseManager dbhub, string platform, string league, string competition, string sorting)
        {
            CompetitionOverview cmp = new CompetitionOverview(db, league, competition, platform);
            try
            {
                db.Query("create index if not exists matchplayers_puuid on matchplayers (puuid)", new Dictionary<string, object>());
            }
            catch { }
            try
            {
                db.Query("create index if not exists teammatches_idteamlisting on teammatches (idteamlisting)", new Dictionary<string, object>());
            }
            catch { }
            try
            {
                db.Query("create index if not exists teammatches_idcoach on teammatches (idcoach)", new Dictionary<string, object>());
            }
            catch { }
            Dictionary<String, object> args = new Dictionary<string, object>();
            args["p0"] = platform;
            args["p1"] = league;
            args["p2"] = competition;


            cmp.Sorting = sorting;
            string cmpId = "";
            string cmpDateWhere = "";
            try
            {
                cmp.Info = dbhub.Query("select platform, league, competition, lastupdate, collect,earliestgame from competitions where platform=@p0 and league=@p1 and competition=@p2", args);
            }
            catch { }
            try
            {
                cmp.Matches = db.Query("select idcompetition, platform, leaguename,competitionname,started, finished, date,vs,  duration, conceded, coachhome, idraceimghome, idracehome, logohome, teamhome, scorehome, scoreaway, teamaway, logoaway, idraceaway, idraceimgaway, coachaway, concededaway, idteamlistingaway, idteamlistinghome, idcompetition from leaguematches where platform=@p0 and leaguename=@p1 and competitionname=@p2 order by finished desc limit 100", args);
                int colIindex = 0;
                foreach(var c in cmp.Matches.cols)
                {
                    if (c == "idcompetition")
                        break;
                    colIindex++;
                }

                



                    if (cmp.Matches.rows.Count >1 && 
                    colIindex < cmp.Matches.cols.Count &&
                    cmp.Matches.rows[0][colIindex] != "" && cmp.Matches.rows[0][colIindex] != "0" &&
                    cmp.Matches.rows[cmp.Matches.rows.Count - 1][colIindex] != "" && cmp.Matches.rows[cmp.Matches.rows.Count - 1][colIindex] != "0" &&
                    cmp.Matches.rows[0][colIindex] != cmp.Matches.rows[cmp.Matches.rows.Count - 1][colIindex])
                {
                    cmpId = cmp.Matches.rows[0][colIindex];

                }
                if(cmpId!="")
                {
                    var res = db.Query("select finished from leaguematches where idcompetition==" + cmpId + " order by finished asc limit 1", new Dictionary<string, object>());
                    if (res.num > 0)
                        cmpDateWhere = " and finished >= '"+res.rows[0]+"' ";

                   
                }

            } catch(Exception e) {
                string err = e.ToString();
            }

            string cmpWhere = cmpId != "" ? " and idcompetition==" + cmpId + " " : "";
            try
            {
                if (cmpId != "")
                {
                    // Hackish to update active team status etc....
                    db.Query("update leaguestandings set activeteam=0 where idcompetition<>" + cmpId + "", null);
                }
            }
            catch (Exception e)
            {
                string err = e.ToString();
            }
            try
            {
                cmp.NumMatches = db.Query("select count(*) from leaguematches where platform=@p0 and leaguename=@p1 and competitionname=@p2 "+ cmpDateWhere, args);
            }
            catch { }
            try
            {
                cmp.Schedule = db.Query("select s.*,m.scorehome as score1,m.scoreaway as score2 from scheduled as s join leaguematches as m on m.uuid=match_uuid where s.platform=@p0 and league=@p1 and competition=@p2 union all select s.*,null as score1, null as score2 from scheduled as s where s.platform=@p0 and league=@p1 and competition=@p2 and match_uuid='' " + cmpDateWhere + " order by competition_round asc limit 1000", args);
                try
                {
                    var usersched = db.ReadClass<UserSchedule>("select * from userschedule");
                    cmp.Schedule.cols.Add("userschedule");

                    int contestIdCol = 0;
                    foreach (var col in cmp.Schedule.cols)
                    {
                        if (col == "contest_id") { break; }
                        contestIdCol++;
                    }

                    foreach (var row in cmp.Schedule.rows)
                    {
                        bool found = false;
                        try
                        {
                            string contestId = row[contestIdCol];
                            foreach (var sched in usersched)
                            {
                                if (sched.contest_id.ToString() == contestId)
                                {
                                    found = true;
                                    row.Add(sched.date);
                                    break;
                                }
                            }
                        }
                        catch { }
                        if (!found)
                        {
                            row.Add(null);
                        }
                    }
                }catch(Exception e)
                {
                    Console.WriteLine("E " + e.ToString());
                }

            }
           
            catch { }
            try
            {

                cmp.NumCoaches = db.Query("select count(distinct coachname) from leaguestandings where platform=@p0 and leaguename=@p1 and competitionname=@p2 " + cmpWhere, args);
            }
            catch { }
            try
            {
                cmp.NumTeams = db.Query("select count(*) from leaguestandings where platform=@p0 and leaguename=@p1 and competitionname=@p2  " + cmpWhere, args);
            }
            catch { }
            try
            {
                cmp.LeagueStandings = db.Query("select * from leaguestandings where platform=@p0 and leaguename=@p1 and competitionname=@p2 and activeteam=1 "+cmpWhere+" order by sort desc, tddiff desc, td desc limit 100", args);
            }
            catch { }

            int topNum = 10;
            try
            {
                cmp.TopTeams = new List<TopListContent>();
                cmp.TopTeams.Add(new TopListContent("Most violent", "Cas", getTeamMatchStatQuery(db, "inflictedcasualties", topNum)));
                cmp.TopTeams.Add(new TopListContent("Passers", "Passes", getTeamMatchStatQuery(db, "inflictedpasses", topNum)));
                cmp.TopTeams.Add(new TopListContent("Scorers", "TD", getTeamMatchStatQuery(db, "inflictedtouchdowns", topNum)));
                cmp.TopTeams.Add(new TopListContent("Blockers", "Blocks", getTeamMatchStatQuery(db, "inflictedtackles", topNum)));
                cmp.TopTeams.Add(new TopListContent("Killers", "Deaths", getTeamMatchStatQuery(db, "inflicteddead", topNum)));
                cmp.TopTeams.Add(new TopListContent("Foulers", "Expul.", getTeamMatchStatQuery(db, "sustainedexpulsions", topNum)));
                cmp.TopTeams.Add(new TopListContent("Worst beaten", "Cas-", getTeamMatchStatQuery(db, "sustainedcasualties", topNum)));
                cmp.TopTeams.Add(new TopListContent("Slowest", "Minutes", getTeamMatchStatQuery(db, "duration", topNum)));
                cmp.TopTeams.Add(new TopListContent("Fastest", "Minutes", getTeamMatchStatQuery(db, "duration", topNum, true)));
                cmp.TopTeams.Add(new TopListContent("Earners", "Gold", getTeamMatchStatQuery(db, "cashearned", topNum)));

                cmp.TopGames = new List<TopListContent>();
                cmp.TopGames.Add(new TopListContent("Most violent", "Cas", getMatchStatQuery(db, "inflictedcasualties", "sustainedcasualties", topNum)));
                cmp.TopGames.Add(new TopListContent("Scores", "TD", getMatchStatQuery(db, "inflictedtouchdowns", "sustainedtouchdowns", topNum)));
                cmp.TopGames.Add(new TopListContent("Blocks", "Blocks", getMatchStatQuery(db, "inflictedtackles", "sustainedtackles", topNum)));
                cmp.TopGames.Add(new TopListContent("Kills", "Deaths", getMatchStatQuery(db, "inflicteddead", "sustaineddead", topNum)));
                cmp.TopGames.Add(new TopListContent("Rank", "Rank", getMatchStatQuery(db, "rank", "rankopp", topNum)));

                cmp.TopCoaches = new List<TopListContent>();
                cmp.TopCoaches.Add(new TopListContent("Winner", "%", getCoachMatchStatQuery(db, "(100*win+50*draw)/(win+draw+loss)", topNum, false, 10)));
                cmp.TopCoaches.Add(new TopListContent("Opposition", "Rank", getCoachMatchStatQuery(db, "rankopp", topNum, false, 5)));
                cmp.TopCoaches.Add(new TopListContent("Most violent", "Cas", getCoachMatchStatQuery(db, "inflictedcasualties", topNum)));
                cmp.TopCoaches.Add(new TopListContent("Passes", "Passes", getCoachMatchStatQuery(db, "inflictedpasses", topNum)));
                cmp.TopCoaches.Add(new TopListContent("Scores", "TD", getCoachMatchStatQuery(db, "inflictedtouchdowns", topNum)));
                cmp.TopCoaches.Add(new TopListContent("Blocks", "Blocks", getCoachMatchStatQuery(db, "inflictedtackles", topNum)));
                cmp.TopCoaches.Add(new TopListContent("Kills", "Deaths", getCoachMatchStatQuery(db, "inflicteddead", topNum)));
                cmp.TopCoaches.Add(new TopListContent("Fouls", "Expul.", getCoachMatchStatQuery(db, "sustainedexpulsions", topNum)));

                cmp.TopPlayers = new List<TopListContent>();
                cmp.TopPlayers.Add(new TopListContent("Most violent", "Cas", getPlayerMatchStatQuery(db, "inflictedcasualties", topNum)));
                cmp.TopPlayers.Add(new TopListContent("Passer", "Passes", getPlayerMatchStatQuery(db, "inflictedpasses", topNum)));
                cmp.TopPlayers.Add(new TopListContent("Catcher", "Catches", getPlayerMatchStatQuery(db, "inflictedcatches", topNum)));
                cmp.TopPlayers.Add(new TopListContent("Scorer", "TD", getPlayerMatchStatQuery(db, "inflictedtouchdowns", topNum)));
                cmp.TopPlayers.Add(new TopListContent("Blocker", "Blocks", getPlayerMatchStatQuery(db, "inflictedtackles", topNum)));
                cmp.TopPlayers.Add(new TopListContent("Killer", "Deaths", getPlayerMatchStatQuery(db, "inflicteddead", topNum)));
                // Don't exist on player            cmp.TopPlayers.Add(new TopListContent("Cheater", "Expul.", getPlayerMatchStatQuery(db, "sustainedexpulsions", topNum)));
                cmp.TopPlayers.Add(new TopListContent("Worst beaten", "Cas-", getPlayerMatchStatQuery(db, "sustainedcasualties", topNum)));
                cmp.TopPlayers.Add(new TopListContent("Interceptors", "Ints.", getPlayerMatchStatQuery(db, "inflictedinterceptions", topNum)));
                cmp.TopPlayers.Add(new TopListContent("Runner", "meters", getPlayerMatchStatQuery(db, "inflictedmetersrunning", topNum)));
                cmp.TopPlayers.Add(new TopListContent("Experienced", "SPP", getPlayerMatchStatQuery(db, "xp_gain", topNum)));

                cmp.TopRaces = new List<TopListContent>();
                var res = db.Query("select idraces from leaguestandings group by idraces order by rank", null);
                foreach(var row in res.rows)
                {
                    var idrace = row[0];
                    TopListContent content = new TopListContent(((BB2StatsLib.RaceEnum)int.Parse(idrace)).ToString(), "", 
                    new QueryResult[] { 
                        db.Query("select idracesimg, idraces, teamname, idteamlisting, rank, wins, draws, losses from leaguestandings where idraces=@race and activeteam=1 order by rank desc limit @topnum", new Dictionary<string, object>() { {"race",idrace }, { "topnum", topNum } })
                    });
                    cmp.TopRaces.Add(content);
                }
            }
            catch { }
            return cmp;
        }
        */
        /*
        static QueryResult[] getCoachMatchStatQuery(DatabaseManager db, string stat, int num, bool invertOrder = false, int minGP = 3)
        {
            string q = "select avg(" + stat + ") as value,count(*) as gp,coach, idteamlisting from teammatches group by idcoach  having gp >= " + minGP + " order by avg(" + stat + ") " + (invertOrder ? "asc" : "desc") + " limit " + num;
            try
            {
                return new QueryResult[1] {
                db.Query(q, new Dictionary<string, object>())
            };
            }catch(Exception ex)
            {
                Console.WriteLine("q:" + q);
                return new QueryResult[1] { new QueryResult() };
            }
        }
        static QueryResult[] getMatchStatQuery(DatabaseManager db, string stat1, string stat2, int num, bool invertOrder = false)
        {
            var res = db.Query("select (" + stat1 + "+" + stat2 + ") as value, vs,teamname, idteamlisting, teamnameopp, idteamlistingopp from teammatches  order by (" + stat1 + "+" + stat2 + ") " + (invertOrder ? "asc" : "desc") + " limit " + num * 2, new Dictionary<string, object>());
            List<List<string>> newRows = new List<List<string>>();
            // Cut away half due to how they are presented
            int j = -1;
            string lastT1 = "";
            string lastT2 = "";
            try
            {
                for (int i = 0; i < num;)
                {
                    j++;
                    if (j < res.rows.Count)
                    {
                        if (lastT1 == res.rows[j][4] && lastT2 == res.rows[j][2])
                            continue;

                        newRows.Add(res.rows[j]);
                        lastT1 = res.rows[j][2];
                        lastT2 = res.rows[j][4];
                        i++;
                    }
                    else break;
                }
            }catch(Exception e)
            {
                Console.WriteLine(e.ToString());
                return new QueryResult[1] { new QueryResult() };
            }
            res.rows = newRows;
            return new QueryResult[1] {
                res
                
            };

        }

        static QueryResult[] getTeamMatchStatQuery(DatabaseManager db, string stat, int num, bool invertOrder = false)
        {
            try { 
            return new QueryResult[2] {
                db.Query("select sum(" + stat + ") as value,count(*) as gp,teamname, idteamlisting from teammatches group by idteamlisting  having gp >= 3 order by sum("+ stat + ") "+(invertOrder?"asc":"desc")+" limit " + num, new Dictionary<string, object>()),
                db.Query("select avg(" + stat + ") as value,count(*) as gp,teamname, idteamlisting from teammatches group by idteamlisting  having gp >= 3 order by avg("+ stat + ") "+(invertOrder?"asc":"desc")+" limit " + num, new Dictionary<string, object>())
            };
        }catch(Exception ex)
            {
                Console.WriteLine(ex);
                return new QueryResult[1] { new QueryResult() };
}
        }

        static QueryResult[] getPlayerMatchStatQuery(DatabaseManager db, string stat, int num, bool invertOrder = false)
        {
            try
            {
                return new QueryResult[2] {
                db.Query("select sum(" + stat + ") as value,count(*) as gp,name, teamname, idteam, idteamlisting from matchplayers group by puuid having gp >= 3 order by sum("+ stat + ") "+(invertOrder?"asc":"desc")+" limit " + num, new Dictionary<string, object>()),
                db.Query("select avg(" + stat + ") as value,count(*) as gp,name, teamname, idteam, idteamlisting from matchplayers group by puuid having gp >= 3  order by avg("+ stat + ") "+(invertOrder?"asc":"desc")+" limit " + num, new Dictionary<string, object>())
            };
            }catch(Exception ex)
            {
                Console.WriteLine(ex);
                return new QueryResult[1] { new QueryResult() };
}
        }

        public string Sorting { get; set; }
        public QueryResult Info { get; set; }
        public QueryResult Matches { get; set; }
        public QueryResult NumMatches { get;set;}
        public QueryResult Schedule { get; set; }
        public QueryResult NumCoaches { get; set; }
        public QueryResult NumTeams { get; set; }
        public QueryResult LeagueStandings { get; set; }

        public List<TopListContent> TopTeams { get; set; }
        public List<TopListContent> TopPlayers { get; set; }
        public List<TopListContent> TopGames { get; set; }
        public List<TopListContent> TopCoaches { get; set; }
        public List<TopListContent> TopRaces { get; set; }
        */
    }
    /*
    public class TopListContent
    {
        public string Title { get; set; }
        public string ValueKey { get; set; }
        public QueryResult[] Result { get; set; }
        public TopListContent()
        {

        }
        public TopListContent(string aTitle, string aKey, QueryResult[] aRes)
        {
            Title = aTitle;
            ValueKey = aKey;
            Result = aRes;
        }
    }
    */
}
