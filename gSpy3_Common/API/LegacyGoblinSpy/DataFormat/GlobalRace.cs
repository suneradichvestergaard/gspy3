﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace goblinSpy.DataFormat
{
    public class GlobalRace
    {
        public int idrace { get; set; }
        public string race { get; set; }
        public bool bash { get; set; }
        public bool dash { get; set; }
        public bool hybrid { get; set; }
        public bool stunty { get; set; }
        public int tier { get; set; }
        public int journeymencost { get; set; }
        public GlobalRace() { }
        public GlobalRace(int id, string r, bool b, bool d, bool h, bool s, int t, int aLonerCost)
        {
            journeymencost = aLonerCost;
            idrace = id;
            race = r;
            bash = b;
            dash = d;
            hybrid = h;
            stunty = s;
            tier = t;
        }
    }
}
