﻿using goblinSpy.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace goblinSpy.DataFormat
{
    public class Schedule
    {
        public List<ScheduledMatch> upcoming_matches { get; set; }
        /*
        public static int Import(Database.DatabaseManager db, string storageFolder, string specificFile, string platform, string league, string competition)
        {
            int count = 0;
            foreach (string file in Directory.GetFiles(storageFolder, "schedule.*.zip"))
            {
                if (specificFile != null && Path.GetFileName(specificFile) != Path.GetFileName(file))
                    continue;

                if (File.Exists(file))
                {

                    // Remove old schedule, since schedule is always the latest
                    Dictionary<string, object> parameters = new Dictionary<string, object>();
                    parameters["p1"] = platform;
                    parameters["c1"] = competition;
                    parameters["l1"] = league;
                    try
                    {
                        // Don't do it, we import from several locations now
                   //     db.Query("delete from schedule where platform=@p1 and competition=@c1 and league=@l1", parameters);
                    }
                    catch(Exception e3)
                    {
                        Console.WriteLine(e3.ToString() + " when deleting old schedule");
                    }
                   // db.StartTransaction();
                    try
                    {
                        string[] rawFiles = Zipper.Unzip(file);
                        if (rawFiles != null)
                        {
                            foreach (string f in rawFiles)
                            {
                                using (var sr = File.OpenText(f))
                                {
                                    string datastring = sr.ReadToEnd();
                                    Schedule ms = SimpleJson.SimpleJson.DeserializeObject<Schedule>(datastring);

                                    if (ms.upcoming_matches != null && ms.upcoming_matches.Count > 0 && ms.upcoming_matches[0].format != "ladder")
                                    {

                                        foreach (ScheduledMatch m in ms.upcoming_matches)
                                        {
                                            

                                            if (file.Contains("." + Match.Platform_PS4 + "."))
                                                m.platform = Match.Platform_PS4;
                                            else if (file.Contains("." + Match.Platform_XBOX1 + "."))
                                                m.platform = Match.Platform_XBOX1;
                                            else
                                                m.platform = Match.Platform_PC;

                                            count++;
                                            try
                                            {
                                                m.Import(db);
                                            }
                                            catch (Exception e4)
                                            {
                                                Console.WriteLine("Exception while importing schedule: " + e4.ToString());
                                            }
                                        }
                                    }
                                }
                                File.Delete(f);
                            }
                            if (rawFiles.Length > 0)
                                Directory.Delete(Path.GetDirectoryName(rawFiles[0]), true);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                  //  db.StopTransaction();
                }
                else Console.WriteLine("Unable to find the stored file " + file);
            }
            return count;
        }
        */
    }

    public class ScheduledMatch
    {
        public int smver { get; set; }
        public string league { get; set; }
        public string competition { get; set; }
        public string platform { get; set; }
        public int competition_id { get; set; }
        public int contest_id { get; set; }
        public string format { get; set; }
   //     [Database.DatabaseManager.DBIgnore]
        public int round { get; set; }
        public int competition_round { get; set; }

        public string type { get; set; }
        public string status { get; set; }
        public string stadium { get; set; }
        public string match_uuid { get; set; }
        public string match_id { get; set; }

   //     [Database.DatabaseManager.DBIgnore]
        public List<ScheduledOpponent> opponents { get; set; }

        // Instead of opponents
        public string idteam1 { get; set; }
        public string idteam2 { get; set; }

        public string date { get; set; }

        /*
        public void Import(Database.DatabaseManager db)
        {
            smver = 2;
            try
            {
                competition_round = round;

                try
                {
                    // Alter table in case it has the old coach id....
                    var res = db.Query("select smver from schedule limit 1", null);
                    if (res == null || res.rows == null || res.rows.Count == 0 || res.rows[0][0] != "2")
                    {

                        Console.WriteLine("Recreating schedule");
                        db.Query("drop table schedule", null);
                        db.RegisterClass<ScheduledMatch>("schedule", "contest_id");
                    }
                }
                catch
                {
                    Console.WriteLine("Recreating schedule");
                    db.Query("drop table schedule", null);
                    db.RegisterClass<ScheduledMatch>("schedule", "contest_id");
                }

                if (opponents != null)
                {

                    /*  foreach (var opp in opponents)
                      {
                          db.WriteClass(opp.coach);
                          db.WriteClass(opp.team);
                      }
                      */
       /*             if (opponents.Count > 0)
                        idteam1 = opponents[0].team.id.ToString();
                    if (opponents.Count > 1)
                        idteam2 = opponents[1].team.id.ToString();


                    db.WriteClass(this);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        */
    }

    public class ScheduledOpponent
    {
     //   [Database.DatabaseManager.DBIgnore]
        public Coach coach { get; set; }
     //   [Database.DatabaseManager.DBIgnore]
        public Team team { get; set; }
    }






    

}
