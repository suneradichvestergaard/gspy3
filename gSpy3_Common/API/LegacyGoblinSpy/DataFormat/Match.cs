﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace goblinSpy.DataFormat
{
    public class Match
    {
        public const string Platform_PC = "pc";
        public const string Platform_XBOX1 = "xb1";
        public const string Platform_PS4 = "ps4";

        public string uuid { get; set; }
        public int idcompetition { get; set; }
        public string leaguename { get; set; }
        public string competitionname { get; set; }
        public string stadium { get; set; }
        public int levelstadium { get; set; }
        public string structstadium { get; set; }
        public string started { get; set; }
        public string finished { get; set; }

        // Given from bb api json, but without db equivalent
        public List<CoachStats> coaches { get; set; }
        public List<TeamStats> teams  { get; set; }

        // Created IDs to handle the items above in db
        public string idcoachstatshome { get; set; }
        public string idcoachstatsaway { get; set; }
        public string idteamstatshome { get; set; }
        public string idteamstatsaway { get; set; }

        // Custom column for external stats
        public string externalstats { get; set; }

        // platform
        public string platform { get; set; }

        public int checkedplayerstats { get; set; }


        public GlobalMatch ToGlobal(bool home, TeamStats t1, TeamStats t2, string aIdCompetition, int aGamevalue, int aGamevaluediff)
        {
            var m = this;
            return new GlobalMatch()
            {
                idcompetition = m.platform + aIdCompetition,
                id = m.uuid + (home ? 0 : 1),
                uuid = m.uuid,
                ishome = home ? 1 : 0,
                idteamstats1 = t1.idteamstats,
                idteamstats2 = t2.idteamstats,
                idteam1 = m.platform + t1.idteamlisting,
                idteam2 = m.platform + t2.idteamlisting,
                idrace1 = t1.idraces,
                idrace2 = t2.idraces,
                gamevalue = aGamevalue,
                gametvdiff = aGamevaluediff,
                gp = 1,
                win = t1.score > t2.score ? 1 : 0,
                draw = t1.score == t2.score ? 1 : 0,
                loss = t1.score < t2.score ? 1 : 0,
                tvdiff = t1.value - t2.value,
                value = t1.value,

                structstadium = m.structstadium,
                started = m.started,
                finished = m.finished,
                duration = (int)(DateTime.Parse(finished) - DateTime.Parse(started)).TotalMinutes,
                score1 = t1.score,
                score2 = t2.score,
                cas1 = t1.inflictedcasualties,
                cas2 = t2.inflictedcasualties,
                concede1 = t2.mvp > 1 ? 1 : 0,
                concede2 = t1.mvp > 1 ? 1 : 0
            };
        }

/*        public void Import(DatabaseManager db, SqliteTransaction trans  = null)
        {
            if (string.IsNullOrEmpty(this.platform))
                this.platform = Platform_PC;

            this.checkedplayerstats = 0;

            foreach (var c in coaches)
            {
              c.idcoachstats = uuid + "-" + c.idcoach;
                if (c == coaches[0])
                    idcoachstatshome = c.idcoachstats;
                else
                    idcoachstatsaway = c.idcoachstats;
                db.WriteClass(c, trans);
            }
            foreach(var t in teams)
            {
                t.idmatch = uuid;
                t.idteamstats = uuid + "-" + t.idteamlisting;
                t.InitializeForWriting();
                if (t == teams[0])
                    idteamstatshome = t.idteamstats;
                else
                    idteamstatsaway = t.idteamstats;
                db.WriteClass(t, trans);
            }

            db.WriteClass(this, trans);
            
        }*/
    }
}
