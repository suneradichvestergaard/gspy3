﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.API.LegacyGoblinSpy
{
    public class Database
    {
        public static string GetDBName(string name)
        {
            string newName = "";
            char last = ' ';
            foreach (char c in name)
            {
                if (c != name[0] && char.IsUpper(c) && char.IsLower(last))
                    newName += "_" + char.ToLower(c);
                else newName += char.ToLower(c);
                last = c;
            }
            return newName;
        }
    }
}
