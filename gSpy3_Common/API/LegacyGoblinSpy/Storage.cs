﻿using goblinSpy.DataFormat;
using goblinSpy.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace goblinSpy.Database
{
    public class Storage
    {
        public static string GetFolder() { return Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Data"); }

        public static string[] GetDataFolders()
        {
            return Directory.GetDirectories(GetFolder());
        }
        public static string GetDataFilename(string folder, string query, string platform, string league, string competition)
        {
            if (platform == Match.Platform_PC)
                platform = ""; // backwards compatibility
            else platform = "." + platform;
            return Path.Combine(folder, query + "." + SaneFilePath.CreateSafePath(league + "." + competition) + platform + ".zip");
        }
        public static string GetDataFolder(DateTime day)
        {
            return Path.Combine(GetFolder(), day.ToString("yyyy-MM-dd"));
        }

        public static string GetDatabaseName(string league, string competition, string platform)
        {
            return Path.Combine("Database", SaneFilePath.CreateSafePath(league + " - " + competition + " - " + platform) + ".sqlite");
        }
        public static string GetOverviewName(string league, string competition, string platform)
        {
            if (platform == "pc")
                return Path.Combine("Overview", SaneFilePath.CreateSafePath(league + " - " + competition) + ".json");
            else
            {

            }
            return Path.Combine("Overview", SaneFilePath.CreateSafePath(league + " - " + competition) + "." + platform + ".json");
        }

    }
}
