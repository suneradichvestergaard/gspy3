﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace goblinSpy.Helpers
{
    public class SaneFilePath
    {
        public static string CreateSanePath(string path, string replaceString = "")
        {
            string safeName = Regex.Replace(
                path,
                @"\W",  /*Matches any nonword character. Equivalent to '[^A-Za-z0-9_]'*/
                replaceString,
                RegexOptions.IgnoreCase);
            return safeName;
        }
        public static string CreateSafePath(string filename)
        {
            byte[] data = UTF8Encoding.UTF8.GetBytes(filename);
            string Base32StandardAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
            StringBuilder result = new StringBuilder(Math.Max((int)Math.Ceiling(data.Length * 8 / 5.0), 1));
            byte[] emptyBuff = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            byte[] buff = new byte[8];
            // take input five bytes at a time to chunk it up for encoding
            for (int i = 0; i < data.Length; i += 5)
            {
                int bytes = Math.Min(data.Length - i, 5);
                // parse five bytes at a time using an 8 byte ulong
                Array.Copy(emptyBuff, buff, emptyBuff.Length);
                Array.Copy(data, i, buff, buff.Length - (bytes + 1), bytes);
                Array.Reverse(buff);
                ulong val = BitConverter.ToUInt64(buff, 0);
                for (int bitOffset = ((bytes + 1) * 8) - 5; bitOffset > 3; bitOffset -= 5)
                {
                    result.Append(Base32StandardAlphabet[(int)((val >> bitOffset) & 0x1f)]);
                }
            }

            return result.ToString();

        }


    }
}
