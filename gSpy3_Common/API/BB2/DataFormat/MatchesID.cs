﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.API.BB2.DataFormat
{
    public class MatchesID
    {
        public List<MatchID> matches { get; set; }
    }


    public class MatchID
    {
        public string uuid;
        public int id;
        public int idleague;
    }
}
