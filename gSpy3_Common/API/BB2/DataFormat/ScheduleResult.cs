﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.API.BB2.DataFormat
{
    public class ScheduleResult
    {
        public List<ScheduledMatch> upcoming_matches { get; set; }
    }

    public class ScheduledMatch
    {
        public int smver { get; set; }
        public string league { get; set; }
        public string competition { get; set; }
        public string platform { get; set; }
        public int competition_id { get; set; }
        public int? contest_id { get; set; }
        public string format { get; set; }
        public int? round { get; set; }

        public string type { get; set; }
        public string status { get; set; }
        public string stadium { get; set; }
        public string match_uuid { get; set; }
        public string match_id { get; set; }

        public List<ScheduledOpponent>? opponents { get; set; }

    }
    public class ScheduledOpponent
    {
        public Coach? coach { get; set; }
        public Team? team { get; set; }
    }
}
