﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.API.BB2.DataFormat
{
    public class Team
    {
        public int id { get; set; }
        public string team { get; set; }
        public string logo { get; set; }
        public string value { get; set; }
        public string motto { get; set; }
        public string race { get; set; }
        public string coach { get; set; }

        public int active { get; set; }

        public string platform { get; set; }
        public string competition { get; set; }
        public string league { get; set; }


   
    }


    public class Teams
    {
        public List<Team> teams;

    }
}
