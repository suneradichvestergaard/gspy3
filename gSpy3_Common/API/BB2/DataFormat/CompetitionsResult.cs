﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.API.BB2.DataFormat
{
    public class CompetitionsResult
    {
        public List<CompetitionsResultItem> competitions { get; set; }
    }

    public class CompetitionsResultItem
    {
        public int? id { get; set; }
        public string name { get; set; }
        public string format { get; set; }
        public string turn_duration { get; set; }
        public string date_created { get; set; }
    }
}
