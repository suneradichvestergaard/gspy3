﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.API.BB2.DataFormat
{
    public class TeamStats
    {
        public string idmatch { get; set; }
        public string idteamstats { get; set; }
        public int idteamlisting { get; set; }
        public int idraces { get; set; }
        public string teamname { get; set; }
        public string teamlogo { get; set; }
        public int value { get; set; }
        public int score { get; set; }
        public int cashbeforematch { get; set; }
        public int popularitybeforematch { get; set; }
        public int popularitygain { get; set; }
        public int cashspentinducements { get; set; }
        public int cashearned { get; set; }
        public int cashearnedbeforeconcession { get; set; }
        public int winningsdice { get; set; }
        public int spirallingexpenses { get; set; }
        public int nbsupporters { get; set; }
        public int possessionball { get; set; }
        public int occupationown { get; set; }
        public int occupationtheir { get; set; }
        public int mvp { get; set; }
        public int inflictedpasses { get; set; }
        public int inflictedcatches { get; set; }
        public int inflictedinterceptions { get; set; }
        public int inflictedtouchdowns { get; set; }
        public int inflictedcasualties { get; set; }
        public int inflictedtackles { get; set; }
        public int inflictedko { get; set; }
        public int inflictedinjuries { get; set; }
        public int inflicteddead { get; set; }
        public int inflictedmetersrunning { get; set; }
        public int inflictedmeterspassing { get; set; }
        public int inflictedpushouts { get; set; }
        public int sustainedexpulsions { get; set; }
        public int sustainedcasualties { get; set; }
        public int sustainedko { get; set; }
        public int sustainedinjuries { get; set; }
        public int sustaineddead { get; set; }



        public TeamStats()
        {
        }

       

   
    }

    public class TeamStats_Current
    {
        public int? cheerleaders { get; set; }
        public int? apothecary { get; set; }
        public int? rerolls { get; set; }
        public int? assistantcoaches { get; set; }
    }
}
