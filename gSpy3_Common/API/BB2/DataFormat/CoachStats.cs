﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.API.BB2.DataFormat
{
    public class CoachStats
    {
        public string idcoachstats { get; set; }
        public int idcoach { get; set; }
        public string coachname { get; set; }
        public int coachcyanearned { get; set; }
        public int coachxpearned { get; set; }
        public bool active { get; set; }

        public CoachStats()
        {
            active = true;
        }
    }
}
