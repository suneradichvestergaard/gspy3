﻿using gSpy3_Common.API.BB2.DataFormat;
using gSpy3_Common.Database.AccessInterface;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace gSpy3_Common.API.BB2
{
    public class BB2API : API 
    {
        const string QueryURL_Matches = "http://web.cyanide-studio.com/ws/bb2/matches/?key={{key}}&league={{league}}&competition={{competition}}&limit=3000&start={{start}}&end={{end}}&platform={{platform}}";
        const string QueryURL_MatchesID = "http://web.cyanide-studio.com/ws/bb2/matches/?key={{key}}&league={{league}}&limit={{limit}}&start={{start}}&end={{end}}&platform={{platform}}&id_only=1";
        const string QueryURL_Match = "https://web.cyanide-studio.com/ws/bb2/match/?key={{key}}&uuid={{uuid}}";
        const string QueryURL_Replay = "https://web.cyanide-studio.com/ws/bb2/replay/?key={{key}}&uuid={{uuid}}&format=zip";
        const string QueryURL_Schedule = "http://web.cyanide-studio.com/ws/bb2/contests/?key={{key}}&league={{league}}&competition={{competition}}&limit=500&platform={{platform}}";
        const string QueryURL_SingleMatch = "http://web.cyanide-studio.com/ws/bb2/match/?key={{key}}&uuid={{uuid}}";
        const string QueryURL_Coaches = "http://web.cyanide-studio.com/ws/bb2/coaches/?key={{key}}&league={{league}}&competition={{competition}}&limit=100000&platform={{platform}}";
        const string QueryURL_Teams = "http://web.cyanide-studio.com/ws/bb2/teams/?key={{key}}&league={{league}}&competition={{competition}}&limit=100000&platform={{platform}}";
        const string QueryURL_League = "http://web.cyanide-studio.com/ws/bb2/league/?key={{key}}&league_name={{league}}&platform={{platform}}";
        const string QueryURL_Competitions = "http://web.cyanide-studio.com/ws/bb2/competitions/?key={{key}}&league_name={{league}}&platform={{platform}}";

        string key;
        public BB2API(string aKey)
        {
            key = aKey;
        }

        public List<UInt64> GetMatchIDs(int module, string platform, string from, string to, List<string> leagues,int limit)
        {
            while (!ResourceManager.UseBB2API(module, 1))
            {
                System.Threading.Thread.Sleep(1000);
            }
            List<string> leagues2 = new List<string>();
            foreach(var l in leagues)
            {
                leagues2.Add(HttpUtility.UrlEncode(l));
            }
            List<UInt64> ret = new List<ulong>();

            Parallel.ForEach(leagues, (league) =>
            {
                var matchesIDs = Get<MatchesID>(QueryURL_MatchesID.Replace("{{key}}", key).Replace("{{league}}", HttpUtility.UrlEncode(league)).Replace("{{limit}}", "" + limit).Replace("{{start}}", from).Replace("{{end}}", to).Replace("{{platform}}", platform));
                if (matchesIDs.matches != null)
                {
                    lock (ret)
                    {
                        foreach (var m in matchesIDs.matches)
                        {
                            ret.Add(UInt64.Parse(m.uuid, NumberStyles.HexNumber));
                        }
                    }
                }
            });

            /*
            var matchesIDs =  Get<MatchesID>(QueryURL_MatchesID.Replace("{{key}}", key).Replace("{{league}}", string.Join(',',leagues2)).Replace("{{limit}}", "" + limit).Replace("{{start}}", from).Replace("{{end}}", to).Replace("{{platform}}", platform));
            if (matchesIDs.matches != null)
            {
                foreach (var m in matchesIDs.matches)
                {
                    ret.Add(UInt64.Parse(m.uuid, NumberStyles.HexNumber));
                }
            }*/

            return ret;
            

        }
        public MatchResult GetMatch(int module,UInt64 mid)
        {
            while (!ResourceManager.UseBB2API(module, 1))
            {
                System.Threading.Thread.Sleep(1000);
            }
            

            return Get<MatchResult>(QueryURL_Match.Replace("{{key}}", key).Replace("{{uuid}}", string.Join(',', mid.ToString("x"))));

        }


        public LeagueResult GetLeague(int module, string platform, string league_name)
        {
            while (!ResourceManager.UseBB2API(module, 1))
            {
                System.Threading.Thread.Sleep(1000);
            }


            return Get<LeagueResult>(QueryURL_League.Replace("{{key}}", key).Replace("{{league}}", HttpUtility.UrlEncode(league_name)).Replace("{{platform}}", platform));

        }
        public CompetitionsResult GetCompetitions(int module, string platform, string league_name)
        {
            while (!ResourceManager.UseBB2API(module, 1))
            {
                System.Threading.Thread.Sleep(1000);
            }


            return Get<CompetitionsResult>(QueryURL_Competitions.Replace("{{key}}", key).Replace("{{league}}", HttpUtility.UrlEncode(league_name)).Replace("{{platform}}", platform)+"&limit=1000");

        }
        public CoachResult GetCoaches(int module, string platform, string league_name, string competition_name)
        {
            while (!ResourceManager.UseBB2API(module, 1))
            {
                System.Threading.Thread.Sleep(1000);
            }


            return Get<CoachResult>(QueryURL_Coaches.Replace("{{key}}", key).Replace("{{competition}}", HttpUtility.UrlEncode(competition_name)).Replace("{{league}}", HttpUtility.UrlEncode(league_name)).Replace("{{platform}}", platform));

        }

        public ScheduleResult GetSchedule(int module, string platform, string league, string competition)
        {
            while (!ResourceManager.UseBB2API(module, 1))
            {
                System.Threading.Thread.Sleep(1000);
            }


            var res1 = Get<ScheduleResult>(QueryURL_Schedule.Replace("{{key}}", key).Replace("{{league}}", HttpUtility.UrlEncode(league)).Replace("{{competition}}", HttpUtility.UrlEncode(competition)).Replace("{{platform}}", platform) + "&status=played");
            var res2 = Get<ScheduleResult>(QueryURL_Schedule.Replace("{{key}}", key).Replace("{{league}}", HttpUtility.UrlEncode(league)).Replace("{{competition}}", HttpUtility.UrlEncode(competition)).Replace("{{platform}}", platform) + "&status=scheduled");
            foreach (var item in res2.upcoming_matches) {
                res1.upcoming_matches.Add(item);
            }
            return res1;

        }

        public TeamsResult GetTeams(int module, string platform, string league_name, string competition_name)
        {
            while (!ResourceManager.UseBB2API(module, 1))
            {
                System.Threading.Thread.Sleep(1000);
            }


            return Get<TeamsResult>(QueryURL_Teams.Replace("{{key}}", key).Replace("{{league}}", HttpUtility.UrlEncode(league_name)).Replace("{{platform}}", platform).Replace("{{competition}}", competition_name) );

        }

        public void DownloadMatch(int module, string uuid, string target)
        {
            string dir = Path.GetDirectoryName(target);
            if (Directory.Exists(dir) == false)
            {
                Directory.CreateDirectory(dir);
            }

            while (!ResourceManager.UseBB2API(module, 1))
            {
                System.Threading.Thread.Sleep(1000);
            }
            string url = QueryURL_Replay.Replace("{{key}}", key).Replace("{{uuid}}", uuid);

            /*
            Process proc = new Process();
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.CreateNoWindow = true;
            proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            proc.StartInfo.FileName = "curl";
            proc.StartInfo.Arguments = "-o \"" + target +" \"" + url + "\"";
            proc.StartInfo.RedirectStandardError = false;
            proc.StartInfo.RedirectStandardOutput = false;
            proc.StartInfo.UseShellExecute = true;
            proc.StartInfo.CreateNoWindow = true;
            proc.Start();
            proc.WaitForExit(5000);
            */
            /*
            using (var wc = new WebClient())
            {
                wc.DownloadFile(url, target);
            }*/

            var client = new HttpClient();

            // Create the HttpContent for the form to be posted.
            // Get the response.
            HttpResponseMessage response = client.GetAsync(url).Result;

            using (var stream = response.Content.ReadAsStreamAsync().Result)
            {
                var fileInfo = new FileInfo(target);
                using (var fileStream = fileInfo.OpenWrite())
                {
                    stream.CopyToAsync(fileStream).Wait();
                }
                stream.Flush();
            }
        }

    }
}
