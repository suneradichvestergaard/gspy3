﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace gSpy3_Common.API
{
    public class API
    {
        private class MyWebClient : WebClient
        {
            protected override WebRequest GetWebRequest(Uri uri)
            {
                WebRequest w = base.GetWebRequest(uri);
                w.Timeout = 20 * 60 * 1000;
                return w;
            }
        }

        
        public T Get<T>(string url)
        {
            using (MyWebClient wc = new MyWebClient())
            {
                return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(wc.DownloadString(url));
            }
        }
        public string GetRaw(string url)
        {
            using (MyWebClient wc = new MyWebClient())
            {
                return (wc.DownloadString(url));
            }
        }
    }
}
