﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BB2StatsLib.Views.Replay
{
    [Serializable]
    public class ReplayMoveAction : ReplayActionBase
    {
        public List<int[]> Steps { get; set; }
        public ReplayMoveAction() { }
        public ReplayMoveAction(ReplayActionPosition from, ReplayActionPosition to)
		{
            Steps = new List<int[]>();
            Steps.Add(new int[] { from.X, from.Y });
            Steps.Add(new int[] { to.X, to.Y });
		}
        public void AddStep(ReplayActionPosition pos)
        {
            Steps.Add(new int[] { pos.X, pos.Y });
        }
    }
}
