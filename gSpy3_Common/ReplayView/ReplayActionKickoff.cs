﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BB2StatsLib.Views.Replay
{
    [Serializable]
    public class ReplayActionKickoff
    {
        public int KickOffResult { get; set; }

        public ReplayActionKickoff() { }
        public ReplayActionKickoff(int aResult)
        {
            KickOffResult = aResult;
        }

    }
}
