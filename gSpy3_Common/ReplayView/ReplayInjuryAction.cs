﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BB2StatsLib.Views.Replay
{
    [Serializable]
    public class ReplayInjuryAction : ReplayActionBase
    {
        public int Cause {get;set;}
        public List<int> Roll { get; set; }
        public ReplayInjuryAction() { }
        public ReplayInjuryAction(int causeIndex, List<int> roll)
        {
            Cause = causeIndex;
            Roll = roll;
        }
    }
}
