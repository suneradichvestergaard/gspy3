//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using System;

using System.Globalization;
using System.Collections.Generic;
using BB2StatsLib.Views.Replay;
using System.Reflection;
using BB2StatsLib.BB2Replay;

namespace BB2StatsLib
{
	[Serializable]
	public class GameStats 
	{
        public string ParserVersion { get; set; }
		public string Date {get;set;}
		public string Time {get;set;}
        public string LeagueName { get; set; }
        public string CompetitionName { get; set; }
		public string GameId {get;set;}
        public int Round { get; set; }
        public int RegistrationMode { get; set; }
        public int MaxRounds { get; set; }
        public int LeaguePhase { get; set; }
        public int LeagueEdition { get; set; }
        public TeamStats[] TeamStats { get; set; }

        /// <summary>
        /// Replay data. This will be nulled in the exported view.
        /// </summary>
        public ReplayView Replay { get; set; }

        public bool IsManual { get; set; }
        
        public GameStats()
		{
		}
        /// <summary>
        /// Create from a raw replay file (.bbrz)
        /// </summary>
        /// <param name="replayId"></param>
        /// <param name="replay"></param>
        /// <returns></returns>
		public static GameStats CreateFromReplay(string replayId,Replay replay)
		{
			GameStats stats = new GameStats ();
			stats.ParseFromReplay (replayId,replay);
			return stats;
		}
      
        /// <summary>
        /// Parse from a raw replay file (.bbrz)
        /// </summary>
        /// <param name="replayId"></param>
        /// <param name="replay"></param>
		public void ParseFromReplay(string replayId,Replay replay)
		{
            IsManual = false;
            ParserVersion = Assembly.GetAssembly(typeof(GameStats)).GetName().Version.ToString();
            List<CoachInfos> coaches = new List<CoachInfos>();
            foreach (var step in replay.ReplayStep)
            {
                if (step.GameInfos != null)
                {
                    if (step.GameInfos.RowLeague != null && step.GameInfos.RowLeague.Name != null)
                        LeagueName = step.GameInfos.RowLeague.Name;
                    if (step.GameInfos.RowCompetition != null && step.GameInfos.RowCompetition.Name != null)
                    {
                        CompetitionName = step.GameInfos.RowCompetition.Name;
                        try
                        {
                            Round = step.GameInfos.RowCompetition.CurrentRound;
                            MaxRounds = step.GameInfos.RowCompetition.NbRounds;
                            RegistrationMode = step.GameInfos.RowCompetition.RegistrationMode;
                            LeaguePhase = step.GameInfos.RowCompetition.LeaguePhase;
                            LeagueEdition = step.GameInfos.RowCompetition.LeagueEdition;

                        }
                        catch { }
                    }
                    if (step.GameInfos.CoachesInfos != null)
                        coaches = step.GameInfos.CoachesInfos;
                    
                    if (LeagueName != null && CompetitionName != null && coaches != null)
                        break;
                }
            }
            if (LeagueName == null)
                LeagueName = "UnknownLeague";
            if (CompetitionName == null)
                CompetitionName = "";
			GameId = replayId;
			TeamStats = new TeamStats[2];
			TeamStats [0] = new BB2StatsLib.TeamStats ();
			TeamStats [1] = new BB2StatsLib.TeamStats ();
			TeamStats [0].IsHome = true;
			TeamStats [1].IsHome = false;
            Replay = ReplayView.CreateFromReplay(replay);

            

            // Pars standard stats
			foreach (ReplayStep step in replay.ReplayStep) 
			{
				int teamIndex = 0;
				if(step.BoardState != null && step.BoardState.ListTeams != null)
				{
					foreach( TeamState ts in step.BoardState.ListTeams)
					{
						TeamStats[teamIndex].ParseTeamState(ts);

						teamIndex++;
					}
				}
				teamIndex = 0;
				if(step.RulesEventGameFinished != null)
				{
					if(step.RulesEventGameFinished.MatchResult.CoachResults != null)
					{
						foreach( CoachResult res in step.RulesEventGameFinished.MatchResult.CoachResults)
						{
                            
							TeamStats[teamIndex].ParseCoachResult(res,(coaches==null || coaches.Count <= teamIndex) ? null :  coaches[teamIndex]);
							
							teamIndex++;
						}
					}
					if(step.RulesEventGameFinished.MatchResult.Row.Finished!="")
					{
                        DateTime dt = DateTime.MinValue;
                        try
                        {
                            dt =
                            DateTime.ParseExact(step.RulesEventGameFinished.MatchResult.Row.Finished,
                                                "yyyy-MM-dd HH:mm:ss.ffffff",
                                                CultureInfo.InvariantCulture,
                                                DateTimeStyles.AssumeUniversal |
                                                DateTimeStyles.AdjustToUniversal);
                        }
                        catch { }
						Date = dt.ToString("yyyy-MM-dd");
						Time = dt.ToString("HH:mm:ss");
                        TeamStats[0].Score = step.RulesEventGameFinished.MatchResult.Row.HomeScore;
                        TeamStats[1].Score = step.RulesEventGameFinished.MatchResult.Row.AwayScore;
                        TeamStats[1].ScoreAgainst = step.RulesEventGameFinished.MatchResult.Row.HomeScore;
                        TeamStats[0].ScoreAgainst = step.RulesEventGameFinished.MatchResult.Row.AwayScore;
                    }

				}
			}




			
            TeamStats[0].TouchdownsAgainst = TeamStats[1].Touchdowns;
            TeamStats[1].TouchdownsAgainst = TeamStats[0].Touchdowns;
            TeamStats[0].LeagueName = replay.ReplayStep[0].GameInfos.RowLeague.Name;
            TeamStats[1].LeagueName = replay.ReplayStep[0].GameInfos.RowLeague.Name;
            TeamStats[0].CompetitionName = replay.ReplayStep[0].GameInfos.RowCompetition.Name;
            TeamStats[1].CompetitionName = replay.ReplayStep[0].GameInfos.RowCompetition.Name;
            if (TeamStats[0].CompetitionName == null)
                TeamStats[0].CompetitionName = "";
            if (TeamStats[1].CompetitionName == null)
                TeamStats[1].CompetitionName = "";
            
 
            CalculateAdvancedStats();

            TeamStats[0].SumStatsFromPlayers();
            TeamStats[1].SumStatsFromPlayers();
        }
        /// <summary>
        /// Create stats that might not part of the original replay file, but that we can calculate
        /// </summary>
        public void CalculateAdvancedStats()
        {
            

            for(int i=0; i < TeamStats.Length;i++)
            {
                TeamStats[i].CalculateAdvancedStats(this, TeamStats[i == 0 ? 1 : 0]);
                
            }
        }

  
	
	}



}

