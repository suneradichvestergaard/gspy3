﻿using BB2StatsLib.Views.Replay;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.ReplayView
{
    [Serializable]
    public class RollStats
    {
        public string Action;
        public double Chance;
        public int Rolls;
        public int OK;
        public RollStats()
        {
        }
        public RollStats(ReplayItemType action, double chance)
        {
            Rolls = 0;
            OK = 0;
            Chance = chance;
            Action = action.ToString();
        }
    }
}
