﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BB2StatsLib.Views.Replay
{
    [Serializable]
    public class ReplayActionBase
    {
        public static int ParseDice(string diceString)
        {

            diceString = diceString.Substring(1, diceString.Length - 2);
            List<BlockDice> dices = new List<BlockDice>();
            foreach (string sDice in diceString.Split(','))
            {
                return (int.Parse(sDice));
            }
            return -1;
        }
        public static List<int> ParseDices(string diceString)
        {
            List<int> list = new List<int>();
            if(diceString.StartsWith("("))
                diceString = diceString.Substring(1, diceString.Length - 2);

            List<BlockDice> dices = new List<BlockDice>();
            foreach (string sDice in diceString.Split(','))
            {
                list.Add(int.Parse(sDice));
            }
            return list;
        }
    }
}
