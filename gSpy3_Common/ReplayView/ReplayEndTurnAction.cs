﻿using BB2StatsLib.BB2Replay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BB2StatsLib.Views.Replay
{
    [Serializable]
    public class ReplayEndTurnAction
    {
        public int Turn { get; set; }
        public int NextTeam { get; set; }
        public int NextTeamRerolls { get; set; }
        public ReplayActionPosition BallPosition { get; set; }
        public bool IsBallHeld { get; set; }
        public List<ReplayPlayer> Players { get; set; }
        
        public ReplayEndTurnAction()
        {
            NextTeam = -1;
            NextTeamRerolls = -1;
        }
        public ReplayEndTurnAction(int aTurn, int aNextTeam, int aNextTeamRerolls, BallData ball)
        {
            Turn = aTurn;
            NextTeam = aNextTeam;
            NextTeamRerolls = aNextTeamRerolls;
            IsBallHeld = ball.IsHeld != 0;
            BallPosition = new ReplayActionPosition(ball.Cell);
            Players = new List<ReplayPlayer>();
        }
    }
    [Serializable]
    public class ReplayPlayer
    {
        public int PID { get; set; }
        public int[] Pos { get; set; }
        public int Dazed { get; set; } // 0=No, 1=down, 2=stunned
        public ReplayPlayer()
        {
        }
        public ReplayPlayer(int pid, Location loc, int dazed)
        {
            PID = pid;
            Pos = new int[] { loc.x, loc.y };
            Dazed = dazed;
        }
    }

}
