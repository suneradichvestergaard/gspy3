﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BB2StatsLib.Views.Replay
{
    [Serializable]
    public class ReplayRollAction
    {
        public int Req { get; set; }
        public List<int> Roll { get; set; }
        public int[] Pos { get; set; }
        public bool RR { get; set; }
        public ReplayRollAction() { }
        public ReplayRollAction(int target, List<int> roll, BB2Replay.Location pos, bool rerolled )
        {
            Req = target;
            Roll = roll;
            if (pos != null)
                Pos = new int[] { pos.x, pos.y };
            else Pos = new int[] { -1, -1 };
            RR = rerolled;
        }
    }

}
