﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BB2StatsLib.Views.Replay;

namespace BB2StatsLib.Views.CalculatedStats
{
    public class CompletionCalculator : IStatCalculator
    {
        ReplayView _replayView;
        int completions = 0;
        int[] successfulPassToThisTurn = null;

        public void AddFromReplayItem(ReplayItem item, int index, TeamStats ownTeam, TeamStats otherTeam,int turn)
        {
            PlayerStats ps = ownTeam.FindUniquePlayer(item.PID);
            if (ps == null)
                return;
            
            if(item.AID == (int)ReplayItemType.Pass)
            {
                ReplayRollAction ra = _replayView.Rolls[item.I];
                successfulPassToThisTurn = LuckCalculator.IsRollSuccessful(ra.Roll[0], ra.Req) ? ra.Pos : null;
            }
            if (item.AID == (int)ReplayItemType.Catch && successfulPassToThisTurn != null)
            {

                ReplayRollAction ra = _replayView.Rolls[item.I];
                if (ra.Pos[0] == successfulPassToThisTurn[0] && ra.Pos[1] == successfulPassToThisTurn[1] && LuckCalculator.IsRollSuccessful(ra.Roll[0], ra.Req))
                {
                    completions++;
                    ps.Completions++;
                }
                else
                    successfulPassToThisTurn = null;
            }
        }

        public void StoreResults(GameStats game, TeamStats stats, TeamStats otherTeam)
        {
            stats.Completions = completions;
        }

        public void TurnStarted(ReplayView aReplayView)
        {
            _replayView = aReplayView;
            successfulPassToThisTurn = null;
        }
    }
}
