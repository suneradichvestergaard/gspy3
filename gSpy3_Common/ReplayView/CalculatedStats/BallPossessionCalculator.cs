﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BB2StatsLib.Views.Replay;

namespace BB2StatsLib.Views.CalculatedStats
{
    public class BallPossessionCalculator : IStatCalculator
    {
        ReplayView _replayView;
        int turnsHeld = 0;
        int turnCount = 0;
        int notHeldCount = 0;
        int wasHeldCount = 0;
        Dictionary<string, int> posToPlayerIdMap = new Dictionary<string, int>();

        public BallPossessionCalculator()
        {
            turnsHeld = 0;
            turnCount = 0;
            notHeldCount = 0;
            wasHeldCount = 0;
        }

        #region IStatCalculator Members

        public void TurnStarted(Replay.ReplayView aReplayView)
        {
            _replayView = aReplayView;
        }

        public void AddFromReplayItem(Replay.ReplayItem item, int index, TeamStats ownTeam, TeamStats otherTeam, int turn)
        {
            if (item.AID == (int)ReplayItemType.EndTurn)
            {
                
                ReplayEndTurnAction endTurnAction = _replayView.EndTurns[item.I];
                if (endTurnAction.Turn > 0)
                {
                    turnCount++;
                    if (endTurnAction.IsBallHeld)
                    {
                        wasHeldCount++;
                        int pid = 0;
                        if (posToPlayerIdMap.TryGetValue(endTurnAction.BallPosition.X + "," + endTurnAction.BallPosition.Y, out pid))
                        {
                            if (ownTeam.FindUniquePlayer(pid) != null)
                                turnsHeld++;

                        }
                    }
                    else
                        notHeldCount++;
                }
            }
            else if (item.AID == (int)ReplayItemType.NoRoll) // move
            {
                ReplayMoveAction move = _replayView.Moves[item.I];
                int[] lastStep = move.Steps.Last();
                posToPlayerIdMap[lastStep[0] + "," + lastStep[1]] = item.PID;
            }
        }

        public void StoreResults(GameStats game, TeamStats stats, TeamStats otherTeam)
        {
            if(turnCount>0)
             stats.BallPossession = (100 * turnsHeld) / turnCount;
        }

        #endregion
    }
}
