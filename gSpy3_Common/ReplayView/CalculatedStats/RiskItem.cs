﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BB2StatsLib.Views.CalculatedStats
{
    public class RiskItem
    {
        public bool IsSuccess = true;
        public bool IsRerollUsed = false;
        public double SuccessChance = 0;

    }
}
