﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BB2StatsLib.Views.Replay;
using System.Reflection;

namespace BB2StatsLib.Views.CalculatedStats
{
    public class SimpleActionCalculator : IStatCalculator
    {
        public int Success = 0;
        public int Fail = 0;
        public ReplayItemType ItemType = ReplayItemType.Armor;
        PropertyInfo _property;
        PropertyInfo _teamProperty;
        PropertyInfo _teamFailProperty;
        ReplayView _replayView;

        public SimpleActionCalculator()
        {
        }
        public SimpleActionCalculator(ReplayItemType aType, string propertyName)
        {
            ItemType = aType;
            _property = typeof(PlayerStats).GetProperty(propertyName);
            _teamProperty = typeof(TeamStats).GetProperty(propertyName);
            _teamFailProperty = typeof(TeamStats).GetProperty(propertyName + "Failed");
        }
        public void TurnStarted(ReplayView aReplayView)
        {
            _replayView = aReplayView;
        }

        /// <summary>
        /// Adds calculations based on a single replay item (a move, a block etc)
        /// </summary>
        /// <param name="item"></param>
        /// <param name="index"
        public void AddFromReplayItem(ReplayItem item, int index, TeamStats ownTeam, TeamStats otherTeam, int turn)
        {
            if (item.AID != (int)ItemType)
                return;

            ReplayRollAction action = item.I < 0 || item.I >= _replayView.Rolls.Count ? null : _replayView.Rolls[item.I];
            
            // Try to find own player
            PlayerStats ps = ownTeam.FindUniquePlayer(item.PID);
            
            if (ps == null)
                return;
            if (action == null)
            {
                Success++;
                int val = (int)_property.GetValue(ps, null);
                _property.SetValue(ps, (val + 1), null);
            }
            else
            {

                int roll = action.Roll.Sum();
                if (roll != 1 && ((action.Roll.Count == 1 && roll == 6) || roll >= action.Req))
                {
                    Success++;
                    int val = (int)_property.GetValue(ps, null);
                    _property.SetValue(ps, (val + 1), null);

                    // val = (int)_teamProperty.GetValue(ownTeam, null);
                    // _teamProperty.SetValue(ownTeam, (val + 1), null);


                }
                else
                {
                    Fail++;

                    if (_teamFailProperty != null)
                    {
                        int val = (int)_teamFailProperty.GetValue(ownTeam, null);
                        _teamFailProperty.SetValue(ownTeam, (val + 1), null);
                    }
                }
            }
        }

        public void StoreResults(GameStats game, TeamStats stats, TeamStats otherTeam)
        {
            // Already stored above
        }
    }
}
