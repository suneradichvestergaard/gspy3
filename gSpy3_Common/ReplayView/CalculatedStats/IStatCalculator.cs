﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BB2StatsLib.Views.Replay;

namespace BB2StatsLib.Views.CalculatedStats
{
    public interface IStatCalculator
    {
        void TurnStarted(ReplayView aReplayView);
        /// <summary>
        /// Adds calculations based on a single replay item (a move, a block etc)
        /// </summary>
        /// <param name="item"></param>
        /// <param name="index"
        void AddFromReplayItem(ReplayItem item, int index, TeamStats ownTeam, TeamStats otherTeam, int turn);

        void StoreResults(GameStats game, TeamStats stats, TeamStats otherTeam);
        
    }
}
