﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BB2StatsLib.Views.Replay;
using gSpy3_BB2MatchParser.BB2Replay;
using gSpy3_Common.ReplayView;

namespace BB2StatsLib.Views.CalculatedStats
{
    public enum LuckCalculatorType { All, Passing, Movement, Bashing }
    public class LuckCalculator : IStatCalculator
    {
        LuckCalculatorType _luckType = LuckCalculatorType.All;
        ReplayView _replayView;

        static double[] _blockChanceWithoutBlock = new double[] { 800.0 / 216.0, 400.0 / 36.0, 1200.0 / 36.0, 2000.0 / 36.0, 15200.0 / 216.0 };
        static double[] _blockChanceWithBlock = new double[] { 2700.0 / 216.0, 900.0 / 36.0, 300.0 / 6.0, 2700.0 / 36.0, 18900.0 / 216.0 };
        static double[] _armorRollChance = new double[] { 91.67, 83.333, 72.22, 58.3333, 41.6667, 27.77778, 16.66667, 8.333, 3.0 };
        static double[] _blockChanceWithoutBlockAndDefDown = new double[] { 0.46, 2.8, 16.7, 30.6, 42.1 };
        List<RollStats> _luckMap = new List<RollStats>();

        int blocksGood = 0;
        int blocksNeutral = 0;
        int blocksBad = 0;


        public LuckCalculator()
        {
            
        }
        public LuckCalculator(LuckCalculatorType aType)
        {
            _luckType = aType;
        }

        public void TurnStarted(ReplayView aReplayView)
        {
            _replayView = aReplayView;
        }

        public static bool IsRollSuccessful(int roll, int target)
        {
            return roll == 6 || (roll != 1 && roll >= target);
        }
        public static double GetChanceOfArmourBreak(int armourValue)
        {
            int index = armourValue - 4;
            if (index < 0)
            {
                index = 0;
                
            }
            if (index >= _armorRollChance.Length)
                index = _armorRollChance.Length;
            
            return _armorRollChance[index];
        }
        public static double GetRollChance(int required)
        {
            return 100.0 - RiskCalculator.GetRollRisk(required);
        }
        public static double GetBlockChance(int dices, List<string> blockerSkills, List<string> targetSkills)
        {
            if (dices > 0) // White dices
                dices += 1;
            if (dices < 0) // Red dices
                dices = 3 + dices;
            double chance = 0;
            bool blockWorks = false;
            bool defDownWorks = true;

            
            if (blockerSkills != null && (blockerSkills.Contains(SkillConsts.Block)))
                blockWorks = true; // Yay, we got block

            if (targetSkills != null && (targetSkills.Contains(SkillConsts.Block) ||
                                        targetSkills.Contains(SkillConsts.Wrestle)))
                blockWorks = false; // Nay, target got block or wrestle

            if (targetSkills != null && (targetSkills.Contains(SkillConsts.Dodge)))
            {
                defDownWorks = false; // Nay, target got dodge
                if (blockerSkills != null && (blockerSkills.Contains(SkillConsts.Tackle)))
                    defDownWorks = true; // Yay, we got Tackle
            }

            if(!blockWorks && !defDownWorks)
                chance = (_blockChanceWithoutBlockAndDefDown[dices]);
            else if(!blockWorks && defDownWorks)
                chance = (_blockChanceWithoutBlock[dices]);
            else if(blockWorks && !defDownWorks)
                chance = (_blockChanceWithoutBlock[dices]);
            else
                chance = (_blockChanceWithBlock[dices]);
              
            return chance;
        }

        void AddRoll(ReplayItemType itemType,double chance, bool success)
        {
            if (chance < 0)
                throw new Exception("No negative chances please");

            RollStats stats = null;
            foreach (RollStats rollstats in _luckMap)
            {
                if (rollstats.Action == itemType.ToString() && rollstats.Chance == chance)
                {
                    stats = rollstats;
                    break;
                }
            }
            if (stats == null)
            {
                stats = new RollStats(itemType, chance);
                _luckMap.Add(stats);
            }

            if (chance == 70)
                System.Diagnostics.Debug.Assert(false);
            
            stats.Rolls++;
            stats.OK += success?1:0;
        }
        /// <summary>
        /// Adds calculations based on a single replay item (a move, a block etc)
        /// </summary>
        /// <param name="item"></param>
        /// <param name="index"
        public void AddFromReplayItem(ReplayItem item, int index, TeamStats ownTeam, TeamStats otherTeam, int turn)
        {
            PlayerStats player = ownTeam.FindUniquePlayer(item.PID);
            
            if (player == null) // Only calc luck for own players
                return;

            switch ((ReplayItemType)item.AID)
            {
                


                case ReplayItemType.Injury:
                    if (_luckType == LuckCalculatorType.All || _luckType == LuckCalculatorType.Bashing)
                    {
                        ReplayInjuryAction roll = _replayView.Injuries[item.I];
                        int requiredRoll = 8; // 8+ to knockout is considered success
                       

                        // Try to find the blocker...
                        if (index > 0 && _replayView.Items[index - 2].AID == (int)ReplayItemType.Block &&
                            _replayView.Blocks[_replayView.Items[index - 2].I].TID == player.PlayerId)
                        {

                            PlayerStats blocker = otherTeam.FindUniquePlayer(_replayView.Items[index - 2].PID);
                            if (blocker != null && blocker.Skills != null && blocker.Skills.Contains(SkillConsts.MightyBlow))
                            {
                                // Grab last armour roll
                                try { 
                                ReplayRollAction armourRoll = _replayView.Rolls[_replayView.Items[index - 1].I];
                                if(armourRoll.Roll.Sum() > armourRoll.Req) // MB not used yet
                                    requiredRoll--;
                                }
                                catch
                                {
                                    Console.WriteLine("No armor roll before injury error");
                                }
                            }
                        }
                        // Chance is the same as for armor break (2D6 towards a req+)
                        double chance = 100 - GetChanceOfArmourBreak(requiredRoll);
                        AddRoll((ReplayItemType)item.AID, chance, (roll.Roll.Sum() < requiredRoll));
                    }
                    break;

                case ReplayItemType.Armor:
                    if (_luckType == LuckCalculatorType.All || _luckType == LuckCalculatorType.Bashing)
                    {
                        ReplayRollAction roll = _replayView.Rolls[item.I];
                        int requiredRoll = roll.Req;

                        // Try to find the blocker...
                        if (index > 0 && _replayView.Items[index - 1].AID == (int)ReplayItemType.Block &&
                            _replayView.Blocks[_replayView.Items[index - 1].I].TID == player.PlayerId)
                        {
                            PlayerStats blocker = otherTeam.FindUniquePlayer(_replayView.Items[index - 1].PID);
                            if (blocker != null && blocker.Skills != null && blocker.Skills.Contains(SkillConsts.MightyBlow))
                            {
                                // mighty blow doesn't seem to be added to required roll, so we add it here
                                requiredRoll--;
                            }
                        }

                        double chance = 100-GetChanceOfArmourBreak(requiredRoll);
                        AddRoll((ReplayItemType)item.AID, chance, (roll.Roll.Sum() < requiredRoll));
                        
                    }
                    break;
                case ReplayItemType.Block:
                    if (_luckType == LuckCalculatorType.All || _luckType == LuckCalculatorType.Bashing)
                    {
                        ReplayBlockAction roll = _replayView.Blocks[item.I];
                        PlayerStats targetPlayer = otherTeam.FindUniquePlayer(roll.TID);
                        double chance = GetBlockChance(roll.Roll.Count, player.Skills, targetPlayer!=null?targetPlayer.Skills:null);

                        // Did the block succeed ?
                        bool blockSucceeded = false;
                        bool blockFailed = false;
                        for (int i = index+1; i < _replayView.Items.Count; i++)
                        {
                            if (_replayView.Items[i].AID == (int)ReplayItemType.Block) // Someone blocks. Most likely a reroll, so this was a failure
                            {
                                break;
                            }
                            if (_replayView.Items[i].AID == (int)ReplayItemType.NoRoll) // Someone moves. not allowed
                                break;

                            if (_replayView.Items[i].AID == (int)ReplayItemType.Armor && _replayView.Items[i].PID != player.PlayerId)
                            {
                                blockSucceeded = true;
                                break;
                            }
  
                            // First item for another player, so it's either correct or this is not a successful break
                            if (_replayView.Items[i].PID != item.PID)
                            {
                                break;
                            }


                        }
                        AddRoll((ReplayItemType)item.AID, chance, blockSucceeded);

                        bool haveBlockOrWrestle = false;
                        if(player.Skills!= null)
                        { 
                            foreach(var sk in player.Skills)
                            {
                                if (sk == SkillConsts.Block || sk == SkillConsts.Wrestle || sk == SkillConsts.Juggernaut)
                                    haveBlockOrWrestle = true;
                            }
                        }
                        if (roll.Dice == (int)(BlockDice.BothDown) && haveBlockOrWrestle == false)
                            blockFailed = true;
                        if (roll.Dice == (int)(BlockDice.Skull))
                            blockFailed = true;

                        if (_luckType == LuckCalculatorType.Bashing)
                        {
                            if (blockFailed)
                                blocksBad++;
                            else if (blockSucceeded)
                                blocksGood++; // caused armor roll
                            else
                                blocksNeutral++;
                        }
                    }
                    break;

                case ReplayItemType.Dodge:
                    if (_luckType == LuckCalculatorType.All || _luckType == LuckCalculatorType.Movement)
                    {
                        ReplayRollAction roll = _replayView.Rolls[item.I];
                        double chance = GetRollChance(roll.Req);
                        AddRoll((ReplayItemType)item.AID, chance, IsRollSuccessful(roll.Roll.Sum(), roll.Req));
                    }
                    break;
                case ReplayItemType.Leap:
                    if (_luckType == LuckCalculatorType.All || _luckType == LuckCalculatorType.Movement)
                    {
                        if (item.I >= 0 && item.I < _replayView.Rolls.Count)
                        {
                            ReplayRollAction roll = _replayView.Rolls[item.I];
                            double chance = GetRollChance(roll.Req);
                            AddRoll((ReplayItemType)item.AID, chance, IsRollSuccessful(roll.Roll.Sum(), roll.Req));
                        }
                        else Console.WriteLine("LEAP roll not found");

                    }
                    break;
                case ReplayItemType.GFI:
                    if (_luckType == LuckCalculatorType.All || _luckType == LuckCalculatorType.Movement)
                    {
                        ReplayRollAction roll = _replayView.Rolls[item.I];
                        double chance = GetRollChance(roll.Req);
                        AddRoll((ReplayItemType)item.AID, chance, IsRollSuccessful(roll.Roll.Sum(), roll.Req));
                        
                    }
                    break;
                case ReplayItemType.PickUp:
                    if (_luckType == LuckCalculatorType.All || _luckType == LuckCalculatorType.Passing)
                    {
                        ReplayRollAction roll = _replayView.Rolls[item.I];
                        double chance = GetRollChance(roll.Req);
                        AddRoll((ReplayItemType)item.AID, chance, IsRollSuccessful(roll.Roll.Sum(), roll.Req));
                       
                    }
                    break;
                case ReplayItemType.Pass:
                case ReplayItemType.Catch:
                    if (_luckType == LuckCalculatorType.All || _luckType == LuckCalculatorType.Passing)
                    {
                        ReplayRollAction roll = _replayView.Rolls[item.I];
                        double chance = GetRollChance(roll.Req);
                        AddRoll((ReplayItemType)item.AID, chance, IsRollSuccessful(roll.Roll.Sum(), roll.Req));
                        
                    }
                    break;
                    // Not sure about the ID's of these
                case ReplayItemType.WildAnimal:
                case ReplayItemType.BoneHead:
                case ReplayItemType.BloodLust:
                case ReplayItemType.Hungry:
                case ReplayItemType.ReallyStupid:
                    if (_luckType == LuckCalculatorType.All)
                    {
                        try
                        {
                            if (item.I >= 0 && item.I < _replayView.Rolls.Count)
                            {
                                ReplayRollAction roll = _replayView.Rolls[item.I];
                                double chance = GetRollChance(roll.Req);
                                AddRoll((ReplayItemType)item.AID, chance, IsRollSuccessful(roll.Roll.Sum(), roll.Req));
                                if(IsRollSuccessful(roll.Roll.Sum(), roll.Req)==false)
                                {
                                    ownTeam.Boneheads++;
                                    player.Boneheads++;
                                }
                            }
                            else Console.WriteLine("WildAnimal ROLL NOT FOUND");
                        }
                        catch 
                        { 
                            
                            Console.WriteLine("ERROR: Roll not handled: "+item.AID); 
                        }
                    }
                    break;
                
            }
        }


        public void StoreResults(GameStats game, TeamStats stats, TeamStats otherTeam)
        {
            double totExpected = 0;
            double totSuccessful = 0;

            if (_luckType == LuckCalculatorType.Bashing)
            {
                stats.BlocksFailed += blocksBad;
                stats.BlocksNeutral += blocksNeutral;
                stats.BlocksGood += blocksGood;
            }

            foreach (RollStats rstats in _luckMap)
            {

                double expected = rstats.Rolls * rstats.Chance / 100.0;
                double successfull = rstats.OK;
                totExpected += expected;
                totSuccessful += successfull;

                if (_luckType == LuckCalculatorType.All)
                {
                    if (stats.Rolls == null)
                        stats.Rolls = new List<RollStats>();
                    RollStats tgt = null;
                    foreach (RollStats rs2 in stats.Rolls)
                    {
                        if (rs2.Action == rstats.Action && rs2.Chance == rstats.Chance)
                        {
                            tgt = rs2;
                            break;
                        }
                    }
                    if (tgt == null)
                    {
                        tgt = new RollStats((ReplayItemType)Enum.Parse(typeof(ReplayItemType), rstats.Action), rstats.Chance);
                        stats.Rolls.Add(tgt);
                    }
                    tgt.Rolls += rstats.Rolls;
                    tgt.OK += rstats.OK;
                }
            }
            
            
            double luck = 100;
            if (totExpected > 0)
                luck = totSuccessful * 100.0 / totExpected;
            switch (_luckType)
            {
                case LuckCalculatorType.All:
                    stats.Luck = (int)luck;
                    break;
                case LuckCalculatorType.Bashing:
                    stats.BlockLuck = (int)luck;
                    break;
                case LuckCalculatorType.Movement:
                    stats.MoveLuck = (int)luck;
                    break;
                case LuckCalculatorType.Passing:
                    stats.PassLuck = (int)luck;
                    break;
            }
        }
    }


    
   
}
