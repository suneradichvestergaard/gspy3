﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BB2StatsLib.Views.CalculatedStats
{
    public class RiskPath
    {
        public double SuccessChance
        {
            get
            {
                double success = 1;
                foreach (RiskItem item in SuccessChain)
                    success *= item.SuccessChance;
                return success;
            }
        }
   
        public List<RiskItem> SuccessChain = new List<RiskItem>();

        public void AddPath(RiskPath path)
        {
            foreach (RiskItem item in path.SuccessChain)
                SuccessChain.Add(item);
        }

        public void AddSuccessRoll(double successChance, bool isRerollUsed)
        {
            RiskItem newItem = new RiskItem();
            newItem.IsSuccess = true;
            newItem.SuccessChance = successChance;
            newItem.IsRerollUsed = isRerollUsed;
            SuccessChain.Add(newItem);
        }
        public void AddFailureRoll(double successChance, bool isRerollUsed)
        {
            RiskItem newItem = new RiskItem();
            newItem.IsSuccess = false;
            newItem.SuccessChance = successChance;
            newItem.IsRerollUsed = isRerollUsed;
            SuccessChain.Add(newItem);
        }
    }
}
