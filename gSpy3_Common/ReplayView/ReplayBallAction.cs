﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BB2StatsLib.Views.Replay
{
    [Serializable]
    public class ReplayBallAction : ReplayActionBase
    {
        public ReplayActionPosition Pos { get; set; }
        public bool IsBallHeld { get; set; }
        public int PID { get; set; }
    }
}
