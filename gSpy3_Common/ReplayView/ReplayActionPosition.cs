﻿using BB2StatsLib.BB2Replay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BB2StatsLib.Views.Replay
{
    [Serializable]
    public class ReplayActionPosition
    {
        public int X { get; set; }
        public int Y { get; set; }

        public ReplayActionPosition()
        {
        }
        public ReplayActionPosition(Cell cell)
        {
            if (cell != null)
            {
                X = cell.x;
                Y = cell.y;
            }
            else
            {
                X = -1;
                Y = -1;
            }

        }
        public ReplayActionPosition(Location cell)
        {
            if (cell != null)
            {
                X = cell.x;
                Y = cell.y;
            }
            else
            {
                X = -1;
                Y = -1;
            }
        }
        public override string ToString()
        {
            return X + "," + Y;
        }

    }
}
