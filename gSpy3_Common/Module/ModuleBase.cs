﻿using System;
using System.Collections.Generic;
using System.Text;
using gSpy3_Common.Database;
using gSpy3_Common.Helpers;

namespace gSpy3_Common.Module
{
    public class ModuleBase
    {
        public enum ModuleEnum { gSpy3_API = 1, gSpy3_LegacyCollector, gSpy3_BB2MatchCollector, gSpy3_BB2MatchParser, gSpy3_DiscordBot, gSpy3_Maintenance }

        public ModuleEnum ID { get; set; }
        public string ModuleName { get; set; }
        Database.DatabaseManager dbManager;

        public ModuleBase(ModuleEnum moduleId, Database.DatabaseManager dbm)
        {
            ID = moduleId;
            dbManager = dbm;
            ModuleName = moduleId.ToString();

            ResetUnfinishedWork();
        }

        public void ResetUnfinishedWork()
        {
            // Reset unfinished work
            var res = dbManager.BackendDB.Query("update work set started=null where not started is null and finished is null and idmodule=@id",
                new Dictionary<string, string>()
                {
                            {"@id", ((int)this.ID).ToString() },
                });
        }

        public int RemoveOldWork()
        {
            // Remove any old work
            DateTime oldDate = DateTime.Now - new TimeSpan(48, 0, 0);
            var res = dbManager.BackendDB.Query("delete from work where idmodule=@id and finished < @old_date",
                new Dictionary<string, string>()
                {
                            {"@id", ((int)this.ID).ToString() },
                            {"@old_date", oldDate.ToDatabaseUniversalString() },
                });
            return res.Affected;
        }

        public T GetState<T>(string name, Transaction transaction=null) where T: class
        {
            try
            {
                var res =  dbManager.BackendDB.Query("select value from modulestates where idmodule=@idmodule and name=BINARY @name", new Dictionary<string, string>()
            {
                {"@idmodule", ""+(int)ID },
                {"@name", name }
            }, transaction);
                if (res.Rows.Count == 0) return null;
                return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(res.Rows[0][0]);
            }
            catch(Exception ex)
            {
                return null;
            }
        }
        public void SetState<T>(string name,T value, Transaction transaction = null) 
        {
            dbManager.BackendDB.Query("replace into modulestates (value,idmodule,name) values (@value,@idmodule,BINARY @name)", new Dictionary<string, string>()
            {
                {"@idmodule", ""+(int)ID },
                {"@name", name },
                {"@value", Newtonsoft.Json.JsonConvert.SerializeObject(value)},
            }, transaction);
        }
        protected virtual int GetMaxFuturePlanning()
        {
            return 3;
        }
        protected virtual int GetNumWorkNeeded()
        {
            var res = dbManager.BackendDB.Query("select count(*) from work where idmodule=@id and started is NULL",
                new Dictionary<string, string>()
                {
                            {"@id", ((int)this.ID).ToString() },
                });
            int maxFuturePlanning = GetMaxFuturePlanning();
            if (res.Rows.Count == 0)
            {
                return maxFuturePlanning;
            }
            int numNeeded = maxFuturePlanning - int.Parse(res.Rows[0][0]);
            if(numNeeded < 0)
            {
                numNeeded = 0;
            }
            return numNeeded;   
        }
        public DateTime? GetLastFinishedCommand(string command)
        {
            var res = dbManager.BackendDB.Query("select finished from work where idmodule=@id and finished is not NULL and command=@command order by finished desc limit 1",
                new Dictionary<string, string>()
                {
                            {"@id", ((int)this.ID).ToString() },
                            {"@command", command },
                });
            if (res.Rows.Count == 0) return null;
            return DateTime.Parse(res.Rows[0][0] + "Z");
        }
        public DateTime? GetLastAddedCommand(string command)
        {
            var res = dbManager.BackendDB.Query("select added from work where idmodule=@id and added is not NULL and finished is null and started is null and command=@command order by added desc limit 1",
                new Dictionary<string, string>()
                {
                            {"@id", ((int)this.ID).ToString() },
                            {"@command", command },
                });
            if (res.Rows.Count == 0) return null;
            return DateTime.Parse(res.Rows[0][0] + "Z");
        }
        public void AddWork(string command, string data, int weight)
        {
            var res = dbManager.BackendDB.Query("insert into work (idmodule,command, data, weight, added) values(@id, @command,@data, @weight, @added)",
               new Dictionary<string, string>()
               {
                            {"@id", ((int)this.ID).ToString() },
                            {"@data", data },
                            {"@weight", ((int)weight).ToString() },
                            {"@added", DateTime.Now.ToDatabaseUniversalString() },
                            {"@command", command },
               });
        }

        public bool GetNextWork(out int idwork, out string command, out string data)
        {
            // Grab next work
            var res = dbManager.BackendDB.Query("select idwork, command,data from work where idmodule=@id and started is NULL limit 1",
               new Dictionary<string, string>()
               {
                            {"@id", ((int)this.ID).ToString() }
               });
            if (res.Rows.Count == 0) { command = null; data = null; idwork = 0;  return false; }
            idwork = int.Parse(res.Rows[0][res.Cols["idwork"]]);
            command = (res.Rows[0][res.Cols["command"]]);
            data = (res.Rows[0][res.Cols["data"]]);

            // Mark work as started
            dbManager.BackendDB.Query("update work set started=@started where idwork=@idwork",
               new Dictionary<string, string>()
               {
                            {"@idwork", idwork.ToString() },
                             {"@started", DateTime.Now.ToDatabaseUniversalString() },
               });

            // Give all work extra weight due to age
            dbManager.BackendDB.Query("update work set weight=weight+1",null);



            return true;
        }


        public void FinishWork(int idwork, string result)
        {
            dbManager.BackendDB.Query("update work set finished=@finished, result=@result, success=1 where idwork=@idwork",
               new Dictionary<string, string>()
               {
                            {"@idwork", idwork.ToString() },
                            {"@finished",  DateTime.Now.ToDatabaseUniversalString() },
                            {"@result", result },
               });
        }
        public void FailWork(int idwork, string error)
        {
            if (error.Length > 255) error = error.Substring(0, 255);
            dbManager.BackendDB.Query("update work set finished=@finished, result=@result, success=0 where idwork=@idwork",
               new Dictionary<string, string>()
               {
                            {"@idwork", idwork.ToString() },
                            {"@finished",  DateTime.Now.ToDatabaseUniversalString() },
                             {"@result", error },
               });
        }
        /// <summary>
        /// Perform heartbeat tick
        /// </summary>
        public void Tick()
        {
            // Send heartbeat
            dbManager.BackendDB.Query("replace into modules set idmodule=@id, module_name=@name, heartbeat_datetime=@time",
                new Dictionary<string, string>()
                    {
                        { "@id", ((int)ID).ToString() },
                        { "@name", ModuleName },
                        { "@time", DateTime.Now.ToDatabaseUniversalString()  } 
                    });
            // Heartbeat to keep connection alive
            dbManager.StatsDB.Query("select count(*) from competitions");

        }
    }
}
