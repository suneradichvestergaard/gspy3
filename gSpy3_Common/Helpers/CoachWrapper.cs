﻿using gSpy3_Common.Database.Format;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Helpers
{
    public class CoachWrapper
    {
        public OriginID idorigin { get; set; }
        public string coach_name { get; set; }
        public int idcoach { get; set; }
        public int idcompetition { get; set; }
        public CoachWrapper(int aIdCoach, string aName, OriginID aOriginID)
        {
            idcoach = aIdCoach;
            coach_name = aName;
            idorigin = aOriginID;
        }

        public override string ToString()
        {
            OriginID org = (OriginID)idorigin;
            string sOrg = org != OriginID.BB2_pc ? ("@" + OriginConversion.ToLegacyGoblinSpy(org)) : "";
            return coach_name + sOrg + "#" + idcoach + "";
        }
    }
}
