﻿using gSpy3_Common.Database.Format;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Helpers
{
    public class RaceParser
    {
        public static int? Parse(string str)
        {
            switch (str.ToLower())
            {
                case "delf": return (int)RaceEnum.DarkElf;
                case "chorf": return (int)RaceEnum.ChaosDwarf;
                case "dorf": return (int)RaceEnum.Dwarf;
            }

            foreach (var value in Enum.GetNames(typeof(RaceEnum)))
            {
                if (value.ToLower().StartsWith(str.ToLower()))
                {
                    return (int)(RaceEnum)Enum.Parse(typeof(RaceEnum), value);
                }
            }
            return null;
        }

    }
}
