﻿using gSpy3_Common.Database.Format;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Helpers
{
    public class TeamWrapper
    {
        public OriginID idorigin { get; set; }
        public string team_name { get; set; }
        public int idteam { get; set; }
        public int idcompetition { get; set; }
        public TeamWrapper(int aIdTeam, string aName, OriginID aOriginID)
        {
            idteam = aIdTeam;
            team_name = aName;
            idorigin = aOriginID;
        }

        public override string ToString()
        {
            OriginID org = (OriginID)idorigin;
            string sOrg = org != OriginID.BB2_pc ? ("@" + OriginConversion.ToLegacyGoblinSpy(org)) : "";
            return team_name + sOrg + "#" + idteam + "";
        }
    }
}
