﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gSpy3_Common.Helpers
{
   static class FormatFromDictionaryClass
    {
        static string FormatFromDictionary(this string formatString, Dictionary<string, string> valueDict)
        {
            int i = 0;
            StringBuilder newFormatString = new StringBuilder(formatString);
            Dictionary<string, int> keyToInt = new Dictionary<string, int>();
            foreach (var tuple in valueDict)
            {
                newFormatString = newFormatString.Replace("{" + tuple.Key + "}", "{" + i.ToString() + "}");
                keyToInt.Add(tuple.Key, i);
                i++;
            }
            return String.Format(newFormatString.ToString(), valueDict.OrderBy(x => keyToInt[x.Key]).Select(x => x.Value).ToArray());
        }
    }
}
