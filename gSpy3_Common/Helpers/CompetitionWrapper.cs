﻿using gSpy3_Common.Database.Format;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common
{
    public class CompetitionWrapper
    {
        public int idcompetition { get; set; }
        public string name { get; set; }
        public OriginID idorigin { get; set; }
        public CompetitionWrapper() { }
        public CompetitionWrapper(int aIdCompetition, string aName, OriginID aOriginID)
        {
            idcompetition = aIdCompetition;
            name = aName;
            idorigin = aOriginID;
        }

        public override string ToString()
        {
            OriginID org = (OriginID)idorigin;
            string sOrg = org != OriginID.BB2_pc ? ("@" + OriginConversion.ToLegacyGoblinSpy(org)) : "";
            return name + sOrg + "#" + idcompetition + "";
        }
    }
}
