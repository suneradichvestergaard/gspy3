﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace gSpy3_Common
{
    public class Log
    {
        string _section = "";

        static object _lock = new object();
        static StreamWriter _writer = null;
        static string _filename = null;


        public Log()
        {

        }

        static string GetFilename()
        {
            if (_filename == null)
            {
                _filename = Path.Combine(Path.GetTempPath(), Assembly.GetEntryAssembly().GetName().Name, "Logs", DateTime.Now.ToString("yyyy-MM-dd_HH-mm_") + Assembly.GetExecutingAssembly().GetName().Name + ".txt");
            }
            return _filename;
        }

        void Write(string txt, bool error = false)
        {
            new Task(() =>
            {

                try
                {
                    lock (_lock)
                    {
                        if (_writer == null)
                        {
                            var filename = GetFilename();
                            var folder = Path.GetDirectoryName(filename);
                            if (Directory.Exists(folder) == false)
                            {
                                Directory.CreateDirectory(folder);
                            }
                            _writer = File.CreateText(GetFilename());

                            if (_writer == null)
                            {
                                return;
                            }
                        }

                        _writer.WriteLine(txt);
                        _writer.Flush();
                        Console.WriteLine(txt);
                        System.Diagnostics.Debug.WriteLine(txt);
                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.ToString());
                }
            }).Start();
        }

        public Log(string section)
        {
            _section = section;
        }

        protected void Write(string type, string message, object data = null)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("{ \"time\":");
            sb.Append(JsonConvert.SerializeObject(DateTime.Now.ToUniversalTime()));
            sb.Append(",\"type\":");
            sb.Append("\"" + type + "\"");
            sb.Append(",\"section\":\"");
            sb.Append(_section);
            sb.Append("\",\"msg\":");
            sb.Append(JsonConvert.SerializeObject(message));
            if (data != null)
            {
                sb.Append(",\"data\":");
                sb.Append(JsonConvert.SerializeObject(data));
            }
            sb.Append("},");
            Write(sb.ToString());
        }

        public void Error(string message, object data = null)
        {
            Write("ERR", message, data);
        }

        public void Exception(string message, Exception e)
        {
            Write("ERR", message, e);
        }

        public void Info(string message, object data = null)
        {
            Write("INF", message, data);
        }

        public void Trace(string message, object data = null)
        {
            Write("TRC", message, data);
        }

    }
}
