﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace gSpy3_Common
{
    [Serializable]
    public class PrivateSettings
    {
        public string StatsDatabaseConnectionString { get; set; }
        public string BackendDatabaseConnectionString { get; set; }
        public string BB2APIKey { get; set; }
        public string BaseFileFolder { get; set; }
        public string DiscordToken { get; set; }

        const string defaultName = "private-settings.json";
        public static PrivateSettings Load(string path= null)
        {
            if (path == null) path = defaultName;
            if (File.Exists(path) == false) { path = Path.Combine("..\\", path); }
            if (File.Exists(path) == false) { path = Path.Combine("..\\", path); }
            if (File.Exists(path) == false) { path = Path.Combine("..\\", path); }
            if (File.Exists(path) == false) { path = Path.Combine("..\\", path); }
            string content = File.ReadAllText(path);
            return JsonConvert.DeserializeObject<PrivateSettings>(content);
        }
    }
}
