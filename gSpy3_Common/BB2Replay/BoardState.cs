//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;


namespace BB2StatsLib.BB2Replay
{
	[Serializable]
	public class BoardState
	{
		public BallData Ball { get; set; }
		public BoardStateStatistics Statistics{ get; set; }
		public List<TeamState> ListTeams { get; set; }
		public BoardState ()
		{
		}
	}
}

