//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;


namespace BB2StatsLib.BB2Replay
{
	[Serializable]
	public class RulesEventSetUpConfiguration
	{
		public SetupState SetupState{ get; set; }
		public List<PlayerPosition> ListPlayersPositions { get; set; }

		public RulesEventSetUpConfiguration ()
		{
		}
	}
}

