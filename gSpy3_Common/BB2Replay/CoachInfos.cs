//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using System;
namespace BB2StatsLib.BB2Replay
{
	[Serializable]
	public class CoachInfos
	{
		public string Login { get; set; }
		public string UserId { get; set; }
		public int? Slot { get; set; }

		public CoachInfos ()
		{
		}
	}
}

