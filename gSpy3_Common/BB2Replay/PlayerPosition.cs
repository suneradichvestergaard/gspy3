//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
using System;
namespace BB2StatsLib.BB2Replay
{
	[Serializable]
	public class PlayerPosition
	{
		public int PlayerId{get;set;}
		public Location Position{get;set;}

		public PlayerPosition ()
		{
		}
	}
}

