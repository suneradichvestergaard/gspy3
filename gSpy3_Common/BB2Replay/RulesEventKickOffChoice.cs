//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
using System;
namespace BB2StatsLib.BB2Replay
{
	[Serializable]
	public class RulesEventKickOffChoice
	{
		public int ChosingTeam{ get; set; }
		public int KickOffTeam{get;set;}


		public RulesEventKickOffChoice ()
		{
		}
	}
}

