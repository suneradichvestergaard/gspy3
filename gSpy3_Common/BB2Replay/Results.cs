//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Xml.Serialization;
namespace BB2StatsLib.BB2Replay
{
	[Serializable]
	public class Results
	{
        [XmlElement]
		public List<BoardActionResult> BoardActionResult{get;set;}


		public Results ()
		{
		}
	}
}

