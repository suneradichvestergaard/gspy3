//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;


namespace BB2StatsLib.BB2Replay
{
	[Serializable]
	public class RulesEventInducementsInfos
	{
		public List<TeamInducements> TeamInducements{ get; set; }
		public int InducementsCash { get; set; }


		public RulesEventInducementsInfos ()
		{
		}
	}
}

