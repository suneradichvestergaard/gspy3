//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using System;
namespace BB2StatsLib.BB2Replay
{
	[Serializable]
	public class ActorMove
	{
		public Location Cell {get;set;}
		public Location Destination {get;set;}
		public ActorMove ()
		{
		}
	}
}

