﻿using gSpy3_Common.Database.AccessInterface;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database
{
    public class DatabaseManager : IDisposable
    {
        public DatabaseConnection StatsDB;
        public DatabaseConnection BackendDB;

         

        public DatabaseManager(PrivateSettings settings)
        {
            StatsDB = new DatabaseConnectionMySQL(settings.StatsDatabaseConnectionString);
            BackendDB = new DatabaseConnectionMySQL(settings.BackendDatabaseConnectionString);

            LeagueManager.DBManager = this;
            CoachManager.DBManager = this;
            UniqueStringManager.DBManager = this;
            TeamManager.DBManager = this;
            ScheduleManager.DBManager = this;
            MatchManager.DBManager = this;
            ResourceManager.DBManager = this;
            CollectionManager.DBManager = this;
            GroupedStatsManager.DBManager = this;
        }

        public void Dispose()
        {
            if (StatsDB != null) { StatsDB.Dispose(); StatsDB = null; }
            if (BackendDB != null) { BackendDB.Dispose(); BackendDB = null; }
        }
    }
}
