﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Text.RegularExpressions;
using Ubiety.Dns.Core.Records;

namespace gSpy3_Common.Database
{
    public class FilteredQueryManager
    {

        static FilteredQueryManager instance = null;
        public static FilteredQueryManager Get()
        {
            if(instance==null) { instance = new FilteredQueryManager(); }
            return instance;
        }


        public Dictionary<string, FilteredQueryDefinition> Definition { get; set; }

        public FilteredQueryManager()
        {
            Definition = new Dictionary<string, FilteredQueryDefinition>();
            Definition["teamPos"] = new FilteredQueryDefinition("select greatest(0, (position - 10)) from standingsview", null, null, 100, new FilteredQueryRequest() { ordercol = "position", order = "asc" });
			Definition["leagues"] = new FilteredQueryDefinition("select * from leaguesview", null, null, 10000, new FilteredQueryRequest() { ordercol = "collecting desc,idorigin asc,league_name", order = "asc" });
			Definition["league"] = new FilteredQueryDefinition("select * from leagues", null, null, 1, null);
			Definition["leagueComps"] = new FilteredQueryDefinition("select * from competitions", null, null, 500, new FilteredQueryRequest() { ordercol = "active desc,last_game", order = "desc" });
            Definition["comp"] = new FilteredQueryDefinition("select * from competitions as c inner join leagues as l on c.idleague = l.idleague", new Dictionary<string, string>() { { "idleague", "c.idleague" },{"idorigin","c.idorigin" } }, null, 1000, new FilteredQueryRequest() { ordercol = "last_game", order = "desc", limit = 30 });
            Definition["compActivityDates"] = new FilteredQueryDefinition("select count(*) as number, DATE(finished) as finished from matches {where} group by DATE(finished)", null, null, 1000, new FilteredQueryRequest() { ordercol = "finished", order = "asc", limit = 1000 });
            Definition["compActivityRaces"] = new FilteredQueryDefinition("select count(*) as number, idrace from standings as s inner join teams as t on t.idteam=s.idteam {where} group by idrace", null, null, 1000, new FilteredQueryRequest() { ordercol = "idrace", order = "asc", limit = 1000 });
            Definition["compActivityDays"] = new FilteredQueryDefinition("select number,day from (select count(*) as number, DAYOFWEEK(finished) as day from matches {where} group by DAYOFWEEK(finished)) as sub", null, null, 1000, new FilteredQueryRequest() { ordercol = "day", order = "asc", limit = 1000 });
            Definition["compActivityTime"] = new FilteredQueryDefinition("select number, time from (select count(*) as number, DATE_FORMAT(finished,'%H:00:00') as time from matches {where} group by HOUR(finished)) as sub", null, null, 1000, new FilteredQueryRequest() { ordercol = "time", order = "asc", limit = 1000 });
            Definition["compCoachTeams"] = new FilteredQueryDefinition("select *,CAST(FORMAT(100*(wins+draws/2)/(wins+draws+losses),4) as DECIMAL) as win_pct from (select * from standingsview {innerwhere}) as sub {where}", null, null, 1000, new FilteredQueryRequest() { ordercol = "win_pct", order = "desc", limit = 50 });
            Definition["compCoachRaces"] = new FilteredQueryDefinition("select *,CAST(FORMAT(100*(wins+draws/2)/(wins+draws+losses),4) as DECIMAL) as win_pct from (select count(*) as num_teams,idcompetition,avg(ranking) as ranking, idteam,idrace,team_name,logo,twitch,youtube,idcoach,coach_name,sum(kills) as kills,sum(td) as td,sum(cas) as cas,sum(concedes) as concedes,sum(wins) as wins, sum(draws) as draws, sum(losses) as losses,sum(gp) as gp from standingsview {innerwhere} group by idcoach,idrace) as sub {where}", null, null, 1000, new FilteredQueryRequest() { ordercol = "win_pct", order = "desc", limit = 50 });
            //Definition["compCoaches"] = new FilteredQueryDefinition("select *,CAST(FORMAT(100*(wins+draws/2)/(wins+draws+losses),4) as DECIMAL) as win_pct from (select idcompetition,  avg(ranking) as ranking, idteam,idrace,team_name,logo,twitch,youtube,idcoach,coach_name,sum(kills) as kills,sum(td) as td,sum(cas) as cas,sum(concedes) as concedes,sum(wins) as wins, sum(draws) as draws, sum(losses) as losses,sum(gp) as gp,count(*) as num_teams from standingsview {innerwhere} group by idcoach) as sub {where}", null, null, 1000, new FilteredQueryRequest() { ordercol = "win_pct", order = "desc", limit = 50 });
            Definition["compResults"] = new FilteredQueryDefinition("select * from resultsview", null, null, 1000, new FilteredQueryRequest() { ordercol = "finished", order = "desc", limit = 30 });
            Definition["compStandings"] = new FilteredQueryDefinition("select * from standingsview where active=1", null, null, 1000, new FilteredQueryRequest() { ordercol = "sorting", order = "desc", limit = 30 });
            Definition["compStandingsGrouped"] = new FilteredQueryDefinition(
                "select * from (select sum(points) as points, sum(sorting) as sorting, sum(ranking) as ranking,"+
                "idcompetition, idteam,idrace,idcoach, team_name,coach_name,sum(gp) as gp, sum(wins) as wins, sum(draws) as draws, sum(losses) as losses, "+
                "sum(td) as td, sum(td_opp) as td_opp,sum(td_diff) as td_diff, sum(cas) as cas, sum(cas_opp) as cas_opp, sum(cas_diff) as cas_diff, sum(concedes) as concedes,youtube,logo,twitch,active, " +
                "max(team_value) as team_value, sum(kills) as kills, min(position) as position   from standingsview {innerwhere} and active=1 group by idteam) as sub   {where}", 
                
                null, null, 1000, new FilteredQueryRequest() { ordercol = "position", order = "asc", limit = 30 });

            Definition["compTeamStanding"] = new FilteredQueryDefinition("select * from standingsview", null, null, 1000, new FilteredQueryRequest() { ordercol = "position", order = "asc", limit = 30 });
            Definition["compSchedules"] = new FilteredQueryDefinition("select * from schedulesview", null, null, 1000, new FilteredQueryRequest() { ordercol = "schedule_origin_id", order = "asc" });
            Definition["compStatsTeam"] = new FilteredQueryDefinition("select log.logo,p.active,p.idplayertype,p.level,p.xp,pn.player_name,p.ma,p.st,p.ag,p.av,p.pa,sc.skills,s.*,t.team_name,t.idcoach,ch.*,s.idrace,t.idlogo,CAST(FORMAT(100*((s.wins)+(s.draws)/2)/((s.wins)+(s.draws)+(s.losses)),4) as DECIMAL) as win_pct from groupedteammatchstats as s left join players as p on p.idplayer=s.idplayer left join playernames as pn on pn.idplayername=p.idplayername left join skillcombos as sc on sc.idskillcombo=p.idskillcombo left join teams as t on  t.idteam=s.idteam  left join coaches as ch on ch.idcoach=s.idcoach left join logos as log on log.idlogo = t.idlogo ", null, null, 1000, new FilteredQueryRequest() { ordercol = "win_pct", order = "desc", limit = 50 });
            #region compStatsTeamAvg
            Definition["compStatsTeamAvg"] = new FilteredQueryDefinition("select "+
                "log.logo,s.idteam,p.active,p.idplayertype,p.level,p.xp,pn.player_name,p.ma,p.st,p.ag,p.av,p.pa,sc.skills," +
                "s.gp as gp,"+
                "(duration/gp) as duration," +
                    "(td/gp) as td," +
                    "(td_opp/gp) as td_opp," +
                    "(s.wins/gp) as wins," +
                    "(s.draws/gp) as draws," +
                    "(s.losses/gp) as losses," +
                    "(concedes/gp) as concedes," +
                    "(cash_spent/gp) as cash_spent," +
                    "(blocks_for/gp) as blocks_for," +
                    "(blocks_against/gp) as blocks_against," +
                    "(breaks_for/gp) as breaks_for," +
                    "(breaks_against/gp) as breaks_against," +
                    "(stuns_for/gp) as stuns_for," +
                    "(stuns_against/gp) as stuns_against," +
                    "(kos_for/gp) as kos_for," +
                    "(kos_against/gp) as kos_against," +
                    "(casualties_for/gp) as casualties_for," +
                    "(casualties_against/gp) as casualties_against," +
                    "(kills_for/gp) as kills_for," +
                    "(kills_against/gp) as kills_against," +
                     "(passes/gp) as passes," +
                     "(catches/gp) as catches," +
                     "(completions/gp) as completions," +
                    "(interceptions/gp) as interceptions," +
                    "(pass_meters/gp) as pass_meters," +
                    "(run_meters/gp) as run_meters," +
                     "(pushouts/gp) as pushouts," +
                     "(fouls/gp) as fould," +
                     "(expulsions/gp) as expulsions," +
                    "(turnovers/gp) as turnovers," +
                    "(dodges/gp) as dodges," +
                    "(pickups/gp) as pickups," +
                    "(rushes/gp) as rushes," +
                    "(sacks/gp) as sacks," +
                    "(boneheads/gp) as boneheads," +

                "t.team_name,t.idcoach,ch.*,s.idrace,t.idlogo,CAST(FORMAT(100*((s.wins)+(s.draws)/2)/((s.wins)+(s.draws)+(s.losses)),4) as DECIMAL) as win_pct from groupedteammatchstats as s  left join players as p on p.idplayer=s.idplayer left join playernames as pn on pn.idplayername=p.idplayername left join skillcombos as sc on sc.idskillcombo=p.idskillcombo left join teams as t on t.idteam=s.idteam left join coaches as ch on ch.idcoach=s.idcoach  left join logos as log on log.idlogo = t.idlogo ", null, null, 1000, new FilteredQueryRequest() { ordercol = "win_pct", order = "desc", limit = 50 });
            #endregion
            #region compTeamwStats


            Definition["compTeamwStats"] = new FilteredQueryDefinition("select * from (select sw.team_value,sw.youtube,sw.twitch,sw.logo,sw.position,sw.sorting,sw.ranking,sw.points,sw.idcompetition,sw.idcoach,sw.coach_name,sw.idteam,sw.team_name,sw.idrace," +
                    "{aggr}(m.duration) as duration," +
                    "{aggr}(tms.td) as td," +
                    "{aggr}(tms.td_opp) as td_opp," +
                    "{aggr}(win) as wins," +
                    "{aggr}(draw) as draws," +
                    "{aggr}(loss) as losses," +
                    "CAST(FORMAT(100*(sum(win)+sum(draw)/2)/(sum(win)+sum(draw)+sum(loss)),4) as DECIMAL) as win_pct," +
                   "sum(win+draw+loss) as gp," +
                    "{aggr}(conceded) as concedes," +
                    "{aggr}(cash_spent) as cash_spent," +
                    "{aggr}(blocks_for) as blocks_for," +
                    "{aggr}(blocks_against) as blocks_against," +
                    "{aggr}(breaks_for) as breaks_for," +
                    "{aggr}(breaks_against) as breaks_against," +
                    "{aggr}(stuns_for) as stuns_for," +
                    "{aggr}(stuns_against) as stuns_against," +
                    "{aggr}(kos_for) as kos_for," +
                    "{aggr}(kos_against) as kos_against," +
                    "{aggr}(casualties_for) as casualties_for," +
                    "{aggr}(casualties_against) as casualties_against," +
                    "{aggr}(kills_for) as kills_for," +
                    "{aggr}(kills_against) as kills_against," +
                     "{aggr}(passes) as passes," +
                     "{aggr}(catches) as catches," +
                     "{aggr}(completions) as completions," +
                    "{aggr}(interceptions) as interceptions," +
                    "{aggr}(pass_meters) as pass_meters," +
                    "{aggr}(run_meters) as run_meters," +
                     "{aggr}(pushouts) as pushouts," +
                     "{aggr}(fouls) as fould," +
                     "{aggr}(expulsions) as expulsions," +
                     "{aggr}(mvp) as mvp," +
                    "{aggr}(turnovers) as turnovers," +
                    "{aggr}(dodges) as dodges," +
                    "{aggr}(pickups) as pickups," +
                    "{aggr}(rushes) as rushes," +
                    "{aggr}(sacks) as sacks," +
                    "{aggr}(boneheads) as boneheads," +
                    "{aggr}(supporters) as supporters " +
                    " from standingsview as sw inner join teammatchstats as tms on tms.idteam=sw.idteam and tms.idcompetition=sw.idcompetition inner join matches as m on m.idmatch=tms.idmatch {innerwhere} group by {group}) as sub {where}", null, new Dictionary<string, string>()
            {
                {"idcompetition", "tms.idcompetition" },{"idteam", "sw.idteam" }
            }, 1000, new FilteredQueryRequest() { ordercol = "sorting", order = "desc", limit = 50, group="idteam",aggr="sum" },new string[] {"idplayer", "idteam","idcoach","idrace"});

            #endregion
            #region compTeamMatches
            Definition["compTeamMatches"] = new FilteredQueryDefinition("select * from (select  c.coach_name,ts2.conceded as conceded_opp,t.team_name as team_name, t.idrace as idrace,t2.idrace as idrace_away,ts2.team_value as team_value_away,logo1.logo as logo,logo2.logo as logo_away,t2.idteam as idteam_away,t2.team_name as team_name_away,c2.idcoach as idcoach_away,c2.coach_name as coach_name_away,ts.*,m.finished,m.started,m.duration from matches as m " +
                                                                            " inner join teams as t on t.idteam = m.idteam_home" +
                                                                            " inner join teams as t2 on t2.idteam = m.idteam_away" +
                                                                            " inner join coaches as c on c.idcoach = t.idcoach" +
                                                                            " inner join coaches as c2 on c2.idcoach = t2.idcoach" +
                                                                            " inner join teammatchstats as ts on ts.idmatch = m.idmatch and ts.home=1" +
                                                                            " inner join teammatchstats as ts2 on ts2.idmatch = m.idmatch and ts2.home=0" +
                                                                            " left join logos as logo1 on logo1.idlogo=t.idlogo" +
                                                                            " left join logos as logo2 on logo2.idlogo=t2.idlogo" +

                                                                            " {where} " +
                                                                            " union all" +
                                                                            " select    c.coach_name,ts2.conceded as conceded_opp,t.team_name as team_name, t.idrace as idrace,t2.idrace as idrace_away,ts2.team_value as team_value_away,logo1.logo as logo,logo2.logo as logo_away,t2.idteam as idteam_away,t2.team_name as team_name_away,c2.idcoach as idcoach_away,c2.coach_name as coach_name_away,ts.*,m.finished,m.started,m.duration from matches as m " +
                                                                            " inner join teams as t on t.idteam = m.idteam_away" +
                                                                            " inner join teams as t2 on t2.idteam = m.idteam_home" +
                                                                            " inner join coaches as c on c.idcoach = t.idcoach" +
                                                                            " inner join coaches as c2 on c2.idcoach = t2.idcoach" +
                                                                            " inner join teammatchstats as ts on ts.idmatch = m.idmatch and ts.home=0" +
                                                                            " inner join teammatchstats as ts2 on ts2.idmatch = m.idmatch and ts2.home=1" +
                                                                            " left join logos as logo1 on logo1.idlogo=t.idlogo" +
                                                                            " left join logos as logo2 on logo2.idlogo=t2.idlogo" +
                                                                            " {where}) as sub {order} {limit} " +
                                                                            "", 
                                                                            new Dictionary<string, string>()
                                                                            {
                                                                                {"idcompetition", "ts.idcompetition" },
                                                                                {"idteam", "t.idteam" },
                                                                               {"idcoach", "t.idcoach" },
                                                                               {"coach_name", "c.coach_name" },
                                                                                {"idrace_away", "t2.idrace" },
                                                                                {"coach_name_away", "c2.coach_name" },
                                                                                {"team_name_away", "t2.team_name" },
                                                                                {"team_name", "t.team_name" },
                                                                              {"idcoach_away", "t2.idcoach" },
                                                                                {"idteam_away", "t2.idteam" },
                                                                            }
                                                                            , null, 1000, new FilteredQueryRequest() { ordercol = "finished", order = "desc", limit = 50 });


            #endregion
            Definition["compMatches"] = Definition["compTeamMatches"];
            Definition["compTeamPlayers"] = new FilteredQueryDefinition("select * from (select active,p.idteam, p.idplayer,player_origin_id, p.idplayertype,pn.player_name,idleague,m.idcompetition, m.idorigin,p.ma,p.ag,p.av,p.st,skills,cb1.cas as cas_state,cb2.cas as cas_sustained,p.xp,{aggr}(ps.xp_gain) as xp_gain,p.level,p.number,{aggr}(td) as td,{aggr}(run_meters) as run_meters,{aggr}(pass_meters) as pass_meters,{aggr}(game_played) as gp,{aggr}(passes) as passes,{aggr}(catches) as catches,{aggr}(completions) as completions,{aggr}(pushouts) as pushouts,{aggr}(blocks_for) as blocks_for,{aggr}(breaks_for) as breaks_for,{aggr}(stuns_for) as stuns_for,{aggr}(kos_for) as kos_for,{aggr}(casualties_for) as casualties_for,{aggr}(kills_for) as kills_for, {aggr}(blocks_against) as blocks_against,{aggr}(breaks_against) as breaks_against, {aggr}(stuns_against) as stuns_against, {aggr}(kos_against) as kos_against, {aggr}(casualties_against) as casualties_against, {aggr}(kills_against) as kills_against, sum(turnovers) as turnovers, {aggr}(interceptions) as interceptions, {aggr}(expulsions) as expulsions from players as p inner join playermatchstats as ps on p.idplayer=ps.idplayer inner join matches as m on m.idmatch=ps.idmatch inner join playernames as pn on pn.idplayername=p.idplayername inner join skillcombos as sg on sg.idskillcombo = p.idskillcombo inner join casualtycombos as cb1 on cb1.idcasualtycombo=p.idcasstate  inner join casualtycombos as cb2 on cb2.idcasualtycombo=p.idcassustained {innerwhere} and active=1  group by p.idplayer) as sub  {where}  {order} {limit}",
                                                                             null
                                                                            ,
                                                                             new Dictionary<string, string>()
                                                                            {
                                                                                {"idcompetition", "m.idcompetition" },
                                                                                {"idorigin", "m.idorigin" },
                                                                               {"idplayer", "p.idplayer" },
                                                                                {"idplayertype", "p.idplayertype" },
                                                                                {"idteam", "p.idteam" },
                                                                                {"ma", "p.ma" },
                                                                                {"av", "p.av" },
                                                                                {"ag", "p.ag" },
                                                                                {"st", "p.st" },
                                                                                {"skills", "p.skills" },
                                                                                {"cas_state", "p.cas_state" },
                                                                                {"cas_sustained", "p.cas_sustained" },
                                                                            }
                                                                             , 1000, new FilteredQueryRequest() { ordercol = "number", order = "asc", limit = 50, aggr="sum" });
            #region compPlayers
            Definition["compPlayers"] = new FilteredQueryDefinition("select * from (select t.team_name, st.idcompetition,st.active,p.idteam, p.idplayer,player_origin_id, p.idplayertype,pn.player_name,idleague, st.idorigin,p.ma,p.ag,p.av,p.st,skills,cb1.cas as cas_state,cb2.cas as cas_sustained,p.xp,{aggr}(ps.xp_gain) as xp_gain,p.level,p.number,{aggr}(mvp) as mvp,{aggr}(run_meters) as run_meters,{aggr}(pass_meters) as pass_meters,{aggr}(ps.td) as td,sum(ps.game_played) as gp,{aggr}(ps.passes) as passes,{aggr}(ps.catches) as catches,{aggr}(ps.completions) as completions,{aggr}(ps.pushouts) as pushouts,{aggr}(ps.blocks_for) as blocks_for,{aggr}(ps.breaks_for) as breaks_for,{aggr}(ps.stuns_for) as stuns_for,{aggr}(ps.kos_for) as kos_for,{aggr}(ps.casualties_for) as casualties_for,{aggr}(ps.kills_for) as kills_for, {aggr}(blocks_against) as blocks_against,{aggr}(breaks_against) as breaks_against, {aggr}(ps.stuns_against) as stuns_against, {aggr}(ps.kos_against) as kos_against, {aggr}(ps.casualties_against) as casualties_against, {aggr}(ps.kills_against) as kills_against, {aggr}(ps.turnovers) as turnovers, {aggr}(ps.interceptions) as interceptions, {aggr}(ps.fouls) as fouls, {aggr}(ps.expulsions) as expulsions from players as p  inner join playermatchstats as ps on p.idplayer=ps.idplayer inner join playernames as pn on pn.idplayername=p.idplayername inner join skillcombos as sg on sg.idskillcombo = p.idskillcombo inner join casualtycombos as cb1 on cb1.idcasualtycombo=p.idcasstate  inner join casualtycombos as cb2 on cb2.idcasualtycombo=p.idcassustained inner join standings as st on st.idteam = p.idteam and st.idcompetition=ps.idcompetition inner join teams as t on t.idteam=st.idteam   {innerwhere}  group by p.idplayer) as sub  {where}  {order} {limit}",
                                                                                        null
                                                                                       ,
                                                                                        new Dictionary<string, string>()
                                                                                       {

                                                                                {"active", "p.active" },
                                                                                {"idcompetition", "st.idcompetition" },
                                                                                {"idplayer", "p.idplayer" },
                                                                                {"idplayertype", "p.idplayertype" },
                                                                                {"idteam", "p.idteam" },
                                                                                {"ma", "p.ma" },
                                                                                {"av", "p.av" },
                                                                                {"ag", "p.ag" },
                                                                                {"st", "p.st" },
                                                                                {"skills", "p.skills" },
                                                                                {"cas_state", "p.cas_state" },
                                                                                {"cas_sustained", "p.cas_sustained" },
                                                                                       }
                                                                                        , 1000, new FilteredQueryRequest() { ordercol = "number", order = "asc", limit = 50 });

            #endregion
            Definition["compTeamGraveyard"] = new FilteredQueryDefinition("select sub.*,h.idmatch,m2.idcompetition,m2.idcompetition as idtargetcompetition,h.idtarget,h.idtargetname,h.idhighlighttype as killed_by,h.idhighlighttype,h.turn,h.subturn,h.idcause,h.weight,pns.player_name as target_name  from (select active,max(m.finished) as last_game,p.idteam, p.idplayer,player_origin_id, p.idplayertype,pn.player_name,idleague, m.idorigin,p.ma,p.ag,p.av,p.st,skills,cb1.cas as cas_state,cb2.cas as cas_sustained,p.xp,sum(ps.xp_gain) as xp_gain,sum(ps.run_meters) as run_meters, sum(ps.pass_meters) as pass_meters,p.level,p.number,sum(td) as td,sum(game_played) as gp,sum(passes) as passes,sum(catches) as catches,sum(completions) as completions,sum(pushouts) as pushouts,sum(blocks_for) as blocks_for,sum(breaks_for) as breaks_for,sum(stuns_for) as stuns_for,sum(kos_for) as kos_for,sum(casualties_for) as casualties_for,sum(kills_for) as kills_for, sum(blocks_against) as blocks_against,sum(breaks_against) as breaks_against, sum(stuns_against) as stuns_against, sum(kos_against) as kos_against, sum(casualties_against) as casualties_against, sum(kills_against) as kills_against, sum(turnovers) as turnovers, sum(interceptions) as interceptions, sum(expulsions) as expulsions from players as p inner join playermatchstats as ps on p.idplayer=ps.idplayer  inner join matches as m on m.idmatch=ps.idmatch inner join playernames as pn on pn.idplayername=p.idplayername inner join skillcombos as sg on sg.idskillcombo = p.idskillcombo inner join casualtycombos as cb1 on cb1.idcasualtycombo=p.idcasstate  inner join casualtycombos as cb2 on cb2.idcasualtycombo=p.idcassustained {innerwhere} and active=0 and cb1.cas like \"%Dead%\" group by p.idplayer) as sub left join highlights as h on h.idplayer=sub.idplayer and h.idhighlighttype<=2 left join playernames as pns on pns.idplayername=h.idtargetname left join matches as m2 on m2.idmatch=h.idmatch  {where}  {order} {limit}",
                                                                       new Dictionary<string, string>()
                                                                           {    {"idcompetition", "m2.idcompetition" },
                                                                       
                                                                           }
                                                                           ,
                                                                            new Dictionary<string, string>()
                                                                           {    {"idcompetition", "m.idcompetition" },
                                                                                {"idplayer", "p.idplayer" },
                                                                                {"idplayertype", "p.idplayertype" },
                                                                                {"idteam", "p.idteam" },
                                                                                {"ma", "p.ma" },
                                                                                {"av", "p.av" },
                                                                                {"ag", "p.ag" },
                                                                                {"st", "p.st" },
                                                                                {"skills", "p.skills" },
                                                                                {"cas_state", "p.cas_state" },
                                                                                {"cas_sustained", "p.cas_sustained" },
                                                                           }
                                                                            , 1000, new FilteredQueryRequest() { ordercol = "last_game", order = "desc", limit = 50 });

            Definition["compTeamSchedule"] = new FilteredQueryDefinition("select * from teamschedulesview", null, null, 1000, new FilteredQueryRequest() { ordercol = "round", order = "asc", limit = 50 });
            Definition["team"] = new FilteredQueryDefinition("select * from teams", null, null, 5, null);
            Definition["coach"] = new FilteredQueryDefinition("select * from coaches", null, null, 5, null);
            Definition["coachCompetitions"] = new FilteredQueryDefinition("select * from (select s.idcompetition, ch.coach_name,ch.idcoach,c.last_game, c.competition_name,l.idleague, l.league_name, CAST(FORMAT(100*(sum(wins)+sum(draws)/2)/(sum(wins)+sum(draws)+sum(losses)),4) as DECIMAL) as win_pct,sum(wins) as wins, sum(draws) as draws, sum(losses) as losses,sum(concedes) as concedes  from teams as t inner join standings as s on s.idteam = t.idteam inner join coaches as ch on ch.idcoach = t.idcoach inner join competitions as c on c.idcompetition=s.idcompetition inner join leagues as l on l.idleague = c.idleague {innerwhere} group by s.idcompetition) as sub {where}", null, new Dictionary<string, string>() { { "idcoach", "ch.idcoach" }, { "idteam", "t.idteam" }, { "idcompetition", "s.idcompetition" } }, 1000, new FilteredQueryRequest() { ordercol = "last_game", order = "desc", limit = 50 });
            Definition["match"] = new FilteredQueryDefinition("select * from matches", null, null, 1, null);
            Definition["matchTeams"] = new FilteredQueryDefinition("select * from teammatchstats as ts " +
                                                                            "inner join matches as m on m.idmatch=ts.idmatch " +
                                                                            "inner join teams as t on t.idteam=ts.idteam " +
                                                                            "left join logos as lg on lg.idlogo = t.idlogo " +
                                                                            "inner join coaches as c on c.idcoach = t.idcoach " +
                                                                            "inner join competitions as co on co.idcompetition=ts.idcompetition", new Dictionary<string, string>()
                                                                            {
                                                                                {"idmatch", "m.idmatch" },
                                                                                {"idteam", "t.idteam" },
                                                                                {"idcoach", "c.idcoach" },
                                                                            }, null, 1000, new FilteredQueryRequest() { ordercol = "home", order = "asc", limit = 50 });
            Definition["matchPlayers"] = new FilteredQueryDefinition("select lg.*,sg.*,pn.player_name, t.idrace,t.idlogo,t.team_name,p.idteam,co.coach_name,t.idcoach,ps.*, cb1.cas as cas_state, cb2.cas as cas_sustained from playermatchstats as ps   left join players as p on p.idplayer = ps.idplayer inner join teams as t on t.idteam=ps.idteam left join logos as lg on lg.idlogo = t.idlogo inner join competitions as c on c.idcompetition = ps.idcompetition inner join coaches as co on co.idcoach=t.idcoach  inner join skillcombos as sg on sg.idskillcombo = p.idskillcombo inner join casualtycombos as cb1 on cb1.idcasualtycombo=p.idcasstate  inner join casualtycombos as cb2 on cb2.idcasualtycombo=p.idcassustained inner join playernames as pn on pn.idplayername=ps.idplayername", 
                null, null, 1000, new FilteredQueryRequest()
            {
                ordercol="t.idteam desc,p.number", order="asc", limit=50
            });
            Definition["searchCoach"] = new FilteredQueryDefinition("select * from coaches", null, null, 1000, new FilteredQueryRequest() { ordercol = "coach_name", order = "asc", limit = 5 });
            Definition["searchTeam"] = new FilteredQueryDefinition("select * from standings as s inner join teams as t on t.idteam = s.idteam left join logos as lg on lg.idlogo = t.idlogo inner join coaches as ch on ch.idcoach=t.idcoach inner join competitions as c on c.idcompetition=s.idcompetition", null, null, 1000, new FilteredQueryRequest() { ordercol = "team_name", order = "asc", limit = 5 });
            Definition["searchCompetition"] = new FilteredQueryDefinition("select * from competitions", null, null, 1000, new FilteredQueryRequest() { ordercol = "competition_name", order = "asc", limit = 5 });
            Definition["lastComps"] = new FilteredQueryDefinition("select * from competitions as c inner join leagues as l on l.idleague=c.idleague", null, null, 1000, new FilteredQueryRequest() { ordercol = "last_game", order = "desc", limit = 10 });
            Definition["lastCoachComps"] = new FilteredQueryDefinition("select * from (select sum(s.active) as active,l.idleague,l.league_name,c.idcompetition,c.competition_name,CAST(FORMAT(100*(sum(wins)+sum(draws)/2)/(sum(wins)+sum(draws)+sum(losses)),4) as DECIMAL) as win_pct ,sum(wins) as wins,sum(draws) as draws,sum(losses) as losses, sum(concedes) as concedes,c.last_game,ch.idcoach,ch.coach_name from standings as s inner join teams as t on t.idteam = s.idteam inner join coaches as ch on ch.idcoach=t.idcoach inner join competitions as c on c.idcompetition=s.idcompetition and c.active=1 inner join leagues as l on l.idleague=c.idleague {innerwhere} group by ch.idcoach) as sub {where}   ", new Dictionary<string, string>()
            {
               
            }, null, 1000, new FilteredQueryRequest()
            {
                ordercol = "last_game",
                order = "desc",
                limit = 10,
               
            });
            Definition["leagueCollections"] = new FilteredQueryDefinition("select * from collections as c", null, null, 1000, new FilteredQueryRequest() { });
            Definition["collection"] = new FilteredQueryDefinition("select * from collections as c", null, null, 1000, new FilteredQueryRequest() { });
            Definition["collectionCompetitions"] = new FilteredQueryDefinition("select * from competitions as co inner join collections as c on co.idleague = c.idleague ", new Dictionary<string, string>()
            {
                  {"idleague","c.idleague" }
            }, null, 1000, new FilteredQueryRequest() { ordercol = "last_game", order = "desc", limit = 1000 });
            Definition["collectionTeams"] = new FilteredQueryDefinition("select s.* from standingsview as s inner join competitions as c on c.idcompetition=s.idcompetition ", new Dictionary<string, string>()
            {
                {"sorting","s.sorting" }
             
            }, null, 1000, new FilteredQueryRequest() { ordercol = "sorting", order = "desc", limit = 30 });


        }

        static string GetRemap(Dictionary<string,string> map, string value)
        {
            if (map == null || value==null) { return value; }
			string newValue = "";
			if(map.TryGetValue(value, out newValue))
            {
				return newValue;
            }
			return value;
		}

        public Tuple<string,Dictionary<string,string>> CreateSQL(FilteredQueryRequest req, Dictionary<string,string> replacer = null)
        {
            var definition = Definition[req.id];

            var dict = new Dictionary<string, string>();
            if (req.idmap != null)
            {
                foreach (var kvp in req.idmap)
                {
                    dict["@" + kvp.Key] = kvp.Value;
                }
            }
            if (replacer != null)
            {
                foreach (var kvp in replacer)
                {
                    string repKey = kvp.Key;
                    if (repKey.Contains(":")) repKey = repKey.Substring(repKey.IndexOf(":") + 1);
                    dict["@rep_" + repKey] = kvp.Value;
                }
            }
            req.ApplyDefaults(definition.Default);
			if(req.limit > definition.MaxRows)
            {
				req.limit = definition.MaxRows;
            }
			if (req.from == null) { req.from = 0; }
			
			string query = definition.QueryFormat;

            string where = "";
			string innerWhere = "";
			List<string> parameters = new List<string>();
			if (req.idmap == null) { req.idmap = new Dictionary<string, string>(); }

            // Start with the normal "where" based on idmap
            if (req.idmap.Count > 0)
            {
                if (query.Contains("where ", StringComparison.InvariantCultureIgnoreCase))
                {
                    where += "and ";
                    innerWhere += "and ";
                }
                else
                {
                    where += "where ";
                    innerWhere += "where ";
                }
                bool first = true;
                foreach (var kvp in req.idmap)
                {
                    if (!first)
                    {
                        where += "and ";
                        innerWhere += "and ";
                    }
                    first = false;
                    if (replacer!=null && replacer.ContainsKey("in:" + kvp.Key))
                    {

                        where += GetRemap(definition.OuterMappings, kvp.Key) + " in ("+replacer["in:"+kvp.Key]+") ";
                        innerWhere += GetRemap(definition.InnerMappings, kvp.Key) + " in (" + replacer["in:" + kvp.Key] + ") ";
                    }
                    else
                    {
                        where += GetRemap(definition.OuterMappings, kvp.Key) + "=@" + kvp.Key + " ";
                        innerWhere += GetRemap(definition.InnerMappings, kvp.Key) + "=@" + kvp.Key + " ";
                    }
                    //parameters.Add(kvp.Value);
                }
            }
            /*
			// If we have "innerwhere" then we need to add extra parameters for them
			int numBaseParams = parameters.Count;
			int numWheres = Regex.Matches(query, "{innerwhere}").Count;
			for(int n=0; n < numWheres;n++)
			{
				for(int np=0; np < numBaseParams; np++ )
				{
					parameters.Add(parameters[np]);
				}
			}

			int startOfOuterParams = parameters.Count;
			*/
            // Then filters (only on Where, innerWhere only uses idMap. This is since filters should affect sums which are outer values)
            if (req.filters != null && req.filters.Count > 0)
            {
                if (req.idmap == null || req.idmap.Count <= 0 )
                {
                    where += "where (";
                }
                else
                {
                    where += "and (";
                }
                bool firstFilter = true;
                foreach (var kvp in req.filters)
                {
                    if (firstFilter == false)
                    {
                        where += " and (";
                    }
                    firstFilter = false;
                    string filterString = kvp.Value;
                    
                    where += ParseFilter(filterString, definition, kvp.Key, ref parameters);
                    where += ")";
                }
            }
            /*
			// Duplicate where params if needed
			int numParams = parameters.Count;
			int numWhereParams = numParams - startOfOuterParams;

			numWheres = Regex.Matches(query, "{where}").Count; 
			for(int n = 1; n < numWheres; n++) 
			{
				for(int np = 0; np < numBaseParams; np++) 
				{
					parameters.Add(parameters[np]);
				}
				for (int np = 0; np < numWhereParams; np++)
				{
					parameters.Add(parameters[np+startOfOuterParams]);
				}
			}*/

			// Then order etc
			string order = "";
			string innerOrder = "";
			req.ordercol = req.ordercol?.Replace(";", "");
			if (string.IsNullOrEmpty(req.ordercol)==false) 
			{
				order += " order by " + GetRemap(definition.OuterMappings, req.ordercol);
				innerOrder += " order by " + GetRemap(definition.InnerMappings, req.ordercol);
				if (req.order == "asc") 
				{
					order += " asc";
					innerOrder += " asc";
				}
				else
				{
					order += " desc";
					innerOrder += " desc";

				}
			}
			string limit = "";
			if(req.limit != null && req.limit != 0 )  
			{
                limit += " limit " + req.from + "," + req.limit + " ";
			}

			if(query.IndexOf("{where}") >= 0 || query.IndexOf("{innerwhere}") >= 0) 
			{
				query = query.Replace("{where}", where);
				query = query.Replace("{innerwhere}", innerWhere);
			}
			else
			{
				query += " " + where;
			}
			if (query.IndexOf("{order}") >= 0 || query.IndexOf("{innerorder}") >= 0) 
			{
				query = query.Replace("{order}", order);
				query = query.Replace("{innerorder}", innerOrder);
			}
			else
			{
				query += " " + order;
			}
			if (query.IndexOf("{limit}") >= 0) 
			{
				query = query.Replace("{limit}", limit);
			}
			else
			{
				query += " " + limit;
			}

            int i = 0;
            foreach(var p in parameters)
            {
                dict["@p_" + i] = p;
                i++;
            }

            // And finally custom groups and aggrevation
            if (string.IsNullOrEmpty(req.group) == false && definition.Groups!=null)
            {
                foreach(var gr in definition.Groups)
                {
                    if(gr == req.group)
                    {
                        query=query.Replace("{group}", req.group);
                        break;
                    }
                }
            }
            if(string.IsNullOrEmpty(req.aggr) == false)
            {
                switch (req.aggr)
                {
                    case "sum":
                    case "avg":
                        query = query.Replace("{aggr}", req.aggr);
                        break;
                }
            }
            if (replacer != null)
            {
                foreach (var kvp in replacer)
                {
                    query = query.Replace("{" + kvp.Key + "}", kvp.Value);
                }

            }

            return new Tuple<string,Dictionary<string,string>>(query, dict); 
        }

        public static string ParseFilter(string filterString, FilteredQueryDefinition definition,string filteredColumn, ref List<string> parameters)
        {
            string where = "";
            filterString = filterString.Replace('\"', ' ').Replace('\'', ' ');
            if (filterString.IndexOf("table ", StringComparison.InvariantCultureIgnoreCase) >= 0 ||
                filterString.IndexOf("database ", StringComparison.InvariantCultureIgnoreCase) >= 0 ||
                filterString.IndexOf("delete ", StringComparison.InvariantCultureIgnoreCase) >= 0 ||
                filterString.IndexOf("truncate ", StringComparison.InvariantCultureIgnoreCase) >= 0 ||
                filterString.IndexOf("alter ", StringComparison.InvariantCultureIgnoreCase) >= 0 ||
                filterString.IndexOf("drop ", StringComparison.InvariantCultureIgnoreCase) >= 0)
            {
                return "";
            }



            Stack<string> filterStack = new Stack<string>();
            Stack<string> valueStack = new Stack<string>();
            filterStack.Push("" + filterString);
            bool first = true;
            while (filterStack.Count > 0)
            {
                filterString = filterStack.Pop();

                if(filterString=="|"|| filterString=="&"||filterString == "!")
                {
                    int num = int.Parse(filterStack.Pop());
                    string str = "";
                    for(int i=0; i < num; i++)
                    {
                        if (i != 0)
                        {
                            str += filterString.Replace("|", " or ").Replace("&", " and ");
                        }
                        else if (filterString == "!") str += " not ";
                        string b = valueStack.Pop();
                        str += b;
                    }
                    valueStack.Push(str);
                }
                else if(filterString == "()")
                {
                    valueStack.Push( "(" + valueStack.Pop() + ")");
                }
                else
                {
                    string[] orFilters = Split(filterString,'|');
                    if(orFilters.Length > 1)
                    {
                        filterStack.Push(""+orFilters.Length);
                        filterStack.Push("|");
                        foreach (var filt in orFilters)
                        {
                            filterStack.Push(filt.Trim());
                        }
                        continue;
                    }
                    string[] andFilter = Split(filterString,'&');
                    if (andFilter.Length > 1)
                    {
                        filterStack.Push("" + andFilter.Length);
                        filterStack.Push("&");
                        foreach (var filt in andFilter)
                        {
                            filterStack.Push(filt.Trim());
                        }
                        continue;
                    }
                    if(filterString.StartsWith("(") && filterString.EndsWith(")"))
                    {
                        filterStack.Push("()");
                        filterStack.Push(filterString.Substring(1, filterString.Length - 2).Trim());
                        continue;
                    }

                    if(filterString.StartsWith("!"))
                    {
                        filterStack.Push("" + 1);
                        filterStack.Push("!");
                        filterStack.Push(filterString.Substring(1, filterString.Length - 1).Trim());
                        continue;
                    }

                    where = filterString;
                    first = false;
                     if (filterString.IndexOf(">=") == 0 ||
                            filterString.IndexOf("<=") == 0)
                    {
                        valueStack.Push(GetRemap(definition.OuterMappings, filteredColumn) + " " + filterString.Substring(0, 2) + " @p_" + parameters.Count + " ");
                        filterString = filterString.Substring(2).Trim();
                        parameters.Add(filterString);
                    }
                    else if (filterString.IndexOf(">") == 0 ||
                        filterString.IndexOf("<") == 0)
                    {
                        valueStack.Push(GetRemap(definition.OuterMappings, filteredColumn) + " " + filterString.Substring(0, 1) + " @p_" + parameters.Count + " ");
                        filterString = filterString.Substring(1).Trim();
                        parameters.Add(filterString);
                    }
                    else if (filterString.StartsWith("=")==false && filterString.Contains("=") || filterString.Contains(" like "))
                    {
                        valueStack.Push(filterString);
                    }
                    else

                    {
                        if (filterString.StartsWith("=") == false) filterString = "*" + filterString + "*";
                        else filterString = filterString.Substring(1).Trim();
                        bool hasAsterix = filterString.IndexOf("*") >= 0;
                        filterString = filterString.Replace("*", "%");
                        if (hasAsterix)
                        {
                            valueStack.Push(GetRemap(definition.OuterMappings, filteredColumn) + " like @p_" + parameters.Count);
                        }
                        else
                        {
                            valueStack.Push(GetRemap(definition.OuterMappings, filteredColumn) + " = @p_" + parameters.Count);
                        }

                        parameters.Add(filterString);
                    }

                }
               
                /*

                string[] orFilters = filterString.Split("|");
                bool firstOr = true;
                foreach (var orFilter in orFilters)
                {
                    if (!firstOr)
                    {
                        where += "or ";
                    }
                    firstOr = false;
                    // Then split on AND
                    string[] andFilters = orFilter.Split("&");
                    bool firstAnd = true;
                    foreach (var andFilter in andFilters)
                    {
                        if (!firstAnd)
                        {
                            where += "and ";
                        }
                        string filterWithOp = andFilter.Trim();
                        if (filterWithOp.IndexOf("!") == 0)
                        {
                            where += "not ";
                            filterWithOp = filterWithOp.Substring(1).Trim();
                        }

                        if (filterWithOp.IndexOf(">=") == 0 ||
                            filterWithOp.IndexOf("<=") == 0)
                        {
                            where += GetRemap(definition.OuterMappings, filteredColumn) + " " + filterWithOp.Substring(0, 2) + " @p_" + parameters.Count + " ";
                            filterWithOp = filterWithOp.Substring(2).Trim();
                            parameters.Add(filterWithOp);
                        }
                        else if (filterWithOp.IndexOf(">") == 0 ||
                            filterWithOp.IndexOf("<") == 0)
                        {
                            where += GetRemap(definition.OuterMappings, filteredColumn) + " " + filterWithOp.Substring(0, 1) + " @p_" + parameters.Count + " ";
                            filterWithOp = filterWithOp.Substring(1).Trim();
                            parameters.Add(filterWithOp);
                        }
                        else
                        {
                            if (filterWithOp.IndexOf("=") == 0)
                            {
                                filterWithOp = filterWithOp.Substring(1).Trim();
                            }
                            else
                            {
                                filterWithOp = "*" + filterWithOp + "*";
                            }
                            filterWithOp = filterWithOp.Trim();
                            bool hasAsterix = filterWithOp.IndexOf("*") >= 0;
                            filterWithOp = filterWithOp.Replace("*", "%");
                            if (hasAsterix)
                            {
                                where += GetRemap(definition.OuterMappings, filteredColumn) + " like @p_" + parameters.Count + " ";
                            }
                            else
                            {
                                where += GetRemap(definition.OuterMappings, filteredColumn) + " = @p_" + parameters.Count + " ";
                            }

                            parameters.Add(filterWithOp);

                        }
                        firstAnd = false;

                    }
                }
                */

            }
            where = valueStack.Pop();
            return where;
        }


        public static string[] Split(string str,char split)
        {
            bool inQuote = false;
            List<string> res = new List<string>();
            int depth = 0;
            string collect = "";
            char lastChar = 'a';
            foreach(char c in str)
            {
                if (lastChar!='\\' && c == '\"') inQuote = !inQuote;
                else if (inQuote == false)
                {
                    if (c == '(') depth++;
                    if (c == ')') depth--;
                    if (c == split && depth == 0)
                    {
                        res.Add(collect);
                        collect = "";
                    }
                    else collect += c;
                }
                else collect += c;
                lastChar = c;
            }
            if (collect != "")
            {
                res.Add(collect);
            }
            return res.ToArray();
        }

    }


    public class FilteredQueryDefinition
    {
        public string QueryFormat { get; set; }
	public Dictionary<string, string> OuterMappings { get; set; }
	public Dictionary<string, string> InnerMappings { get; set; }
	public FilteredQueryRequest Default { get; set; }
        public string[] Groups { get; set; }

    public int MaxRows { get; set; }

        public FilteredQueryDefinition() { }
 
        public FilteredQueryDefinition(string format, Dictionary<string, string> outerMappings, Dictionary<string, string> innerMappings, int maxRows, FilteredQueryRequest defaultValues, string[] groups = null) 
        {
            Groups = groups;
            QueryFormat = format;
			OuterMappings = outerMappings;
			InnerMappings = innerMappings;
            MaxRows = maxRows;
			Default = defaultValues;

			if (InnerMappings == null) InnerMappings = new Dictionary<string, string>();
			if (OuterMappings == null) OuterMappings = new Dictionary<string, string>();
        }
    }
}
