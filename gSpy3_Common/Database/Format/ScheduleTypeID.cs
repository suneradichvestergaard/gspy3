﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database.Format
{
    public enum ScheduleTypeID
    {
        single_match = 1, two_legged = 2,best_of_3 = 3, best_of_5 = 5
    }
    public static class ScheduleTypeConversion
    {
        public static ScheduleTypeID FromLegacyGoblinSpy(string scheduleType)
        {
            return (ScheduleTypeID)Enum.Parse(typeof(ScheduleTypeID), scheduleType);
        }
    }
}
