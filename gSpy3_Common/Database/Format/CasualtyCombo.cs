﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database.Format
{
    class CasualtyCombo : DatabaseItem
    {
        public int? idcasualtycombo { get; set; }
        public string cas { get; set; }

        public override string GetIndexColumn()
        {
            return "idcasualtycombo";
        }
        public override string GetIndexColumnValue()
        {
            return "" + idcasualtycombo;
        }
        public override string[] GetInsertIndexColumns()
        {
            return new string[] { "cas" };
        }
        public override string[] GetInsertIndexColumnValues()
        {
            return new string[] { "" + cas };
        }
    }
}

