﻿using gSpy3_Common.API.LegacyGoblinSpy;
using gSpy3_Common.API.LegacyGoblinSpy.DataFormat;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database.Format
{
    public class League
    {
        public int? idleague { get; set; }
        public int idorigin { get; set; } 
        public int? league_origin_id { get; set; }
        public string league_name { get; set; }
        public byte collecting { get; set; }
        public League() { }
        public League(StatsPublicLeague league) 
        { 
            idorigin = (int)OriginConversion.FromLegacyGoblinSpy(league.platform);
            league_name = league.name;
            collecting = 0;
            foreach(var item in league.list)
            {
                if (item.active) collecting = 1;
            }
            league_origin_id = league.id != null &&league.id!=0 ? league.id : 0;

        }
    }
}
