﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database.Format
{
    class SkillCombo : DatabaseItem
    {
        public int? idskillcombo { get; set; }
        public string skills { get; set; }

        public override string GetIndexColumn()
        {
            return "idskillcombo";
        }
        public override string GetIndexColumnValue()
        {
            return "" + idskillcombo;
        }
        public override string[] GetInsertIndexColumns()
        {
            return new string[] { "skills" };
        }
        public override string[] GetInsertIndexColumnValues()
        {
            return new string[] { "" + skills };
        }
    }

}
