﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database.Format
{
    public enum HighlightEnum
    {
        KilledByPlayer = 1,
        Killed = 2,

    }
    public class Highlight : DatabaseItem
    {
        public int idhighlight { get; set; }
        public UInt64 idmatch { get; set; }
        public int idplayer { get; set; }
        public int idplayername { get; set; }
        public int idtarget { get; set; }
        public int idtargetname { get; set; }
        public int idcause { get; set; }
        public int idhighlighttype { get; set; }
        public int turn { get; set; }
        public int subturn { get; set; }
        public int weight { get; set; }

        public override string GetIndexColumn()
        {
            return "idhighlight";
        }
        public override string GetIndexColumnValue()
        {
            return "" + idhighlight;
        }
        public override string[] GetInsertIndexColumns()
        {
            throw new NotImplementedException();
        }
        public override string[] GetInsertIndexColumnValues()
        {
            throw new NotImplementedException();
        }
    }
}
