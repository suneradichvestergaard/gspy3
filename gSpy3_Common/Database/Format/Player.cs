﻿using Org.BouncyCastle.Asn1.X509.Qualified;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database.Format
{
    public class Player : DatabaseItem
    {
        public int? idplayer { get; set; }
        public int idorigin { get; set; }
        public int? player_origin_id { get; set; }
        public int idplayername { get; set; }
        public int idteam { get; set; }
        public int idleague { get; set; }
        public int ma { get; set; }
        public int ag { get; set; }
        public int av { get; set; }
        public int st { get; set; }
        public int pa { get; set; }
        public int idskillcombo { get; set; }
        public int idcasstate { get; set; }
        public int idcassustained { get; set; }
        public int xp { get; set; }
        public int xp_gain { get; set; }
        public int level { get; set; }
        public int idplayertype { get; set; }
        public int active { get; set; }
        public int number { get; set; }

        public Player() { }
        public Player(PlayerMatchStats p, int idleague) 
        {
            idplayer = p.idplayer;
            idplayername = p.idplayername;
            idteam = p.idteam;
            this.idleague = idleague;
            ma = p.ma;
            ag = p.ag;
            av = p.av;
            st = p.st;
            pa = p.pa;
            idskillcombo = p.idskillcombo;
            idcasstate = p.idcasstate;
            idcassustained = p.idcassustained;
            xp = p.xp;
            xp_gain = p.xp_gain;
            level = p.level;
            idplayertype = p.idplayertype;
            active = p.game_played;
            number = p.number;
    }
        public Player(API.BB2.DataFormat.MatchResultPlayer p, OriginID idorigin, int idleague,int idteam, int name, int skills,int casstate,int cassus,int playertype)
        {
            this.idorigin = (int)idorigin;
            player_origin_id = p.id;
            idplayername = name;
            this.idteam = idteam;
            this.idleague = idleague;
            ma = p.attributes.ma;
            st = p.attributes.st;
            ag = p.attributes.ag;
            av = p.attributes.av;
            this.idskillcombo = skills;
            this.idcasstate = casstate;
            this.idcassustained = cassus;
            xp = p.xp;
            xp_gain = p.xp_gain;
            level = p.level;
            this.idplayertype = playertype;
            active = p.matchplayed;
            number = p.number;
            

        }

        public override string GetIndexColumn()
        {
            return "idplayer";
        }
        public override string GetIndexColumnValue()
        {
            return "" + idplayer;
        }
        public override string[] GetInsertIndexColumns()
        {
            return new string[] { "idorigin", "player_origin_id" };
        }
        public override string[] GetInsertIndexColumnValues()
        {
            return new string[] { "" + idorigin, "" + player_origin_id };
        }
    }
}
