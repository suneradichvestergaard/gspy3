﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database.Format
{
    public class Coach : DatabaseItem
    {
        public int? idcoach { get; set; }
        public int idorigin { get; set; }
        public int? coach_origin_id { get; set; }
        public string coach_name { get; set; }
        public string? youtube { get; set; }
        public string? twitch { get; set; }
        public string? lang { get; set; }
        public string? country { get; set; }

        public Coach() { }
        public Coach(API.BB2.DataFormat.CoachStats coach, OriginID idorigin)
        {
            this.idorigin = (int)idorigin;
            coach_origin_id = coach.idcoach;
            coach_name = coach.coachname;
        }

        public override string GetIndexColumn()
        {
            return "idcoach";
        }
        public override string GetIndexColumnValue()
        {
            return "" + idcoach;
        }
        public override string[] GetInsertIndexColumns()
        {
            return new string[] { "idorigin", "coach_origin_id" };
        }
        public override string[] GetInsertIndexColumnValues()
        {
            return new string[] { "" + idorigin, ""+coach_origin_id };
        }


    }
}
