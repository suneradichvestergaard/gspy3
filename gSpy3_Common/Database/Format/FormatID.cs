﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace gSpy3_Common.Database.Format
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum FormatID
    {
        MatchMaking = 0,
        SingleElimination = 1,
        RoundRobin = 2,
        Swiss = 3,

    }

    public static class FormatIDConversion
    {
        public static FormatID FromLegacyGoblinSpy(string format)
        {
            switch (format)
            {
                case "single_elimination": return FormatID.SingleElimination;
                case "round_robin": return FormatID.RoundRobin;
                case "swiss": return FormatID.Swiss;
                case "ladder": return FormatID.MatchMaking;
                case "": return FormatID.MatchMaking;
            }
            throw new Exception("Unknown format");
            return FormatID.MatchMaking;
        }
    }
}
