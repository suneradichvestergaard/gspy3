﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database.Format
{
    public class PlayerName : DatabaseItem
    {
        public int? idplayername { get; set; }
        public string player_name { get; set; }

        public override string GetIndexColumn()
        {
            return "idplayername";
        }
        public override string GetIndexColumnValue()
        {
            return "" + idplayername;
        }
        public override string[] GetInsertIndexColumns()
        {
            return new string[] { "player_name" };
        }
        public override string[] GetInsertIndexColumnValues()
        {
            return new string[] { "" + player_name };
        }
    }
}

