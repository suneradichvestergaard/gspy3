﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database.Format
{
    public class Stadium : DatabaseItem
    {
        public int? idstadium { get; set; }
        public string stadium { get; set; }

        public override string GetIndexColumn()
        {
            return "idstadium";
        }
        public override string GetIndexColumnValue()
        {
            return "" + idstadium;
        }
        public override string[] GetInsertIndexColumns()
        {
            return new string[] { "stadium" };
        }
        public override string[] GetInsertIndexColumnValues()
        {
            return new string[] { "" + stadium };
        }
    }
}
