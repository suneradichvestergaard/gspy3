﻿using gSpy3_Common.API.LegacyGoblinSpy.DataFormat;
using Org.BouncyCastle.Asn1.Cmp;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database.Format
{
    public class Competition
    {
        public int idorigin { get; set; }
        public int? idcompetition { get; set; }
        public int idleague { get; set; }
        public int? competition_origin_id { get; set; }
        public string competition_name { get; set; }
        public byte active { get; set; }
        public int? format { get; set; }
        public int? turn_duration { get; set; }
        public string? sorting { get; set; }
        public int? num_coaches { get; set; }
        public int? num_teams { get; set; }
        public int? num_games { get; set; }
        public string? last_game { get; set; }

        public Competition()
        {

        }
        public Competition(League owner, StatsPublicCompetition comp)
        {
            this.idleague = (int)owner.idleague;
            idorigin = owner.idorigin;
            competition_origin_id = comp.id != null && comp.id != 0 ? comp.id : 0;
            competition_name = comp.name;
            active = comp.active ? (byte)1 : (byte)0 ;
            format = (int)FormatIDConversion.FromLegacyGoblinSpy(comp.format);
            sorting = comp.sorting;
            num_coaches = comp.numcoaches;
            num_teams = comp.numteams;
            num_games = comp.numgames;
            last_game = comp.lastgame == ""?null:comp.lastgame;
        
            
        }
    }

}
