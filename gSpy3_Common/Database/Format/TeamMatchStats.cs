﻿using gSpy3_Common.API.BB2.DataFormat;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database.Format
{
    public class TeamMatchStats : DatabaseItem
    {
        public UInt64? idteammatchstat { get; set; }
        public int idorigin { get; set; }
        public UInt64 teammatchstat_origin_id { get; set; }
        public int idteam { get; set; }
        public UInt64 idmatch { get; set; }
        public int idcompetition { get; set; }

        public int home { get; set; }
        public int td { get; set; }
        public int td_opp { get; set; }
        public int score { get; set; }
        public int score_opp { get; set; }
        public int win { get; set; }
        public int loss { get; set; }
        public int conceded { get; set; }
        public int cash_before_match { get; set; }
        public int cash_spent { get; set; }
        public int cash_earned { get; set; }
        public int popularity_before_match { get; set; }
        public int popularity_gained { get; set; }
        public int blocks_for { get; set; }
        public int breaks_for { get; set; }
        public int stuns_for { get; set; }
        public int kos_for { get; set; }
        public int casualties_for { get; set; }
        public int kills_for { get; set; }

        public int possession { get; set; }

        public int blocks_against { get; set; }
        public int breaks_against { get; set; }
        public int stuns_against { get; set; }
        public int kos_against { get; set; }
        public int casualties_against { get; set; }
        public int kills_against { get; set; }
        public int supporters { get; set; }
        public int passes { get; set; }
        public int catches { get; set; }
        public int completions { get; set; }
        public int dodges { get; set; }
        public int fouls { get; set; }
        public int pushouts { get; set; }
        public int turnovers { get; set; }
        public int pass_meters { get; set; }
        public int run_meters { get; set; }
        public int occ_own { get; set; }
        public int occ_their { get; set; }
        public int interceptions { get; set; }
        public int expulsions { get; set; }
        public int cheerleaders { get; set; }
        public int apo { get; set; }
        public int rerolls { get; set; }
        public int asscoaches { get; set; }
        public int draw { get; set; }
        public int mvp { get; set; }
        public int team_value { get; set; }


        public int? dodges_failed { get; set; }
        public int? passes_failed { get; set; }
        public int? catches_failed { get; set; }
        public int? rushes { get; set; }
        public int? rushes_failed { get; set; }
        public int? risk { get; set; }
        public int? luck { get; set; }
        public int? pickups { get; set; }
        public int? pickups_failed { get; set; }
        public int? sacks { get; set; }

        public int? boneheads { get; set; }

        public TeamMatchStats() { }
        public TeamMatchStats(TeamStats_Current tcur,MatchResultTeam t, MatchResultTeam topp, UInt64 orgidmatch, OriginID origin,int idcompetition, int idteam, UInt64 idmatch, bool isHome)
        {
            idorigin = (int)origin;
            teammatchstat_origin_id = orgidmatch;
            this.idteam = idteam;
            this.idmatch = idmatch;
            this.idcompetition = idcompetition;
            this.home = isHome ? 1 : 0;
            td = t.inflictedtouchdowns;
            td_opp = topp.inflictedtouchdowns;
            score = t.score;
            score_opp = topp.score;
            win = score > score_opp ? 1 : 0;
            loss = score < score_opp ? 1 : 0;
            draw = score == score_opp ? 1 : 0;
            conceded = topp.mvp > 1 ? 1 : 0;
            cash_before_match = t.cashbeforematch;
            cash_spent = t.cashspentinducements;
            cash_earned = t.cashearned;
            popularity_before_match = t.popularitybeforematch;
            popularity_gained = t.popularitygain;
            blocks_for = t.inflictedtackles;
            breaks_for = t.inflictedinjuries;
            stuns_for = t.inflictedinjuries - (t.inflictedko + t.inflictedcasualties);
            if (stuns_for < 0) stuns_for = 0;
            kos_for = t.inflictedko;
            casualties_for = t.inflictedcasualties;
            kills_for = t.inflicteddead;
            blocks_against = topp.inflictedtackles;
            breaks_against = t.sustainedinjuries;
            stuns_against = t.sustainedinjuries - (t.sustainedko + t.sustainedcasualties);
            if (stuns_against < 0) stuns_against = 0;
            kos_against = t.sustainedko;
            casualties_against = t.sustainedcasualties;
            kills_against = t.sustaineddead;
            possession = t.possessionball;
            supporters = t.nbsupporters;
            passes = t.inflictedpasses;
            catches = t.inflictedcatches;
            pushouts = t.inflictedpushouts;
            pass_meters = t.inflictedmeterspassing;
            run_meters = t.inflictedmetersrunning;
            occ_own = t.occupationown;
            occ_their = t.occupationtheir;
            expulsions = t.sustainedexpulsions;
            cheerleaders = tcur.cheerleaders == null?0:(int)tcur.cheerleaders;
            apo = tcur.apothecary==null?0:(int)tcur.apothecary;
            rerolls = tcur.rerolls == null? 0:(int)tcur.rerolls;
            asscoaches = tcur.assistantcoaches==null?0:(int)tcur.assistantcoaches;
            mvp = t.mvp;
            team_value = t.value;
            interceptions = t.inflictedinterceptions;
            if (t.roster != null)
            {
                bool getInterceptions = interceptions == 0;
                bool getPushouts = pushouts == 0;
                foreach (var pl in t.roster)
                {
                    if (pl != null)
                    {
                        if (getInterceptions) 
                        { 
                            if(pl.inflictedinterceptions!=0) interceptions += pl.inflictedinterceptions;
                            else if (pl.stats.inflictedinterceptions != 0) interceptions += pl.stats.inflictedinterceptions;
                        }
                        if (getPushouts) 
                        { 
                            if(pl.inflictedpushouts!=0) pushouts += pl.inflictedpushouts;
                            else if (pl.stats.inflictedpushouts != 0) pushouts += pl.stats.inflictedpushouts;
                        }
                    }

                }
            }
            
        }


        public override string GetIndexColumn()
        {
            return "idteammatchstat";
        }
        public override string GetIndexColumnValue()
        {
            return "" + idteammatchstat;
        }
        public override string[] GetInsertIndexColumns()
        {
            return new string[] { "idorigin", "teammatchstat_origin_id" };
        }
        public override string[] GetInsertIndexColumnValues()
        {
            return new string[] { "" + idorigin, "" + teammatchstat_origin_id };
        }
    }
}
