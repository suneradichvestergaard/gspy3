﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database.Format
{
    public class Team : DatabaseItem
    {
        public int? idteam { get; set; }
        public int idorigin { get; set; }
        public int? team_origin_id { get; set; }
        public string team_name { get; set; }
        public int idrace { get; set; }
        public int idcoach { get; set; }
        public int idlogo { get; set; }
        public int idmotto { get; set; }
        public int active{ get; set; }

        public Team() { active = 1; }
        public Team(API.BB2.DataFormat.MatchResultTeam t, OriginID idorigin, int idcoach)
        {
            this.idorigin = (int)idorigin;
            team_origin_id = t.idteamlisting;
            team_name = t.teamname;
            active = 1;
            idrace = t.idraces;
            this.idcoach = idcoach;
        }

        public override string GetIndexColumn()
        {
            return "idteam";
        }
        public override string GetIndexColumnValue()
        {
            return "" + idteam;
        }
        public override string[] GetInsertIndexColumns()
        {
            return new string[] { "idorigin", "team_origin_id" };
        }
        public override string[] GetInsertIndexColumnValues()
        {
            return new string[] { "" + idorigin, "" + team_origin_id };
        }
    }
}
