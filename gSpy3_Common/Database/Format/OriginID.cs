﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace gSpy3_Common.Database.Format
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum OriginID
    {
        BB2_pc = 1,
        BB2_xb1 = 2,
        BB2_ps4 = 3, 

    }

    public static class OriginConversion
    {
        public static OriginID FromLegacyGoblinSpy(string platform)
        {
            switch (platform)
            {
                case "ps4": return OriginID.BB2_ps4;
                case "xb1": return OriginID.BB2_xb1;
            }
            return OriginID.BB2_pc;
        }
        public static string ToLegacyGoblinSpy(OriginID idorigin)
        {
            switch (idorigin)
            {
                case OriginID.BB2_ps4:
                    return "ps4";
                case OriginID.BB2_xb1:
                    return "xb1";
            }
            return "pc";
        }
    }


}
