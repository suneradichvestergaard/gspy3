﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace gSpy3_Common.Database.Format
{
    public class Schedule : DatabaseItem
    {
        public UInt64? idschedule { get; set; }
        public int idorigin { get; set; }
        public int schedule_origin_id { get; set; }
        public int idteam_home { get; set; }
        public int idteam_away { get; set; }
        public int round { get; set; }
        public int idcompetition { get; set; }
        public int? idscheduletype { get; set; }

        public UInt64? idmatch { get; set; }
        public UInt64? match_origin_id { get; set; }

        public Schedule() { }
        public Schedule(OriginID idorigin, API.BB2.DataFormat.ScheduledMatch s, int idcompetition, int idteam_home, int idteam_away)
        {
            this.idorigin = (int)idorigin;
            schedule_origin_id = (int)s.contest_id;
            this.idteam_home = idteam_home;
            this.idteam_away = idteam_away;
            round = (int)s.round;
            this.idcompetition = idcompetition;
            this.idscheduletype = (int)ScheduleTypeConversion.FromLegacyGoblinSpy(s.type);
            this.idmatch = null;
            if (string.IsNullOrEmpty(s.match_uuid) == false)
                this.match_origin_id = UInt64.Parse(s.match_uuid, NumberStyles.HexNumber);
            else
                match_origin_id = 0;
        }

        public override string GetIndexColumn()
        {
            return "idschedule";
        }
        public override string GetIndexColumnValue()
        {
            return "" + idschedule;
        }
        public override string[] GetInsertIndexColumns()
        {
            return new string[] { "idorigin", "schedule_origin_id" };
        }
        public override string[] GetInsertIndexColumnValues()
        {
            return new string[] { "" + idorigin, "" + schedule_origin_id };
        }
    }
   }
