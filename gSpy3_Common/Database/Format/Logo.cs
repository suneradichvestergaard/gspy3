﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database.Format
{
    public class Logo : DatabaseItem
    {
        public int? idlogo { get; set; }
        public string logo { get; set; }

        public override string GetIndexColumn()
        {
            return "idlogo";
        }
        public override string GetIndexColumnValue()
        {
            return "" + idlogo;
        }
        public override string[] GetInsertIndexColumns()
        {
            return new string[] { "logo" };
        }
        public override string[] GetInsertIndexColumnValues()
        {
            return new string[] { "" + logo };
        }
    }
}
