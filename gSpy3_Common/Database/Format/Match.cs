﻿using gSpy3_Common.Database.AccessInterface;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database.Format
{
   public class Match : DatabaseItem
    {
        public UInt64? idmatch { get; set; }
        public int idorigin { get; set; }
        public UInt64 match_origin_id { get; set; }
        public int idcompetition { get; set; }
        public string started { get; set; }
        public string finished { get; set; }
        public int duration { get; set; }

        public int idteam_home { get; set; }
        public int idteam_away { get; set; }
        public int idstadium { get; set; }

        public Match() { }
        public Match(API.BB2.DataFormat.MatchResult m, int idcompetition, int idteam_home, int idteam_away, int idstadium)
        {
            idorigin =(int) OriginConversion.FromLegacyGoblinSpy(m.match.platform);
            match_origin_id = UInt64.Parse(m.uuid, System.Globalization.NumberStyles.HexNumber);
            started = m.match.started;
            finished = m.match.finished;
            duration = (int)(DateTime.Parse(m.match.finished) - DateTime.Parse(m.match.started)).TotalMinutes;
            this.idcompetition = idcompetition;
            this.idteam_home = idteam_home;
            this.idteam_away = idteam_away;
            this.idstadium = idstadium;

        }



        public override string GetIndexColumn()
        {
            return "idmatch";
        }
        public override string GetIndexColumnValue()
        {
            return "" + idmatch;
        }
        public override string[] GetInsertIndexColumns()
        {
            return new string[] { "idorigin", "match_origin_id" };
        }
        public override string[] GetInsertIndexColumnValues()
        {
            return new string[] { "" + idorigin, "" + match_origin_id };
        }
    }
}
