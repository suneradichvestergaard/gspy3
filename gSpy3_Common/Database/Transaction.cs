﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database
{
    public class Transaction
    {
        public object TransactionObject { get { return transaction.GetObject(); } }
        ITransactionObject transaction;
        public Transaction(ITransactionObject trans)
        {
            transaction = trans;
        }

        public void Commit()
        {
            transaction.Commit();
        }
        public void Rollback()
        {
            transaction.Rollback();
        }
    }

    public interface ITransactionObject
        {
        void Commit();
        void Rollback();
        object GetObject();
    }
}
