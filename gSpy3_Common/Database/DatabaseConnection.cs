﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;

namespace gSpy3_Common.Database
{
    public abstract class DatabaseConnection : IDisposable
    {

        public abstract ConnectionState State { get; }
        public abstract bool IsPasswordExpired { get; }
        protected abstract void Open();
        protected abstract void Close();
        protected abstract ITransactionObject BeginConnectionTransaction();

        object connectionLock = new object();
        public abstract void Dispose();

        public Transaction BeginTransaction()
        {
            if (State != System.Data.ConnectionState.Open || IsPasswordExpired)
            {
                Close();
                Open();
            }
            return new Transaction(BeginConnectionTransaction());
        }
        public Dictionary<string, int> GetIndexMap<T>(string table, string where, Dictionary<string, string> parameters, Transaction transaction = null) where T : DatabaseItem, new()
        {
            T t = new T();
            return GetIndexMap(t.GetIndexColumn(), t.GetInsertIndexColumns(), table, where, parameters, transaction);
        }

        public Dictionary<string, int> GetIndexMap(string indexcolumn, string[] insertindexcolumns, string table, string where, Dictionary<string,string> parameters, Transaction transaction = null) 
        {
            string sql = "select " + indexcolumn + "," + string.Join(',', insertindexcolumns) + " from " + table + " " + where;
            var res= Query(sql, parameters, transaction, 60 * 60 * 20);
            Dictionary<string, int> map = new Dictionary<string, int>();
            foreach(var row in res.Rows)
            {
                StringBuilder id = new StringBuilder();
                 for(int i=1; i < res.Cols.Count; i++ )
                {
                    if (i > 1) id.Append(";");
                    id.Append(row[i]);
                }
                map[id.ToString()] = int.Parse(row[0]);

            }
            return map;
            
        }

        public int? Find(string table, DatabaseItem item, Transaction transaction)
        {
            string sql = "select " + item.GetIndexColumn() + " from " + table + " where ";
            var cols = item.GetInsertIndexColumns();
            var vals = item.GetInsertIndexColumnValues();
            var parms = new Dictionary<string, string>();
            for(int i=0; i < cols.Length; i++)
            {
                if (i != 0) sql += " and ";
                sql += cols[i] + "=@" + cols[i];
                parms["@" + cols[i]] = vals[i];

            }
            var res = Query(sql, parms, transaction);
            if (res.Rows.Count == 0) return null;
            return int.Parse(res.Rows[0][0]);
        }

        public List<T> Select<T>(string select, Dictionary<string,string> map, Transaction transaction=null) where T: class
        {
            var res = Query(select, map, transaction);
            if (res == null) { return null; }
            List<T> list = new List<T>();
            foreach(var row in res.Rows)
            {
                T t = (T)Activator.CreateInstance(typeof(T));
                foreach (var prop in typeof(T).GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance))
                {
                    int icol = 0;
                    if(res.Cols.TryGetValue(prop.Name, out icol))
                    {
                        prop.SetValue(t, ConvertFromString(prop.PropertyType,row[icol]));
                    }
                }
                list.Add(t);

            }
            return list;
        }

        public string ConvertToString(object value)
        {
            try
            {
                if (value == null) return "";
                var t = value.GetType();
                if (t == typeof(float)) { return ((float)value).ToString(CultureInfo.InvariantCulture); }
                if (t == typeof(double)) { return ((double)value).ToString(CultureInfo.InvariantCulture); }
                if (t == typeof(float?)) { return value==null?"":((float)value).ToString(CultureInfo.InvariantCulture); }
                if (t == typeof(double?)) { return value == null? "":((double)value).ToString(CultureInfo.InvariantCulture); }
                return value.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            throw new Exception("Unkown type used in conversion: " + value.ToString());
        }
        public object ConvertFromString(Type t, string value)
        {
            try
            {
                if (t == typeof(string)) { return value; }
                if (t == typeof(byte)) { return byte.Parse(value); }
                if (t == typeof(int)) { return int.Parse(value); }
                if (t == typeof(long)) { return long.Parse(value); }
                if (t == typeof(UInt64)) { return UInt64.Parse(value); }
                if (t == typeof(float)) { return float.Parse(value); }
                if (t == typeof(double)) { return double.Parse(value); }
                if (t == typeof(bool)) { return bool.Parse(value); }
                if (t == typeof(byte?)) { if (value == null) return null; byte v; if (!byte.TryParse(value,out v)) return null; return v; }
                if (t == typeof(int?)) { if (value == null) return null; int iv; if (!int.TryParse(value, out iv)) return null; return iv; }
                if (t == typeof(float?)){ if (value == null) return null; float fv; if (!float.TryParse(value, out fv)) return null; return fv;    }
                if (t == typeof(double?)) { if (value == null) return null; double dv; if (!double.TryParse(value, out dv)) return null; return dv; }
                if (t == typeof(bool?)) { if (value == null) return null; bool bv; if (!bool.TryParse(value, out bv)) return null; return bv; }
                if (t == typeof(long?)) { if (value == null) return null; long bv; if (!long.TryParse(value, out bv)) return null; return bv; }
                if (t == typeof(UInt64?)) { if (value == null) return null; UInt64 bv; if (!UInt64.TryParse(value, out bv)) return null; return bv; }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            throw new Exception("Unkown type used in conversion: " + t.ToString());
        }


  
        public QueryResult Update<T>(string table, T item, string where, string[] exclude, Transaction transaction=null) where T: class
        {
            Dictionary<string, string> parms = new Dictionary<string, string>();
            StringBuilder sb = new StringBuilder();
            sb.Append("update " + table + " set ");
            bool first = true;
            foreach(var prop in typeof(T).GetProperties(System.Reflection.BindingFlags.Public| System.Reflection.BindingFlags.Instance))
            {
                var val = (prop.GetValue(item));
                if (val != null)
                {
                    parms["@"+prop.Name] = ConvertToString(val);
                    if (exclude.Contains(prop.Name) == false)
                    {
                        if (first == false) { sb.Append(","); }
                        sb.Append(prop.Name);
                        sb.Append("=@");
                        sb.Append(prop.Name);
                        first = false;
                    }
                }
            }

            sb.Append(" ");
            sb.Append(where);
            return Query(sb.ToString(), parms, transaction, 60 * 60 * 20);
        }

        public List<T> InsertIfMissing<T>(string table, List<T> items, Transaction transaction = null) where T : DatabaseItem
        {
            try
            {
                if (items.Count == 0) return new List<T>();

                // Try batch first
                InsertBatch(table, items, new string[] { items[0].GetIndexColumn() }, transaction);
                return items;
            }
            catch
            {
                // Failure, so do one at a time instead
                List<T> added = new List<T>();
                foreach(var item in items)
                {
                    try
                    {
                        var res = Insert(table, item, transaction);
                        if(res.Success)
                            added.Add(item);

                    }catch(Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine(ex.ToString()); // This may be totally fine as we are saying that you only should insert if missing
                    }
                }
                return added;
            }
         
        }

        public List<T> InsertIfMissing<T>(string table, List<T> items, string where, Dictionary<string,string> parameters, Transaction transaction=null) where T : DatabaseItem
        {
            if (items.Count == 0) return new List<T>();
            List<T> toInsert = new List<T>();
            var res = Query("select " + string.Join(',', items[0].GetInsertIndexColumns()) + " from " + table + " " + where, parameters, transaction);
            Dictionary<string, bool> map = new Dictionary<string, bool>();
            foreach (var row in res.Rows)
            {
                string id = string.Join(';', row);
                map[id] = true;
            }
            foreach (var item in items)
            {
                string id = string.Join(';', item.GetInsertIndexColumnValues());
                if (!map.ContainsKey(id))
                {
                    toInsert.Add(item);
                    map[id] = true;
                }
            }

            try
            {


                InsertBatch(table, toInsert, new string[] { items[0].GetIndexColumn() }, transaction);
                return toInsert;
            }
            catch
            {
                // Failure, so do one at a time instead
                List<T> added = new List<T>();
                foreach (var item in toInsert)
                {
                    try
                    {
                        var res2 = Insert(table, item, transaction);
                        if (res2.Success)
                            added.Add(item);

                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine(ex.ToString()); // This may be totally fine as we are saying that you only should insert if missing
                    }
                }
                return added;
            }
            
        }



        public void InsertBatch<T>(string table, List<T> items, string[] exclude, Transaction transaction=null, int maxsize=1024*500) where T: class
        {
            try
            {
                int breakAtSize =  1024 * 500; // Enough with chunks of 0.5MB
                breakAtSize = maxsize;
                StringBuilder sb = new StringBuilder(breakAtSize*2);
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                if (items.Count == 0) return;

                sb.Append( "insert into " + table + " ( ");
                bool first = true;
                foreach (var prop in typeof(T).GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance))
                {
                    if (!exclude.Contains(prop.Name))
                    {
                        if (first == false) { sb.Append(","); }
                        sb.Append(prop.Name);
                        first = false;
                    }
                }
                sb.Append(") values ");

                int itemsWritten = 0;
                while (itemsWritten < items.Count)
                {

                    int propertyIndex = 0;
                    StringBuilder sbInside = new StringBuilder();
                    sbInside.Append(sb.ToString());
                    bool firstInRound = true;
                    for (; itemsWritten < items.Count ; )
                    {
                        if (!firstInRound)
                        {
                            sbInside.Append(",");
                        }
                        firstInRound = false;
                        sbInside.Append("(");
                        var item = items[itemsWritten];
                        first = true;
                        foreach (var prop in typeof(T).GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance))
                        {
                            if (!exclude.Contains(prop.Name))
                            {
                                if (first == false) { sbInside.Append(","); }
                                var val = (prop.GetValue(item));
                                string pName = "@p" + propertyIndex;
                                if (val != null)
                                {
                                    sbInside.Append(pName);
                                    parameters[pName] = ConvertToString(val);
                                    propertyIndex++;
                                }
                                else
                                {
                                    sbInside.Append(pName);
                                    parameters[pName] = "";
                                    propertyIndex++;
                                }
                                first = false;
                            }
                        }
                        itemsWritten++;
                        sbInside.Append(")");

                        if(sbInside.Length > breakAtSize)
                        {
                            firstInRound = true;
                            break;
                        }
                    }

                    var qRes = Query(sbInside.ToString(), parameters, transaction, 60 * 60 * 20);
                    if (qRes.Success == false)
                    {
                        throw new Exception("Error inserting item");
                    }

                }
            }catch(Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete "where" and then insert all given items in a batch
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="table"></param>
        /// <param name="items"></param>
        /// <param name="where"></param>
        /// <param name="parameters"></param>
        /// <param name="exclude"></param>
        /// <param name="transaction"></param>
        public void InsertClean<T>(string table, List<T> items, string where, Dictionary<string,string> parameters,string[] exclude,  Transaction transaction = null) where T: class
        {
          
            Query("delete " + table + " " + where, parameters, transaction); // Clean

            InsertBatch(table, items, exclude, transaction);

        }

        public QueryResult Insert<T>(string table, T item,Transaction transaction = null) where T : class
        {
            Dictionary<string, string> parms = new Dictionary<string, string>();
            StringBuilder sb = new StringBuilder();
            sb.Append("insert into " + table + " ( ");
            bool first = true;
            foreach (var prop in typeof(T).GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance))
            {
                var val = (prop.GetValue(item));
                if (val != null)
                {
                    parms["@" + prop.Name] = ConvertToString(val);
                    if (first == false) { sb.Append(","); }
                    sb.Append(prop.Name);
                    first = false;
                }
            }
            sb.Append(") values (");
            first = true;
            foreach (var prop in typeof(T).GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance))
            {
                var val = (prop.GetValue(item));
                if (val != null)
                {
                    if (first == false) { sb.Append(","); }
                    sb.Append("@" + prop.Name);
                    first = false;
                }
            }
            sb.Append(")");
            return Query(sb.ToString(), parms, transaction, 60 * 60 * 20);
        }


        public QueryResult Query(string sql, Dictionary<string,string> parameters = null,  Transaction transaction = null, int? overrideTimeout=null)
        {
            lock (connectionLock)
            {
                if (State != System.Data.ConnectionState.Open || IsPasswordExpired)
                {
                    Close();
                    Open();
                }
                var res = QueryRaw(sql, parameters, transaction,overrideTimeout);
                return res;
            }
        }


        protected abstract QueryResult QueryRaw(string sql, Dictionary<string, string> parameters, Transaction transaction, int? overrideTimeout = null);

    }
}
