﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database
{
    public class FilteredQueryResult
    {
        public FilteredQueryRequest Request { get; set; }
        public QueryResult Result { get; set; }

        public FilteredQueryResult() { }
        public FilteredQueryResult(FilteredQueryRequest req, QueryResult res)
        {
            Request = req;
            Result = res;
        }
    }
}
