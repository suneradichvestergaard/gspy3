﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database
{
    public abstract class DatabaseItem
    {
        public abstract string GetIndexColumn();
        public abstract string GetIndexColumnValue();
        public abstract string[] GetInsertIndexColumns();
        public abstract string[] GetInsertIndexColumnValues();
        public virtual string GetInsertIndexID()
        {
            return string.Join(';', GetInsertIndexColumnValues());
        }

    }
}
