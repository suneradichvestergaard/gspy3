﻿using gSpy3_Common.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using static gSpy3_Common.Module.ModuleBase;

namespace gSpy3_Common.Database.AccessInterface
{
    public class GroupedStatsManager
    {
        /// <summary>
        /// Must be set before using manager
        /// </summary>
        public static DatabaseManager DBManager = null;

        public static int GetHash(string str)
        {
            return (int)MurmurHash2.Hash(str);
        }

    
        public static bool DoesGroupedStatsExist(string idcompname, int grouptype)
        {
            int hash = GetHash(idcompname);
            string q = "select * from groupedteammatchstats where idgrouptype=" + grouptype + " and idcollectionhash=" + hash + "  limit 1";
            var res = DBManager.StatsDB.Query(q);
            return res != null && res.Rows.Count > 0;
        }

        public static DateTime GetLastGroupUpdate(string idcompname)
        {
            try
            {
                int hash = GetHash(idcompname);
                string q = "select value from modulestates where idmodule=" + (int)(ModuleEnum.gSpy3_Maintenance) + " and value!=\"\" and name=\"GroupUpdate_" + hash + "\"";
                var res = DBManager.BackendDB.Query(q);
                if (res != null && res.Rows.Count > 0)
                {
                    return DateTime.Parse(res.Rows[0][0]);
                }
                return DateTime.MinValue;
            }
            catch
            {
                
                return DateTime.MinValue;
            }
        }
        public static void SetLastGroupUpdate(string idcompname, DateTime when)
        {
            int hash = GetHash(idcompname);
            string q = "insert into modulestates (idmodule,name,value) values (" + (int)(ModuleEnum.gSpy3_Maintenance) + ",\"GroupUpdate_" + hash + "\",\""+when.ToDatabaseUniversalString()+"\")";
            if (GetLastGroupUpdate(idcompname).Equals(DateTime.MinValue) == false)
            {
                q = "update modulestates set value=\""+when.ToDatabaseUniversalString()+"\" where idmodule="+ (int)(ModuleEnum.gSpy3_Maintenance) + " and name=\"GroupUpdate_" + hash + "\"";

            }
            var res = DBManager.BackendDB.Query(q);
            if (!res.Success)
            {
                throw new Exception("Failed to set modulestate");
            }
        }
        public static void CreateGroupedStats(int idcompetition)
        {
            int hash = GetHash("" + idcompetition);
            CreateGroupedTeamStats(hash, "" + idcompetition);
            CreateGroupedPlayerStats(hash, "" + idcompetition);
            CreateGroupedCoachStats(hash, "" + idcompetition);
            CreateGroupedRaceStats(hash, "" + idcompetition);
        }
        public static void CreateGroupedTeamStats(int hash, string idcompname)
        {
            CreateGroupedStats(hash, idcompname, "s.idteam", "s.idteam","idcoach","0", "idrace", 0);
            SetLastGroupUpdate(idcompname, DateTime.Now);
        }
        public static void CreateGroupedCoachStats(int hash, string idcompname)
        {
            CreateGroupedStats(hash, idcompname, "t.idcoach", "0", "idcoach", "0", "0",1);
            SetLastGroupUpdate(idcompname, DateTime.Now);
        }
        public static void CreateGroupedPlayerStats(int hash, string idcompname)
        {
            CreateGroupedStats(hash, idcompname, "s.idplayer", "s.idteam", "0", "idplayer", "0",2);
            SetLastGroupUpdate(idcompname, DateTime.Now);
        }
        public static void CreateGroupedRaceStats(int hash, string idcompname)
        {
            CreateGroupedStats(hash, idcompname, "t.idrace", "0", "0", "0", "idrace",3);
            SetLastGroupUpdate(idcompname, DateTime.Now);
        }
        static string GetQuery(int hash, string idcompname,string idlist, string group, string idteam, string idcoach, string idplayer, string idrace, int grouptype)
        {
           return  "select "+grouptype+" as idgrouptype," + idteam + " as idteam," + idcoach + " as idcoach," + idplayer + " as idplayer," + idrace + " as idrace, " + hash + " as idcollectionhash, " +
                "sum(s.td) as td, " +
                (idplayer == "0" ? "sum(td_opp) as td_opp, " : "") +
                "sum(win) as wins, " +
                "sum(draw) as draws, " +
                "sum(loss) as losses, " +
                "(sum(win)+sum(draw)+sum(loss)) as gp, " +
                (idplayer == "0" ? "sum(conceded) as concedes, " : "") +
                (idplayer == "0" ? "sum(cash_spent) as cash_spent, " : "") +
                 (idplayer == "0" ? "sum(duration) as duration, " : "") +
                "sum(s.blocks_for) as blocks_for, " +
                "sum(s.breaks_for) as breaks_for, " +
                "sum(s.stuns_for) as stuns_for, " +
                "sum(s.kos_for) as kos_for, " +
                "sum(s.casualties_for) as casualties_for, " +
                "sum(s.kills_for) as kills_for, " +

                "sum(s.blocks_against) as blocks_against, " +
                "sum(s.breaks_against) as breaks_against, " +
                "sum(s.stuns_against) as stuns_against, " +
                "sum(s.kos_against) as kos_against, " +
                "sum(s.casualties_against) as casualties_against, " +
                "sum(s.kills_against) as kills_against, " +

                "sum(s.passes) as passes, " +
                "sum(s.catches) as catches, " +
                "sum(s.interceptions) as interceptions, " +
                "sum(s.completions) as completions, " +
                "sum(s.pushouts) as pushouts, " +
                "sum(s.turnovers) as turnovers, " +
                (idplayer == "0" ? "sum(pass_meters) as pass_meters, " : "") +
                (idplayer == "0" ? "sum(run_meters) as run_meters, " : "") +
                "sum(s.deflections) as deflections, " +
                "sum(s.expulsions) as expulsions, " +
                "sum(s.dodges) as dodges, " +
                "sum(s.fouls) as fouls, " +
                "sum(s.rushes) as rushes, " +
                (idplayer == "0" ? "sum(risk) as risk, " : "") +
                (idplayer == "0" ? "sum(luck) as luck, " : "") +
                "sum(s.pickups) as pickups, " +
                "sum(s.sacks) as sacks, " +
                "sum(s.boneheads) as boneheads " +
                "from " + (idplayer != "0" ? "playermatchstats" : "teammatchstats") + " as s " +((idplayer!="0")?"inner join teammatchstats as tms on tms.idteam=s.idteam and tms.idmatch=s.idmatch":"inner join matches as m on m.idmatch=s.idmatch ") + " inner join teams as t on t.idteam=s.idteam "  + " where s.idcompetition in (" + idlist + ") group by " +
                group;
        }
        static void CreateGroupedStats(int hash, string idcompname,string group, string idteam,string idcoach,string idplayer, string idrace, int grouptype)
        {
            DateTime t1 = DateTime.Now;

            // 1. prepare query for possibly many competitions
            List<CompetitionWrapper> results;
            Dictionary<string, List<CompetitionWrapper>> ambigous;
            string idlist = InputParser.GetCompetitionIDListFromIDNameOrAlias(LeagueManager.DBManager, idcompname, out results, out ambigous);

            if (ambigous != null && ambigous.Count > 0)
            {
                throw new Exception("Ambigous competition input: " + Environment.NewLine + string.Join(Environment.NewLine, ambigous.ToJSONString()));
            }
            if (results.Count == 0)
            {
                throw new Exception("No matching competition");
            }

            // 1.5 check if we should just skip this , in case it's playersstats and too big competition
            bool markEmpty = false;
            if (grouptype == 2) // playerstats
            {
                var numPl = DBManager.StatsDB.Query("select count(*) from matches where idcompetition in (" + idlist + ")", null, null, 60 * 60 * 20);
                if(int.Parse(numPl.Rows[0][0]) > 100000)
                {
                    markEmpty = true;
                }
            }

            // 2. Grab data
            Transaction trans = null;
            try
            {
                QueryResult stepRes = null;
                int numSteps = 1;
                if (grouptype == 2) // playerstats
                {
                    stepRes = DBManager.StatsDB.Query("select idteam from standings where idcompetition in (" + idlist + ")", null, trans, 60 * 60 * 20);
                    numSteps = stepRes.Rows.Count;
                }
                Console.WriteLine("");
                for (int i = 0; i < numSteps; i++)
                {
                    var query = GetQuery(hash, idcompname, idlist, group, idteam, idcoach, idplayer, idrace, grouptype);
                    if (grouptype == 2) // playerstats
                    {
                        Console.Write("\r" + i + " / " + numSteps + "  ");


                        var plTeamList = new List<string>();
                        for (int j = 0; i < numSteps && j < 10000; i++)
                        {
                            plTeamList.Add(stepRes.Rows[i][0]);
                            j++;
                        }
                        i--;


                        // query = query.Replace("where s.idcompetition in (" + idlist + ")", "where s.idteam=" + stepRes.Rows[i][0] + "");
                        query = query.Replace("where s.idcompetition in (" + idlist + ")", "where s.idteam in (" + string.Join(',', plTeamList) + ")");
                    }
                    var qres = DBManager.StatsDB.Query(query, null, trans, 60 * 60 * 20);

                    if (!qres.Success)
                    {
                        throw new Exception("Unable to get data: " + qres.Error);
                    }
                    if (qres.Rows.Count == 0 || markEmpty)
                    {
                        // No data. just put something there to shut them up
                        var row = new string[qres.Cols.Count];
                        foreach (var col in qres.Cols)
                        {
                            if (col.Key == "idgrouptype")
                                row[qres.Cols["idgrouptype"]] = "" + grouptype;
                            else if (col.Key == "idcollectionhash")
                                row[qres.Cols["idcollectionhash"]] = "" + hash;
                            else row[col.Value] = "0";

                        }
                        qres.Rows = new List<string[]>();
                        qres.Rows.Add(row);


                    }

                    StringBuilder inserter = new StringBuilder();
                    inserter.Append("replace into groupedteammatchstats (");
                    bool firstCol = true;
                    foreach (var col in qres.Cols)
                    {
                        if (!firstCol) inserter.Append(",");
                        firstCol = false;
                        inserter.Append(col.Key);
                    }
                    inserter.Append(") values ");
                    string baseSQL = inserter.ToString();
                    int count = 0;
                    StringBuilder sqlBuilder = new StringBuilder();
                    sqlBuilder.Append(baseSQL);
                    foreach (var row in qres.Rows)
                    {
                        if (count > 0) sqlBuilder.Append(",");
                        count++;

                        sqlBuilder.Append("(");
                        firstCol = true;
                        foreach (var col in qres.Cols)
                        {
                            if (!firstCol) sqlBuilder.Append(",");
                            firstCol = false;
                            sqlBuilder.Append(row[col.Value] == "" ? "null" : row[col.Value]);
                        }
                        sqlBuilder.Append(")");
                        if (count > 1000)
                        {

                            string sql = sqlBuilder.ToString();
                            var res = DBManager.StatsDB.Query(sql, null, trans, 60 * 60 * 20);
                            if (!res.Success)
                            {
                                throw new Exception("Unable to insert data: " + res.Error);
                            }

                            count = 0;
                            sqlBuilder = new StringBuilder();
                            sqlBuilder.Append(baseSQL);
                        }
                    }
                    if (count > 0)
                    {
                        string sql = sqlBuilder.ToString();
                        var res = DBManager.StatsDB.Query(sql, null, trans, 60 * 60 * 20);
                        if (!res.Success)
                        {
                            throw new Exception("Unable to insert data: " + res.Error);
                        }

                    }

                    if (markEmpty)
                    {
                        i = numSteps;
                    }

                }

                if (trans != null)
                {
                    trans.Commit();
                }
                trans = null;
            }catch(Exception ex)
            {
                if (trans != null)
                {
                    trans.Rollback();
                }
                trans = null;
                throw ex;
            }

            Console.WriteLine("Grouped stats in " + (int)(DateTime.Now - t1).TotalMilliseconds + " ms");

        }


    }
}
