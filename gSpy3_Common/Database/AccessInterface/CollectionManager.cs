﻿using gSpy3_Common.Database.Format;
using gSpy3_Common.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database.AccessInterface
{
    public class CollectionManager
    {
        /// <summary>
        /// Must be set before using manager
        /// </summary>
        public static DatabaseManager DBManager = null;


        public static List<Tuple<int,OriginID>> GetActiveLeagues()
        {
            var res = DBManager.StatsDB.Query("select idleague,idorigin from leagues where collecting=1", null);
            List<Tuple<int, OriginID>> list = new List<Tuple<int, OriginID>>();
            foreach(var item in res.Rows)
            {
                list.Add(new Tuple<int, OriginID>(int.Parse(item[0]), (OriginID)int.Parse(item[1])));
            }
            return list;
        }
        public static List<int> GetOfficialLeagues()
        {
            var res = DBManager.StatsDB.Query("select idleague from leagues where collecting=1 and league_name like 'Cabalvision Official League'", null);
            List<int> list = new List<int>();
            foreach (var item in res.Rows)
            {
                list.Add(int.Parse(item[0]));
            }
            return list;
        }
        public static List<int> GetCollectedLeagues()
        {
            var res = DBManager.BackendDB.Query("select idleague from leagueupdates where collecting=1", null);
            List<int> list = new List<int>();
            foreach (var item in res.Rows)
            {
                list.Add(int.Parse(item[0]));
            }
            return list;
        }
        public static void SetCollectedLeagues(List<Tuple<int, OriginID>> ids)
        {
            foreach(var id in ids)
            {
                DBManager.BackendDB.Query("replace leagueupdates set collecting=1,idleague=@idleague, idorigin=@idorigin, activation_date=@nowdate", new Dictionary<string, string>()
                {
                    {"@nowdate",""+DateTime.Now.ToDatabaseUniversalString() },
                    {"@idleague",""+id.Item1 },
                    {"@idorigin",""+(int)id.Item2 }
                });
            }
        }
        public static List<Tuple<int, OriginID>> GetNextLeaguesToCollect()
        {
            var res = DBManager.BackendDB.Query("select idleague,idorigin from leagueupdates where collecting=1 order by latest_check asc limit 100", null);
            List<Tuple<int, OriginID>> list = new List<Tuple<int, OriginID>>();
            foreach (var item in res.Rows)
            {
                list.Add(new Tuple<int, OriginID>(int.Parse(item[0]), (OriginID)int.Parse(item[1])));
            }
            return list;
        }
        public static void SetLatestLeagueCheck(List<int> leagues)
        {
            foreach(var l in leagues)
            {
                var res = DBManager.BackendDB.Query("update leagueupdates set latest_check=@last_check where idleague=@idleague",new Dictionary<string, string>()
                {
                    {"@last_check", DateTime.Now.ToDatabaseUniversalString() },
                    {"@idleague", ""+l }
                });

            }
        }
        public static void SetLatestGame(int idleague, string latest_game)
        {
          
                var res = DBManager.BackendDB.Query("update leagueupdates set latest_game=@latest_game where idleague=@idleague and (latest_game is null or latest_game < @latest_game)", new Dictionary<string, string>()
                {
                    {"@latest_game", latest_game },
                    {"@idleague", ""+idleague}
                });

            
        }
    }
}
