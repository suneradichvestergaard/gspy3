﻿using gSpy3_Common.Database.Format;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database.AccessInterface
{
    public class MatchManager
    {
        /// <summary>
        /// Must be set before using league manager
        /// </summary>
        public static DatabaseManager DBManager = null;
        public static List<PlayerMatchStats> AddIfMissing(List<PlayerMatchStats> playermatchstats, Transaction transaction = null)
        {
            if (playermatchstats.Count == 0) return new List<PlayerMatchStats>();
            return DBManager.StatsDB.InsertIfMissing("playermatchstats", playermatchstats, transaction);

        }
        public static List<Match> AddIfMissing(List<Match> matches, Transaction transaction = null)
        {
            if (matches.Count == 0) return new List<Match>();
            return DBManager.StatsDB.InsertIfMissing("matches", matches, transaction);

        }
        public static List<TeamMatchStats> AddIfMissing(List<TeamMatchStats> matches, Transaction transaction = null)
        {
            if (matches.Count == 0) return new List<TeamMatchStats>();
            return DBManager.StatsDB.InsertIfMissing("teammatchstats", matches, transaction);

        }
        public static bool DownloadReplay(string idcompname, UInt64 idmatch)
        {
            DBManager.StatsDB.Query("update matches set workstatus=0 where idmatch=" + idmatch);
            return true;
        }

        public static Match FindMatch(OriginID idorigin, UInt64 match_origin_id, Transaction trans = null)
        {
            var res = DBManager.StatsDB.Select<Match>("select * from matches where idorigin=@idorigin and match_origin_id=@match_origin_id", new Dictionary<string, string>()
            {
                {"@idorigin", ((int)idorigin).ToString() },
                {"@match_origin_id", ((UInt64)match_origin_id).ToString() },

            }, trans);
            if (res != null && res.Count > 0)
            {
                return res[0];
            }
            return null;
        }
    }
}
