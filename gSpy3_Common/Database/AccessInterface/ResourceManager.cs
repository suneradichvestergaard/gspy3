﻿using System;
using System.Collections.Generic;
using System.Text;
using gSpy3_Common.Helpers;

namespace gSpy3_Common.Database.AccessInterface
{
    public class ResourceManager
    {
        public enum ResourceID { BB2APIAccess }

        /// <summary>
        /// Must be set before using league manager
        /// </summary>
        public static DatabaseManager DBManager = null;

        public static void AddResourceUse(int idmodule, ResourceID idresource, int amount)
        {
            var res = DBManager.BackendDB.Query("insert into resourceusage set idmodule=@idmodule,idresource=@idresource, amount=@amount, usetime=@usetime",
                new Dictionary<string, string>()
                {
                    {"@idmodule", ""+idmodule },
                    {"@idresource", ""+(int)idresource },
                    {"@amount", ""+amount },
                    {"@usetime", ""+DateTime.Now.ToDatabaseUniversalString() },
                })
                ;
        }

        public static void CleanupResourceUse()
        {
            DBManager.BackendDB.Query("delete from resourceusage where usetime < @oldest", new Dictionary<string, string>()
            {
                {"@oldest", (DateTime.Now-new TimeSpan(2,0,0,0)).ToDatabaseUniversalString() }
            });
        }

        /// <summary>
        /// Try to request use of BB2API.
        /// If we are not throttled, then we return true and it's ok to continue. Otherwise sleep a while and try again
        /// </summary>
        /// <param name="idmodule"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public static bool UseBB2API(int idmodule, int amount)
        {
            try
            {
                if (IsBB2InsideLimit(amount) == false)
                {
                    return false;
                }

                // Ok, we got past the throttle
                AddResourceUse(idmodule, ResourceID.BB2APIAccess, amount);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        static bool _BB2WasWithinLimit = true;

        public static bool IsBB2InsideLimit(int amount)
        {
            var duration = new TimeSpan(1, 0, 0, 0);
            var res = DBManager.BackendDB.Query("select min(usetime),max(usetime),sum(amount) from resourceusage where usetime > @oldest and idresource=@idresource", new Dictionary<string, string>()
                {
                    {"@oldest", (DateTime.Now-duration).ToDatabaseUniversalString() },
                    {"@idresource", ""+(int)ResourceID.BB2APIAccess },
                });
            if (res.Rows.Count > 0 && res.Rows[0][0] != "")
            {
                string sMinDate = res.Rows[0][0];
                string sMaxDate = res.Rows[0][1];
                string sAmount = res.Rows[0][2];
                int amountRet = int.Parse(sAmount);
                if (amountRet > 0)
                {
                    DateTime minDate = DateTime.Parse(sMinDate + "Z");
                    DateTime maxDate = DateTime.Now;
                    if (duration.TotalMinutes > 0)
                    {
                        double callsPerMinute = (amount + amountRet) / duration.TotalMinutes;
                        if (callsPerMinute > 17)
                        {
                            if (_BB2WasWithinLimit)
                            {
                                Console.WriteLine("BB2API throttled at " + callsPerMinute + " /min");
                            }
                            _BB2WasWithinLimit = false;
                            return false;
                        }
                    }
                }
            }
            _BB2WasWithinLimit = true;
            return true;
        }
    }
}
