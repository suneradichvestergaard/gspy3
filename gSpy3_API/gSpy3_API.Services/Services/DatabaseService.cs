﻿using AutoMapper;
using gSpy3_API.API.Common.Settings;
using gSpy3_API.Services.Contracts;
using gSpy3_Common.Database;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_API.Services.Services
{
    public class DatabaseService : IDatabaseService
    {
        DatabaseManager _dbManager;


        public DatabaseService(DatabaseManager dbManager)
        {
            _dbManager = dbManager;
        }

        public DatabaseConnection GetBackendConnection()
        {
            return _dbManager.BackendDB;
        }

        public DatabaseConnection GetStatsConnection()
        {
            return _dbManager.StatsDB;
        }
    }
}
