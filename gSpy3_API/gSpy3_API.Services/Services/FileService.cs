﻿using gSpy3_API.Services.Contracts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace gSpy3_API.Services
{
    public class FileService : IFileService
    {
        string _basePath;


        public FileService(string basePath)
        {
            _basePath = basePath;
        }

        public string LoadFile(string path)
        {
            return File.ReadAllText(Path.Combine(_basePath,path));
        }


    }
}
