﻿using gSpy3_Common.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_API.Services.Contracts
{
    public interface IDatabaseService
    {
        DatabaseConnection GetBackendConnection();
        DatabaseConnection GetStatsConnection();
    }
}
