﻿using AutoMapper;
using DC = gSpy3_API.API.DataContracts;
using S = gSpy3_API.Services.Model;

namespace gSpy3_API.IoC.Configuration.AutoMapper.Profiles
{
    public class APIMappingProfile : Profile
    {
        public APIMappingProfile()
        {
            CreateMap<DC.User, S.User>().ReverseMap();
            CreateMap<DC.Address, S.Address>().ReverseMap();
        }
    }
}
