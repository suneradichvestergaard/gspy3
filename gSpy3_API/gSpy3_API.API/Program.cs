﻿using gSpy3_Common;
using gSpy3_Common.API.BB2;
using gSpy3_Common.Database;
using gSpy3_Common.Database.AccessInterface;
using gSpy3_Common.Module;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using System;
using System.IO;
using System.Net;
using System.Reflection;
using System.Timers;


#pragma warning disable CS1591
namespace gSpy3_API.API
{
    public class Program : ModuleBase
    {
        Log log = new Log();
        Timer heartbeatTimer = null;
        static DatabaseManager dbManager = null;
        static BB2API bb2API = null;
        public static void Main(string[] args)
        {
            try
            {
                PrivateSettings settings = null;
                settings = PrivateSettings.Load();
                dbManager = new DatabaseManager(settings);
                bb2API = new BB2API(settings.BB2APIKey);
                LeagueManager.BB2API = bb2API;
                LeagueManager.ModuleID = (int)ModuleEnum.gSpy3_API;
                Program prg = new Program(args);
            }catch(Exception ex)
            {
                Log log = new Log();
                log.Error("Error", ex);
            }
        }

        protected Program(string[] args) : base(ModuleBase.ModuleEnum.gSpy3_API, dbManager)
        {
            log.Info("Starting gSpy3_API v " + Assembly.GetExecutingAssembly().GetName().Version);

            heartbeatTimer = new Timer(1); // trigger right away and set interval in callback 
            heartbeatTimer.Elapsed += HeartbeatTimer_Elapsed;
            heartbeatTimer.Start();

            Startup.DBManager = dbManager;
            
            CreateWebHostBuilder(args).Build().Run();

            log.Info("Program exit");
        }

        private void HeartbeatTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            heartbeatTimer.Enabled = false;
            heartbeatTimer.Interval = 1000 * 60; // Beat once per minute
            base.Tick(); // Tick to send heartbeat
            heartbeatTimer.Enabled = true;
        }

        //.NET CORE 3.0
        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
        
            WebHost.CreateDefaultBuilder(args)
       
          .ConfigureKestrel(options =>
          {
              Log log = new Log();
              log.Info("Listening to 667");
              options.Listen(IPAddress.Any, 667);

              string path = "server.pfx";
              if (File.Exists(Path.GetFullPath(path)) == false) { path = "..\\" + path; }
              if (File.Exists(Path.GetFullPath(path)) == false) { path = "..\\" + path; }
              if (File.Exists(Path.GetFullPath(path)) == false) { path = "..\\" + path; }
              path = Path.GetFullPath(path);
              if (File.Exists(path))
              {
                  string pass = File.ReadAllText(path + ".pass");
                  log.Info("Listening to 666");
                  options.Listen(IPAddress.Any, 666,
                  listenOptions =>
                  {
                      listenOptions.UseHttps(path,
                          pass);
                  });

                  options.Listen(IPAddress.Any, 6666,
                 listenOptions =>
                 {
                     listenOptions.UseHttps(path,
                         pass);
                 });
              }
              else
              {
                  log.Info("Unable to find certificate: " + path);

              }

          }).UseStartup<Startup>();


    }
}
