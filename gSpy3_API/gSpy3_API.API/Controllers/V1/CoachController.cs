﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gSpy3_API.API.Controllers.V1
{
    using global::gSpy3_API.API.DataContracts.Requests;
    using global::gSpy3_API.Services.Contracts;
    using gSpy3_Common;
    using gSpy3_Common.API.LegacyGoblinSpy.DataFormat;
    using gSpy3_Common.Database;
    using gSpy3_Common.Database.AccessInterface;
    using gSpy3_Common.Database.Format;
    using gSpy3_Common.Helpers;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Threading.Tasks;

    namespace gSpy3_API.API.Controllers.V1
    {
        [ApiVersion("1.0")]
        [Route("api/comp")]//required for default versioning
        [Route("api/v{version:apiVersion}/coach")]
        [ApiController]
        public class CoachController : Controller
        {
            Log log = new Log();
            private readonly IDatabaseService _db;

            public CoachController(IDatabaseService dbManager)
            {
                _db = dbManager;
            }

            /// <summary>
            /// Change media links for a coach
            /// Media data is {"youtube":URL,"twitch":URL} in body
            /// </summary>
            /// <param name="idcoach"></param>
            /// <param name="media"></param>
            /// <returns></returns>
            [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
            [HttpPost("{idcoach}/media")]
            public async Task<bool> Media(int idcoach, [FromBody] CoachMediaRequest media)
            {
                log.Info("Media " + idcoach, media);
                return CoachManager.AlterMedia(idcoach, media.youtube, media.twitch);
            }


        }

    }

}
