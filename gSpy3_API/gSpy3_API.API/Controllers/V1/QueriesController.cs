﻿using BB2StatsLib;
using BB2StatsLib.BB2Replay;
using gSpy3_API.API.DataContracts;
using gSpy3_API.API.DataContracts.Requests;
using gSpy3_API.API.DataContracts.Responses;
using gSpy3_API.Services.Contracts;
using gSpy3_Common;
using gSpy3_Common.Database;
using gSpy3_Common.Database.AccessInterface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure.MappingViews;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace gSpy3_API.API.Controllers.V1
{
    [ApiVersion("1.0")]
    [Route("api/queries")]//required for default versioning
    [Route("api/v{version:apiVersion}/queries")]
    [ApiController]
    public class QueriesController : Controller
    {
        Log log = new Log();
        private readonly IDatabaseService _db;
        private readonly IFileService _files;
      
        public QueriesController(IDatabaseService dbManager, IFileService fileService)
        {
            _files = fileService;
            _db = dbManager;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        /// <param name="format">cvs,json,scsv</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(string))]
        [HttpGet("{format}")]
        public async Task GetFormatted([FromQuery] string req,string format)
        {
            var eformat =(FormatEnum) Enum.Parse(typeof(FormatEnum), format);
            var res = await Get(req);
            var sb = new StringBuilder();
            var first = res.Response.Values.First();
            string separator = ",";
            switch (eformat)
            {
                case FormatEnum.json:
                    base.Response.Headers.Add("Content-Type", "application/json");
                    base.Response.Headers.Add("Content-Disposition", "attachment;filename=query.json");
                    await base.Response.Body.WriteAsync(UTF8Encoding.UTF8.GetBytes(Newtonsoft.Json.JsonConvert.SerializeObject(res, Formatting.Indented)));
                    return;
                case FormatEnum.scsv:
                    separator = ";";
                    for (int i = 0; i < first.Result.Cols.Count; i++)
                    {
                        if (i != 0) sb.Append(separator);
                        foreach (var col in first.Result.Cols)
                        {
                            if (col.Value == i)
                            {
                                sb.Append(col.Key);
                                break;
                            }
                        }
                    }
                    sb.Append(Environment.NewLine);
                    foreach (var row in first.Result.Rows)
                    {
                        for (int i = 0; i < row.Length; i++)
                        {
                            if (i != 0) sb.Append(separator);
                            sb.Append(row[i]);
                        }
                        sb.Append(Environment.NewLine);
                    }
                    base.Response.Headers.Add("Content-Type","text/csv");
                    base.Response.Headers.Add("Content-Disposition", "attachment;filename=query.csv");
                    await base.Response.Body.WriteAsync(UTF8Encoding.UTF8.GetBytes(sb.ToString()));
                    return;
                case FormatEnum.csv:
                    for (int i = 0; i < first.Result.Cols.Count; i++)
                    {
                        if (i != 0) sb.Append(separator);
                        foreach (var col in first.Result.Cols)
                        {
                            if (col.Value == i)
                            {
                                sb.Append(col.Key);
                                break;
                            }
                        }
                    }
                    sb.Append(Environment.NewLine);
                    foreach (var row in first.Result.Rows)
                    {
                        for (int i = 0; i < row.Length; i++)
                        {
                            if (i != 0) sb.Append(separator);
                            sb.Append(row[i]);
                        }
                        sb.Append(Environment.NewLine);
                    }

                    base.Response.Headers.Add("Content-Type", "text/csv");
                    base.Response.Headers.Add("Content-Disposition", "attachment;filename=query.csv");
                    await base.Response.Body.WriteAsync(UTF8Encoding.UTF8.GetBytes(sb.ToString()));
                    return;
             
            }
            throw new Exception("Unknown format");
        }

        /// <summary>
        /// Returns the results of a collection of filtered queries
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name="req">
        /// JSON dictionary object, where the key is used to keep track of each query (and can therefore be anything) and the value contains the data of the query<br></br>
        /// {<br></br>
        ///   "q1" : {<br></br>
        ///             "id": "string", // This is the id of the base query: leagues, compTeamSchedule, <br></br>
        ///             "filters" : {}, // Filters that adjust the query <br></br>
        ///             "order": "string", // Ordering. "asc" or "desc"<br></br>
        ///             "ordercol":"string", // Semicolon separated list of columns to order on<br></br>
        ///             "from": num, <br></br>
        ///             "limit" :num<br></br>
        ///             
        ///          }
        /// }
        ///   
        /// </param>
        /// <returns>
        /// Returns a collection of query results
        /// </returns>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(QueriesResponse))]
        [HttpGet]
        public async Task<QueriesResponse> Get([FromQuery] string req)
        {
            DateTime start = DateTime.Now;
            if (req == null) { return null; }
            QueriesRequests qreq = JsonConvert.DeserializeObject<QueriesRequests>(req.Replace("%26","&"));
            QueriesResponse res = new QueriesResponse();
            bool success = true;
            foreach(var q in qreq)
            {
                try
                {
                    bool isCollectionCompetition = q.Value.id == "collectionCompetitions" || q.Value.id == "collectionTeams";
                    bool isIdcompetitionvalue = q.Value.idmap != null && q.Value.idmap.ContainsKey("idcompetition") && InputParser.IsIDCompetitionValue(q.Value.idmap["idcompetition"]);
                    QueryResult qres = null;
                    Dictionary<string, string> replacer = new Dictionary<string, string>();
                    string alteredCompName = null;
                    string alteredCompID = null;
                    string savedTeamID = null;
                    string savedCompetitionID = null;
                    string alteredQueryID = null;
                    int idorigin = 0;

                    #region Special pre-processing
                    if (isIdcompetitionvalue) // Have to adjust idcompetition to handle multiple idcompetitions
                    {
                        List<CompetitionWrapper> results;
                        Dictionary<string, List<CompetitionWrapper>> ambigous;
                        replacer["in:idcompetition"] = InputParser.GetCompetitionIDListFromIDNameOrAlias(LeagueManager.DBManager, q.Value.idmap["idcompetition"], out results, out ambigous);
                        if (ambigous != null && ambigous.Count > 0)
                        {
                            throw new Exception("Ambigous competition input: " + Environment.NewLine + string.Join(Environment.NewLine, ambigous.ToJSONString()));
                        }
                        if (results.Count == 0)
                        {
                            throw new Exception("No matching competition");
                        }
                        alteredCompName = results[0].name.StartsWith("@") ? results[0].name : null;
                        alteredCompID = q.Value.idmap["idcompetition"];
                        if (results.Count > 0)
                        {
                            idorigin = (int)results[0].idorigin;
                        }
                    }
                    else if (isCollectionCompetition) // Collections are already handled by InputParser, so this should be rewritten to use that instead
                    {
                        Dictionary<string, string> compfilter = new Dictionary<string, string>();
                        var filterRes = _db.GetStatsConnection().Query("select filter,idleague from collections where idcollection=@idcoll", new Dictionary<string, string>()
                            {
                                {"@idcoll", q.Value.idmap["idcollection"] }
                            });
                        FilteredQueryRequest tmpReq = Newtonsoft.Json.JsonConvert.DeserializeObject<FilteredQueryRequest>(filterRes.Rows[0][0]);
                        tmpReq.id = "leagueComps";
                        tmpReq.limit = 1000;
                        var tmpReqSql = FilteredQueryManager.Get().CreateSQL(tmpReq);
                        var compRes = _db.GetStatsConnection().Query(tmpReqSql.Item1, tmpReqSql.Item2);
                        string str = "";
                        var idcol = compRes.Cols["idcompetition"];
                        foreach (var row in compRes.Rows)
                        {
                            if (str == "") str = "in ("; else str += ",";
                            str += row[idcol];
                        }
                        str += ")";

                        replacer["in:idcompetition"] = str;

                        if (q.Value.filters != null && q.Value.filters.ContainsKey("idcollection"))
                        {
                            q.Value.filters.Remove("idcollection");
                        }
                        if (q.Value.idmap != null && q.Value.idmap.ContainsKey("idcollection"))
                        {
                            q.Value.idmap.Remove("idcollection");
                        }
                        if (q.Value.idmap == null) q.Value.idmap = new Dictionary<string, string>();
                        q.Value.idmap["idleague"] = filterRes.Rows[0][1];

                        var league = LeagueManager.FindLeague(int.Parse(filterRes.Rows[0][1]));
                        idorigin = (int)league.idorigin;
                    }
                    else if (q.Value.idmap != null && q.Value.idmap.ContainsKey("idcompetition"))
                    {
                        try
                        {
                            var comp = LeagueManager.FindCompetition(int.Parse(q.Value.idmap["idcompetition"]));
                            idorigin = (int)comp.idorigin;
                        }
                        catch { }
                    }
                    #region Coach names in id field
                    // Special handling for coach names in the id field
                    string idcoachReplaced = null;
                    if (q.Value.idmap != null && q.Value.idmap.ContainsKey("idcoach"))
                    {
                        var key = q.Value.idmap["idcoach"];
                        if (key.Length > 0 && char.IsNumber(key[0]) == false)
                        {
                            bool foundPlatformID = false;
                            foreach(var platform in Enum.GetNames(typeof(gSpy3_Common.Database.Format.OriginID)))
                            {
                                var idorg = (gSpy3_Common.Database.Format.OriginID)Enum.Parse(typeof(gSpy3_Common.Database.Format.OriginID), platform);
                                var platkey = gSpy3_Common.Database.Format.OriginConversion.ToLegacyGoblinSpy(idorg);
                                if (key.StartsWith(platkey + "_"))
                                {
                                    foundPlatformID = true;
                                    var coachRes = _db.GetStatsConnection().Query("select idcoach from coaches where coach_origin_id=@cid and idorigin=@idorg limit 1", new Dictionary<string, string>()
                                        {
                                            {"@cid", key.Substring((platkey+"_").Length) },
                                            { "@idorg", ""+(int)idorg }
                                        });
                                    if (coachRes.Success && coachRes.Rows.Count > 0)
                                    {
                                        idcoachReplaced = key;
                                        q.Value.idmap["idcoach"] = coachRes.Rows[0][0];
                                    }
                                    break;
                                }
                                
                            }
                            if (!foundPlatformID)
                            {
                                bool isFixed = true;
                                var coachRes = _db.GetStatsConnection().Query("select idcoach from coaches where coach_name " + (isFixed ? "=" : "like") + " @coach " + (idorigin == 0 ? "" : ("and idorigin=" + idorigin)) + " limit 1", new Dictionary<string, string>()
                            {
                                {"@coach", isFixed?key: "%"+key+"%" }
                            });
                                if (coachRes.Success && coachRes.Rows.Count > 0)
                                {
                                    idcoachReplaced = key;
                                    q.Value.idmap["idcoach"] = coachRes.Rows[0][0];
                                }
                            }
                        }
                    }
                    #endregion
                    #region Team names in id field
                    // Special handling for team names in the id field
                    string idteamreplaced = null;
                    if (q.Value.idmap != null && q.Value.idmap.ContainsKey("idteam"))
                    {
                        bool isFixed = true;
                        var key = q.Value.idmap["idteam"];
                        if (key.Length > 0 && char.IsNumber(key[0]) == false)
                        {
                            var teamRes = _db.GetStatsConnection().Query("select idteam from standingsview where team_name " + (isFixed ? "=" : "like") + " @team order by position asc  limit 10", new Dictionary<string, string>()
                            {
                                {"@team", isFixed?key: "%"+key+"%"  }
                            });
                            if (teamRes.Success && teamRes.Rows.Count > 0)
                            {
                                idteamreplaced = key;
                                q.Value.idmap["idteam"] = teamRes.Rows[0][0];
                            }
                        }
                    }
                    #endregion
                    #region Match id in id field
                    // Special handling for team names in the id field
                    string idmatchreplaced = null;
                    if (q.Value.idmap != null && q.Value.idmap.ContainsKey("idmatch"))
                    {
                        var sIdMatch = q.Value.idmap["idmatch"];
                        if (sIdMatch.Contains("_"))
                        {
                            var sNonHex = Int64.Parse(sIdMatch.Substring(sIdMatch.IndexOf("_") + 1), System.Globalization.NumberStyles.HexNumber);
                            var mres = _db.GetStatsConnection().Query("select idmatch from matches where match_origin_id=" + sNonHex);
                            if (mres.Success && mres.Rows.Count > 0)
                            {
                                idmatchreplaced = sIdMatch;
                                q.Value.idmap["idmatch"] = mres.Rows[0][0];
                            }
                        }
                         
                    }
                    #endregion
                    #region File Loading
                    if (q.Value.id == "matchReplayStats")
                    {
                        var sIdMatch = q.Value.idmap["idmatch"];
                        QueryResult matchres = new QueryResult();
                        var idmatch = UInt64.Parse(q.Value.idmap["idmatch"]);
                        matchres = _db.GetStatsConnection().Query("select match_origin_id,finished from matches where idmatch=" + idmatch);
                        var finished = matchres.Rows[0][1];
                        string filename = Path.Combine("matches", finished.Substring(0, finished.IndexOf(" ")), UInt64.Parse(matchres.Rows[0][0]).ToString("x") + ".bbrz.json");
                        qres = new QueryResult();
                        qres.FileData = _files.LoadFile(filename);
                        qres.Success = !string.IsNullOrEmpty(qres.FileData);
                        res.Response[q.Key] = new FilteredQueryResult(q.Value, qres);
                        success &= qres.Success;
                        continue;
                    }
                    #endregion
                    #region CompTeamStanding
                    if (q.Value.id == "compTeamStanding") // Need to grab position of team before performing standings query
                    {
                        var sql = FilteredQueryManager.Get().CreateSQL(new FilteredQueryRequest()
                        {
                         id= "teamPos", idmap = q.Value.idmap
                        }, replacer);
                        string keySql = sql.Item1;
                        sql = new Tuple<string, Dictionary<string, string>>(keySql, sql.Item2);
                        qres = _db.GetStatsConnection().Query(sql.Item1, sql.Item2);

                        q.Value.from = int.Parse(qres.Rows[0][0]);
                        q.Value.limit = 20;
                        savedTeamID = q.Value.idmap["idteam"];
                        q.Value.idmap.Remove("idteam"); // Remove to avoid getting only standing for this team. Need to add it afterwards
                        

                    }
                    #endregion
                    #region Followed collections
                    // Collections are not normally permitted inside a complex query, but we make an exception for getting the followed competitions
                    if (q.Value.filters !=null && q.Value.filters.ContainsKey("idcompetition") && q.Value.filters["idcompetition"].StartsWith("followed:"))
                    {
                        List<string> altered = new List<string>();
                        string[] splits = q.Value.filters["idcompetition"].Substring("followed:".Length).Split('|');
                        foreach(var split in splits)
                        {
                            int id = 0;
                            if(int.TryParse(split,out id)) { altered.Add(split); }
                            else
                            {
                                List<CompetitionWrapper> results;
                                Dictionary<string, List<CompetitionWrapper>> ambigous;
                                var newSplit = InputParser.GetCompetitionIDListFromIDNameOrAlias(LeagueManager.DBManager,split, out results, out ambigous);
                                foreach(var idres in results) { altered.Add(""+idres.idcompetition); }
                            }
                        }
                        q.Value.filters["idcompetition"] = string.Join('|', altered);
                        
                    }
                    #endregion
                    #region Grouped stats
                    // Special handling of grouped stats
                    if (q.Value.id.StartsWith("compStats"))
                    {
                        alteredQueryID = q.Value.id;

                        int groupType = 0;
                        string sGroup = "idteam";
                        if (q.Value.id == "compStatsCoaches") { groupType = 1; sGroup = "idcoach"; }
                        if (q.Value.id == "compStatsPlayer") { groupType = 2; sGroup = "idplayer"; }
                        if (q.Value.id == "compStatsRaces") { groupType = 3; sGroup = "idrace"; }
                        if (q.Value.id == "compStatsTeam") { groupType = 0; sGroup = "idteam"; }
                        string idcomp = "";
                        if(q.Value.filters==null || !q.Value.filters.TryGetValue("idcompetition", out idcomp))
                        {
                            if (q.Value.idmap != null)
                            {
                                q.Value.idmap.TryGetValue("idcompetition", out idcomp);
                            }
                        }
                        if (GroupedStatsManager.DoesGroupedStatsExist(idcomp, groupType) == false)
                        {
                            if (groupType != 2)
                            {
                                q.Value.id = "compTeamwStats";
                                if (string.IsNullOrEmpty(q.Value.aggr))
                                    q.Value.aggr = "sum";
                                q.Value.group = sGroup;
                            } else
                            {
                                q.Value.id = "compPlayers";
                                q.Value.group = "idplayer";
                                if (string.IsNullOrEmpty(q.Value.aggr))
                                    q.Value.aggr = "sum";
                                q.Value.group = sGroup;
                            }
                        }
                        else
                        {
                            q.Value.id = "compStatsTeam";
                            if (q.Value.aggr == "avg")
                            {
                                q.Value.id = "compStatsTeamAvg";
                            }
                            
                            
                            q.Value.idmap["idcollectionhash"] = "" + GroupedStatsManager.GetHash(q.Value.idmap["idcompetition"]);
                            savedCompetitionID = q.Value.idmap["idcompetition"];
                            q.Value.idmap.Remove("idcompetition");
                            q.Value.idmap["idgrouptype"] = ""+ groupType;
                            

                        }
                    }
                    #endregion

                    #region Merged standings of collections
                    if(q.Value.id == "compStandings" && replacer.ContainsKey("in:idcompetition"))
                    {
                        alteredQueryID = "compStandings";
                        q.Value.id = "compStandingsGrouped";
                    }
                    #endregion

                    #region Overtake
                    if (q.Value.id == "overtake")
                    {
                        string from = q.Value.idmap["frompos"];
                        string to = q.Value.idmap["topos"];
                        string qstr = "select * from standingsview where ( position=@from or position=@to)";
                        Dictionary<string, string> map = new Dictionary<string, string>();
                        List<CompetitionWrapper> results;
                        Dictionary<string, List<CompetitionWrapper>> ambigous;
                        var inList = InputParser.GetCompetitionIDListFromIDNameOrAlias(LeagueManager.DBManager, q.Value.idmap["idcompetition"], out results, out ambigous);
                        
                        map["@from"] = from;
                        map["@to"] = to;
                        var overtakeResp =  _db.GetStatsConnection().Query(qstr+" and idcompetition in ("+inList+")", map, null, null);
                        res.Response[q.Key] = new FilteredQueryResult(q.Value,overtakeResp);
                        continue;
                    }
                    #endregion


                    #endregion
                    // Main query path
                    {
                        DateTime t1 = DateTime.Now;
                        var sql = FilteredQueryManager.Get().CreateSQL(q.Value, replacer);

                        string keySql = sql.Item1;
                        sql = new Tuple<string, Dictionary<string, string>>(keySql, sql.Item2);

                        DateTime t2 = DateTime.Now;

                        qres = _db.GetStatsConnection().Query(sql.Item1, sql.Item2, null,10);
                        qres.From = q.Value.from;
                        qres.Limit = q.Value.limit;
                        if (qres.Limit != null && qres.Limit > 0)
                        {
                            qres.Page = 1 + qres.From / qres.Limit;

                        }
                        #region Post processing
                        // Post processing
                        if (alteredCompName != null && qres.Cols.ContainsKey("competition_name"))
                        {
                            int col = qres.Cols["competition_name"];
                            foreach (var row in qres.Rows) { row[col] = alteredCompName; }
                        }
                        if (alteredCompID != null && qres.Cols.ContainsKey("idcompetition"))
                        {
                            int col = qres.Cols["idcompetition"];
                            foreach (var row in qres.Rows) { row[col] = alteredCompID; }

                        }
                        if (idcoachReplaced != null)
                        {
                            q.Value.idmap["idcoach"] = idcoachReplaced;
                        }
                        if (idteamreplaced != null)
                        {
                            q.Value.idmap["idteam"] = idteamreplaced;
                        }
                        if (idmatchreplaced != null)
                        {
                            q.Value.idmap["idmatch"] = idmatchreplaced;
                        }
                        if (savedTeamID!=null)
                        {
                            q.Value.idmap["idteam"] = savedTeamID;
                        }
                        if (savedCompetitionID != null)
                        {
                            q.Value.idmap["idcompetition"] = savedCompetitionID;
                        }
                        if (alteredQueryID != null)
                        {
                            q.Value.id = alteredQueryID;
                        }
                        #endregion

                        DateTime t3 = DateTime.Now;
                        System.Diagnostics.Debug.WriteLine("t1: " + (t2 - t1).TotalMilliseconds + "ms");
                        System.Diagnostics.Debug.WriteLine("t2: " + (t3 - t2).TotalMilliseconds + "ms");

                    }
                    res.Response[q.Key] = new FilteredQueryResult(q.Value, qres);
                    success &= qres.Success;
                }catch(Exception ex)
                {
                    success = false;
                    res.Response[q.Key] = new FilteredQueryResult(q.Value, new QueryResult()
                    {
                        Success = false,
                        Error = ex.ToString()
                    })
                    ;
                }
            }
            res.TimeMs = (int)(DateTime.Now - start).TotalMilliseconds;
            res.Success = success;
            return res;
        }
    }
   
}
