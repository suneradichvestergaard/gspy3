﻿using gSpy3_API.API.DataContracts;
using gSpy3_API.API.DataContracts.Requests;
using gSpy3_API.API.DataContracts.Responses;
using gSpy3_API.Services.Contracts;
using gSpy3_Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gSpy3_API.API.Controllers.V1
{
    [ApiVersion("1.0")]
    [Route("api/server")]//required for default versioning
    [Route("api/v{version:apiVersion}/server")]
    [ApiController]
    public class ServerController : Controller
    {
        Log log = new Log();
        private readonly IDatabaseService _db;

        public ServerController(IDatabaseService dbManager)
        {
            _db = dbManager;
        }

        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(QueriesResponse))]
        [HttpGet("status")]
        public async Task<ServerStatus> Get()
        {

            ServerStatus status = new ServerStatus();

            var backend = _db.GetBackendConnection();
            status.Modules = backend.Select<ModuleStatus>("select * from modules",null,null);
       
            foreach(var mod in status.Modules)
            {
                mod.work = backend.Select<WorkStatus>("select * from work where idmodule=" + mod.idmodule + " order by started desc limit 10", null, null);
            }

            return status;
        }
    }
}
