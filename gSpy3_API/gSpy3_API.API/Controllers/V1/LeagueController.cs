﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gSpy3_API.API.Controllers.V1
{
    using global::gSpy3_API.Services.Contracts;
    using gSpy3_Common;
    using gSpy3_Common.API.LegacyGoblinSpy.DataFormat;
    using gSpy3_Common.Database;
    using gSpy3_Common.Database.AccessInterface;
    using gSpy3_Common.Database.Format;
    using gSpy3_Common.Helpers;
    using gSpy3_Common.Module;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Threading.Tasks;

    namespace gSpy3_API.API.Controllers.V1
    {
        [ApiVersion("1.0")]
        [Route("api/league")]//required for default versioning
        [Route("api/v{version:apiVersion}/league")]
        [ApiController]
        public class LeagueController : Controller
        {
            Log log = new Log();
            private readonly IDatabaseService _db;

            public LeagueController(IDatabaseService dbManager)
            {
                _db = dbManager;
            }

            /// <summary>
            /// Activates a new league for collecting
            /// </summary>
            /// <remarks>
            /// </remarks>
            /// <param name="origin">
            /// Origin where the data is collected from.
            /// </param>
            /// <param name="names">
            /// JSON Array of league names on body. Must match the in-game name exactly in order to collect properly
            /// </param>
            /// <returns>
            /// </returns>
            [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
            [HttpPost("activate/{origin}")]
            public async Task<RegisterResult> Activate(string origin,[FromBody] string[] names)
            {
                int idorigin = 0;
                try
                {
                    idorigin = (int)Enum.Parse(typeof(OriginID), origin);
                }
                catch
                {
                    idorigin = (int)OriginConversion.FromLegacyGoblinSpy(origin);
                }
                log.Info("Activate " + origin, names);
                List<League> list = new List<League>();
                foreach(var name in names)
                {
             

                    list.Add(new League()
                    {
                        idorigin = idorigin,
                        league_name = name,
                        collecting = 1
                    });
                }
                return  LeagueManager.RegisterLeague(list);

            }

            /// <summary>
            /// Request collection of missing data
            /// </summary>
            /// <param name="idleague"></param>
            /// <param name="from">From datetime (UTC) to collect</param>
            /// <param name="to">To datetime (UTC) to collect</param>

            /// <returns></returns>
            [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
            [HttpPost("{idleague}/request/collection/{from}/{to}")]
            public async Task<bool> RequestCollection(int idleague, string from, string to)
            {
                log.Info("RequestCollection " + idleague, from+" <-> "+to);
                var league = LeagueManager.FindLeague(idleague);
                string data = "{\"idleagues\":[" + league.idleague + "],\"platform\":\"" + OriginConversion.ToLegacyGoblinSpy((OriginID)league.idorigin) + "\",\"from\":\"" + DateTime.Parse(from).ToDatabaseUniversalString() + "\",\"to\":\"" + DateTime.Parse(to).ToDatabaseUniversalString() + "\"}";
                var res = _db.GetBackendConnection().Query("insert into work (idmodule,command,data,weight,added) values (@idmodule,@command,@data,@weight,@added)", new Dictionary<string, string>()
                {
                    { "@idmodule", ""+(int)ModuleBase.ModuleEnum.gSpy3_BB2MatchCollector },
                    {"@command","RequestUpdateLeagues" },
                    { "@data", data},
                    { "@weight", ""+100},
                    { "@added", DateTime.Now.ToDatabaseUniversalString()},
                });
                return res.Success;
            }




            /// <summary>
            /// Get competitions belonging to a league together with league id
            /// </summary>
            /// <remarks>
            /// </remarks>
            /// <param name="idorigin">
            /// Data origin (BB2_PC, BB2_PS4, BB2_XB1)
            /// </param>
            /// <param name="league">
            /// Name of league
            /// </param>
            /// <returns>
            /// </returns>
            [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
            [HttpGet("competitions/{idorigin}/{league}")]
            public async Task<QueryResult> GetCompetitions(OriginID idorigin, string league)
            {
                return LeagueManager.GetCompetitions(idorigin,league);

            }

        }




    }

}
