﻿using gSpy3_API.API.DataContracts.Requests;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_API.API.DataContracts.Responses
{
    public class UpdateSchedulesResponse
    {
        public bool success { get; set; }
        public string error{ get; set; }
        public List<UpdateSchedulesResponseItem> schedules { get; set; }
    }

    public class UpdateSchedulesResponseItem : UpdateSchedulesItem
    {
        public bool success { get; set; }
    }
}
