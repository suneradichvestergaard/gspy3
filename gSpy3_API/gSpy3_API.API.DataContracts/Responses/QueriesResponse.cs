﻿using gSpy3_Common.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_API.API.DataContracts.Responses
{
    public class QueriesResponse
    {
        public Dictionary<string, FilteredQueryResult> Response { get; set; }
        public bool Success { get; set; }
        public int TimeMs { get; set; }

        public QueriesResponse()
            {
            Response = new Dictionary<string, FilteredQueryResult>();
            }
    }
}
