﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_API.API.DataContracts.Requests
{
    public class UpdateSchedulesRequest
    {
        public List<UpdateSchedulesItem> schedules { get; set; }
    }

    public class UpdateSchedulesItem
    {
        public UInt64 idcontest { get; set; }
        public string datetime { get; set;}
    }
  
}
