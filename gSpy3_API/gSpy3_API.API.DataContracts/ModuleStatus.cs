﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_API.API.DataContracts
{
    public class ModuleStatus
    {
        public string idmodule { get; set; }
        public string module_name { get; set; }
        public string heartbeat_datetime { get; set; }

        public List<WorkStatus> work { get; set; }
    }
}
