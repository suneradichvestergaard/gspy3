﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_API.API.DataContracts
{
    public class WorkStatus
    {
        public int idwork { get; set; }
        public int idmodule { get; set; }
        public string command { get; set; }
        public string data { get; set; }
        public int weight { get; set; }
        public string added { get; set; }
        public string started { get; set; }
        public string finished { get; set; }
        public string source { get; set; }
        public string result { get; set; }
        public int? success { get; set; }
    }
}
