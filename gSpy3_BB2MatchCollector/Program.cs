﻿using gSpy3_Common;
using gSpy3_Common.Database;
using System;

namespace gSpy3_BB2MatchCollector
{
    class Program
    {
        static void Main(string[] args)
        {
            Log log = new Log();
            log.Info("gSpy3_BB2MatchCollector v " + typeof(Program).Assembly.GetName().Version);
            try
            {
                var settings = PrivateSettings.Load();
                DatabaseManager dbManager = new DatabaseManager(settings);

                BB2MatchCollector collector = new BB2MatchCollector(dbManager, settings.BB2APIKey);
                collector.Run();
            }
            catch (Exception ex)
            {
                log.Error("Error", ex.ToString());
            }
            log.Info("Program exit");
        }
    }
}
