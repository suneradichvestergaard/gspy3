﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace gSpy3_BB2MatchCollector
{
    public class UpdateCompetitionWork
    {
        public List<int> idleagues { get; set; }
        public string from { get; set; }
        public string to { get; set; }
        public string platform { get; set; }
    }
}
