﻿using gSpy3_Common;
using gSpy3_Common.Database;
using System;
using System.IO;

namespace gSpy3_BB2MatchParser
{
    class Program
    {
        static void Main(string[] args)
        {


            Log log = new Log();
            log.Info("gSpy3_BB2MatchParser v " + typeof(Program).Assembly.GetName().Version);
            try
            {
                var settings = PrivateSettings.Load();
                DatabaseManager dbManager = new DatabaseManager(settings);

                BB2MatchParser parser = new BB2MatchParser(dbManager, settings.BB2APIKey, Path.Combine(settings.BaseFileFolder,"matches"));
                parser.Run();
            }
            catch (Exception ex)
            {
                log.Error("Error", ex.ToString());
            }
            log.Info("Program exit");
        }
    }
}
