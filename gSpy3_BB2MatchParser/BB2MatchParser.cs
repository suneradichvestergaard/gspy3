﻿using BB2StatsLib;
using BB2StatsLib.BB2Replay;
using BB2StatsLib.Views.CalculatedStats;
using BB2StatsLib.Views.Replay;
using gSpy3_Common;
using gSpy3_Common.API.BB2;
using gSpy3_Common.Database;
using gSpy3_Common.Database.AccessInterface;
using gSpy3_Common.Database.Format;
using gSpy3_Common.Helpers;
using gSpy3_Common.Module;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Timers;

namespace gSpy3_BB2MatchParser
{
    public class BB2MatchParser : ModuleBase
    {
        DatabaseManager dbManager;
        Timer heartbeatTimer;
        protected enum Work { ParseMatch }
        Log log = new Log();
        BB2API bb2;
        string basePath = "";


        public BB2MatchParser(DatabaseManager dbm, string bb2APIKey, string aBasePath) : base(ModuleEnum.gSpy3_BB2MatchParser, dbm)
        {
            basePath = aBasePath;
            bb2 = new BB2API(bb2APIKey);
            dbManager = dbm;
            heartbeatTimer = new Timer();
            heartbeatTimer.Interval = 1;
            heartbeatTimer.Elapsed += HeartbeatTimer_Elapsed;
            heartbeatTimer.Start();
        }

        private void HeartbeatTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            heartbeatTimer.Enabled = false;
            heartbeatTimer.Interval = 1000 * 60;
            base.Tick();
            heartbeatTimer.Enabled = true;
        }

        protected override int GetMaxFuturePlanning()
        {
            return 1;
        }

        public void Run()
        {

#if DEBUG

            ParseMatch(new ParserWork() { finished = "2020-10-12 10:00", uuid = "100088e1ea", idorigin = (int)OriginID.BB2_pc, idmatch = 0 });
#endif

            log.Info("Running");
            while (true)
            {
                try
                {
                var numRemoved = base.RemoveOldWork();
                if (numRemoved > 0)
                {
                        log.Info("Removed " + numRemoved + " old work items");
                }

                // Plan new work, if we don't have enough
                try
                {
                    int workNeeded = GetNumWorkNeeded();

                    if (workNeeded > 0)
                    {
                            int days = 1;
                        var res = dbManager.StatsDB.Query("select idmatch, match_origin_id, finished, workstatus, idorigin from matches where workstatus = 0 and finished > @oldest order by finished desc limit 5",
                            new Dictionary<string, string>()
                            {
                                {"@oldest",(DateTime.Now-new TimeSpan(days,0,0,0)).ToDatabaseUniversalString() }
                            }
                                );
                        foreach (var row in res.Rows)
                        {
                            var work = new ParserWork()
                            {
                                idmatch = UInt64.Parse(row[0]),
                                uuid = UInt64.Parse(row[1]).ToString("x"),
                                finished = row[2],
                                idorigin = int.Parse(row[4])

                            };
                            AddWork(Work.ParseMatch.ToString(), Newtonsoft.Json.JsonConvert.SerializeObject(work), 10);
                            workNeeded--;
                            if(workNeeded == 0)
                                {
                                    break;
                                }
                        }


                    }


                    
                }
                catch (Exception ex)
                {
                    log.Info("Error in work planning", ex.ToString());
                }

                // Execute work
                int idwork = 0;
                try
                {

                    string command, data;
                    if (base.GetNextWork(out idwork, out command, out data))
                    {
                        log.Info("Work: " + command + " : " + data);
                        string result = "";
                        switch (command)
                        {
                            case "ParseMatch":
                                result = ParseMatch(Newtonsoft.Json.JsonConvert.DeserializeObject<ParserWork>(data));
                                break;

                        }
                        log.Info(result);
                        base.FinishWork(idwork, result);

                    }else System.Threading.Thread.Sleep(30000);

                }
                catch (Exception ex)
                {
                    log.Error("Error in work execution", ex.ToString());
                    base.FailWork(idwork, ex.ToString());
                }
            }
                catch (Exception ex)
                {
                    log.Error("Error in main loop: " + ex.ToString());
                }
                System.Threading.Thread.Sleep(1000);
            }
        }

        public string GetFilePath(string uuid, string finished)
        {
            return Path.Combine(basePath, finished.Substring(0, finished.IndexOf(" ")), uuid) + ".bbrz";
        }

        string ParseMatch(ParserWork work)
        {
            if (work.idorigin == 0) return "No origin given";
#if DEBUG
#else
            var res = dbManager.StatsDB.Query("select idmatch, match_origin_id, finished, workstatus, idorigin from matches where workstatus = 0 and idmatch=" + work.idmatch);
            if(res.Rows.Count == 0)
            {
                return "Already parsed";
            }
#endif


           string filepath = GetFilePath(work.uuid, work.finished);
            if (File.Exists(filepath)==false || new FileInfo(filepath).Length < 1024)
            {
                if (File.Exists(filepath)) { File.Delete(filepath); }
                bb2.DownloadMatch((int)ID, work.uuid, filepath);
            }
            var tmpFolder = Path.Combine(Path.GetTempPath(), "bb2parser");
            if (Directory.Exists(tmpFolder) == false)
            {
                Directory.CreateDirectory(tmpFolder);
            }
            string text = "";
            try
            {
                var archive = ZipFile.OpenRead(filepath);
                var entry = archive.Entries[0].Open();
                StreamReader reader = new StreamReader(entry);
                text = reader.ReadToEnd();
                reader.Dispose();
                entry.Dispose();
                archive.Dispose();
            }
            catch
            {
                dbManager.StatsDB.Query("update matches set workstatus=1 where idmatch=" + work.idmatch);
                return "Match " + work.idmatch + " failed to download. Clear workstatus to try again";
            }

            var replay = Replay.CreateFromString(text);



            var trans = dbManager.StatsDB.BeginTransaction();
            try
            {

                var plTypeMap = UniqueStringManager.GetPlayerTypeMap();
                var plNameMap = UniqueStringManager.GetPlayerNameMap();
                var parsedMatch = GameStats.CreateFromReplay(work.uuid, replay);
#if DEBUG
                Match match = null;
                TeamMatchStats team1stats = null;
                TeamMatchStats team2stats = null;
#else
                var match = dbManager.StatsDB.Select<Match>("select * from matches where idorigin=" + work.idorigin + " and idmatch=" + work.idmatch, null, trans)[0];
                var team1stats = dbManager.StatsDB.Select<TeamMatchStats>("select * from teammatchstats where idorigin=" + work.idorigin + " and idmatch=" + work.idmatch+" and home=1", null, trans)[0];
                var team2stats = dbManager.StatsDB.Select<TeamMatchStats>("select * from teammatchstats where idorigin=" + work.idorigin + " and idmatch=" + work.idmatch + " and home=0", null, trans)[0];
#endif
                // Write parsed match
                File.WriteAllText(filepath + ".json", Newtonsoft.Json.JsonConvert.SerializeObject(parsedMatch));

                // But also update stats of all included teams and players
                bool firstTeam = true;

                foreach (var teamstats in parsedMatch.TeamStats)
                {
                    var team = TeamManager.FindTeam((OriginID)match.idorigin, int.Parse(teamstats.TeamId), trans);
                    if (team != null)
                    {
                        TeamMatchStats ts = firstTeam ? team1stats : team2stats;
                        ts.dodges = teamstats.Dodges;
                        ts.fouls = teamstats.Fouls;
                        ts.turnovers = teamstats.Turnovers;
                        ts.boneheads = teamstats.Boneheads;
                        ts.catches_failed = teamstats.CatchesFailed;
                        ts.completions = teamstats.Completions;
                        ts.dodges_failed = teamstats.DodgesFailed;
                        ts.luck = teamstats.Luck;
                        ts.passes_failed = teamstats.PassesFailed;
                        ts.pickups = teamstats.Pickups;
                        ts.pickups_failed = teamstats.PickupsFailed;
                        ts.risk = teamstats.Risk;
                        ts.sacks = teamstats.Sacks;
                        ts.rushes = teamstats.GFIs;
                        ts.rushes_failed = teamstats.GFIsFailed;
                        dbManager.StatsDB.Update("teammatchstats", ts, "where idteammatchstat=" + ts.idteammatchstat, new string[] { "idteammatchstat" }, trans);

                        foreach (var pstats in teamstats.PlayerStats)
                        {
                            var pl = TeamManager.FindPlayer((OriginID)match.idorigin, (pstats.PlayerId), trans);
                            
                            if (pl != null) // May be null (like a loner for example)
                            {
                                var pls = dbManager.StatsDB.Select<PlayerMatchStats>("select * from playermatchstats where idmatch=" + work.idmatch + " and idplayer="+pl.idplayer, null, trans)[0];
                                pls.dodges = pstats.Dodges;
                                pls.expulsions = pstats.Expulsions;
                                pls.fouls = pstats.Fouls;
                                pls.completions = pstats.Completions;
                                pls.pickups = pstats.Pickups;
                                pls.sacks = pstats.Sacks;
                                pls.rushes = pstats.GFIs;
                                pls.boneheads = pstats.Boneheads;
                                pls.turnovers = pstats.Turnovers;
                                dbManager.StatsDB.Update("playermatchstats", pls, "where idplayermatchstat=" + pls.idplayermatchstat, new string[] { "idplayermatchstat" }, trans);

                                if (pstats.KilledBy != null)
                                {
                                    var killedbypl = TeamManager.FindPlayer((OriginID)match.idorigin, (pstats.KilledBy.KillPlayerId), trans);
                                    var killerpl = TeamManager.FindPlayer((OriginID)match.idorigin, (pstats.KilledBy.KillerPlayerId), trans);
                                    int idkilledname = killedbypl != null ? killedbypl.idplayername : UniqueStringManager.GetOrCreatePlayerName(plNameMap, pstats.KilledBy.KillPlayerName, trans);
                                    int idkillername = killerpl != null ? killerpl.idplayername : UniqueStringManager.GetOrCreatePlayerName(plNameMap, pstats.KilledBy.KillerName, trans);
                                    int idkilled = killedbypl != null ? (int)killedbypl.idplayer : 0;
                                    int idkiller = killerpl != null ? (int)killerpl.idplayer : 0;

                                    dbManager.StatsDB.InsertIfMissing("highlights", new List<Highlight>(){ new Highlight()
                                    {
                                         idcause = pstats.KilledBy.KillCausedBy,
                                          idtarget = idkiller,
                                          idtargetname = idkillername,
                                          idplayername = idkilledname,
                                          idmatch = (UInt64)match.idmatch,
                                           idplayer = idkilled,
                                            idhighlighttype = string.IsNullOrEmpty(pstats.KilledBy.KillerName) ? (int)HighlightEnum.Killed : (int)HighlightEnum.KilledByPlayer,
                                             turn = pstats.KilledBy.Turn,
                                             subturn = pstats.KilledBy.Index,
                                              weight = killedbypl != null ?((killedbypl.level*killedbypl.st*killedbypl.ag)+100) :100

                                    } }, trans); 
                                    
                                }
                            }
                        }
                    }

                    firstTeam = false;
                }




                trans.Commit();
            }catch(Exception ex)
            {
                trans.Rollback();
                log.Error(ex.ToString());
            }


            dbManager.StatsDB.Query("update matches set workstatus=1 where idmatch=" + work.idmatch);
            return "Match "+work.idmatch+" downloaded and parsed";
        }
    }

}
