﻿using gSpy3_Common;
using gSpy3_Common.Database;
using gSpy3_Common.Module;
using System;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using gSpy3_Common.Helpers;
using System.Diagnostics;
using System.IO;
using System.Management;
using System.Runtime.Serialization;
using gSpy3_Common.Database.Format;
using gSpy3_Common.Database.AccessInterface;

namespace gSpy3_Server
{
    class gSpy3Service : ServiceBase
    {
        PrivateSettings privateSettings = null;
        DatabaseManager dbManager = null;
        Log log = new Log();
        Thread worker = null;
        object threadLock = new object();
        Boolean paused = false;
        bool stopping = false;

        public gSpy3Service()
        {
            privateSettings = PrivateSettings.Load();
            dbManager = new DatabaseManager(privateSettings);
            
        }

        public void StartManually()
        {
            OnStart(new string[] { });
        }
        public void StopManually()
        {
            OnStop();
        }
        protected override void OnStart(string[] args)
        {
            log.Info("Starting");
            if (worker != null)
            {
                lock (threadLock)
                {
                    paused = false;
                }
            }
            else
            {
                worker = new Thread(new ParameterizedThreadStart(Worker));
                worker.Start();
            }
            base.OnStart(args);
        }

        protected override void OnStop()
        {
            log.Info("Stopping");
            while (worker != null)
            {
                lock (threadLock)
                {
                    stopping = true;
                }
            }
            log.Info("Stopped");

            foreach (var module in Enum.GetNames(typeof(ModuleBase.ModuleEnum)))
            {
                try
                {
                    using (ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT CommandLine,ProcessId FROM Win32_Process"))
                    using (ManagementObjectCollection processList = searcher.Get())
                    {
                        foreach (ManagementObject process in processList)
                        {
                            var cmdLineObject = process["CommandLine"];
                            if (cmdLineObject != null)
                            {
                                string cmdLine = cmdLineObject.ToString();
                                if (cmdLine.Contains(module))
                                {
                                    log.Info("Killing module " + module);
                                    Process p = Process.GetProcessById(int.Parse(process["ProcessId"].ToString()));
                                    p.Kill();
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error killing module " + module, ex);
                }
            }


            if (dbManager != null)
            {
                dbManager.Dispose();
                dbManager = null;
            }

            base.OnStop();
        }

        protected override void OnPause()
        {
            log.Info("Pausing");
            if (worker != null)
            {
                lock (threadLock)
                {
                    paused = true;
                }
            }
            base.OnPause();
        }

        void Worker(object obj)
        {
            // Fill in any missing origins
            foreach (var origin in Enum.GetNames(typeof(OriginID)))
            {
                int id = ((int)Enum.Parse(typeof(OriginID), origin));
                var res = dbManager.StatsDB.Query("replace into origins set idorigin=@id, origin_name=@name",
                new Dictionary<string, string>()
                    {
                        { "@id", id.ToString() },
                        { "@name", origin }
                    });
            }

            // And races
            foreach (var origin in Enum.GetNames(typeof(RaceEnum)))
            {
                int id = ((int)Enum.Parse(typeof(RaceEnum), origin));
                var res = dbManager.StatsDB.Query("replace into races set idrace=@id, race_name=@name",
                new Dictionary<string, string>()
                    {
                        { "@id", id.ToString() },
                        { "@name", origin }
                    });
            }


            // Make sure modules are registered
            foreach (var module in Enum.GetNames(typeof(ModuleBase.ModuleEnum)))
            {
                int id = ((int)Enum.Parse(typeof(ModuleBase.ModuleEnum), module));
                var res = dbManager.BackendDB.Query("replace into modules set idmodule=@id, module_name=@name, heartbeat_datetime=@time",
                new Dictionary<string, string>()
                    {
                        { "@id", id.ToString() },
                        { "@name", module },
                        { "@time", DateTime.Now.ToDatabaseUniversalString()  }
                    });
            }

            // Make sure we got an AI coach for each origin
            foreach(var value in Enum.GetNames(typeof(OriginID))) {
                if (dbManager.StatsDB.Query("select idcoach from coaches where coach_origin_id=0 and idorigin=@idorigin", new Dictionary<string, string>()
                {
                    { "@idorigin",  ""+(int)(OriginID)Enum.Parse(typeof(OriginID), value) }
                }).Rows.Count == 0)
                {
                    dbManager.StatsDB.Insert("coaches", new Coach()
                    {
                        coach_name = "AI",
                        coach_origin_id = 0,
                        country = null,
                        idcoach = 0,
                        lang = null,
                        idorigin = (int)(OriginID)Enum.Parse(typeof(OriginID), value),
                        twitch = null,
                        youtube = null

                    }); ;
                }
                
            }
            log.Info("Starting main loop");
            // And then just loop like crazy
            while (true)
            {
                #region PauseHandling
                bool isPaused = false;
                bool isStopping = false;
                lock (threadLock)
                {
                    isPaused = paused;
                    isStopping = stopping;
                }
                if (isPaused)
                {
                    System.Threading.Thread.Sleep(1000);
                    continue;
                }
                if (isStopping)
                {
                    break;
                }
                #endregion
                try
                {
                    #region Process starting (if not running)
                    // Check that processes are running
                    var processes = Process.GetProcesses();
                    bool processWasStarted = false;
                    foreach (var module in Enum.GetNames(typeof(ModuleBase.ModuleEnum)))
                    {
                        if(module == ModuleBase.ModuleEnum.gSpy3_LegacyCollector.ToString())
                        {
                            continue; // Skip legacycollector
                        }
                        try
                        {
                            bool processIsRunning = false;
                            using (ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT CommandLine FROM Win32_Process"))
                            using (ManagementObjectCollection processList = searcher.Get())
                            {
                                foreach (ManagementObject process in processList)
                                {
                                    var cmdLineObject = process["CommandLine"];
                                    if (cmdLineObject != null)
                                    {
                                        string cmdLine = cmdLineObject.ToString();
                                        if (cmdLine.Contains(module))
                                        {
                                            processIsRunning = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            
                            if (!processIsRunning)
                            {
                                log.Info($"Unable to find process for module {module}");
                                string path = ("modules\\" + module+ "\\netcoreapp3.1\\" + module+".exe");

                                if (File.Exists(path) == false) { path = "..\\" + path; }
                                if (File.Exists(path) == false) { path = "..\\" + path; }
                                if (File.Exists(path) == false) { path = "..\\" + path; }
                                path = Path.GetFullPath(path);

                                if (File.Exists(path)==false)
                                {
                                    log.Error("Unable to find file " + path);
                                }
                                else
                                {
                                    log.Info("Starting module " + module);
                                    var process = new Process
                                    {
                                        StartInfo = new ProcessStartInfo
                                        {
                                            FileName = path,
                                            UseShellExecute = true,
                                            RedirectStandardOutput = false,
                                            RedirectStandardError = false,
                                            CreateNoWindow = true,
                                            WorkingDirectory = Path.GetDirectoryName(path)
                                        }

                                    };
                                    process.Start();
                                    processWasStarted = true;
                                }
                            }
                        }
                        catch(Exception ex)
                        {
                            log.Error("Error when checking processes, ex");
                        }
                    }
                    if (processWasStarted)
                    {
                        System.Threading.Thread.Sleep(1000);
                    }
                    #endregion
                    #region Heartbeat checking
                    foreach (var module in Enum.GetNames(typeof(ModuleBase.ModuleEnum)))
                    {
                        if(module == ModuleBase.ModuleEnum.gSpy3_LegacyCollector.ToString()) {
                            continue; // Skip legacycollector
                                }
                        int id = ((int)Enum.Parse(typeof(ModuleBase.ModuleEnum), module));
                        var res = dbManager.BackendDB.Query("select heartbeat_datetime from modules where idmodule="+id);
                        foreach(var row in res.Rows)
                        {
                            var time = DateTime.Parse(row[res.Cols["heartbeat_datetime"]]+"Z");
                            if((DateTime.Now-time).TotalSeconds > 5*60) // 5 min without heartbeat
                            {
                                log.Error("Missig heartbeat for module " + module);
                                try
                                {
                                    using (ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT CommandLine,ProcessId FROM Win32_Process"))
                                    using (ManagementObjectCollection processList = searcher.Get())
                                    {
                                        foreach (ManagementObject process in processList)
                                        {
                                            var cmdLineObject = process["CommandLine"];
                                            if (cmdLineObject != null)
                                            {
                                                string cmdLine = cmdLineObject.ToString();
                                                if (cmdLine.Contains(module))
                                                {
                                                    log.Info("Killing module " + module);
                                                    Process p = Process.GetProcessById(int.Parse(process["ProcessId"].ToString()));
                                                    p.Kill();
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }catch(Exception ex)
                                {
                                    log.Error("Error killing module " + module, ex);
                                }
                                System.Threading.Thread.Sleep(30000);
                            }
                        }
                    }
                    #endregion

                    System.Threading.Thread.Sleep(5000); // Sleep a bit
                }
                catch(Exception ex)
                {
                    log.Error("Error in Worker loop", ex);
                }
            }
            lock (threadLock)
            {
                worker = null;
            }
        }
    }
}
